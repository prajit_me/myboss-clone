package com.mybos.bmapp.Component;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.mybos.bmapp.R;

public class LoadingScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_loading_screen);
    }
}
