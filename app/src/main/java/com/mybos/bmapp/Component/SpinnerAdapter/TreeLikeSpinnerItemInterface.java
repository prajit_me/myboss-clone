package com.mybos.bmapp.Component.SpinnerAdapter;

import java.util.List;

/**
 * Created by EmLaAi on 05/04/2018.
 *
 * @author EmLaAi
 */
public interface TreeLikeSpinnerItemInterface {
    boolean isParent();
    boolean isBack();
    int getId();
    String getTittle();
    List<TreeLikeSpinnerItemInterface> getChildren();
    TreeLikeSpinnerItemInterface findChildParent(int id);
}
