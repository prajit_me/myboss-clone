package com.mybos.bmapp.Component.SpinnerAdapter;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.Logger;

/**
 * Created by EmLaAi on 31/03/2018.
 *
 * @author EmLaAi
 */
public class SimpleTitleSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private final Context context;
    private final LayoutInflater inflater;
//    private final int resource;
    private SpinnerSourceCompatible spinnerSource;
    private int displayTextColor;
    private int displayTextSize = 14;

    public SimpleTitleSpinnerAdapter(Context context,SpinnerSourceCompatible source){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.spinnerSource = source;
        displayTextColor = context.getResources().getColor(R.color.text_gray_light);
    }

    public void setDisplayTextColor(int displayTextColor) {
        this.displayTextColor = displayTextColor;
    }

    public void setDisplayTextSize(int displayTextSize) {
        this.displayTextSize = displayTextSize;
    }

    @Override
    public int getCount() {
        if (spinnerSource != null) {
            return spinnerSource.count();
        }
        else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        return spinnerSource.getItem(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null != convertView){
            holder = (ViewHolder) convertView.getTag();
        }else {
            convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder.position = position;
        holder.tvTitle.setText(spinnerSource.getTitle(position));
        holder.tvTitle.setTextColor(displayTextColor);
        holder.tvTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP,displayTextSize);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null != convertView){
            holder = (ViewHolder) convertView.getTag();
        }else {
            convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder.position = position;
        holder.tvTitle.setText(spinnerSource.getTitle(position));
        return convertView;
    }

    static class ViewHolder{
        int position;
        TextView tvTitle;
        ViewHolder(View view){
            this.position = 0;
            tvTitle = view.findViewById(R.id.tvTitle);
        }

    }
    interface SpinnerWithSourceCompatibleListener<T extends BaseModel>{
        void didSelectedItem(int position,T model);
    }
}
