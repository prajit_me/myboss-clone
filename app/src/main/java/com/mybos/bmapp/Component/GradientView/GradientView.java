package com.mybos.bmapp.Component.GradientView;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.mybos.bmapp.R;

public class GradientView  extends View {
    private int[] gradientColors = new int[]{
            getResources().getColor(R.color.nav_start_gradient),
            getResources().getColor(R.color.nav_middle_gradient),
            getResources().getColor(R.color.nav_end_gradient)};
    private Paint p = new Paint();
    public GradientView(Context context) {
        super(context);
        drawGradient();
    }

    public GradientView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        drawGradient();
    }

    public GradientView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        drawGradient();
    }

    public GradientView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        drawGradient();
    }

    public void drawGradient(){
//        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.TL_BR,gradientColors);
//        setBackground(gradientDrawable);
        measure(0,0);
        int largerSize = getWidth() < getHeight() ? getHeight() : getWidth();
        int x = - Math.abs(largerSize - getHeight()) / 2;
        int y = - Math.abs(largerSize - getWidth()) / 2;
        ShapeDrawable.ShaderFactory factory = new ShapeDrawable.ShaderFactory() {
            @Override
            public Shader resize(int width, int height) {
                return new LinearGradient(x,y,largerSize,largerSize,gradientColors,null, Shader.TileMode.CLAMP);
            }
        };
        PaintDrawable paintDrawable = new PaintDrawable();
        paintDrawable.setShape(new RectShape());
        paintDrawable.setShaderFactory(factory);
        setBackground(paintDrawable);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        p.setAntiAlias(true);
        int largerSize = getWidth() < getHeight() ? getHeight() : getWidth();
        int x = - Math.abs(largerSize - getHeight()) / 2;
        int y = - Math.abs(largerSize - getWidth()) / 2;
        p.setShader(new LinearGradient(x,y,largerSize,largerSize,gradientColors,null, Shader.TileMode.CLAMP));
        RectF rectF;
        rectF = new RectF(x,y,largerSize,largerSize);
        canvas.drawRect(rectF,p);
    }
}