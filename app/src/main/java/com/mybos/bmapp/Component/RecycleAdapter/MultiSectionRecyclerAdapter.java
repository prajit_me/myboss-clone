package com.mybos.bmapp.Component.RecycleAdapter;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentInterface;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 11/04/2018.
 *
 * @author EmLaAi
 */
public class MultiSectionRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected static final int VIEW_HEADER_TYPE = 0;
    protected static final int VIEW_NORMAL_TYPE = 1;
    protected List<SectionList> souceObject = new ArrayList<>();
    protected List<SourceHolder> displaySource = new ArrayList<>();
    protected OnItemSelectedListener listener;
//    protected @LayoutRes int headerLayout = R.layout.recycler_header_view;
//    protected @LayoutRes int cellLayout = R.layout.recycle_item_view;

    public void addSection(SectionList section){
        souceObject.add(section);
    }
    public void clearSource(){
        souceObject.clear();
    }

    public void parseSource(){
        displaySource = new ArrayList<>();
        for (SectionList section:souceObject){
            SourceHolder header = new SourceHolder(VIEW_HEADER_TYPE,null,section.name);
            displaySource.add(header);
            for (BaseModel model:section.source){
                SourceHolder item = new SourceHolder(VIEW_NORMAL_TYPE,model,null);
                displaySource.add(item);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        SourceHolder holder = displaySource.get(position);
        return holder.viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_HEADER_TYPE){
            View view = inflater.inflate(getHeaderLayout(),parent,false);
            return creatHeaderViewHolder(view);
        }else {
            View view = inflater.inflate(getItemLayout(),parent,false);
            return creatNormalCellHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder){
            SourceHolder sourceHolder = displaySource.get(position);
            if (sourceHolder.viewType == VIEW_HEADER_TYPE){
                ((HeaderViewHolder) holder).textView.setText(sourceHolder.name);
            }
        }
        if (holder instanceof ItemViewHolder){
            SourceHolder sourceHolder = displaySource.get(position);
            if (sourceHolder.viewType == VIEW_NORMAL_TYPE){
                if (null != sourceHolder.source){
                    if (sourceHolder.source instanceof AttachmentInterface){
                        ((ItemViewHolder) holder).txtTitle.setText(((AttachmentInterface) sourceHolder.source).getAttachmentTitle());
                        ((ItemViewHolder) holder).txtSubTitle.setText(((AttachmentInterface) sourceHolder.source).getSubTitle());
                        ((ItemViewHolder) holder).cardView.setOnClickListener(v -> {
                            if (listener != null){
                                listener.onClick(displaySource.get(position).source);
                            }
                        });
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return displaySource.size();
    }

    protected RecyclerView.ViewHolder creatHeaderViewHolder(View view){
        return new HeaderViewHolder(view);
    }
    protected RecyclerView.ViewHolder creatNormalCellHolder(View view){
        return new ItemViewHolder(view);
    }

    public void setListener(OnItemSelectedListener listener) {
        this.listener = listener;
    }

    protected @LayoutRes int getHeaderLayout(){
        return R.layout.recycler_header_view;
    }
    protected @LayoutRes int getItemLayout(){
        return R.layout.recycle_item_view;
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView textView;
        public HeaderViewHolder(View view){
            super(view);
            textView = view.findViewById(R.id.tvTitle);
        }
    }

    public static class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView txtTitle,txtSubTitle;
        CardView cardView;
        public ItemViewHolder(View itemView) {
            super(itemView);
            txtTitle = itemView.findViewById(R.id.tvTitle);
            txtSubTitle = itemView.findViewById(R.id.tvSubTitle);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    public class SourceHolder{
        public int viewType;
        public String name;
        public BaseModel source;
        SourceHolder(int viewType,BaseModel source,String name){
            this.viewType = viewType;
            this.source = source;
            this.name = name;
        }
    }
    public static class SectionList{
        String name;
        List<BaseModel> source;
        public SectionList(String sectionName,List<BaseModel> sectionItem){
            this.name = sectionName;
            this.source = sectionItem;
        }
    }

    public interface OnItemSelectedListener{
        void onClick(BaseModel baseModel);
    }
}
