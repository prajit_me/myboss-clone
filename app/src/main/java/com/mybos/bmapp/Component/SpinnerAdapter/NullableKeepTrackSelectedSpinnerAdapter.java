package com.mybos.bmapp.Component.SpinnerAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 03/04/2018.
 *
 * @author EmLaAi
 */
public class NullableKeepTrackSelectedSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private Context context;
    private LayoutInflater inflater;
    private KeepTrackSelectedItemList resource;
    private List<SpinnerItemInterface> displaySource;

    public NullableKeepTrackSelectedSpinnerAdapter(Context context, KeepTrackSelectedItemList resource){
        this.context = context;
        this.resource = resource;
        updateResouce(resource.getUnselectedItem());
        this.inflater = LayoutInflater.from(context);
    }

    public void updateResouce(List<BaseModel> modelList){
        this.displaySource = new ArrayList<>();
        this.displaySource.add(null);
        for (BaseModel model:modelList){
            if (model instanceof SpinnerItemInterface){
                this.displaySource.add((SpinnerItemInterface) model);
            }
        }
    }
    public void changeMainSource(KeepTrackSelectedItemList resource){
        this.resource = resource;
        updateResouce(this.resource.getUnselectedItem());
    }

    public void selectItem(SpinnerItemInterface itemInterface){
        resource.selectId(itemInterface.getSpinnerItemId());
        updateResouce(resource.getUnselectedItem());

        this.notifyDataSetChanged();
    }

    public void deselectItem(SpinnerItemInterface itemInterface){
        resource.deselectId(itemInterface.getSpinnerItemId());
        updateResouce(resource.getUnselectedItem());
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return displaySource.size();
    }

    @Override
    public Object getItem(int position) {
        return displaySource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (null != convertView){
            holder = (ViewHolder) convertView.getTag();
        }else {
            convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }
        holder.position = position;
        if (null != displaySource.get(position)){
            holder.tvTitle.setText(null);
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        Logger.shared().i(position);
        if (position == 0){
            if (null == convertView){
                convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                convertView.setTag(R.id.spinner_view_type,0);
            }else{
                if (!convertView.getTag(R.id.spinner_view_type).equals(0)){
                    convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                    convertView.setTag(R.id.spinner_view_type,0);
                }
            }
        }else{
            ViewHolder holder;
            if (null != convertView){
                if (convertView.getTag(R.id.spinner_view_type).equals(1)) {
                    holder = (ViewHolder) convertView.getTag();
                }else {
                    convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                    convertView.setTag(R.id.spinner_view_type,1);
                    holder = new ViewHolder(convertView);
                    convertView.setTag(holder);
                }
            }else {
                convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                convertView.setTag(R.id.spinner_view_type,1);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            holder.position = position;

            try {
                if (position>=1 && position<displaySource.size()){
                    holder.tvTitle.setText(displaySource.get(position).getTittle());
                }
            }
            catch (IndexOutOfBoundsException IndexOutOfBoundsExc_End){
                try {
                    holder.tvTitle.setText(displaySource.get(position - 1).getTittle());
                }
                catch (IndexOutOfBoundsException IndexOutOfBoundsExc_Begin){
                    holder.tvTitle.setText(displaySource.get(0).getTittle());
                }
            }
        }
        return convertView;
    }

    static class ViewHolder{
        int position;
        TextView tvTitle;


        ViewHolder(View view){
            this.position = 0;
            tvTitle = view.findViewById(R.id.tvTitle);
        }

    }
}
