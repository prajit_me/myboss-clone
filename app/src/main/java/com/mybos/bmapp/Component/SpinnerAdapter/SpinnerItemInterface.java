package com.mybos.bmapp.Component.SpinnerAdapter;

/**
 * Created by EmLaAi on 04/04/2018.
 *
 * @author EmLaAi
 */
public interface SpinnerItemInterface<T> {
    T getSpinnerItemId();
    String getTittle();
}
