package com.mybos.bmapp.Component.SearchEditText;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by EmLaAi on 27/03/2018.
 *
 * @author EmLaAi
 */

public class SearchEditText extends EditText {
    public SearchEditText(Context context) {
        super(context);
    }

    public SearchEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SearchEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public SearchEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
