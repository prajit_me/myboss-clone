package com.mybos.bmapp.Component.TagViewGroup;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

/**
 * Created by EmLaAi on 03/04/2018.
 *
 * @author EmLaAi
 */
public class TagViewLinearLayout extends LinearLayout {
    private List<TextView> tags;

    public TagViewLinearLayout(Context context) {
        super(context);
    }

    public TagViewLinearLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public TagViewLinearLayout(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public TagViewLinearLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {

        super.onLayout(changed, l, t, r, b);
    }
}
