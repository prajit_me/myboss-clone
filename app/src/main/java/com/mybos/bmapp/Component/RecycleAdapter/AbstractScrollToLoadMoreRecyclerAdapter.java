package com.mybos.bmapp.Component.RecycleAdapter;

import androidx.annotation.LayoutRes;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Event.OnNeedLoadMoreListener;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 21/04/2018.
 *
 * @author EmLaAi
 */
public abstract class AbstractScrollToLoadMoreRecyclerAdapter<T extends BaseModel> extends RecyclerView.Adapter {
    private final int VIEW_NORMAL = 0;
    private final int VIEW_LOADING = 1;

    private boolean isLoading = false;
    private boolean isEnd = false;
    private int maximumItems = 0;
    private int visibleItemThreshold = 2;
    private OnNeedLoadMoreListener loadMoreListener;
    protected List<T> sourceList = new ArrayList<>();

    public void setup(RecyclerView recyclerView){
        final LinearLayoutManager linearLayout = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int total = linearLayout.getItemCount();
                int lastVisibleItemPosition = linearLayout.findLastVisibleItemPosition();
                if(!isLoading && total < (lastVisibleItemPosition + visibleItemThreshold)){
                    if(null != loadMoreListener){
                        loadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }
    @Override
    public int getItemViewType(int position) {
        return position < sourceList.size() ? VIEW_NORMAL : VIEW_LOADING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_NORMAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(getItemLayout(), parent, false);
            return getItemViewHolder(view);
        }else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_card_like_loading_more_placeholder,parent,false);
            return new LoadingViewHolder(view);
        }
    }
    @Override
    public int getItemCount() {
        return null != sourceList ?( !isEnd? sourceList.size() + 1 : sourceList.size()): 0;
    }
    public void setOnNeedLoadMoreListener(OnNeedLoadMoreListener listener){
        this.loadMoreListener = listener;
    }
    public void setLoaded(){
        this.isLoading = false;
    }

    public void setMaximum(int maximum){
        if (this.maximumItems != maximum){
            this.maximumItems = maximum;
        }
    }

    public void removePosition(int position){
        sourceList.remove(position);
        notifyItemRemoved(position);
    }

    public void reset(){
        sourceList.clear();
        isEnd = false;
    }

    public void updateModel(List<T> models){
        setLoaded();
        this.sourceList.addAll(models);
        if (this.sourceList.size() >= maximumItems){
            isEnd = true;
        }
    }

    abstract public @LayoutRes int getItemLayout();
    abstract public RecyclerView.ViewHolder getItemViewHolder(View view);


    class LoadingViewHolder extends RecyclerView.ViewHolder{
        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }
}
