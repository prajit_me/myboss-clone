package com.mybos.bmapp.Component.RecycleAdapter;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 19/04/2018.
 *
 * @author EmLaAi
 */
public abstract class AbstractMultiSectionRecyclerAdaper extends RecyclerView.Adapter {
    private static final int VIEW_HEADER_TYPE = 0;
    private static final int VIEW_NORMAL_TYPE = 1;
    protected List<MultiSectionRecyclerAdapter.SectionList> souceObject = new ArrayList<>();
    protected List<MultiSectionRecyclerAdapter.SourceHolder> displaySource = new ArrayList<>();


    abstract protected RecyclerView.ViewHolder creatHeaderViewHolder(View view);
    abstract protected RecyclerView.ViewHolder creatNormalCellHolder(View view);
    abstract protected @LayoutRes int getHeaderLayout();
    abstract protected @LayoutRes int getItemLayout();

    public void addSection(MultiSectionRecyclerAdapter.SectionList section){
        souceObject.add(section);
    }
    public void clearSource(){
        souceObject.clear();
    }

    public void parseSource(){
        displaySource = new ArrayList<>();
//        for (MultiSectionRecyclerAdapter.SectionList section:souceObject){
//            MultiSectionRecyclerAdapter.SourceHolder header = new MultiSectionRecyclerAdapter.SourceHolder(VIEW_HEADER_TYPE,null,section.name);
//            displaySource.add(header);
//            for (BaseModel model:section.source){
//                MultiSectionRecyclerAdapter.SourceHolder item = new MultiSectionRecyclerAdapter.SourceHolder(VIEW_NORMAL_TYPE,model,null);
//                displaySource.add(item);
//            }
//        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        MultiSectionRecyclerAdapter.SourceHolder holder = displaySource.get(position);
        return holder.viewType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == VIEW_HEADER_TYPE){
            View view = inflater.inflate(getHeaderLayout(),parent,false);
            return creatHeaderViewHolder(view);
        }else {
            View view = inflater.inflate(getItemLayout(),parent,false);
            return creatNormalCellHolder(view);
        }
    }

    @Override
    public int getItemCount() {
        return displaySource.size();
    }
}
