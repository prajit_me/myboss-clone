package com.mybos.bmapp.Component.SpinnerAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 05/04/2018.
 *
 * @author EmLaAi
 */
public class TreeLikeSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private Context context;
    private LayoutInflater inflater;
    private TreeLikeSpinnerItemInterface parentNode = null;
    private List<TreeLikeSpinnerItemInterface> source;
    private List<TreeLikeSpinnerItemInterface> displaySource;
    public TreeLikeSpinnerAdapter(Context context,List<TreeLikeSpinnerItemInterface> source){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.source = source;
        this.displaySource = new ArrayList<>();
        updateSource(this.source);
    }

    public void updateSource(List<TreeLikeSpinnerItemInterface> source){
        displaySource.clear();
        notifyDataSetChanged();
        displaySource.add(null);
        displaySource.addAll(source);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return displaySource.size();
    }

    @Override
    public Object getItem(int position) {
        return displaySource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0){
            if (null == convertView){
                convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                convertView.setTag(R.id.spinner_view_type,0);
            }else{
                if (!convertView.getTag(R.id.spinner_view_type).equals(0)){
                    convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                    convertView.setTag(R.id.spinner_view_type,0);
                }
            }
        }else{
            ViewHolder holder;
            if (null != convertView){
                if (convertView.getTag(R.id.spinner_view_type).equals(1)) {
                    holder = (ViewHolder) convertView.getTag();
                }else {
                    convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                    convertView.setTag(R.id.spinner_view_type,1);
                    holder = new ViewHolder(convertView);
                    convertView.setTag(holder);
                }
            }else {
                convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                convertView.setTag(R.id.spinner_view_type,1);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            holder.position = position;

            holder.tvTitle.setText(displaySource.get(position).getTittle());
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (position == 0){
            if (null == convertView){
                convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                convertView.setTag(R.id.spinner_view_type,0);
            }else{
                if (!convertView.getTag(R.id.spinner_view_type).equals(0)){
                    convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                    convertView.setTag(R.id.spinner_view_type,0);
                }
            }
        }else{
            TreeViewHolder holder;
            if (null != convertView){
                if (convertView.getTag(R.id.spinner_view_type).equals(1)) {
                    holder = (TreeViewHolder) convertView.getTag();
                }else {
                    convertView = inflater.inflate(R.layout.spinner_tree_like_item, parent, false);
                    convertView.setTag(R.id.spinner_view_type,1);
                    holder = new TreeViewHolder(convertView,needDisplaySubViewPositioin -> {
                        Object object = TreeLikeSpinnerAdapter.this.getItem(needDisplaySubViewPositioin);
                        if (object instanceof TreeLikeSpinnerItemInterface){
                            if (((TreeLikeSpinnerItemInterface) object).isParent()){
                                parentNode =(TreeLikeSpinnerItemInterface) object;
                                updateSource(((TreeLikeSpinnerItemInterface) object).getChildren());
                            }
                        }
                    },()->{
                        if (null == parentNode){
                            updateSource(this.source);
                            return;
                        }
                        boolean isMatch = false;
                        for (TreeLikeSpinnerItemInterface directChild:source){
                            if (directChild.getId() == parentNode.getId()){
                                updateSource(this.source);
                                isMatch = true;
                            }
                        }
                        if (isMatch) return;
                        for (TreeLikeSpinnerItemInterface loop:source){
                            TreeLikeSpinnerItemInterface posibleParent = loop.findChildParent(parentNode.getId());
                            if (null != posibleParent){
                                updateSource(posibleParent.getChildren());
                                parentNode = posibleParent;
                                isMatch = true;
                            }
                        }
                        if (isMatch) return;
                        updateSource(this.source);
                    });
                    convertView.setTag(holder);
                }
            }else {
                convertView = inflater.inflate(R.layout.spinner_tree_like_item, parent, false);
                convertView.setTag(R.id.spinner_view_type,1);
                holder = new TreeViewHolder(convertView,needDisplaySubViewPositioin -> {
                    Object object = TreeLikeSpinnerAdapter.this.getItem(needDisplaySubViewPositioin);
                    if (object instanceof TreeLikeSpinnerItemInterface){
                        if (((TreeLikeSpinnerItemInterface) object).isParent()){
                            parentNode =(TreeLikeSpinnerItemInterface) object;
                            updateSource(((TreeLikeSpinnerItemInterface) object).getChildren());
                        }
                    }
                },()->{
                    if (null == parentNode){
                        updateSource(this.source);
                        return;
                    }
                    boolean isMatch = false;
                    for (TreeLikeSpinnerItemInterface directChild:source){
                        if (directChild.getId() == parentNode.getId()){
                            updateSource(this.source);
                            isMatch = true;
                        }
                    }
                    if (isMatch) return;
                    for (TreeLikeSpinnerItemInterface loop:source){
                        TreeLikeSpinnerItemInterface posibleParent = loop.findChildParent(parentNode.getId());
                        if (null != posibleParent){
                            updateSource(posibleParent.getChildren());
                            parentNode = posibleParent;
                            isMatch = true;
                        }
                    }
                    if (isMatch) return;
                    updateSource(this.source);
                });
                convertView.setTag(holder);
            }
            holder.position = position;

            holder.tvTitle.setText(displaySource.get(position).getTittle());
            if (displaySource.get(position).isParent()){
                holder.displaySubTreeButton.setVisibility(View.VISIBLE);
                holder.displaySubTreeButton.forceLayout();
            }else{
                holder.displaySubTreeButton.setVisibility(View.INVISIBLE);
                holder.displaySubTreeButton.forceLayout();
            }
            if (displaySource.get(position).isBack()){
                holder.btnBack.setVisibility(View.VISIBLE);
                holder.displaySubTreeButton.forceLayout();
            }else{
                holder.btnBack.setVisibility(View.GONE);
                holder.displaySubTreeButton.forceLayout();
            }
        }
        return convertView;
    }



    static class ViewHolder{
        int position;
        TextView tvTitle;


        ViewHolder(View view){
            this.position = 0;
            tvTitle = view.findViewById(R.id.tvTitle);
        }
    }

    static class TreeViewHolder implements View.OnClickListener{
        int position;
        TextView tvTitle;
        TextView btnBack;
        ImageView displaySubTreeButton;
        TreeViewHolderOnClickListener listener;

        TreeViewHolder(View view,TreeViewHolderOnClickListener listener,TreeViewHolderBackClickListener backClickListener){
            this.listener = listener;
            tvTitle = view.findViewById(R.id.tvTitle);
            btnBack = view.findViewById(R.id.btnBack);
            btnBack.setOnClickListener(v -> backClickListener.makeABackClick());
            displaySubTreeButton = view.findViewById(R.id.btnShowSubTree);
            displaySubTreeButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.haveClickDisplaySubView(this.position);
        }

    }
    interface TreeViewHolderOnClickListener{
        void haveClickDisplaySubView(int position);
    }
    interface TreeViewHolderBackClickListener{
        void makeABackClick();
    }
}
