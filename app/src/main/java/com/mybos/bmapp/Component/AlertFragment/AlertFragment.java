package com.mybos.bmapp.Component.AlertFragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mybos.bmapp.R;

import java.util.List;


public class AlertFragment extends DialogFragment {
    private static final String TITLE = "TITLE";
    private static final String MESSAGE = "MESSAGE";

    private AlertFragmentDatasource datasource;

    public AlertFragment(){
        // requuire for dialog fragment
    }

    public static AlertFragment getInstanced(String title,String message,AlertFragmentDatasource datasource){
        AlertFragment fragment = new AlertFragment();
        fragment.datasource = datasource;
        Bundle bundle = new Bundle();
        bundle.putString(TITLE,title);
        bundle.putString(MESSAGE,message);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.f_alert, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);
        if (null != getArguments()) {
            String title = getArguments().getString(TITLE);
            ((TextView) view.findViewById(R.id.txtTitle)).setText(title);
            String message = getArguments().getString(MESSAGE);
            ((TextView) view.findViewById(R.id.txtMessage)).setText(message);
            LinearLayout layout = view.findViewById(R.id.layoutBottomButton);

            // check if datasource have any buttons
            if (null != datasource){
                List<Button> buttons = datasource.getListOfButton(this);
                if (null != buttons && buttons.size() > 0 ){
                    for (Button button:buttons){
                        layout.addView(button);
                    }
                    return;
                }
            }

            // button ok
            Button ok = new Button(getContext());
            if (null != getContext()) {
                ok.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                ok.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            }
            ok.setText(R.string.OK);
            ok.setOnClickListener((v)->{
                this.dismiss();
            });
            layout.addView(ok);

            // button send
//            Button send = new Button(getContext());
//            send.setText(R.string.Send);
//            send.setOnClickListener((v)->{
//                this.dismiss();
//            });
//            layout.addView(send);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        if (null != getActivity()){
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int width = (int)Math.round(displayMetrics.widthPixels * 0.8);
            if (null != getDialog().getWindow()) {
                getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        }
    }


    public interface AlertFragmentDatasource{
        List<Button> getListOfButton(AlertFragment alertFragment);
    }
}
