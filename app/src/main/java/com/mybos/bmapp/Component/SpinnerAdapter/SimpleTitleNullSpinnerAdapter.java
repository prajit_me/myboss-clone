package com.mybos.bmapp.Component.SpinnerAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.R;

import java.util.List;

/**
 * Created by EmLaAi on 08/04/2018.
 *
 * @author EmLaAi
 */
public class SimpleTitleNullSpinnerAdapter extends BaseAdapter implements SpinnerAdapter {
    private Context context;
    private LayoutInflater inflater;
    private SpinnerSourceCompatible source;
    private SpinnerSourceCompatible displaySource;
    public SimpleTitleNullSpinnerAdapter(Context context,SpinnerSourceCompatible source){
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.source = source;
    }

    public void setSource(SpinnerSourceCompatible source) {
        this.source = source;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return source.count() + 1;
    }

    @Override
    public Object getItem(int position) {
        if (position == 0) return null;
        return source.getItem(position - 1);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (position == 0){
            if (null == convertView){
                convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                convertView.setTag(R.id.spinner_view_type,0);
            }else{
                if (!convertView.getTag(R.id.spinner_view_type).equals(0)){
                    convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                    convertView.setTag(R.id.spinner_view_type,0);
                }
            }
        }else{
            ViewHolder holder;
            if (null != convertView){
                if (convertView.getTag(R.id.spinner_view_type).equals(1)) {
                    holder = (ViewHolder) convertView.getTag();
                }else {
                    convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                    convertView.setTag(R.id.spinner_view_type,1);
                    holder = new ViewHolder(convertView);
                    convertView.setTag(holder);
                }
            }else {
                convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                convertView.setTag(R.id.spinner_view_type,1);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            holder.position = position;

            holder.tvTitle.setText(source.getTitle(position - 1));
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (position == 0){
            if (null == convertView){
                convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                convertView.setTag(R.id.spinner_view_type,0);
            }else{
                if (!convertView.getTag(R.id.spinner_view_type).equals(0)){
                    convertView = inflater.inflate(R.layout.spinner_zero_height,parent,false);
                    convertView.setTag(R.id.spinner_view_type,0);
                }
            }
        }else{
            ViewHolder holder;
            if (null != convertView){
                if (convertView.getTag(R.id.spinner_view_type).equals(1)) {
                    holder = (ViewHolder) convertView.getTag();
                }else {
                    convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                    convertView.setTag(R.id.spinner_view_type,1);
                    holder = new ViewHolder(convertView);
                    convertView.setTag(holder);
                }
            }else {
                convertView = inflater.inflate(R.layout.spinner_title_only, parent, false);
                convertView.setTag(R.id.spinner_view_type,1);
                holder = new ViewHolder(convertView);
                convertView.setTag(holder);
            }
            holder.position = position;

            holder.tvTitle.setText(source.getTitle(position - 1));
        }
        return convertView;

    }

    static class ViewHolder{
        int position;
        TextView tvTitle;


        ViewHolder(View view){
            this.position = 0;
            tvTitle = view.findViewById(R.id.tvTitle);
        }

    }
}
