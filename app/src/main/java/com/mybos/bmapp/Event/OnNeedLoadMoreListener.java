package com.mybos.bmapp.Event;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public interface OnNeedLoadMoreListener {
    void onLoadMore();
}
