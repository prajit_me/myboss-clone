package com.mybos.bmapp.Data.Base;

import java.util.List;

/**
 * Created by EmLaAi on 04/04/2018.
 *
 * @author EmLaAi
 */
public interface KeepTrackSelectedItemList<T> {
    void selectId(T id);
    void deselectId(T id);
    List<BaseModel> getSelectedItem();
    List<BaseModel> getUnselectedItem();
}
