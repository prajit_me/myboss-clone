package com.mybos.bmapp.Data.Functional;

import androidx.annotation.NonNull;

/**
 * Model class for alert button
 */
public class AlertButton {
    /**
     * interface define button action
     */
    public interface AlertAction{
        void onClick();
    }
    private String name;
    private AlertAction action;

    public AlertButton(@NonNull String name, AlertAction action){
        this.name = name;
        this.action = action;

    }

    /**
     * get button name
     * @return button name
     */
    public String getName() {
        return name;
    }

    /**
     * get button action
     * @return button action @see AlertAction
     */
    public AlertAction getAction() {
        return action;
    }
}
