package com.mybos.bmapp.Data.Mapper.Assets;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentFile;
import com.mybos.bmapp.Data.Model.BuildingAssets.AssetFile;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetHistory;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.util.ArrayList;
import java.util.Map;

public class AssetMapper extends BaseMapper {

    public void getBuildingAssetList(Context context, int buildingId, String selectedCategory,int page, SuccessCallback<ArrayList<BuildingAsset>> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.BUILDING_ASSET_TABLE),Integer.toString(buildingId))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyBodyBuilder urlData = new VolleyBodyBuilder();
                    urlData.setParameter("f", selectedCategory).
                            setParameter("k", "").
                            setParameter("p", page).
                            setParameter("s", 50);

                    VolleyRequest.shared(mContext).post(url, header, urlData.build(), (response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONArray BuildingAssetJsonList = ((JSONObject) object).optJSONArray("data");
                                    ArrayList<BuildingAsset> BuildingAssetList = new ArrayList<>();

                                    int Size = BuildingAssetJsonList.length();
                                    for (int i = 0; i < Size; i++) {
                                        JSONObject CurrentAssetObject = BuildingAssetJsonList.optJSONObject(i);

                                        BuildingAsset CurrentAsset = new BuildingAsset();
                                        CurrentAsset.setAdded(CurrentAssetObject.optString("added"));
                                        CurrentAsset.setBarcode(CurrentAssetObject.optInt("barcode"));
                                        CurrentAsset.setBuildingID(CurrentAssetObject.optInt("building"));
                                        CurrentAsset.setCategory(CurrentAssetObject.optInt("category"));
                                        CurrentAsset.setCategoryName(CurrentAssetObject.optString("categoryName"));

                                        JSONArray ContractorTextList = CurrentAssetObject.optJSONArray("contractors");
                                        if (ContractorTextList.length() > 0) {
                                            for (int j = 0; j < ContractorTextList.length(); j++) {
                                                CurrentAsset.addToContractorText(ContractorTextList.getString(j));
                                            }
                                        }

                                        CurrentAsset.setDescription(CurrentAssetObject.optString("description"));
                                        CurrentAsset.setAssetID(CurrentAssetObject.optInt("id"));
                                        CurrentAsset.setLocation(CurrentAssetObject.optString("location"));
                                        CurrentAsset.setMake(CurrentAssetObject.optString("make"));
                                        CurrentAsset.setModel(CurrentAssetObject.optString("model"));
                                        CurrentAsset.setName(CurrentAssetObject.optString("name"));
                                        CurrentAsset.setSerial(CurrentAssetObject.optInt("serial"));
                                        CurrentAsset.setUpdate(CurrentAssetObject.optString("updated"));
                                        CurrentAsset.setAssetValue(CurrentAssetObject.optInt("value"));
                                        CurrentAsset.setWarrantyExpire(CurrentAssetObject.optString("warranty_expire"));
                                        CurrentAsset.setWarrantyTitle(CurrentAssetObject.optString("warranty_title"));

                                        BuildingAssetList.add(CurrentAsset);
                                    }

                                    onSuccess.onSuccess(BuildingAssetList);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException |InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void getSpecificBuildingAsset(Context context, BuildingAsset selectedAsset, SuccessCallback<BuildingAsset> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.SPECIFIC_BUILDING_ASSET), Integer.toString(selectedAsset.getBuildingID()), Integer.toString(selectedAsset.getAssetID()))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    Log.e("check_url",url);
                    VolleyRequest.shared(mContext).get(url, header, (response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONObject CurrentAssetObject = ((JSONObject) object).optJSONObject("data");

                                    BuildingAsset CurrentAsset = new BuildingAsset();
                                    CurrentAsset.setAdded(CurrentAssetObject.optString("added"));
                                    CurrentAsset.setBarcode(CurrentAssetObject.optInt("barcode"));
                                    CurrentAsset.setBuildingID(CurrentAssetObject.optInt("building"));
                                    CurrentAsset.setCategory(CurrentAssetObject.optInt("category"));
                                    CurrentAsset.setCategoryName(CurrentAssetObject.optString("categoryName"));

                                    JSONArray ContractorTextList = CurrentAssetObject.optJSONArray("contractorText");
                                    if (ContractorTextList.length() > 0) {
                                        for (int j = 0; j < ContractorTextList.length(); j++) {
                                            CurrentAsset.addToContractorText(ContractorTextList.getString(j));
                                        }
                                    }

                                    JSONArray ContractorList = CurrentAssetObject.optJSONArray("contractors");
                                    if (ContractorList.length() > 0) {
                                        for (int j = 0; j < ContractorList.length(); j++) {
                                            JSONObject CurrentListedContractor = ContractorList.optJSONObject(j);
                                            Contractor CurrentContractor = new Contractor();

                                            CurrentContractor.setAddress(CurrentListedContractor.optString("address"));
                                            CurrentContractor.setCategory(CurrentListedContractor.optInt("category"));
                                            CurrentContractor.setCategoryName(CurrentListedContractor.optString("categoryName"));
                                            CurrentContractor.setCompany(CurrentListedContractor.optString("company"));
                                            CurrentContractor.setEmail(CurrentListedContractor.optString("email"));
                                            CurrentContractor.setFacebook(CurrentListedContractor.optString("facebook"));
                                            CurrentContractor.setFax(CurrentListedContractor.optString("fax"));
                                            CurrentContractor.setGoogle(CurrentListedContractor.optString("google"));
                                            CurrentContractor.setId(CurrentListedContractor.optInt("id"));
                                            CurrentContractor.setIndustry(CurrentListedContractor.optInt("industry"));
                                            CurrentContractor.setInfo(CurrentListedContractor.optString("info"));

                                            JSONObject CurrentListedContractorMainContact = CurrentListedContractor.optJSONObject("mainContact");
                                            Contractor.MainContact CurrentContractorMainContact = new Contractor.MainContact();
                                            CurrentContractorMainContact.setContractor(CurrentListedContractorMainContact.optInt("contractor"));
                                            CurrentContractorMainContact.setEmail(CurrentListedContractorMainContact.optString("email"));
                                            CurrentContractorMainContact.setId(CurrentListedContractorMainContact.optInt("id"));
                                            CurrentContractorMainContact.setMain(CurrentListedContractorMainContact.optInt("main"));
                                            CurrentContractorMainContact.setName(CurrentListedContractorMainContact.optString("name"));
                                            CurrentContractorMainContact.setNumber(CurrentListedContractorMainContact.optString("number"));
                                            CurrentContractor.setMainContact(CurrentContractorMainContact);

                                            CurrentContractor.setPhone(CurrentListedContractor.optString("phone"));
                                            CurrentContractor.setPreferred(CurrentListedContractor.optInt("preferred"));
                                            CurrentContractor.setWebsite(CurrentListedContractor.optString("website"));

                                            CurrentAsset.addToContractors(CurrentContractor);
                                        }
                                    }

                                    CurrentAsset.setDescription(CurrentAssetObject.optString("description"));

                                    JSONArray DocumentList = CurrentAssetObject.optJSONArray("documents");
                                    if (DocumentList.length() > 0) {
                                        for (int j = 0; j < DocumentList.length(); j++) {
                                            JSONObject CurrentDocument = DocumentList.optJSONObject(j);

                                            AssetFile AssetDocument = new AssetFile();
                                            AssetDocument.setAdded(CurrentDocument.optString("added"));
                                            AssetDocument.setDetail(CurrentDocument.optString("detail"));
                                            AssetDocument.setExpire(CurrentDocument.optString("expire"));
                                            AssetDocument.setFileName(CurrentDocument.optString("file_name"));
                                            AssetDocument.setFileType(CurrentDocument.optString("file_type"));
                                            AssetDocument.setAssetFileID(CurrentDocument.optInt("id"));
                                            AssetDocument.setAssetFileName(CurrentDocument.optString("name"));
                                            AssetDocument.setAssetFileSize(CurrentDocument.optInt("size"));
                                            AssetDocument.setAssetFileSized(CurrentDocument.optString("sized"));
                                            AssetDocument.setSubject(CurrentDocument.optString("subject"));
                                            AssetDocument.setAssetFileType(CurrentDocument.optInt("type"));
                                            AssetDocument.setURL(CurrentDocument.optString("url"));

                                            CurrentAsset.addToDocuments(AssetDocument);
                                        }
                                    }

                                    CurrentAsset.setAssetID(CurrentAssetObject.optInt("id"));
                                    CurrentAsset.setLocation(CurrentAssetObject.optString("location"));
                                    CurrentAsset.setMake(CurrentAssetObject.optString("make"));
                                    CurrentAsset.setModel(CurrentAssetObject.optString("model"));
                                    CurrentAsset.setName(CurrentAssetObject.optString("name"));

                                    JSONArray PhotoList = CurrentAssetObject.optJSONArray("photos");
                                    if (PhotoList.length() > 0) {
                                        for (int j = 0; j < PhotoList.length(); j++) {
                                            JSONObject CurrentPhoto = PhotoList.optJSONObject(j);

                                            AssetFile AssetPhoto = new AssetFile();
                                            AssetPhoto.setAdded(CurrentPhoto.optString("added"));
                                            AssetPhoto.setDetail(CurrentPhoto.optString("detail"));
                                            AssetPhoto.setExpire(CurrentPhoto.optString("expire"));
                                            AssetPhoto.setFileName(CurrentPhoto.optString("file_name"));
                                            AssetPhoto.setFileType(CurrentPhoto.optString("file_type"));
                                            AssetPhoto.setAssetFileID(CurrentPhoto.optInt("id"));
                                            AssetPhoto.setAssetFileName(CurrentPhoto.optString("name"));
                                            AssetPhoto.setAssetFileSize(CurrentPhoto.optInt("size"));
                                            AssetPhoto.setAssetFileSized(CurrentPhoto.optString("sized"));
                                            AssetPhoto.setSubject(CurrentPhoto.optString("subject"));
                                            AssetPhoto.setAssetFileType(CurrentPhoto.optInt("type"));
                                            AssetPhoto.setURL(CurrentPhoto.optString("url"));

                                            CurrentAsset.addToPhotos(AssetPhoto);
                                        }
                                    }

                                    CurrentAsset.setSerial(CurrentAssetObject.optInt("serial"));
                                    CurrentAsset.setUpdate(CurrentAssetObject.optString("updated"));
                                    CurrentAsset.setAssetValue(CurrentAssetObject.optInt("value"));

                                    JSONObject WarrantyDocument = CurrentAssetObject.optJSONObject("warranty_document");
                                    if (WarrantyDocument != null) {
                                        AssetFile CurrentWarrantyDocument = new AssetFile();
                                        CurrentWarrantyDocument.setAdded(WarrantyDocument.optString("added"));
                                        CurrentWarrantyDocument.setDetail(WarrantyDocument.optString("detail"));
                                        CurrentWarrantyDocument.setExpire(WarrantyDocument.optString("expire"));
                                        CurrentWarrantyDocument.setFileName(WarrantyDocument.optString("file_name"));
                                        CurrentWarrantyDocument.setFileType(WarrantyDocument.optString("file_type"));
                                        CurrentWarrantyDocument.setAssetFileID(WarrantyDocument.optInt("id"));
                                        CurrentWarrantyDocument.setAssetFileName(WarrantyDocument.optString("name"));
                                        CurrentWarrantyDocument.setAssetFileSize(WarrantyDocument.optInt("size"));
                                        CurrentWarrantyDocument.setAssetFileSized(WarrantyDocument.optString("sized"));
                                        CurrentWarrantyDocument.setSubject(WarrantyDocument.optString("subject"));
                                        CurrentWarrantyDocument.setAssetFileType(WarrantyDocument.optInt("type"));
                                        CurrentWarrantyDocument.setURL(WarrantyDocument.optString("url"));
                                        CurrentAsset.addToWarrantyDocument(CurrentWarrantyDocument);
                                    }

                                    CurrentAsset.setWarrantyExpire(CurrentAssetObject.optString("warranty_expire"));
                                    CurrentAsset.setWarrantyTitle(CurrentAssetObject.optString("warranty_title"));

                                    onSuccess.onSuccess(CurrentAsset);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException |InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void getSpecificBuildingAssetHistory(Context context, BuildingAsset selectedAsset, SuccessCallback<ArrayList<BuildingAssetHistory>> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.SPECIFIC_BUILDING_ASSET_HISTORY), Integer.toString(selectedAsset.getBuildingID()), Integer.toString(selectedAsset.getAssetID()))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();

                    VolleyRequest.shared(mContext).get(url, header, (response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONArray CurrentAssetHistoryArray = ((JSONObject) object).optJSONArray("data");
                                    ArrayList<BuildingAssetHistory> AssetHistoryList = new ArrayList<>();

                                    int Size = CurrentAssetHistoryArray.length();
                                    if (Size > 0) {
                                        for (int i = 0; i < Size; i++) {
                                            JSONObject CurrentAssetHistoryObject = CurrentAssetHistoryArray.getJSONObject(i);

                                            BuildingAssetHistory CurrentAssetHistory = new BuildingAssetHistory();
                                            CurrentAssetHistory.setAdded(CurrentAssetHistoryObject.optString("added"));
                                            CurrentAssetHistory.setArea(CurrentAssetHistoryObject.optInt("area"));
                                            CurrentAssetHistory.setAreaText(CurrentAssetHistoryObject.optString("area_text"));
                                            CurrentAssetHistory.setBuildingID(CurrentAssetHistoryObject.optInt("building"));
                                            CurrentAssetHistory.setCompletionDate(CurrentAssetHistoryObject.optString("completion_date"));
                                            CurrentAssetHistory.setDetail(CurrentAssetHistoryObject.optString("detail"));
                                            CurrentAssetHistory.setDue(CurrentAssetHistoryObject.optString("due"));
                                            CurrentAssetHistory.setHistoryNote(CurrentAssetHistoryObject.optString("historyNote"));
                                            CurrentAssetHistory.setHistory_Note(CurrentAssetHistoryObject.optString("history_note"));
                                            CurrentAssetHistory.setHistoryID(CurrentAssetHistoryObject.optInt("id"));
                                            CurrentAssetHistory.setIsDuplicate(CurrentAssetHistoryObject.optInt("is_duplicate"));
                                            CurrentAssetHistory.setIsRead(CurrentAssetHistoryObject.optInt("is_read"));
                                            CurrentAssetHistory.setIsReport(CurrentAssetHistoryObject.optInt("is_report"));
                                            CurrentAssetHistory.setLoggedBy(CurrentAssetHistoryObject.optString("logged_by"));
                                            CurrentAssetHistory.setHistoryNumber(CurrentAssetHistoryObject.optInt("number"));
                                            CurrentAssetHistory.setPriority(CurrentAssetHistoryObject.optInt("priority"));
                                            CurrentAssetHistory.setPriorityText(CurrentAssetHistoryObject.optString("priority_text"));
                                            CurrentAssetHistory.setStatus(CurrentAssetHistoryObject.optInt("sStatus"));
                                            CurrentAssetHistory.setStarred(CurrentAssetHistoryObject.optInt("starred"));
                                            CurrentAssetHistory.setHistoryStart(CurrentAssetHistoryObject.optString("start"));
                                            CurrentAssetHistory.setStatus(CurrentAssetHistoryObject.optInt("status"));
                                            CurrentAssetHistory.setStatusText(CurrentAssetHistoryObject.optString("status_text"));
                                            CurrentAssetHistory.setSubStatus(CurrentAssetHistoryObject.optString("sub_status"));
                                            CurrentAssetHistory.setSubject(CurrentAssetHistoryObject.optString("subject"));
                                            CurrentAssetHistory.setSync(CurrentAssetHistoryObject.optInt("sync"));
                                            CurrentAssetHistory.setHistoryType(CurrentAssetHistoryObject.optInt("type"));
                                            CurrentAssetHistory.setTypeText(CurrentAssetHistoryObject.optString("type_text"));
                                            CurrentAssetHistory.setUpdated(CurrentAssetHistoryObject.optString("updated"));
                                            CurrentAssetHistory.setWorkOrder(CurrentAssetHistoryObject.optString("work_order"));
                                            AssetHistoryList.add(CurrentAssetHistory);
                                        }
                                    }

                                    onSuccess.onSuccess(AssetHistoryList);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException |InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void createAsset(Context context, int buildingId, VolleyMultipartBuilder builder,
                            SuccessCallback<String> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.BUILDING_ASSET_CREATING),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                Log.e("checking_create",url+"----"+String.valueOf(builder));
                VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
//                                AttachmentFile file = new AttachmentFile();
//                                JSONObject fileJSON = ((JSONObject) object).getJSONObject("file");
                                onSuccess.onSuccess(((JSONObject) object).optString("id") );
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),"failed"));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);

            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }


    public void editAsset(Context context, int buildingId, String assetId, VolleyMultipartBuilder builder,
                            SuccessCallback<String> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.BUILDING_ASSET_EDITING),Integer.toString(buildingId),assetId)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                Log.e("checking_create",url+"----"+String.valueOf(builder));
                VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
//                                AttachmentFile file = new AttachmentFile();
//                                JSONObject fileJSON = ((JSONObject) object).getJSONObject("file");
                                onSuccess.onSuccess(((JSONObject) object).optString("id") );
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),"failed"));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);

            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

}
