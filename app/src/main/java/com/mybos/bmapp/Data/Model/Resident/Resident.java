package com.mybos.bmapp.Data.Model.Resident;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Base.BaseModel;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class Resident extends BaseModel implements Parcelable {
    private int uid;
    private String apartment;
    private int main;
    private int lot;
    private String firstName;
    private int id;
    private String lastName;
    private int building;
    private int login;
    private String mobile;
    private int type;
    private String email;
    private int member;
    private String secondNumber;
    private String phone;
    private String userName;
    private String name;
    private String typeText;
    private String memberText;

    public Resident(){}

    protected Resident(Parcel in) {
        uid = in.readInt();
        apartment = in.readString();
        main = in.readInt();
        lot = in.readInt();
        firstName = in.readString();
        id = in.readInt();
        lastName = in.readString();
        building = in.readInt();
        login = in.readInt();
        mobile = in.readString();
        type = in.readInt();
        email = in.readString();
        member = in.readInt();
        secondNumber = in.readString();
        phone = in.readString();
        userName = in.readString();
        name = in.readString();
        typeText = in.readString();
        memberText = in.readString();
    }

    public static final Creator<Resident> CREATOR = new Creator<Resident>() {
        @Override
        public Resident createFromParcel(Parcel in) {
            return new Resident(in);
        }

        @Override
        public Resident[] newArray(int size) {
            return new Resident[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMain(int main) {
        this.main = main;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setLogin(int login) {
        this.login = login;
    }

    public void setMember(int member) {
        this.member = member;
    }

    public void setMemberText(String memberText) {
        this.memberText = memberText;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setSecondNumber(String secondNumber) {
        this.secondNumber = secondNumber;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getId() {
        return id;
    }

    public int getUid() {
        return uid;
    }

    public String getFullName() {
        String resultString = this.firstName + " " + this.lastName;
        if (this.firstName.equals("null")&&this.lastName.equals("null")){
            resultString = this.name;
        }
        return resultString.replace("null","");
    }

    public String getApartment() {
        return apartment;
    }

    public String getPhone() {
        return phone;
    }

    public String getMobile() {
        return mobile;
    }

    public String getEmail() {
        return email;
    }

    public int getMember() {
        return member;
    }

    public String getMemberText() {
        return memberText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(uid);
        dest.writeString(apartment);
        dest.writeInt(main);
        dest.writeInt(lot);
        dest.writeString(firstName);
        dest.writeInt(id);
        dest.writeString(lastName);
        dest.writeInt(building);
        dest.writeInt(login);
        dest.writeString(mobile);
        dest.writeInt(type);
        dest.writeString(email);
        dest.writeInt(member);
        dest.writeString(secondNumber);
        dest.writeString(phone);
        dest.writeString(userName);
        dest.writeString(name);
        dest.writeString(typeText);
        dest.writeString(memberText);
    }
}
