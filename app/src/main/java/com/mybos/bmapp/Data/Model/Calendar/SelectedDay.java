package com.mybos.bmapp.Data.Model.Calendar;

import java.util.ArrayList;

public class SelectedDay {

    private static String date = "";
    private static String dateOfweek = "";
    private static String fullDate = "";

    private static ArrayList<String> allDayOfCurrentMonth = new ArrayList<>();

    public static String getDate() {
        return date;
    }

    public static void setDate(String date) {
        SelectedDay.date = date;
    }

    public static String getDateOfweek() {
        return dateOfweek;
    }

    public static void setDateOfweek(String dateOfweek) {
        SelectedDay.dateOfweek = dateOfweek;
    }

    public static String getFullDate() {
        return fullDate;
    }

    public static void setFullDate(String fullDate) {
        SelectedDay.fullDate = fullDate;
    }

    public static ArrayList<String> getAllDayOfCurrentMonth() {
        return allDayOfCurrentMonth;
    }

    public static void setAllDayOfCurrentMonth(ArrayList<String> allDayOfCurrentMonth) {
        SelectedDay.allDayOfCurrentMonth = allDayOfCurrentMonth;
    }
}
