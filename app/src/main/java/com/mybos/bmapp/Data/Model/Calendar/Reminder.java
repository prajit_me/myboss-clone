package com.mybos.bmapp.Data.Model.Calendar;

import java.util.ArrayList;

public class Reminder {

    private String id = "";
    private String building = "";
    private String title = "";
    private String date = "";
    private String time = "";
    private String comment = "";
    private String dashboard = "";
    private String fired = "";
    private static String curMonth = "";
    private static String curYear = "";

    public static Reminder getReminderFromIdOfDate(String id, String date){
        for (Reminder reminder: DaysEvent.getDayEFromDateOfDayEs(date).getReminder()
             ) {
            if (id.equals(reminder.getId())){
                return reminder;
            }
        }
        return null;
    }


    private static ArrayList<Reminder> reminders = new ArrayList<>();

    public static String getCurMonth() {
        return curMonth;
    }

    public static void setCurMonth(String curMonth) {
        Reminder.curMonth = curMonth;
    }

    public static String getCurYear() {
        return curYear;
    }

    public static void setCurYear(String curYear) {
        Reminder.curYear = curYear;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDashboard() {
        return dashboard;
    }

    public void setDashboard(String dashboard) {
        this.dashboard = dashboard;
    }

    public String getFired() {
        return fired;
    }

    public void setFired(String fired) {
        this.fired = fired;
    }

    static public ArrayList<Reminder> getReminders() {
        return reminders;
    }

    static public void setReminders(ArrayList<Reminder> reminders) {
        Reminder.reminders = reminders;
    }
}
