package com.mybos.bmapp.Data.Mapper.BuildingCase;

import android.content.Context;
import android.text.Html;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentFile;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadInvoice;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadQuote;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatus;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseType;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseTypeList;
import com.mybos.bmapp.Data.Model.Cases.Message;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Data.Model.Filter.Type;
import com.mybos.bmapp.Data.Model.Filter.TypeList;
import com.mybos.bmapp.Data.Response.BuildingCaseResponse;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 24/03/2018.
 *
 * @author EmLaAi
 */

public class BuildingCaseMapper extends BaseMapper {
    public void getList(Context context, int buildingId , SearchTableQueryBuilder searchTableQueryBuilder, SuccessCallback<BuildingCaseResponse> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = searchTableQueryBuilder.buildPostBody();
                VolleyRequest.shared(mContext).post(url,header,body,(response)->{
                    try {
                        Object obj = new JSONTokener(response).nextValue();
                        if (obj instanceof JSONObject){
                            if (((JSONObject) obj).optInt("state") == 200){
                                int maximum = ((JSONObject) obj).getInt("total");
                                List<BuildingCase> temp = new ArrayList<>();
                                JSONArray jsonBuildingList =((JSONObject) obj).getJSONArray("data");
                                for (int i = 0; i < jsonBuildingList.length(); i++) {
                                    JSONObject jsonBuilding = (JSONObject) jsonBuildingList.get(i);
                                    BuildingCase buildingCase = new BuildingCase();
                                    buildingCase.setId(jsonBuilding.optInt("id"));
                                    buildingCase.setNumber(jsonBuilding.optInt("number"));
                                    buildingCase.setBuilding(jsonBuilding.optInt("building"));
                                    buildingCase.setAdded(jsonBuilding.optString("added"));
                                    buildingCase.setSubject(jsonBuilding.optString("subject"));
                                    buildingCase.setDetail(jsonBuilding.optString("detail"));
                                    buildingCase.setType(jsonBuilding.optInt("type"));
                                    buildingCase.setPriority(jsonBuilding.optInt("priority"));
                                    buildingCase.setStart(jsonBuilding.optString("start"));
                                    buildingCase.setDue(jsonBuilding.optString("due"));
                                    buildingCase.setStatus(jsonBuilding.optInt("status"));
                                    buildingCase.setSubStatus(jsonBuilding.optInt("sub_status"));
                                    buildingCase.setArea(jsonBuilding.optInt("area"));
                                    buildingCase.setWorkOrder(jsonBuilding.optInt("work_order"));
                                    buildingCase.setIsRead(jsonBuilding.optInt("is_read"));
                                    buildingCase.setIsReport(jsonBuilding.optInt("is_report"));
                                    buildingCase.setLoggedBy(jsonBuilding.optString("logged_by"));
                                    buildingCase.setIsDuplicate(jsonBuilding.optInt("is_duplicate"));
                                    buildingCase.setCompletionDate(jsonBuilding.optString("completion_date"));
                                    buildingCase.setUpdated(jsonBuilding.optString("updated"));
                                    buildingCase.setHistoryNote(jsonBuilding.optString("history_note"));
                                    buildingCase.setTypeText(jsonBuilding.optString("type_text"));
                                    buildingCase.setStatusText(jsonBuilding.optString("status_text"));
                                    buildingCase.setAreaText(jsonBuilding.optString("area_text"));
                                    buildingCase.setPriorityText(jsonBuilding.optString("priority_text"));
                                    buildingCase.setHasVideo(jsonBuilding.optInt("hasVideo"));
                                    buildingCase.setHasAttachment(jsonBuilding.optInt("hasAttachment"));
                                    buildingCase.setWoSent(jsonBuilding.optInt("woSent"));
                                    buildingCase.setsStatus(jsonBuilding.optInt("sStatus"));
                                    ArrayList<String> tempContractorList = new ArrayList<>();
                                    if (!jsonBuilding.optString("contractors","null").equals("null")){
                                        Log.e("check_contractor",jsonBuilding.optString("contractors","null"));
                                        JSONArray contractors = jsonBuilding.getJSONArray("contractors");
                                        for (int j = 0; j<contractors.length();j++){
                                            tempContractorList.add(contractors.getString(j));
                                        }
                                        buildingCase.setContractorList(tempContractorList);
                                    }
                                    temp.add(buildingCase);
                                }
                                BuildingCaseResponse caseResponse = new BuildingCaseResponse(maximum,temp);
                                onSuccess.onSuccess(caseResponse);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) obj).optInt("state"),((JSONObject) obj).getString("message")));
                            }
                        }else{
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else{
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void get(Context context,int buildingId, String caseId, SuccessCallback<BuildingCase> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_ITEM),Integer.toString(buildingId),caseId)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                JSONObject caseInfoJson = ((JSONObject) object).optJSONObject("data");
                                if (null != caseInfoJson){
                                    BuildingCase buildingCase = new BuildingCase();
                                    buildingCase.setId(caseInfoJson.optInt("id"));
                                    Message.setSelectedReId(caseInfoJson.optString("requestId"));
                                    buildingCase.setNumber(caseInfoJson.optInt("number"));
                                    buildingCase.setBuilding(caseInfoJson.optInt("building"));
                                    buildingCase.setAdded(caseInfoJson.optString("added"));
                                    buildingCase.setSubject(caseInfoJson.optString("subject"));
                                    //TODO: remove html entity
                                    String convertString = Html.fromHtml(caseInfoJson.optString("detail")).toString();
                                    buildingCase.setDetail(convertString);
                                    buildingCase.setType(caseInfoJson.optInt("type"));
                                    buildingCase.setPriority(caseInfoJson.optInt("priority"));
                                    buildingCase.setStart(caseInfoJson.optString("start"));
                                    buildingCase.setDue(caseInfoJson.optString("due"));
                                    buildingCase.setStatus(caseInfoJson.optInt("status"));
                                    buildingCase.setSubStatus(caseInfoJson.optInt("sub_status"));
                                    buildingCase.setArea(caseInfoJson.optInt("area"));
                                    buildingCase.setWorkOrder(caseInfoJson.optInt("work_order"));
                                    buildingCase.setIsRead(caseInfoJson.optInt("is_read"));
                                    buildingCase.setIsReport(caseInfoJson.optInt("is_report"));
                                    buildingCase.setLoggedBy(caseInfoJson.optString("logged_by"));
                                    buildingCase.setIsDuplicate(caseInfoJson.optInt("is_duplicate"));
                                    buildingCase.setCompletionDate(caseInfoJson.optString("completion_date"));
                                    buildingCase.setUpdated(caseInfoJson.optString("updated"));
                                    buildingCase.setHistoryNote(caseInfoJson.optString("history_note"));
                                    buildingCase.setStarred((caseInfoJson.has("starred") && !caseInfoJson.isNull("starred"))?caseInfoJson.optString("starred"):null);
                                    buildingCase.setTypeText(caseInfoJson.optString("type_text"));
                                    buildingCase.setStatusText(caseInfoJson.optString("status_text"));
                                    buildingCase.setAreaText(caseInfoJson.optString("area_text"));
                                    buildingCase.setPriorityText(caseInfoJson.optString("priority_text"));
                                    buildingCase.setsStatus(caseInfoJson.optInt("sStatus"));

                                    JSONArray assetsJsonArray = caseInfoJson.optJSONArray("assets");
                                    for (int i = 0; i < assetsJsonArray.length(); i++) {
                                        buildingCase.addAssets(assetsJsonArray.optInt(i));
                                    }
                                    JSONArray assetDataJsonArray = caseInfoJson.optJSONArray("assetData");
                                    for (int i = 0; i < assetDataJsonArray.length(); i++) {
                                        buildingCase.addAssetData(assetDataJsonArray.optString(i));
                                    }

                                    JSONArray contractorJsonArray = caseInfoJson.optJSONArray("contractors");
                                    for (int i = 0; i < contractorJsonArray.length(); i++) {
                                        buildingCase.addContractors(contractorJsonArray.optInt(i));
                                    }

                                    JSONArray contractorDataJsonArray = caseInfoJson.optJSONArray("contractorData");
                                    for (int i = 0; i < contractorDataJsonArray.length(); i++) {
                                        buildingCase.addContractorData(contractorDataJsonArray.optString(i));
                                    }

                                    JSONArray apartmentJsonArray = caseInfoJson.optJSONArray("apartments");
                                    for (int i = 0; i < apartmentJsonArray.length(); i++) {
                                        buildingCase.addApartment(apartmentJsonArray.optString(i));
                                    }

                                    JSONArray photosJsonArray = caseInfoJson.optJSONArray("photos");
                                    for (int i = 0; i < photosJsonArray.length(); i++) {
                                        JSONObject photoJson = photosJsonArray.getJSONObject(i);
                                        AttachmentFile file = new AttachmentFile();
                                        file.setId(photoJson.optInt("id"));
                                        file.setName(photoJson.optString("name"));
                                        file.setType(photoJson.optInt("type"));
                                        file.setFileName(photoJson.optString("file_name"));
                                        file.setSized(photoJson.optString("sized"));
                                        file.setSize(photoJson.optInt("size"));
                                        file.setAdded(photoJson.optString("added"));
                                        file.setFileType(photoJson.optString("file_type"));
                                        file.setUrl(photoJson.optString("url"));
                                        file.setSubject(photoJson.optString("subject"));
                                        file.setDetail(photoJson.optString("detail"));
                                        file.setExpire(photoJson.optString("expire"));
                                        buildingCase.addAttachmentFile(file);
                                    }

                                    JSONArray docsJsonArray = caseInfoJson.optJSONArray("documents");
                                    for (int i = 0; i < docsJsonArray.length(); i++) {
                                        JSONObject docJson = docsJsonArray.getJSONObject(i);
                                        AttachmentFile file = new AttachmentFile();
                                        file.setId(docJson.optInt("id"));
                                        file.setName(docJson.optString("name"));
                                        file.setType(docJson.optInt("type"));
                                        file.setFileName(docJson.optString("file_name"));
                                        file.setSized(docJson.optString("sized"));
                                        file.setSize(docJson.optInt("size"));
                                        file.setAdded(docJson.optString("added"));
                                        file.setFileType(docJson.optString("file_type"));
                                        file.setUrl(docJson.optString("url"));
                                        file.setSubject(docJson.optString("subject"));
                                        file.setDetail(docJson.optString("detail"));
                                        file.setExpire(docJson.optString("expire"));
                                        buildingCase.addDocsFile(file);
                                    }

                                    JSONArray quoteJsonArray = caseInfoJson.optJSONArray("quotes");
                                    for (int i = 0; i < quoteJsonArray.length(); i++) {
                                        JSONObject quoteJson = quoteJsonArray.optJSONObject(i);
                                        AttachmentUploadQuote quote = new AttachmentUploadQuote();
                                        quote.setAmount(quoteJson.optDouble("amount"));
                                        quote.setType(quoteJson.optString("type"));
                                        quote.setDate(quoteJson.optString("date"));

                                        JSONObject fileJson = quoteJson.optJSONObject("file");
                                        if (null != fileJson) {
                                            AttachmentFile file = new AttachmentFile();
                                            file.setId(fileJson.optInt("id"));
                                            file.setName(fileJson.optString("name"));
                                            file.setType(fileJson.optInt("type"));
                                            file.setFileName(fileJson.optString("file_name"));
                                            file.setSized(fileJson.optString("sized"));
                                            file.setSize(fileJson.optInt("size"));
                                            file.setAdded(fileJson.optString("added"));
                                            file.setFileType(fileJson.optString("file_type"));
                                            file.setUrl(fileJson.optString("url"));
                                            file.setSubject(fileJson.optString("subject"));
                                            file.setDetail(fileJson.optString("detail"));
                                            file.setExpire(fileJson.optString("expire"));

                                            quote.setFile(file);
                                        }

                                        JSONObject contractorJson = quoteJson.optJSONObject("contractor");
                                        if(null != contractorJson){
                                            Contractor contractor = new Contractor();
                                            contractor.setCompany(contractorJson.optString("company"));
                                            contractor.setAddress(contractorJson.optString("address"));
                                            contractor.setPreferred(contractorJson.optInt("preferred"));
                                            contractor.setPhone(contractorJson.optString("phone"));
                                            contractor.setFax(contractorJson.optString("fax"));
                                            contractor.setEmail(contractorJson.optString("email"));
                                            contractor.setFacebook(contractorJson.optString("facebook"));
                                            contractor.setWebsite(contractorJson.optString("website"));
                                            contractor.setGoogle(contractorJson.optString("google"));
                                            contractor.setIndustry(contractorJson.optInt("industry"));
                                            contractor.setId(contractorJson.optInt("id"));
                                            contractor.setInfo(contractorJson.optString("info"));
                                            contractor.setCategory(contractorJson.optInt("category"));
                                            contractor.setCategoryName(contractorJson.optString("categoryName"));

                                            Contractor.MainContact contact = new Contractor.MainContact();
                                            JSONObject mainContactJson = contractorJson.optJSONObject("mainContact");
                                            if (mainContactJson != null) {
                                                contact.setMain(mainContactJson.optInt("main"));
                                                contact.setNumber(mainContactJson.optString("number"));
                                                contact.setName(mainContactJson.optString("name"));
                                                contact.setEmail(mainContactJson.optString("email"));
                                                contact.setId(mainContactJson.optInt("id"));
                                                contact.setContractor(mainContactJson.optInt("contractor"));
                                            }

                                            contractor.setMainContact(contact);
                                            quote.setContractor(contractor);
                                        }

                                        buildingCase.addAttachmentQuote(quote);
                                    }

                                    JSONArray invoiceJsonArray = caseInfoJson.optJSONArray("invoices");
                                    for (int i = 0; i < invoiceJsonArray.length(); i++) {
                                        AttachmentUploadInvoice invoice = new AttachmentUploadInvoice();
                                        JSONObject invoiceJson = invoiceJsonArray.getJSONObject(i);
                                        invoice.setAmount(invoiceJson.optDouble("amount"));
                                        invoice.setNumber(invoiceJson.optString("number"));
                                        invoice.setDate(invoiceJson.optString("date"));
                                        invoice.setType(invoiceJson.optString("type"));

                                        JSONObject fileJson = invoiceJson.optJSONObject("file");
                                        if (null != fileJson){
                                            AttachmentFile file = new AttachmentFile();
                                            file.setId(fileJson.optInt("id"));
                                            file.setName(fileJson.optString("name"));
                                            file.setType(fileJson.optInt("type"));
                                            file.setFileName(fileJson.optString("file_name"));
                                            file.setSized(fileJson.optString("sized"));
                                            file.setSize(fileJson.optInt("size"));
                                            file.setAdded(fileJson.optString("added"));
                                            file.setFileType(fileJson.optString("file_type"));
                                            file.setUrl(fileJson.optString("url"));
                                            file.setSubject(fileJson.optString("subject"));
                                            file.setDetail(fileJson.optString("detail"));
                                            file.setExpire(fileJson.optString("expire"));

                                            invoice.setFile(file);
                                        }


                                        JSONObject contractorJson = invoiceJson.optJSONObject("contractor");
                                        if (null != contractorJson){
                                            Contractor contractor = new Contractor();
                                            contractor.setCompany(contractorJson.optString("company"));
                                            contractor.setAddress(contractorJson.optString("address"));
                                            contractor.setPreferred(contractorJson.optInt("preferred"));
                                            contractor.setPhone(contractorJson.optString("phone"));
                                            contractor.setFax(contractorJson.optString("fax"));
                                            contractor.setEmail(contractorJson.optString("email"));
                                            contractor.setFacebook(contractorJson.optString("facebook"));
                                            contractor.setWebsite(contractorJson.optString("website"));
                                            contractor.setGoogle(contractorJson.optString("google"));
                                            contractor.setIndustry(contractorJson.optInt("industry"));
                                            contractor.setId(contractorJson.optInt("id"));
                                            contractor.setInfo(contractorJson.optString("info"));
                                            contractor.setCategory(contractorJson.optInt("category"));
                                            contractor.setCategoryName(contractorJson.optString("categoryName"));

                                            Contractor.MainContact contact = new Contractor.MainContact();
                                            JSONObject mainContactJson = contractorJson.optJSONObject("mainContact");
                                            BuildingCaseMapper.parseJson(contact,mainContactJson);

                                            contractor.setMainContact(contact);
                                            invoice.setContractor(contractor);
                                        }


                                        buildingCase.addAttachmentInvoice(invoice);
                                    }

                                    Logger.shared().o(buildingCase);
                                    onSuccess.onSuccess(buildingCase);
                                }else {
                                    throw new InvalidResponse();
                                }
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void save(Context context, int buildingId, ContractorList contractorList,BuildingCase buildingCase, SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_ITEM),Integer.toString(buildingId),Integer.toString(buildingCase.getId()))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyBodyBuilder builder = new VolleyBodyBuilder()
                        .setParameter("id",buildingCase.getId())
                        .setParameter("number",buildingCase.getNumber())
                        .setParameter("building",buildingCase.getBuilding())
                        .setParameter("added",buildingCase.getAdded())
                        .setParameter("subject",buildingCase.getSubject())
                        .setParameter("detail",buildingCase.getDetail())
                        .setParameter("type",buildingCase.getType())
                        .setParameter( "priority",  buildingCase.getPriority())
                        .setParameter( "start",  buildingCase.getStart())
                        .setParameter( "due", buildingCase.getDue())
                        .setParameter( "status", buildingCase.getStatus())
                        .setParameter( "sub_status", buildingCase.getSubStatus())
                        .setParameter( "area", buildingCase.getArea())
                        .setParameter( "work_order", buildingCase.getWorkOrder())
                        .setParameter( "is_read", buildingCase.getIsRead())
                        .setParameter( "is_report", buildingCase.getIsReport())
                        .setParameter( "logged_by", buildingCase.getLoggedBy())
                        .setParameter( "is_duplicate", buildingCase.getIsDuplicate())
                        .setParameter( "completion_date", buildingCase.getCompletionDate())
                        .setParameter( "updated", buildingCase.getUpdated())
                        .setParameter( "history_note", buildingCase.getHistoryNote())
                        .setParameter( "hisroryNote",  "")
                        .setParameter( "starred", buildingCase.getStarred())
                        .setParameter( "type_text", buildingCase.getTypeText())
                        .setParameter( "status_text", buildingCase.getStatusText())
                        .setParameter( "sStatus", buildingCase.getsStatus())
                        .setParameter( "area_text", buildingCase.getAreaText())
                        .setParameter( "priority_text", buildingCase.getPriorityText())
                        .setStringList("apartments",buildingCase.getApartmentsSelectedList())
                        .setIntegerList( "assets", buildingCase.getAssets())
                        .setStringList( "assetData", buildingCase.getAssetData())
                        .setIntegerList( "contractors", contractorList.getSelectedId())
                        .setStringList( "contractorData", contractorList.getSelectedName());
                List<AttachmentFile> fileList = buildingCase.getPhotos();
                if (fileList.size() > 0) {
                    List<Map<String,Object>> fileListJSON = new ArrayList<>();
                    for (AttachmentFile file:fileList){
                        fileListJSON.add(BuildingCaseMapper.buildRawJsonFromFile(file));
                    }
                    builder.addSubRawJSONList("photos",fileListJSON);
                }
                List<AttachmentUploadQuote> quoteList = buildingCase.getQuotes();
                if (quoteList.size() > 0){
                    List<Map<String,Object>> quoteListJSON = new ArrayList<>();
                    for (AttachmentUploadQuote quote:quoteList){
                        quoteListJSON.add(BuildingCaseMapper.buildRawJsonFromQuote(quote));
                    }
                    builder.addSubRawJSONList("quotes",quoteListJSON);
                }
                List<AttachmentUploadInvoice> invoiceList = buildingCase.getInvoices();
                if (invoiceList.size() > 0){
                    List<Map<String,Object>> invoiceListJSON = new ArrayList<>();
                    for (AttachmentUploadInvoice invoice:invoiceList){
                        invoiceListJSON.add(BuildingCaseMapper.buildRawJsonFromInvoice(invoice));
                    }
                    builder.addSubRawJSONList("invoices",invoiceListJSON);
                }
                VolleyRequest.shared(mContext).post(url,header,builder.build(),response->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                onSuccess.onSuccess(null);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void update(Context context,int buildingId,int caseId,int statusId,
                       SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_ITEM),
                                Integer.toString(buildingId),Integer.toString(caseId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder().setParameter("status",statusId).build();
                VolleyRequest.shared(mContext).put(url,header,body,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                onSuccess.onSuccess(null);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getMessage(Context context,int buildingId,String caseId,SuccessCallback<String> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_GET_MESSAGE),Integer.toString(buildingId),
                                caseId)
                        .build();
                Log.e("hug_log",url);
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,response->{

                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            String state = ((JSONObject) object).optString("state");
                            Log.e("hug_log",state);
                            ArrayList<Message> tempList = new ArrayList<>();
                            JSONArray data = ((JSONObject) object).getJSONArray("data");
                            for (int i = 0; i < data.length(); i++){
                                JSONObject Curobject = data.getJSONObject(i);
                                Message message = new Message();
                                message.setId(Curobject.optString("id"));
                                message.setState(Curobject.optString("state"));
                                message.setComment(Curobject.optString("comment"));
                                message.setTime(Curobject.optString("time"));
                                message.setRequestId(Curobject.optString("requestId"));
                                message.setWriterName(Curobject.optString("writerName"));
                                message.setWriter(Curobject.optString("writer"));

                                tempList.add(message);
                            }
                            Message.setMessages(new ArrayList<>(tempList));
                            onSuccess.onSuccess("success");
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void sendMessage(Context context,int buildingId,String caseId,String comment,SuccessCallback<String> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_GET_MESSAGE),
                                Integer.toString(buildingId),caseId)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder()
                        .setParameter("comment", comment)
                        .build();
                VolleyRequest.shared(mContext).post(url,header,body,response->{
                    onSuccess.onSuccess("Success");
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void updateMessage(Context context,int BuildingId, String caseId, String idComment,String comment,SuccessCallback<String> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_E_D_MESSAGE),
                                String.valueOf(BuildingId),caseId,idComment)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder()
                        .setParameter("comment", comment)
                        .build();
                VolleyRequest.shared(mContext).post(url,header,body,response->{
                    onSuccess.onSuccess("Success");
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void deleteMessage(Context context,int buildingId,String caseId,String idComment,SuccessCallback<String> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_E_D_MESSAGE),
                                String.valueOf(buildingId),caseId,idComment)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder()
                        .build();
                VolleyRequest.shared(mContext).delete(url,header,body,response->{
                    onSuccess.onSuccess("Success");
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }


    public void delete(Context context,int buildingId,int caseId,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if(null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_ITEM),Integer.toString(buildingId),Integer.toString(caseId))
                        .build();
                Map<String,String> headers = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder().setParameter("status",3).build();
                VolleyRequest.shared(mContext).put(url,headers,body,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                onSuccess.onSuccess(null);
                            }else{
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else{
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else{
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getAllCaseType(Context context,int buildingId,SuccessCallback<BuildingCaseTypeList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_TYPE_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                BuildingCaseTypeList list = new BuildingCaseTypeList();
                                JSONArray caseTypeArrayJson = ((JSONObject) object).optJSONArray("data");
                                for (int i = 0; i < caseTypeArrayJson.length(); i++) {
                                    BuildingCaseType caseType = new BuildingCaseType();
                                    JSONObject caseTypeJson = caseTypeArrayJson.getJSONObject(i);
                                    caseType.setName(caseTypeJson.optString("name"));
                                    caseType.setId(caseTypeJson.optInt("id"));
                                    caseType.setBuilding(caseTypeJson.optInt("building"));
                                    caseType.setOrder(caseTypeJson.optInt("order"));
                                    list.addCaseType(caseType);
                                }
                                onSuccess.onSuccess(list);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                           throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else{
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getAllCaseStatus(Context context,int buildingId,SuccessCallback<BuildingCaseStatusList> onSuccess,FailureCallback onError){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_STATUS_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                JSONArray statusJsonAray = ((JSONObject) object).optJSONArray("data");
                                List<BuildingCaseStatus> statusList = new ArrayList<>();
                                for (int i = 0; i < statusJsonAray.length(); i++) {
                                    JSONObject statusJson = statusJsonAray.optJSONObject(i);
                                    BuildingCaseStatus status = new BuildingCaseStatus();
                                    status.setId(statusJson.optInt("id"));
                                    status.setName(statusJson.optString("name"));
                                    statusList.add(status);
                                }


                                BuildingCaseStatusList buildingCaseStatusList = new BuildingCaseStatusList();
                                buildingCaseStatusList.addList(mContext,statusList);
                                onSuccess.onSuccess(buildingCaseStatusList);
                            }else {
                                onError.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw  new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onError.onFailure(e);
                    }
                },onError::onFailure);
            }else {
                onError.onFailure(new InternalError());
            }
        });
    }

    public void getAllCaseStatusForFilter(Context context, int buildingId, SuccessCallback<TypeList> onSuccess, FailureCallback onError){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_STATUS_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                JSONArray statusJsonAray = ((JSONObject) object).optJSONArray("data");
                                List<Type> statusList = new ArrayList<>();
                                for (int i = 0; i < statusJsonAray.length(); i++) {
                                    JSONObject statusJson = statusJsonAray.optJSONObject(i);
                                    Type status = new Type();
                                    status.setId(statusJson.optInt("id"));
                                    status.setName(statusJson.optString("name"));
                                    statusList.add(status);
                                }
                                Type typeNew = new Type("New",0);
                                statusList.add(typeNew);
                                Type typeProgress = new Type("In Progress",1);
                                statusList.add(typeProgress);

                                TypeList typeList = new TypeList();
                                typeList.setTypelist(statusList);
                                onSuccess.onSuccess(typeList);
                            }else {
                                onError.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw  new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onError.onFailure(e);
                    }
                },onError::onFailure);
            }else {
                onError.onFailure(new InternalError());
            }
        });
    }


    public static Map<String,Object> buildRawJsonFromFile(AttachmentFile file){
        return new VolleyBodyBuilder()
                .setParameter("id",file.getId())
                .setParameter("type",file.getType())
                .setParameter("name",file.getName())
                .setParameter("file_name",file.getFileName())
                .setParameter("sized",file.getSized())
                .setParameter("size",file.getSize())
                .setParameter("added",file.getAdded())
                .setParameter("file_type",file.getFileType())
                .setParameter("url",file.getLink())
                .setParameter("subject",file.getSubject())
                .setParameter("detail",file.getDetail())
                .setParameter("expire",file.getExpire())
                .export();
    }

    public static Map<String,Object> buildRawJsonFromQuote(AttachmentUploadQuote quote){
        VolleyBodyBuilder builder = new VolleyBodyBuilder()
                .setParameter("amount",quote.getAmount())
                .setParameter("date",quote.getDate())
                .setParameter("type",quote.getType());
        if (quote.getFile() != null){
            builder.addSubRawJSON("file",BuildingCaseMapper.buildRawJsonFromFile(quote.getFile()));
        }
        if (quote.getContractor() != null){
            builder.addSubRawJSON("contractor", ContractorMapper.buildRawJSONDataFromContractor(quote.getContractor()));
        }
        return builder.export();
    }

    public static Map<String,Object> buildRawJsonFromInvoice(AttachmentUploadInvoice invoice){
        VolleyBodyBuilder builder = new VolleyBodyBuilder()
                .setParameter("amount",invoice.getAmount())
                .setParameter("date",invoice.getDate())
                .setParameter("type",invoice.getType())
                .setParameter("number",invoice.getNumber());
        if (invoice.getFile() != null){
            builder.addSubRawJSON("file",BuildingCaseMapper.buildRawJsonFromFile(invoice.getFile()));
        }
        if (invoice.getContractor() != null){
            builder.addSubRawJSON("contractor",ContractorMapper.buildRawJSONDataFromContractor(invoice.getContractor()));
        }
        return builder.export();
    }
    public static void parseJson(Contractor.MainContact mainContact,JSONObject object){
        if (object == null){return;}
        mainContact.setMain(object.optInt("main"));
        mainContact.setNumber(object.optString("number"));
        mainContact.setName(object.optString("name"));
        mainContact.setEmail(object.optString("email"));
        mainContact.setId(object.optInt("id"));
        mainContact.setContractor(object.optInt("contractor"));
    }

}
