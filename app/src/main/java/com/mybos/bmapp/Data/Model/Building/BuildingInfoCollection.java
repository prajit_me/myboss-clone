package com.mybos.bmapp.Data.Model.Building;

public class BuildingInfoCollection {

    private static int SELECTED_BUILDING = 0;
    private static int BUILDING_ID = 0;
    private static String BUILDING_NAME = "";

    public static void setSelectedBuilding(int position){
        SELECTED_BUILDING = position;
    }

    public static int getSelectedBuilding(){
        return SELECTED_BUILDING;
    }

    public static void setBuildingID(int BuildingID){
        BUILDING_ID = BuildingID;
    }

    public static int getBuildingID(){
        return BUILDING_ID;
    }

    public static void setBuildingName(String BuildingName){
        BUILDING_NAME = BuildingName;
    }

    public static String getBuildingName(){
        return BUILDING_NAME;
    }

}
