package com.mybos.bmapp.Data.Model.Key;

public class BuildingKeyHistory {

    private String Added = "";
    private int Back = 0;
    private String Company = "";
    private String Contact = "";
    private String Due = "";
    private String Expiry = "";
    private String Finish = "";
    private String ID = "";
    private String Key = "";
    private String Number = "";
    private String Reason = "";
    private KeyInOut SignIn = new KeyInOut();
    private KeyInOut SignOut = new KeyInOut();

    public String getAdded() {
        return Added;
    }

    public void setAdded(String added) {
        Added = added;
    }

    public int getBack() {
        return Back;
    }

    public void setBack(int back) {
        Back = back;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getDue() {
        return Due;
    }

    public void setDue(String due) {
        Due = due;
    }

    public String getExpiry() {
        return Expiry;
    }

    public void setExpiry(String expiry) {
        Expiry = expiry;
    }

    public String getFinish() {
        return Finish;
    }

    public void setFinish(String finish) {
        Finish = finish;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getNumber() {
        return Number;
    }

    public void setNumber(String number) {
        Number = number;
    }

    public String getReason() {
        return Reason;
    }

    public void setReason(String reason) {
        Reason = reason;
    }

    public KeyInOut getSignIn() {
        return SignIn;
    }

    public void setSignIn(KeyInOut signIn) {
        SignIn = signIn;
    }

    public KeyInOut getSignOut() {
        return SignOut;
    }

    public void setSignOut(KeyInOut signOut) {
        SignOut = signOut;
    }
}
