package com.mybos.bmapp.Data.Model.Calendar;

import java.util.ArrayList;

public class Booking {

    private String id = "";
    private String calendar = "";
    private String date = "";
    private String slot_from = "";
    private String slot_to = "";
    private String apartment = "";
    private String price = "";
    private String invoice = "";
    private String transaction = "";
    private String accounting = "";
    private String comment = "";
    private String status = "";
    private String booker_fname = "";
    private String booker_lname = "";
    private String booker_mobile = "";
    private String booker_phone = "";
    private String booker_email = "";
    private String booker_comment = "";
    private String token = "";
    private String paid = "";
    private String building = "";
    private String calendarName = "";
    private String statusText = "";
    private String buildingName = "";

    public static Booking getBookingFromId(String id){
        for (Booking booking: DaysEvent.getDayEFromDateOfDayEs(SelectedDay.getDate()).getBooking()
             ) {
            if (id.equals(booking.getId())){
                return booking;
            }
        }
        return null;
    }

    private static ArrayList<Booking> bookings = new ArrayList<>();

    public static ArrayList<Booking> getBookings() {
        return bookings;
    }

    public static void setBookings(ArrayList<Booking> bookings) {
        Booking.bookings = bookings;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBooker_fname() {
        return booker_fname;
    }

    public void setBooker_fname(String booker_fname) {
        this.booker_fname = booker_fname;
    }

    public String getBooker_lname() {
        return booker_lname;
    }

    public void setBooker_lname(String booker_lname) {
        this.booker_lname = booker_lname;
    }

    public String getBooker_mobile() {
        return booker_mobile;
    }

    public void setBooker_mobile(String booker_mobile) {
        this.booker_mobile = booker_mobile;
    }

    public String getBooker_phone() {
        return booker_phone;
    }

    public void setBooker_phone(String booker_phone) {
        this.booker_phone = booker_phone;
    }

    public String getBooker_email() {
        return booker_email;
    }

    public void setBooker_email(String booker_email) {
        this.booker_email = booker_email;
    }

    public String getBooker_comment() {
        return booker_comment;
    }

    public void setBooker_comment(String booker_comment) {
        this.booker_comment = booker_comment;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getCalendarName() {
        return calendarName;
    }

    public void setCalendarName(String calendarName) {
        this.calendarName = calendarName;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCalendar() {
        return calendar;
    }

    public void setCalendar(String calendar) {
        this.calendar = calendar;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSlot_from() {
        return slot_from;
    }

    public void setSlot_from(String slot_from) {
        this.slot_from = slot_from;
    }

    public String getSlot_to() {
        return slot_to;
    }

    public void setSlot_to(String slot_to) {
        this.slot_to = slot_to;
    }

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getInvoice() {
        return invoice;
    }

    public void setInvoice(String invoice) {
        this.invoice = invoice;
    }

    public String getTransaction() {
        return transaction;
    }

    public void setTransaction(String transaction) {
        this.transaction = transaction;
    }

    public String getAccounting() {
        return accounting;
    }

    public void setAccounting(String accounting) {
        this.accounting = accounting;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
