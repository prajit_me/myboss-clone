package com.mybos.bmapp.Data.Mapper.Inspection;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Inspection.CreateInspection.MakeInspectionActivity;
import com.mybos.bmapp.Activity.Inspection.InspectionInfoCollector;
import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionArea;
import com.mybos.bmapp.Data.Model.Inspection.InspectionDraft;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItem;
import com.mybos.bmapp.Data.Model.Inspection.InspectionList;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidInspectionResponseError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;
import com.mybos.bmapp.Utils.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 17/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionMapper extends BaseMapper {
    public void getList(Context context, int buildingId, int inspectionType, SearchTableQueryBuilder builder,
                        SuccessCallback<InspectionList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_LIST),Integer.toString(buildingId),Integer.toString(inspectionType))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = builder.buildPostBody();
                Log.e("check_urrl",url);
                VolleyRequest.shared(mContext).post(url,header,body,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray){
                            InspectionList list = new InspectionList();
                            for (int i = 0 ; i < ((JSONArray) object).length() ; i ++){
                                JSONObject inspectionJSON = ((JSONArray) object).getJSONObject(i);
                                Inspection inspection = new Inspection();
                                parseInspection(inspection,inspectionJSON);
                                list.addInspection(inspection);
                            }
                            onSuccess.onSuccess(list);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch(JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getListDraft(Context context, int buildingId,
                             SuccessCallback<ArrayList<InspectionDraft>> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_LIST_DRAFT),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                Log.e("check_urrl",url);
                VolleyRequest.shared(mContext).get(url,header,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        Log.e("check",object.toString());
                        JSONArray data = ((JSONObject)object).getJSONArray("data");
                        if (data instanceof JSONArray){
                            ArrayList<InspectionDraft> list = new ArrayList<>();
                            for (int i = ((JSONArray) data).length()-1 ; i >= 0  ; i --){
                                JSONObject inspectionJSON = ((JSONArray) data).getJSONObject(i);
                                InspectionDraft inspectionDraft = new InspectionDraft();
                                parseInspectionDraft(inspectionDraft,inspectionJSON);
                                list.add(inspectionDraft);
                            }
                            Collections.sort(list);
                            onSuccess.onSuccess(list);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch(JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getInspectionDraft(Context context, int buildingId,String historyId,String inspectionId,
                             SuccessCallback<JSONObject> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_HISTORY),Integer.toString(buildingId),inspectionId,historyId)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
//                Log.e("check_urrl_get_draft",url);
                VolleyRequest.shared(mContext).get(url,header,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        Log.e("check_get_draft",((JSONObject)object).getJSONObject("data").toString());
                        JSONObject data = ((JSONObject)object).getJSONObject("data");
                        ((MakeInspectionActivity)context).setApartment(((JSONObject)object).getJSONObject("history").optString("apartment","-1"));
                        onSuccess.onSuccess(data);

                    }catch(JSONException e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void get(Context context,int building,int inspectionId,SuccessCallback<Inspection> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_ITEM),Integer.toString(building),Integer.toString(inspectionId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("id") == inspectionId){
                                Inspection inspection = new Inspection();
                                InspectionMapper.parseInspection(inspection, (JSONObject) object);
                                JSONArray areaJsonList = ((JSONObject) object).optJSONArray("area");
                                if (areaJsonList != null) {
                                    for (int i = 0; i < areaJsonList.length(); i++) {
                                        JSONObject areaJson = areaJsonList.optJSONObject(i);
                                        InspectionArea area = new InspectionArea();
                                        InspectionMapper.parseInspectionArea(area, areaJson);
                                        JSONArray itemJsonArray = areaJson.optJSONArray("items");
                                        if (itemJsonArray != null) {
                                            for (int j = 0; j < itemJsonArray.length(); j++) {
                                                JSONObject itemJson = itemJsonArray.optJSONObject(j);
                                                InspectionItem item = new InspectionItem();
                                                InspectionMapper.parseInspectionItem(item, itemJson);
                                                area.addItem(item);
                                            }
                                        }
                                        inspection.addArea(area);
                                    }
                                }
                                onSuccess.onSuccess(inspection);
                            }else {
                                throw new InvalidInspectionResponseError();
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse|InvalidInspectionResponseError e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void saveHistory(Context context,int buildingId,int inspectionId,
                            Inspection inspection,int historyId,String apartment,
                            SuccessCallback<Integer> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_HISTORY),
                                Integer.toString(buildingId),Integer.toString(inspectionId),Integer.toString(historyId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                Integer maxId = inspection.getMaxId();
                if (null != maxId){
                    List<Map<String,Object>> list = new ArrayList<>();
                    for (int i =0 ; i < maxId + 1;i++){
                        list.add(null);
                    }
                    List<InspectionArea> areas = inspection.getAreas();
                    for (InspectionArea area:areas){
                        List<InspectionItem> items = area.getItems();
                        for (InspectionItem item:items){
                            Map<String,Object> rawItem = InspectionMapper.buildRawJson(item);
                            rawItem.put("id", item.getId());

                            if (item.getImage() != null){
                                Map<String,Object> rawPhoto = InspectionMapper.buildRawJson(item.getImage());
                                rawItem.put("photo",rawPhoto);
                            }
                            list.set(item.getId(),rawItem);
                        }
                    }
                    JSONArray json = new JSONArray(list);
                    builder.addObject("data",json);
                    if (apartment != null){
                        builder.addString("apartment",apartment);
                    }
                    VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                        try{
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                onSuccess.onSuccess(((JSONObject) object).optInt("id"));
                            }else {
                                throw new InternalError();
                            }
                        }catch (JSONException|InternalError e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    onSuccess.onSuccess(null);
                }
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public static void parseInspection(Inspection inspection, JSONObject object){
        inspection.setId(object.optInt("id"));
        inspection.setType(object.optInt("type"));
        inspection.setBuilding(object.optInt("building"));
        inspection.setName(object.optString("name"));
        inspection.setAdded(object.optString("added"));
        inspection.setTypeText(object.optString("typeText"));
    }

    public static void parseInspectionDraft(InspectionDraft inspection, JSONObject object){
        inspection.setApartment(object.optString("apartment"));
        inspection.setDraft(object.optString("draft"));
        inspection.setFname(object.optString("fname"));
        inspection.setId(object.optString("id"));
        inspection.setInspection(object.optString("inspection"));
        inspection.setInspectionId(object.optString("inspectionId"));
        inspection.setLname(object.optString("lname"));
        inspection.setManager(object.optString("manager"));
        inspection.setName(object.optString("name"));
        inspection.setTime(object.optString("time"));
        inspection.setType(object.optString("type"));
        inspection.setTypeText(object.optString("typeText"));

    }

    public static void parseInspectionArea(InspectionArea area,JSONObject object){
        area.setId(object.optInt("id"));
        area.setInspection(object.optInt("inspection"));
        area.setName(object.optString("name"));
    }

    public static void parseInspectionItem(InspectionItem item,JSONObject object){
        item.setId(object.optInt("id"));
        item.setInspection(object.optInt("inspection"));
        item.setArea(object.optInt("area"));
        item.setName(object.optString("name"));
    }

    public static Map<String,Object> buildRawJson(PhotoModel photoModel){
        return new VolleyBodyBuilder()
                .setParameter("name",photoModel.getName())
                .setParameter("urlData",photoModel.getBase64Image())
                .setParameter("type",photoModel.getMimetype())
                .export();
    }
    public static Map<String,Object> buildRawJson(InspectionItem item){
        VolleyBodyBuilder builder = new VolleyBodyBuilder()
                .setParameter("status",item.isFinish() ? 1 : 2);
        if (item.getComment() != null && item.getComment().trim().length() > 0){
            builder.setParameter("comment",item.getComment());
        }
        return builder.export();
    }

    //  VERSION 2

    public void getNewHistoryID(Context context, int buildingId, int inspectionId, String apartmentID, SuccessCallback<Integer> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext){
                    String url = new VolleyUrlBuilder(2)
                            .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_HISTORY_V2),
                                    Integer.toString(buildingId),
                                    String.valueOf(inspectionId))
                            .build();
                    Log.e("check_url_getNewHisID",url);
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    String data = new VolleyBodyBuilder()
                            .setParameter("apartment",apartmentID)
                            .setParameter("draft",1)
                            .setParameter("id","")
                            .setParameter("inspection",inspectionId)
                            .setParameter("manager","")
                            .setParameter("time", DateUtils.getCurrentTime()).build();
                    Log.e("check_data",data);
                    VolleyRequest.shared(mContext).post(url, header, data, (response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    int NewHistoryID = ((JSONObject) object).optInt("id");
                                    onSuccess.onSuccess(NewHistoryID);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException |InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void saveInspectionItem(Context context, int buildingId, int inspectionId, int historyId, SuccessCallback<Integer> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext) {
                    ArrayList<InspectionItem> ItemList = InspectionInfoCollector.getUpdatedInspectionItemList();
                    int Size = ItemList.size();
                    for (int i = 0; i < Size; i++) {
                        int inspectionItemId = ItemList.get(i).getId();

                        String url = new VolleyUrlBuilder(2)
                                .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_ITEM_V2),
                                        Integer.toString(buildingId),
                                        String.valueOf(inspectionId),
                                        String.valueOf(historyId),
                                        String.valueOf(inspectionItemId))
                                .build();
                        Log.e("check_url_saveInsItem",url);
                        Map<String, String> header = new VolleyHeaderBuilder().build();
                        VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                        int currentPoint = i;
                        //  STATUS
                        try {
                            builder.addString("status",String.valueOf(ItemList.get(i).getIsChecked()));
                        }
                        catch (Exception e){}

                        //  COMMENT
                        try {
                            builder.addString("comment", (null==ItemList.get(i).getComment())?"":ItemList.get(i).getComment());
                        }
                        catch (Exception e){}

                        //  PHOTO
                        try {
                            builder.addFile("photo", ItemList.get(i).getImage().createMultipartFile());
                        }
                        catch (Exception e){}
                        VolleyRequest.shared(mContext).postForm(url, header, builder, (response) -> {
                            try {
                                Object object = new JSONTokener(response).nextValue();Log.e("respone:",object.toString());
                                if (object instanceof JSONObject) {
                                    if (((JSONObject) object).optInt("state") == 200) {
                                        if (currentPoint == Size-1){
                                            onSuccess.onSuccess(200);
                                        }
                                    } else {
                                        onFailure.onFailure(processError(((JSONObject) object).optInt("state"), ((JSONObject) object).getString("message")));
                                    }
                                } else {
                                    throw new InvalidResponse();
                                }
                            } catch (JSONException | InvalidResponse e) {
                                onFailure.onFailure(e);
                            }
                        }, onFailure::onFailure);
                    }
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void saveInspectionAItem(Context context, int buildingId, int inspectionId, int historyId,InspectionItem inspectionItem, SuccessCallback<Integer> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext) {
                        int inspectionItemId = inspectionItem.getId();

                        String url = new VolleyUrlBuilder(2)
                                .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_ITEM_V2),
                                        Integer.toString(buildingId),
                                        String.valueOf(inspectionId),
                                        String.valueOf(historyId),
                                        String.valueOf(inspectionItemId))
                                .build();
                        Log.e("check_url_saveAIte",url);
                        Map<String, String> header = new VolleyHeaderBuilder().build();
                        VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                        //  STATUS
                        try {
                            builder.addString("status",String.valueOf(inspectionItem.getIsChecked()));
                        }
                        catch (Exception e){}

                        //  COMMENT
                        try {
                        builder.addString("comment", inspectionItem.getComment());
                    }
                        catch (Exception e){}

                        //  PHOTO
                        if (null!=inspectionItem.getImage()){
                            try {
                                builder.addFile("photo", inspectionItem.getImage().createMultipartFile());
                            }
                            catch (Exception e){}
                        }
                        Log.e("check_builder",builder.toString());
                        VolleyRequest.shared(mContext).postForm(url, header, builder, (response) -> {
                            try {
                                Object object = new JSONTokener(response).nextValue();
                                Log.e("respone:",object.toString());
                                if (object instanceof JSONObject) {
                                    if (((JSONObject) object).optInt("state") == 200) {
                                        onSuccess.onSuccess(200);
                                    } else {
                                        onFailure.onFailure(processError(((JSONObject) object).optInt("state"), ((JSONObject) object).getString("message")));
                                    }
                                } else {
                                    throw new InvalidResponse();
                                }
                            } catch (JSONException | InvalidResponse e) {
                                onFailure.onFailure(e);
                            }
                        }, onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void saveNewHistoryID(Context context, int buildingId, int inspectionId, int historyId, SuccessCallback<Integer> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext){
                    String url = new VolleyUrlBuilder(2)
                            .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.SAVE_INSPECTION_HISTORY_V2),
                                    Integer.toString(buildingId),
                                    String.valueOf(inspectionId),
                                    String.valueOf(historyId))
                            .build();
                    Log.e("check_url_bug",url);
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyBodyBuilder urlData = new VolleyBodyBuilder();
                    VolleyRequest.shared(mContext).put(url, header, urlData.build(), (response)->{
                        try {
                            onSuccess.onSuccess(1);
                        }catch (Exception e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void convertToCase(Context context,int building,int inspectionId,int historyId,String itemId,SuccessCallback<String> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_CONVERT),Integer.toString(building),Integer.toString(inspectionId),
                                Integer.toString(historyId),itemId)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                Log.e("check_convert",url);
                VolleyRequest.shared(mContext).get(url,header,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                           String state = ((JSONObject) object).optString("state");
                                onSuccess.onSuccess(state);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void saveInspectionItemFromOffline(Context context, int buildingId, int inspectionId, int historyId, ArrayList<InspectionItem> ItemList, SuccessCallback<Integer> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext) {
                    int Size = ItemList.size();
                    for (int i = 0; i < Size; i++) {
                        int inspectionItemId = ItemList.get(i).getId();

                        String url = new VolleyUrlBuilder(2)
                                .parseUrl(VolleyUrlBuilder.URLPATH.inspection(VolleyUrlBuilder.URLPATH.INSPECTION_ITEM_V2),
                                        Integer.toString(buildingId),
                                        String.valueOf(inspectionId),
                                        String.valueOf(historyId),
                                        String.valueOf(inspectionItemId))
                                .build();
                        Map<String, String> header = new VolleyHeaderBuilder().build();
                        VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                        int currentPoint = i;
                        //  STATUS
                        try {
                            builder.addString("status",String.valueOf(ItemList.get(i).getIsChecked()));
                        }
                        catch (Exception e){}
                        //  COMMENT
                        try {
                            builder.addString("comment", (null==ItemList.get(i).getComment())?"":ItemList.get(i).getComment());
                        }
                        catch (Exception e){}

                        //  PHOTO
                        try {
                            builder.addFile("photo", ItemList.get(i).getImage().createMultipartFile());
                        }
                        catch (Exception e){}
                        VolleyRequest.shared(mContext).postForm(url, header, builder, (response) -> {
                            try {
                                Object object = new JSONTokener(response).nextValue();Log.e("respone:",object.toString());
                                if (object instanceof JSONObject) {
                                    if (((JSONObject) object).optInt("state") == 200) {
                                        if (currentPoint == Size-1){
                                            onSuccess.onSuccess(200);
                                        }
                                    } else {
                                        onFailure.onFailure(processError(((JSONObject) object).optInt("state"), ((JSONObject) object).getString("message")));
                                    }
                                } else {
                                    throw new InvalidResponse();
                                }
                            } catch (JSONException | InvalidResponse e) {
                                onFailure.onFailure(e);
                            }
                        }, onFailure::onFailure);
                    }
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }
}

