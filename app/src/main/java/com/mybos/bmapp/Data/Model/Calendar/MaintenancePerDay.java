package com.mybos.bmapp.Data.Model.Calendar;

import java.util.ArrayList;

public class MaintenancePerDay {

    private String id ="";
    private String date ="";
    private String date_ogrinal ="";
    private String change ="";
    private String time ="";
    private String title ="";
    private ArrayList<MaintenanceCommnet> comments= new ArrayList<>();
    private ArrayList<File> invoices = new ArrayList<>();
    private ArrayList<File> documents = new ArrayList<>();
    private String dateStart = "";
    private String dateEnd = "";
    private String status ="";
    private String statusText ="";
    private String type_text ="";
    private String detail ="";

    private static ArrayList<MaintenancePerDay> maintenancePerDays = new ArrayList<>();

    private static MaintenancePerDay maintenancePerDaySelected = new MaintenancePerDay();

    public static ArrayList<MaintenancePerDay> getListByDate(String date){
        ArrayList<MaintenancePerDay> resultList = new ArrayList<>();
        for (MaintenancePerDay maintenancePerDay: maintenancePerDays
             ) {
            if (maintenancePerDay.getDate().equals(date)){
                resultList.add(maintenancePerDay);
            }
        }
        return resultList;
    }

    public static ArrayList<MaintenancePerDay> getMaintenancePerDays() {
        return maintenancePerDays;
    }

    public static void setMaintenancePerDays(ArrayList<MaintenancePerDay> maintenancePerDays) {
        MaintenancePerDay.maintenancePerDays = maintenancePerDays;
    }

    public static MaintenancePerDay getMaintenancePerDaySelected() {
        return maintenancePerDaySelected;
    }

    public static void setMaintenancePerDaySelected(MaintenancePerDay maintenancePerDaySelected) {
        MaintenancePerDay.maintenancePerDaySelected = maintenancePerDaySelected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getChange() {
        return change;
    }

    public void setChange(String change) {
        this.change = change;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ArrayList<MaintenanceCommnet> getComments() {
        return comments;
    }

    public void setComments(ArrayList<MaintenanceCommnet> comments) {
        this.comments = comments;
    }

    public ArrayList<File> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<File> documents) {
        this.documents = documents;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getType_text() {
        return type_text;
    }

    public void setType_text(String type_text) {
        this.type_text = type_text;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public ArrayList<File> getInvoices() {
        return invoices;
    }

    public void setInvoices(ArrayList<File> invoices) {
        this.invoices = invoices;
    }

    public String getDate_ogrinal() {
        return date_ogrinal;
    }

    public void setDate_ogrinal(String date_ogrinal) {
        this.date_ogrinal = date_ogrinal;
    }
}
