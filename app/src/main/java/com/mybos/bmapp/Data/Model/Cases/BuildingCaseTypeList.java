package com.mybos.bmapp.Data.Model.Cases;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseTypeList extends BaseModel implements SpinnerSourceCompatible {
    private List<BuildingCaseType> typeList;
    public BuildingCaseTypeList(){
        this.typeList = new ArrayList<>();
    }

    public void addCaseType(BuildingCaseType caseType){
        typeList.add(caseType);
    }

    public int getSelectedType(int id){
        int SelectedType = 0;
        if (typeList != null) {
            for (int i = 0; i < typeList.size(); i++) {
                if (typeList.get(i).getId() == id) {
                    SelectedType = i;
                    break;
                }
            }
        }

        return SelectedType;
    }

    @Override
    public List<String> getSource() {
        List<String> returnValue = new ArrayList<>();
        for (BuildingCaseType type:typeList) {
            returnValue.add(type.getName());
        }
        return returnValue;
    }

    @Override
    public int count() {
        return typeList.size();
    }

    @Override
    public BaseModel getItem(int position) {
        return typeList.get(position);
    }

    @Override
    public String getTitle(int position) {
        return typeList.get(position).getName();
    }

    public List<BuildingCaseType> getTypeList() {
        return typeList;
    }

    public void setTypeList(List<BuildingCaseType> typeList) {
        this.typeList = typeList;
    }
}
