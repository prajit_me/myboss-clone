package com.mybos.bmapp.Data.Model.Broadcast;

import java.util.ArrayList;

public class Groups {

    private static ArrayList<Group> groups;

    public static ArrayList<Group> getGroups() {
        return groups;
    }

    public static void setGroups(ArrayList<Group> groups) {
        Groups.groups = groups;
    }

    public static void getGroupsWithDefaultGroups() {
        groups = new ArrayList<>();
        groups.add(new Group(0, "All Users", -1));
        groups.add(new Group(1, "All Tenants", -1));
        groups.add(new Group(2, "All Owners", -1));
        groups.add(new Group(3, "Managing Agents", -1));
        groups.add(new Group(4, "Investors", -1));
        groups.add(new Group(5, "Strata Committee", -1));
        groups.add(new Group(6, "Emergency Contacts", -1));
    }

    public static int getIdFromName(String name){
        int id = -1;
        for (Group group:groups) {
            if(group.getName().equals(name)){
                id = group.getId();
            }
        }
        return id;
    }

    public static int getPositionFromName(String name){
        int position = -1;
        for (Group group:groups) {
            if(group.getName().equals(name)){
                position = groups.indexOf(group);
            }
        }
        return position;
    }

}
