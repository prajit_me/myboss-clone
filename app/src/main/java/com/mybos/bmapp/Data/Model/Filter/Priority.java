package com.mybos.bmapp.Data.Model.Filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Component.SpinnerAdapter.SpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

public class Priority extends BaseModel implements SpinnerItemInterface<Integer>, Parcelable {
    private String name="";
    private int id;

    public Priority (){

    }

    protected Priority(Parcel in) {
        name = in.readString();
        id = in.readInt();
    }

    public static final Creator<Priority> CREATOR = new Creator<Priority>() {
        @Override
        public Priority createFromParcel(Parcel in) {
            return new Priority(in);
        }

        @Override
        public Priority[] newArray(int size) {
            return new Priority[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(id);
    }

    @Override
    public Integer getSpinnerItemId() {
        return Integer.valueOf(id);
    }

    @Override
    public String getTittle() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
