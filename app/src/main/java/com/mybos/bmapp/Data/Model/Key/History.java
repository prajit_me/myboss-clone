package com.mybos.bmapp.Data.Model.Key;

import java.util.ArrayList;

public class History {

    public static final String paramHistory = "{\"p\":1,\"s\":50,\"k\":\"\",\"o\":\"added\",\"ot\":\"desc\"}";

    private String id;
    private String key;
    private String added;
    private String contact;
    private String company;
    private String reason;
    private String number;
    private String due;
    private String expiry;
    private String back;
    private String finish;
    private KeyInOut signIn = new KeyInOut();
    private KeyInOut signOut = new KeyInOut();


    private static ArrayList<History> histories = new ArrayList<>();
    private static String keyIdSelected;

    public static ArrayList<History> getHistories() {
        return histories;
    }

    public static void setHistories(ArrayList<History> histories) {
        History.histories = histories;
    }

    public static String getKeyIdSelected() {
        return keyIdSelected;
    }

    public static void setKeyIdSelected(String keyIdSelected) {
        History.keyIdSelected = keyIdSelected;
    }


    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public String getBack() {
        return back;
    }

    public void setBack(String back) {
        this.back = back;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public KeyInOut getSignIn() {
        return signIn;
    }

    public void setSignIn(KeyInOut signIn) {
        this.signIn = signIn;
    }

    public KeyInOut getSignOut() {
        return signOut;
    }

    public void setSignOut(KeyInOut signOut) {
        this.signOut = signOut;
    }
}
