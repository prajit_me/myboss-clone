package com.mybos.bmapp.Data.Functional;

import androidx.annotation.IntDef;
import androidx.annotation.StringDef;
import androidx.annotation.StringRes;

import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by EmLaAi on 24/03/2018.
 *
 * @author EmLaAi
 */

public class SearchTableQueryBuilder {
    private int pageSize;
    private int currentPage;
    private String keyword;
    private String section;
    private String orderBy;
    private String orderType;

    public SearchTableQueryBuilder(){
        this(10,1,"","","","");
    }

    public SearchTableQueryBuilder(int pageSize,int currentPage,String keyword,String section,String orderBy,String orderType){
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.keyword = keyword;
        this.section = section;
        this.orderBy = orderBy;
        this.orderType = orderType;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String buildPostBody(){
        Map<String,Object> mapPost = new HashMap<>();
        mapPost.put("p",this.currentPage);
        mapPost.put("s",this.pageSize);
        mapPost.put("k",this.keyword);
        mapPost.put("f",this.section);
        mapPost.put("o",this.orderBy);
        mapPost.put("ot",this.orderType);
        JSONObject object = new JSONObject(mapPost);
        return object.toString();
    }

    public static class SectionHint{
        public static final String CaseCurrent = "current";
        public static final String CaseCompleted = "completed";
        public static final String CaseTrash = "trash";

        public static final int InspectionCommon = 0;
        public static final int InspectionPrivate = 1;
        public static final int InspectionDraft = 99;

        public static final int BroadcastEmail = 0;
        public static final int BroadcastSMS = 1;

        public static final int KeyPrivate = 0;
        public static final int KeyContractor = 1;

        public static final int KeyApartmentWManagement = 0;
        public static final int KeyApartmentOther = 1;

        public String getCaseSectionValue(@CaseSectionHint String hint){return hint;}
        public int getInspectionSectionValue(@InpecstionSectionHint int hint){return  hint;}
        public int getBroadcastSectionValue(@BroadcastSectionHint int hint){return hint;}
        public int getKeySectionValue(@KeySectionHint int hint){return hint;}
        public int getKeyApartmentSectionValue(@KeyApartmentSectionHint int hint){return hint;}

        @StringDef({CaseCurrent,CaseCompleted,CaseTrash})
        @Retention(RetentionPolicy.SOURCE)
        public @interface CaseSectionHint{}

        @IntDef({InspectionCommon,InspectionPrivate,InspectionDraft})
        @Retention(RetentionPolicy.SOURCE)
        public @interface InpecstionSectionHint{}

        @IntDef({BroadcastEmail,BroadcastSMS})
        @Retention(RetentionPolicy.SOURCE)
        public @interface BroadcastSectionHint{}

        @IntDef({KeyPrivate,KeyContractor})
        @Retention(RetentionPolicy.SOURCE)
        public @interface KeySectionHint{}

        @IntDef({KeyApartmentWManagement,KeyApartmentOther})
        @Retention(RetentionPolicy.SOURCE)
        public @interface KeyApartmentSectionHint{}
    }


}
