package com.mybos.bmapp.Data.Model.Cases;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Collectors;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseStatusList extends BaseModel implements SpinnerSourceCompatible {
    private List<BuildingCaseStatus> caseStatusList;
    public BuildingCaseStatusList(){
        caseStatusList = new ArrayList<>();
    }

    public void addList(Context context,List<BuildingCaseStatus> statusList){
        caseStatusList.addAll(getBaseList(context));
        caseStatusList.addAll(statusList);
    }

    private List<BuildingCaseStatus> getBaseList(Context context){
        BuildingCaseStatus newStatus = new BuildingCaseStatus();
        newStatus.setId(0);
        newStatus.setName(context.getString(R.string.new_case_status_new));
        BuildingCaseStatus inProgress = new BuildingCaseStatus();
        inProgress.setId(1);
        inProgress.setName(context.getString(R.string.new_case_status_in_progress));
        BuildingCaseStatus completed = new BuildingCaseStatus();
        completed.setId(2);
        completed.setName(context.getString(R.string.new_case_status_completed));
        BuildingCaseStatus deleted = new BuildingCaseStatus();
        deleted.setId(3);
        deleted.setName(context.getString(R.string.new_case_status_deleted));
        return new ArrayList<>(Arrays.asList(newStatus,inProgress,completed,deleted));
    }

    public int getSelectedIndex(int id){
        int SelectedIndex = 0;
        if (caseStatusList != null) {
            for (int i = 0; i < caseStatusList.size(); i++) {
                if (caseStatusList.get(i).getId() == id) {
                    SelectedIndex = i;
                    break;
                }
            }
        }
        return SelectedIndex;
    }

    @Override
    public List<String> getSource() {
        List<String> returnValue = new ArrayList<>();
        for (BuildingCaseStatus caseStatus:caseStatusList) {
            returnValue.add(caseStatus.getName());
        }
        return returnValue;
    }

    @Override
    public int count() {
        return caseStatusList.size();
    }

    @Override
    public BaseModel getItem(int position) {
        return caseStatusList.get(position);
    }

    @Override
    public String getTitle(int position) {
        return caseStatusList.get(position).getName();
    }
}
