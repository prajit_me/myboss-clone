package com.mybos.bmapp.Data.Model.Calendar;

import java.util.ArrayList;

public class Maintenance {

    private ArrayList<File> photos = new ArrayList<>();
    private ArrayList<File> documents = new ArrayList<>();
    private ArrayList<String> contractors = new ArrayList<>();
    private ArrayList<String> assets = new ArrayList<>();
    private ArrayList<String> assetText = new ArrayList<>();
    private ArrayList<String> contractorText = new ArrayList<>();
    private ArrayList<String> apartments = new ArrayList<>();

    private String id = "";
    private String type_text = "";
    private String title = "";
    private String subject = "";
    private String detail = "";
    private String start = "";
    private String area = "";
    private String areaText = "";
    private String end = "";
    private String type = "";
    private String auto_wo = "";
    private String auto_time = "";
    private String logged_by = "";
    private String active = "";

    private static ArrayList<Maintenance> maintenances = new ArrayList<>();

    public static ArrayList<Maintenance> getMaintenances() {
        return maintenances;
    }

    public static void setMaintenances(ArrayList<Maintenance> maintenances) {
        Maintenance.maintenances = maintenances;
    }

    public ArrayList<File> getPhotos() {
        return photos;
    }

    public void setPhotos(ArrayList<File> photos) {
        this.photos = photos;
    }

    public ArrayList<File> getDocuments() {
        return documents;
    }

    public void setDocuments(ArrayList<File> documents) {
        this.documents = documents;
    }

    public ArrayList<String> getContractors() {
        return contractors;
    }

    public void setContractors(ArrayList<String> contractors) {
        this.contractors = contractors;
    }

    public ArrayList<String> getAssets() {
        return assets;
    }

    public void setAssets(ArrayList<String> assets) {
        this.assets = assets;
    }

    public ArrayList<String> getAssetText() {
        return assetText;
    }

    public void setAssetText(ArrayList<String> assetText) {
        this.assetText = assetText;
    }

    public ArrayList<String> getContractorText() {
        return contractorText;
    }

    public void setContractorText(ArrayList<String> contractorText) {
        this.contractorText = contractorText;
    }

    public ArrayList<String> getApartments() {
        return apartments;
    }

    public void setApartments(ArrayList<String> apartments) {
        this.apartments = apartments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType_text() {
        return type_text;
    }

    public void setType_text(String type_text) {
        this.type_text = type_text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAreaText() {
        return areaText;
    }

    public void setAreaText(String areaText) {
        this.areaText = areaText;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAuto_wo() {
        return auto_wo;
    }

    public void setAuto_wo(String auto_wo) {
        this.auto_wo = auto_wo;
    }

    public String getAuto_time() {
        return auto_time;
    }

    public void setAuto_time(String auto_time) {
        this.auto_time = auto_time;
    }

    public String getLogged_by() {
        return logged_by;
    }

    public void setLogged_by(String logged_by) {
        this.logged_by = logged_by;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
