package com.mybos.bmapp.Data.Response;

import com.mybos.bmapp.Data.Model.Cases.BuildingCase;

import java.util.List;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public class BuildingCaseResponse {
    private int maximumCounter;
    private List<BuildingCase> model;
    public BuildingCaseResponse(int maximumCounter,List<BuildingCase> model){
        this.maximumCounter = maximumCounter;
        this.model = model;
    }

    public int getMaximumCounter() {
        return maximumCounter;
    }

    public List<BuildingCase> getModel() {
        return model;
    }
}
