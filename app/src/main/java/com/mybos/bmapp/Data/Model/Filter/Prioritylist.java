package com.mybos.bmapp.Data.Model.Filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class Prioritylist extends BaseModel implements KeepTrackSelectedItemList<Integer>, Parcelable, SpinnerSourceCompatible {

    public List<Priority> priorityList = new ArrayList<>();
    public List<Integer> selectedId = new ArrayList<>();

    private static Prioritylist priorityListApply = new Prioritylist();

    public Prioritylist(){
        initPriority();
    }

    public Prioritylist(Prioritylist ob){
        priorityList = new ArrayList<>(ob.getPriorityList());
        selectedId = new ArrayList<>(ob.getSelectedId());
    }

    public void clearData(){
        priorityList = new ArrayList<>();
        selectedId = new ArrayList<>();
    }

    protected Prioritylist(Parcel in) {
        priorityList = in.createTypedArrayList(Priority.CREATOR);
        in.readList(selectedId,Integer.class.getClassLoader());
    }

    public static final Creator<Prioritylist> CREATOR = new Creator<Prioritylist>() {
        @Override
        public Prioritylist createFromParcel(Parcel in) {
            return new Prioritylist(in);
        }

        @Override
        public Prioritylist[] newArray(int size) {
            return new Prioritylist[size];
        }
    };

    public void addPriority(Priority priority){ priorityList.add(priority);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public void selectId(Integer id) {
        if (selectedId.contains(id)){
            return;
        }else {
            selectedId.add(id);
        }
    }

    @Override
    public void deselectId(Integer id) {
        if (selectedId.contains((Integer)id)){
            selectedId.remove((Integer)id);
        }
    }

    @Override
    public List<BaseModel> getSelectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Priority pri:priorityList){
            if (selectedId.contains(pri.getId())){
                returnValue.add(pri);
            }
        }
        return returnValue;
    }

    public List<Integer> getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(List<Integer> selectedId) {
        this.selectedId = selectedId;
    }

    public void setSelectedId(RealmList<Integer> realmList) {
        this.selectedId = new ArrayList<>();
        for (Integer id:realmList
        ) {
            selectedId.add(id);
        }
    }

    public List<Priority> getPriorityList() {
        return priorityList;
    }

    public void setPriorityList(List<Priority> priorityList) {
        this.priorityList = priorityList;
    }

    @Override
    public List<BaseModel> getUnselectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Priority pri:priorityList){
            if (!selectedId.contains(pri.getId())){
                returnValue.add(pri);
            }
        }
        return returnValue;
    }

    public List<String> getSelectedName(){
        List<String> nameList = new ArrayList<>();
        for (Priority pri:priorityList){
            if(selectedId.contains(pri.getId())) {
                nameList.add(pri.getName());
            }
        }
        return nameList;
    }

    @Override
    public List<String> getSource() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public BaseModel getItem(int position) {
        return null;
    }

    @Override
    public String getTitle(int position) {
        return null;
    }

    public static Prioritylist getPriorityListApply() {
        return priorityListApply;
    }

    public static void setPriorityListApply(Prioritylist priorityListApply) {
        Prioritylist.priorityListApply = priorityListApply;
    }

    private void initPriority(){
        Priority filterCP1 = new Priority();
        filterCP1.setName("Low");
        filterCP1.setId(0);
        priorityList.add(filterCP1);

        Priority filterCP2 = new Priority();
        filterCP2.setName("Medium");
        filterCP2.setId(1);
        priorityList.add(filterCP2);

        Priority filterCP3 = new Priority();
        filterCP3.setName("High");
        filterCP3.setId(2);
        priorityList.add(filterCP3);

        Priority filterCP4 = new Priority();
        filterCP4.setName("Urgent");
        filterCP4.setId(3);
        priorityList.add(filterCP4);
    }

}
