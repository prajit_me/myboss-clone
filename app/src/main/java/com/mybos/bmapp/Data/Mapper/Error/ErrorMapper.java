package com.mybos.bmapp.Data.Mapper.Error;

import android.content.Context;

import com.bumptech.glide.load.HttpException;
import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Error.BaseError;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;
import com.mybos.bmapp.Utils.ErrorUtils;

import java.util.Map;

/**
 * Created by EmLaAi on 23/03/2018.
 *
 * @author EmLaAi
 */

public class ErrorMapper extends BaseMapper {
    public void sendError(Exception e, Context context, SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.error(VolleyUrlBuilder.URLPATH.ERROR)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String code;
                if (e instanceof HttpException){
                    code = Integer.toString(((HttpException) e).getStatusCode());
                }else if (e instanceof BaseError){
                    code = Integer.toString(((BaseError) e).getCode());
                }else{
                    code = "-1";
                }
                String data = new VolleyBodyBuilder().setParameter("code", code)
                        .setParameter("class", ErrorUtils.getCallerClassName(e))
                        .setParameter("method", ErrorUtils.getCallerMethod(e))
                        .setParameter("version", ErrorUtils.getVersion())
                        .setParameter("os", ErrorUtils.getOS())
                        .setParameter("exceptionName", e.getClass().getSimpleName())
                        .build();
                VolleyRequest.shared(mContext).post(url, header, data, (response) -> {
                    onSuccess.onSuccess(null);
                }, onFailure::onFailure);
            }else{
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
