package com.mybos.bmapp.Data.Model.Inspection;

import android.os.Parcel;
import android.os.Parcelable;


import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class InspectionOfflinetoSaveList implements Parcelable, RealmModel {

    public InspectionOfflinetoSaveList(){}

    private RealmList<InspectionOfflinetoSave> listInspectionOfflinetoSave = new RealmList<>();

    protected InspectionOfflinetoSaveList(Parcel in) {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InspectionOfflinetoSaveList> CREATOR = new Creator<InspectionOfflinetoSaveList>() {
        @Override
        public InspectionOfflinetoSaveList createFromParcel(Parcel in) {
            return new InspectionOfflinetoSaveList(in);
        }

        @Override
        public InspectionOfflinetoSaveList[] newArray(int size) {
            return new InspectionOfflinetoSaveList[size];
        }
    };

    public RealmList<InspectionOfflinetoSave> getListInspectionOfflinetoSave() {
        return listInspectionOfflinetoSave;
    }

    public void setListInspectionOfflinetoSave(RealmList<InspectionOfflinetoSave> listInspectionOfflinetoSave) {
        this.listInspectionOfflinetoSave = listInspectionOfflinetoSave;
    }

    public void addToList (InspectionOfflinetoSave item) {
        this.listInspectionOfflinetoSave.add(item);
    }

    public static void save(InspectionOfflinetoSaveList InspectionList){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction(mRealm->{
                mRealm.delete(InspectionItemOffline.class);
                mRealm.delete(InspectionOfflinetoSave.class);
                mRealm.delete(InspectionOfflinetoSaveList.class);
                mRealm.insertOrUpdate(InspectionList);
            });
        }
    }

    public static void clear(){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction(mRealm->{
                mRealm.delete(InspectionItemOffline.class);
                mRealm.delete(InspectionOfflinetoSave.class);
                mRealm.delete(InspectionOfflinetoSaveList.class);
            });
        }
    }

    public static InspectionOfflinetoSaveList get(){
        try (Realm realm = Realm.getDefaultInstance()){
            InspectionOfflinetoSaveList inspectionList = realm.where(InspectionOfflinetoSaveList.class).findFirst();
            return null != inspectionList ? realm.copyFromRealm(inspectionList) : null;
        }
    }
}
