package com.mybos.bmapp.Data.Model.Attachment;


import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;

import java.text.NumberFormat;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class AttachmentUploadQuote extends BaseModel implements AttachmentInterface {
    private Double amount;
    private String type;
    private String date;
    private Contractor contractor;
    private AttachmentFile file;

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public void setFile(AttachmentFile file) {
        this.file = file;
    }

    @Override
    public String getAttachmentTitle() {
        return contractor != null ? contractor.getCompany() : "";
    }

    @Override
    public String getSubTitle() {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        numberFormat.setMaximumFractionDigits(0);
        return numberFormat.format(amount);
    }

    @Override
    public String getLink() {
        return file != null ? file.getLink() : "";
    }

    @Override
    public String getFileType() {
        return this.file != null ? this.file.getFileType() : null;
    }

    public Double getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public String getDate() {
        return date;
    }

    public AttachmentFile getFile() {
        return file;
    }

    public Contractor getContractor() {
        return contractor;
    }
}
