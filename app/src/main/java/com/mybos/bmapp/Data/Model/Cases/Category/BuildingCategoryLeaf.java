package com.mybos.bmapp.Data.Model.Cases.Category;

import com.mybos.bmapp.Component.SpinnerAdapter.TreeLikeSpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

import java.util.List;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCategoryLeaf extends BaseModel implements BuildingCategory,TreeLikeSpinnerItemInterface {
    private static final boolean isParent = false;
    private String name;
    private int id;
    private String description;
    private int order;

    public BuildingCategoryLeaf(String name,int id,int order,String description){
        this.name = name;
        this.id = id;
        this.order = order;
        this.description = description;
    }

    @Override
    public boolean isParent() {
        return isParent;
    }

    @Override
    public boolean isBack() {
        return id == BuildingCategory.BACK_ID;
    }

    @Override
    public String getTittle() {
        return this.name;
    }

    @Override
    public List<TreeLikeSpinnerItemInterface> getChildren() {
        return null;
    }

    @Override
    public TreeLikeSpinnerItemInterface findChildParent(int id) {
        return null;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return this.id;
    }

    @Override
    public BuildingCategory getChild(int id) {
        if (id == this.id){
            return this;
        }
        return null;
    }
}
