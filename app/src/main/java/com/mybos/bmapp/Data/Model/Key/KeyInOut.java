package com.mybos.bmapp.Data.Model.Key;

public class KeyInOut {

    private String Added = "";
    private String Detail = "";
    private String Expire = "";
    private String FileName = "";
    private String FileType = "";
    private String ID = "";
    private String Name = "";
    private String Size = "";
    private String Sized = "";
    private String Subject = "";
    private String Type = "";
    private String URL = "";

    private static  String keyIdSelected = "";

    public static String getKeyIdSelected() {
        return keyIdSelected;
    }

    public static void setKeyIdSelected(String keyIdSelected) {
        KeyInOut.keyIdSelected = keyIdSelected;
    }

    public String getAdded() {
        return Added;
    }

    public void setAdded(String added) {
        Added = added;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getExpire() {
        return Expire;
    }

    public void setExpire(String expire) {
        Expire = expire;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getSized() {
        return Sized;
    }

    public void setSized(String sized) {
        Sized = sized;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
