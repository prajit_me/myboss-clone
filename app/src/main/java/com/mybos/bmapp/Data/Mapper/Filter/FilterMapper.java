package com.mybos.bmapp.Data.Mapper.Filter;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Filter.Filter;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Map;

public class FilterMapper extends BaseMapper{
    public void getCategoryTree(Context context, int buildingId, SuccessCallback<String> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CATEGORY_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray){
                            Filter.setAssetsFilterList(convertJsonArraytoArraylist((JSONArray) object));
                            onSuccess.onSuccess("get filter for assets ok");
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException |InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getContractorFilterList(Context context, int buildingId, SuccessCallback<String> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CONTRACTOR_CATEGORY_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray){
                            ArrayList<Filter> temp = new ArrayList<>();
                            for (int i = 0; i < ((JSONArray)object).length();i++){
                                JSONObject itemObject = ((JSONArray) object).getJSONObject(i);
                                Filter filter = new Filter();
                                filter.setId(itemObject.optString("id"));
                                filter.setName(itemObject.optString("name"));
                                temp.add(filter);
                            }
                            Filter.setContractorsFilterList(temp);
                            onSuccess.onSuccess("get filter for assets ok");
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException |InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public ArrayList<Filter> convertJsonArraytoArraylist(JSONArray jsonArray){
        ArrayList<Filter> resultList = new ArrayList<>();
        for (int i = 0; i< jsonArray.length(); i++){
            try {
                JSONObject object = jsonArray.getJSONObject(i);
                Filter filter = new Filter();
                filter.setName(object.optString("name"));
                filter.setId(object.optString("id"));
                JSONArray childrenArray =  object.getJSONArray("children");
                if (childrenArray.length()>0){
                    filter.setChildren(convertJsonArraytoArraylist(childrenArray));
                }
                resultList.add(filter);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return resultList;
    }

}
