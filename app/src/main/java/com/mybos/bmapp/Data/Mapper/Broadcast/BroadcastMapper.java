package com.mybos.bmapp.Data.Mapper.Broadcast;

import android.content.Context;

import com.mybos.bmapp.Activity.Broadcast.BroadcastController;
import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Broadcast.Group;
import com.mybos.bmapp.Data.Model.Broadcast.Groups;
import com.mybos.bmapp.Data.Model.Broadcast.User;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

public class BroadcastMapper extends BaseMapper {

    public void getGroups(Context context, int buildingId, BroadcastController.SuccessCallback<Groups> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, mContext -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_GROUPS),
                        Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url, header, response -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray) {
                            for (int i = 0; i < ((JSONArray) object).length(); i++) {
                                JSONObject inspectionJSON = ((JSONArray) object).getJSONObject(i);
                                Group group = new Group();
                                group.setName(inspectionJSON.getString("name"));
                                group.setId(inspectionJSON.getInt("id"));
                                group.setBuilding(inspectionJSON.getInt("building"));
                                Groups.getGroups().add(group);
                            }
                            onSuccess.onSuccess(new Groups());
                        } else {
                            throw new InvalidResponse();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (InvalidResponse invalidResponse) {
                        invalidResponse.printStackTrace();
                    }

                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getUsers(Context context, int buildingId, int groupId, BroadcastController.SuccessCallback<User> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, mContext -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_USERS),
                        Integer.toString(buildingId), Integer.toString(groupId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url, header, response -> {
                    try {
                        User user = new User();
                        User.setUsers(new ArrayList<User>());
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray) {
                            for (int i = 0; i < ((JSONArray) object).length(); i++) {
                                JSONObject inspectionJSON = ((JSONArray) object).getJSONObject(i);
                                User tmpUser = new User();
                                tmpUser.setId(inspectionJSON.getInt("id"));
                                tmpUser.setUnit(inspectionJSON.getString("unit"));
                                tmpUser.setName(inspectionJSON.getString("name"));
                                tmpUser.setNumber(inspectionJSON.getString("number"));
                                tmpUser.setEmail(inspectionJSON.getString("email"));
                                tmpUser.setType(inspectionJSON.getInt("type"));
                                User.getUsers().add(tmpUser);
                                user = tmpUser;
                            }
                            onSuccess.onSuccess(user);
                        } else {
                            throw new InvalidResponse();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (InvalidResponse invalidResponse) {
                        invalidResponse.printStackTrace();
                    }

                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });

    }

    public void getCredit(Context context, int buildingId, BroadcastController.SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, mContext -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_CREDIT),
                        Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url, header, response -> {
                    try {
                        User user = new User();
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            Broadcast.setCredit(((JSONObject) object).getInt("credit"));
                            onSuccess.onSuccess(new Broadcast());
                        } else {
                            throw new InvalidResponse();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (InvalidResponse invalidResponse) {
                        invalidResponse.printStackTrace();
                    }

                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });

    }


    public void sendContentEmail(Context context, int buildingId, String subject,
                                 String content, SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, mContext -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_EMAIL_CONTENT),
                                Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().build();
                VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                builder.addString("subject", subject);
                builder.addString("message", content);
                VolleyRequest.shared(mContext).postForm(url, header, builder, response -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            Broadcast.setId_mail_content(((JSONObject) object).getInt("id"));
                            onSuccess.onSuccess(new Broadcast());
                        } else {
                            throw new InternalError();
                        }
                    } catch (JSONException | InternalError e) {
                        onFailure.onFailure(e);
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void sendMail(Context context, int buildingId,SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_SEND_MAIL),
                        Integer.toString(buildingId),Integer.toString(Broadcast.getId_mail_content())).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, Broadcast.buildStringFromUsersList(Broadcast.getChoose_users()), (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            Broadcast.setStat_send(((JSONObject) object).getInt("state"));
                        }
                        onSuccess.onSuccess(new Broadcast());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void sendSMS(Context context, int buildingId, String composeSMS,SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_SEND_SMS),
                        Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header,Broadcast.buildDataToSendSMS(Broadcast.getChoose_users(), composeSMS), (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            Broadcast.setStat_send(((JSONObject) object).getInt("state"));
                        }
                        onSuccess.onSuccess(new Broadcast());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void purchaseSMS(Context context, int buildingId, String data,SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.broadcast(VolleyUrlBuilder.URLPATH.BROADCAST_PURCHASE_SMS),
                        Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, data, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            Broadcast.setStat_send(((JSONObject) object).getInt("state"));
                        }
                        onSuccess.onSuccess(new Broadcast());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
