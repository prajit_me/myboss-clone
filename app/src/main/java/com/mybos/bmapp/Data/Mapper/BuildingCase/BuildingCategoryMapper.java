package com.mybos.bmapp.Data.Mapper.BuildingCase;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Cases.Category.BuildingCategory;
import com.mybos.bmapp.Data.Model.Cases.Category.BuildingCategoryLeaf;
import com.mybos.bmapp.Data.Model.Cases.Category.BuildingCategoryParent;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCategoryMapper extends BaseMapper {
    public void getCategoryTree(Context context, int buildingId, SuccessCallback<List<BuildingCategory>> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CATEGORY_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray){
                            List<BuildingCategory> categories = this.buildTreeLikeCategory(mContext,(JSONArray) object);
                            BuildingCategory all = new BuildingCategoryLeaf(mContext.getString(R.string.new_case_category_all_leaf_name),BuildingCategory.ALL_ID,0,null);
                            List<BuildingCategory> returnValue = new ArrayList<>();
                            returnValue.add(all);
                            returnValue.addAll(categories);
                            onSuccess.onSuccess(returnValue);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public List<BuildingCategory> buildTreeLikeCategory(Context context,JSONArray categoryArrayJSON) throws JSONException{
        List<BuildingCategory> returnValue = new ArrayList<>();
        for (int i = 0; i < categoryArrayJSON.length(); i++) {
            JSONObject categoryJSON = categoryArrayJSON.optJSONObject(i);
            String name = categoryJSON.optString("name");
            int id = categoryJSON.optInt("id");
            int order = categoryJSON.optInt("order");
            String description = categoryJSON.has("description") && !categoryJSON.isNull("description")? categoryJSON.optString("description"):null;
            JSONArray childJSON = categoryJSON.getJSONArray("children");
            if (childJSON.length() > 0 ){
                List<BuildingCategory> subTree = buildTreeLikeCategory(context,childJSON);
                BuildingCategoryParent parent = new BuildingCategoryParent(name,id,description,order);
                BuildingCategory back = new BuildingCategoryLeaf(context.getString(R.string.new_case_category_back_leaf_name),BuildingCategory.BACK_ID,0,null);
                parent.addChild(back);
                parent.addChild(subTree);
                returnValue.add(parent);
            }else {
                BuildingCategory leaf = new BuildingCategoryLeaf(name,id,order,description);
                returnValue.add(leaf);
            }
        }
        return returnValue;
    }
}
