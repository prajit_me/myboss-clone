package com.mybos.bmapp.Data.Model.Calendar;

import java.util.ArrayList;

public class DaysEvent {

    private String date = "";

    private ArrayList<Reminder> reminder = new ArrayList<>();
    private ArrayList<Booking> booking = new ArrayList<>();
    private ArrayList<Maintenance> maintanence = new ArrayList<>();

    private static  ArrayList<DaysEvent> daysEvents = new ArrayList<>();

    public static DaysEvent getDayEFromDateOfDayEs(String date){
        DaysEvent result;
        for (DaysEvent daysEvent: daysEvents
             ) {
            if (date.equals(daysEvent.getDate())){
                result = daysEvent;
                return result;
            }
        }
        return null;
    }


    public static ArrayList<DaysEvent> getDaysEvents() {
        return daysEvents;
    }

    public static void setDaysEvents(ArrayList<DaysEvent> daysEvents) {
        DaysEvent.daysEvents = daysEvents;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<Reminder> getReminder() {
        return reminder;
    }

    public void setReminder(ArrayList<Reminder> reminder) {
        this.reminder = reminder;
    }

    public ArrayList<Booking> getBooking() {
        return booking;
    }

    public void setBooking(ArrayList<Booking> booking) {
        this.booking = booking;
    }

    public ArrayList<Maintenance> getMaintanence() {
        return maintanence;
    }

    public void setMaintanence(ArrayList<Maintenance> maintanence) {
        this.maintanence = maintanence;
    }
}
