package com.mybos.bmapp.Data.Model.Inspection;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Apartment.ApartmentList;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by EmLaAi on 17/04/2018.
 *
 * @author EmLaAi
 */
public class Inspection extends BaseModel {
    int id;
    int building;
    String name;
    int type;
    String added;
    String typeText;
    List<InspectionArea> areas;
    String historyIdForDraft;

    private static ArrayList<Inspection> offlineInspection ;

    private static ApartmentList offlineApartmentList;

    public Inspection(){
        areas = new ArrayList<>();
    }

    public static ArrayList<Inspection> getOfflineInspection() {
        return offlineInspection;
    }

    public static void setOfflineInspection(ArrayList<Inspection> offlineInspection) {
        Inspection.offlineInspection = offlineInspection;
    }

    public static ApartmentList getOfflineApartmentList() {
        return offlineApartmentList;
    }

    public static void setOfflineApartmentList(ApartmentList offlineApartmentList) {
        Inspection.offlineApartmentList = offlineApartmentList;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public String getName() {
        return name;
    }

    public String getAddedDateString() {
        Date added = DateUtils.parseDate(this.added,"yyyy-MM-dd'T'HH:mm:ssZZZZZ");
        return DateUtils.getDateString(added);
    }

    public int getId() {
        return id;
    }
    public void addArea(InspectionArea area){
        areas.add(area);
    }

    public List<InspectionArea> getAreas() {
        return areas;
    }

    public Integer getMaxId(){
        Integer maxId = null;
        for (InspectionArea area:areas){
            List<InspectionItem> items = area.getItems();
            for (InspectionItem item:items){
                if (null == maxId){
                    maxId = item.getId();
                }else {
                    if (maxId < item.getId()){
                        maxId = item.getId();
                    }
                }
            }
        }
        return maxId;
    }

    public static Inspection getInspectionFromInspectionId(int id){
        Inspection result = new Inspection();
        for (Inspection ins:offlineInspection
             ) {
            if (ins.getId()==id){
                result = ins;
            }
        }
        return result;
    }

    public  void setDefaultInspectionItem(){
        for (InspectionArea IA: areas
             ) {
                IA.ClearData();
        }
    }

    public String getHistoryIdForDraft() {
        return historyIdForDraft;
    }

    public void setHistoryIdForDraft(String historyIdForDraft) {
        this.historyIdForDraft = historyIdForDraft;
    }
}
