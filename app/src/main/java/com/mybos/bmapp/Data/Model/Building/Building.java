package com.mybos.bmapp.Data.Model.Building;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.net.MalformedURLException;
import java.net.URL;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by EmLaAi on 15/01/2018.
 *
 * @author EmLaAi
 */

@RealmClass
public class Building implements Parcelable,RealmModel {
    private String state = "";
    private String country = "";
    private String name = "";
    private String photoName = "";
    private String postCode = "";
    private String address = "";
    private String uid = "";
    private int prStatus = 0;
    private Integer login = 0;
    private int pcStatus = 0;
    private int status = 0;
    private String wCode="";
    @PrimaryKey
    private int id = 0;
    private String timezone = "";
    private String photo = "";
    private int owner = 0;
    private String logo = "";
    private String suburd = "";
    private String plan = "";

    public Building(){}

    protected Building(Parcel in) {
        state = in.readString();
        country = in.readString();
        name = in.readString();
        photoName = in.readString();
        postCode = in.readString();
        address = in.readString();
        uid = in.readString();
        prStatus = in.readInt();
        if (in.readByte() == 0) {
            login = null;
        } else {
            login = in.readInt();
        }
        pcStatus = in.readInt();
        status = in.readInt();
        id = in.readInt();
        timezone = in.readString();
        photo = in.readString();
        owner = in.readInt();
        logo = in.readString();
        suburd = in.readString();
        plan = in.readString();
    }

    public static final Creator<Building> CREATOR = new Creator<Building>() {
        @Override
        public Building createFromParcel(Parcel in) {
            return new Building(in);
        }

        @Override
        public Building[] newArray(int size) {
            return new Building[size];
        }
    };

    public void setState(String state) {
        this.state = state;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setPrStatus(int prStatus) {
        this.prStatus = prStatus;
    }

    public void setLogin(Integer login) {
        this.login = login;
    }

    public void setPcStatus(int pcStatus) {
        this.pcStatus = pcStatus;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    public void setOwner(int owner) {
        this.owner = owner;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public void setSuburd(String suburd) {
        this.suburd = suburd;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoto() {
        return photo;
    }

    public String getState() {
        return state;
    }

    public String getSuburd() {
        return suburd;
    }

    public String getCountry() {return country;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(state);
        dest.writeString(country);
        dest.writeString(name);
        dest.writeString(photoName);
        dest.writeString(postCode);
        dest.writeString(address);
        dest.writeString(uid);
        dest.writeInt(prStatus);
        if (login == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(login);
        }
        dest.writeInt(pcStatus);
        dest.writeInt(status);
        dest.writeInt(id);
        dest.writeString(timezone);
        dest.writeString(photo);
        dest.writeInt(owner);
        dest.writeString(logo);
        dest.writeString(suburd);
        dest.writeString(plan);
    }

    public String getwCode() {
        return wCode;
    }

    public void setwCode(String wCode) {
        this.wCode = wCode;
    }
}
