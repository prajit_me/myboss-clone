package com.mybos.bmapp.Data.Model.Broadcast;

import android.util.Log;

import java.util.ArrayList;

public class Broadcast {

    private static int credit;
    private static int id_mail_content;
    private static int stat_send;
    private static ArrayList<User> choose_users = new ArrayList<>();

    public static ArrayList<User> getChoose_users() {
        return choose_users;
    }

    public static void setChoose_users(ArrayList<User> choose_users) {
        Broadcast.choose_users = choose_users;
    }

    public static int getId_mail_content() {
        return id_mail_content;
    }

    public static void setId_mail_content(int id_mail_content) {
        Broadcast.id_mail_content = id_mail_content;
    }

    public static int getCredit() {
        return credit;
    }

    public static void setCredit(int credit) {
        Broadcast.credit = credit;
    }

    public static String buildStringFromUsersList(ArrayList<User> users){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\"members\":[");
        for (int i = 0; i < users.size(); i ++){
            User user = users.get(i);
            if (i == 0){
                stringBuffer.append("{\"id\":"+user.getId()+","+
                        "\"unit\":\""+user.getUnit()+"\","+
                        "\"name\":\""+user.getName()+"\","+
                        "\"number\":\""+user.getNumber()+"\","+
                        "\"email\":\""+user.getEmail()+"\","+
                        "\"type\":"+user.getType()+","+
                        "\"selected\":"+true+"}");
            } else {
                stringBuffer.append(",{\"id\":"+user.getId()+","+
                        "\"unit\":\""+user.getUnit()+"\","+
                        "\"name\":\""+user.getName()+"\","+
                        "\"number\":\""+user.getNumber()+"\","+
                        "\"email\":\""+user.getEmail()+"\","+
                        "\"type\":"+user.getType()+","+
                        "\"selected\":"+true+"}");
            }
        }
        stringBuffer.append("]}");
        return stringBuffer.toString();
    }

    public static String buildDataToSendSMS(ArrayList<User> users, String composeSMS){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\"type\":\"sms\",\"message\":\""+composeSMS+"\",\"members\":[");
        for (int i = 0; i < users.size(); i ++){
            User user = users.get(i);
            if (i == 0){
                stringBuffer.append("{\"id\":"+user.getId()+","+
                        "\"unit\":\""+user.getUnit()+"\","+
                        "\"name\":\""+user.getName()+"\","+
                        "\"number\":\""+user.getNumber()+"\","+
                        "\"email\":\""+user.getEmail()+"\","+
                        "\"type\":"+user.getType()+","+
                        "\"selected\":"+true+"}");
            } else {
                stringBuffer.append(",{\"id\":"+user.getId()+","+
                        "\"unit\":\""+user.getUnit()+"\","+
                        "\"name\":\""+user.getName()+"\","+
                        "\"number\":\""+user.getNumber()+"\","+
                        "\"email\":\""+user.getEmail()+"\","+
                        "\"type\":"+user.getType()+","+
                        "\"selected\":"+true+"}");
            }
        }
        stringBuffer.append("]}");
        return stringBuffer.toString();
    }

    public static int getStat_send() {
        return stat_send;
    }

    public static void setStat_send(int stat_send) {
        Broadcast.stat_send = stat_send;
    }
}
