package com.mybos.bmapp.Data.Mapper.Keys;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.Data.Model.Key.BuildingKey;
import com.mybos.bmapp.Data.Model.Key.ContractorKey;
import com.mybos.bmapp.Data.Model.Key.History;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Map;

public class KeyMapper extends BaseMapper {

    public void getPrivateKeyList(Context context, int buildingId, SuccessCallback<ArrayList<Apartment>> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_KEY), Integer.toString(buildingId))
                            .build();
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject) {
                                JSONArray BuildingKeyJsonList = ((JSONObject) object).optJSONArray("apartments");
                                ArrayList<Apartment> apartments = new ArrayList<>();
                                int Size = BuildingKeyJsonList.length();
                                for (int i = 0; i < Size; i++) {
                                    JSONObject CurrentKeyObject = BuildingKeyJsonList.getJSONObject(i);
                                    Apartment apartment = new Apartment();
                                    apartment.setApartment_Id(CurrentKeyObject.getString("id"));
                                    apartment.setApartment_state(CurrentKeyObject.getInt("status"));

                                    apartments.add(apartment);
                                }
                                Apartment.setApartments(apartments);
                                onSuccess.onSuccess(apartments);
                            } else {
                                throw new InvalidResponse();
                            }
                        } catch (JSONException | InvalidResponse e) {
                            onFailure.onFailure(e);
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }

    public void getContractorKeyList(Context context, int buildingId, SuccessCallback<ArrayList<BuildingKey>> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_KEY), Integer.toString(buildingId))
                            .build();
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject) {
                                JSONArray BuildingKeyJsonList = ((JSONObject) object).optJSONArray("keys");
                                ArrayList<BuildingKey> BuildingPrivateKeyList = new ArrayList<>();
                                int Size = BuildingKeyJsonList.length();
                                for (int i = 0; i < Size; i++) {
                                    JSONObject CurrentKeyObject = BuildingKeyJsonList.getJSONObject(i);
                                    BuildingKey CurrentBuildingKey = new BuildingKey();
                                    CurrentBuildingKey.setKeyName(CurrentKeyObject.optString("key"));
                                    CurrentBuildingKey.setKeyStatus(CurrentKeyObject.optString("status"));
                                    CurrentBuildingKey.setStatusText(CurrentKeyObject.optString("statusText"));
                                    CurrentBuildingKey.setKeyID(CurrentKeyObject.optString("id"));
                                    CurrentBuildingKey.setApartment(CurrentKeyObject.optString("apartment"));
                                    CurrentBuildingKey.setBuildingID(CurrentKeyObject.optString("building"));
                                    CurrentBuildingKey.setCardNo(CurrentKeyObject.optString("card_no"));
                                    CurrentBuildingKey.setComment(CurrentKeyObject.optString("comment"));
                                    CurrentBuildingKey.setApartment(CurrentKeyObject.optString("apartment"));

                                    BuildingPrivateKeyList.add(CurrentBuildingKey);
                                }
                                BuildingKey.setBuildingKeys(BuildingPrivateKeyList);
                                onSuccess.onSuccess(BuildingPrivateKeyList);
                            } else {
                                throw new InvalidResponse();
                            }
                        } catch (JSONException | InvalidResponse e) {
                            onFailure.onFailure(e);
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }

    public void getKeyApartment(Context context, int buildingId, String param, SuccessCallback<Apartment> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.apartment(VolleyUrlBuilder.URLPATH.APARTMENT_KEY),
                        Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {

                        ArrayList<Apartment> apartments = new ArrayList<>();
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray) {
                            for (int i = 0; i < ((JSONArray) object).length(); i++) {
                                JSONObject inspectionJSON = ((JSONArray) object).getJSONObject(i);
                                Apartment apartment = new Apartment();
                                apartment.setKey(inspectionJSON.optString("key"));
                                apartment.setKeyId(inspectionJSON.optString("id"));
                                apartment.setKey_status(inspectionJSON.optString("status"));
                                apartment.setTypeText(inspectionJSON.optString("typeText"));
                                apartment.setLocationText(inspectionJSON.optString("locationText"));
                                apartment.setLocation(inspectionJSON.optString("location"));
                                apartment.setOwner(inspectionJSON.optString("owner"));
                                apartments.add(apartment);
                            }
                            Apartment.setApartment_keys(new ArrayList<>(apartments));
                        }
                        onSuccess.onSuccess(new Apartment());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getKeyHistory(Context context, int buildingId, SuccessCallback<History> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.apartment(VolleyUrlBuilder.URLPATH.APARTMENT_KEY_HISTORY),
                        Integer.toString(buildingId), History.getKeyIdSelected()).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, History.paramHistory, (response) -> {
                    try {

                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            JSONArray jsonData = ((JSONObject) object).getJSONArray("data");
                            ArrayList<History> histories = new ArrayList<>();
                            for (int i = 0; i < jsonData.length(); i++) {
                                JSONObject inspectionJSON = ((JSONArray) jsonData).getJSONObject(i);
                                History history = new History();

                                history.setCompany(inspectionJSON.optString("company"));
                                history.setContact(inspectionJSON.optString("contact"));
                                history.setNumber(inspectionJSON.optString("number"));
                                history.setReason(inspectionJSON.optString("reason"));
                                history.setAdded(inspectionJSON.optString("added"));
                                history.setFinish(inspectionJSON.optString("finish"));
                                history.setDue(inspectionJSON.optString("due"));
                                history.setExpiry(inspectionJSON.optString("expiry"));
                                //get url image
                                if(!inspectionJSON.isNull("sign_out")){
                                    JSONObject sigout = inspectionJSON.getJSONObject("sign_out");
                                    history.getSignOut().setURL(sigout.optString("url"));
                                }
                                histories.add(history);
                            }
                            History.setHistories(new ArrayList<>(histories));
                        }
                        onSuccess.onSuccess(new History());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getContractorSignIn(Context context, int buildingId, SuccessCallback<ContractorKey> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.apartment(VolleyUrlBuilder.URLPATH.APARTMENT_KEY_CONTRACTOR),
                                    Integer.toString(buildingId)).build();
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject) {
                                JSONArray BuildingKeyJsonList = ((JSONObject) object).optJSONArray("data");
                                ArrayList<ContractorKey> contractorKeys = new ArrayList<>();
                                int Size = BuildingKeyJsonList.length();
                                for (int i = 0; i < Size; i++) {
                                    JSONObject CurrentKeyObject = BuildingKeyJsonList.getJSONObject(i);
                                    ContractorKey contractorKey = new ContractorKey();
                                    contractorKey.setCompanyName(CurrentKeyObject.optString("company"));
                                    JSONObject mainContact = CurrentKeyObject.getJSONObject("mainContact");
                                    contractorKey.setMc_name(mainContact.optString("name"));
                                    contractorKey.setMc_number(mainContact.optString("number"));
                                    contractorKeys.add(contractorKey);
                                }
                                ContractorKey.setContractors(new ArrayList<>(contractorKeys));
                                onSuccess.onSuccess(new ContractorKey());
                            } else {
                                throw new InvalidResponse();
                            }
                        } catch (JSONException | InvalidResponse e) {
                            onFailure.onFailure(e);
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }

    public void postSignOut(Context context, int buildingId,String keyId , String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.apartment(VolleyUrlBuilder.URLPATH.APARTMENT_KEY_SIGNOUT),
                        Integer.toString(buildingId), keyId).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void postSignIn(Context context, int buildingId,String keyId , String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.apartment(VolleyUrlBuilder.URLPATH.APARTMENT_KEY_SIGNIN),
                        Integer.toString(buildingId), keyId).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

}
