package com.mybos.bmapp.Data.Model.Broadcast;

public class Group {
    private int id;
    private String name;
    private int building;

    public Group(int id, String name, int building) {
        this.id = id;
        this.name = name;
        this.building = building;
    }

    public Group(){}

    public int getBuilding() {
        return building;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
