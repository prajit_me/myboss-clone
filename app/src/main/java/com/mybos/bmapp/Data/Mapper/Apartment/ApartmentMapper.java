package com.mybos.bmapp.Data.Mapper.Apartment;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Apartment.Apartment;
import com.mybos.bmapp.Data.Model.Apartment.ApartmentList;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class ApartmentMapper extends BaseMapper {
    public void get(Context context,int buildingId, SuccessCallback<ApartmentList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.APARTMENT_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                Log.e("check_url_get_apartment",url);
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                JSONArray apartmentArrayJSON = ((JSONObject) object).optJSONArray("data");
                                ApartmentList apartmentList = new ApartmentList();
                                for (int i = 0; i < apartmentArrayJSON.length(); i++) {
                                    JSONObject apartmentJSON = apartmentArrayJSON.optJSONObject(i);
                                    Apartment apartment = new Apartment();
                                    apartment.setId(apartmentJSON.optString("id"));
                                    apartment.setUid(apartmentJSON.optString("uid"));
                                    apartment.setStatus(apartmentJSON.optString("status"));
                                    apartment.setBuilding(apartmentJSON.optInt("building"));
                                    apartment.setIsHotel(apartmentJSON.optInt("is_hotel"));
                                    apartment.setLot(apartmentJSON.has("lot") && !apartmentJSON.isNull("lot") ? apartmentJSON.optString("lot"):null);
                                    apartmentList.addAparment(apartment);
                                }
                                onSuccess.onSuccess(apartmentList);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void get(Context context,int buildingId,int uid){}
}
