package com.mybos.bmapp.Data.Model.Cases.Category;

import com.mybos.bmapp.Component.SpinnerAdapter.TreeLikeSpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCategoryParent extends BaseModel implements BuildingCategory,TreeLikeSpinnerItemInterface {
    private static final boolean isParent = true;
    private String name;
    private int id;
    private String description;
    private int order;
    List<BuildingCategory> children;

    public BuildingCategoryParent(String name , int id ,String description , int order){
        this.name = name;
        this.id = id;
        this.description = description;
        this.order = order;
        this.children = new ArrayList<>();
    }

    @Override
    public boolean isParent() {
        return isParent;
    }

    @Override
    public boolean isBack() {
        return false;
    }

    @Override
    public String getTittle() {
        return this.name;
    }

    @Override
    public List<TreeLikeSpinnerItemInterface> getChildren() {
        List<TreeLikeSpinnerItemInterface> childList = new ArrayList<>();
        for (BuildingCategory child:children){
            childList.add((TreeLikeSpinnerItemInterface) child);
        }
        return childList;
    }

    @Override
    public TreeLikeSpinnerItemInterface findChildParent(int id) {
        boolean isChildMatch = false;
        for (BuildingCategory category:children){
            if (category.getId() == id){
                isChildMatch = true;
            }
        }
        if (isChildMatch) return this;
        TreeLikeSpinnerItemInterface match = null;
        for (BuildingCategory category:children){
            if (category.isParent()){
                match = ((BuildingCategoryParent) category).findChildParent(id);
                if (null != match){
                    break;
                }
            }
        }
        return match;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int getId() {
        return this.id;
    }

    public void addChild(BuildingCategory buildingCategory){
        this.children.add(buildingCategory);
    }
    public void addChild(List<BuildingCategory> buildingCategories){
        this.children.addAll(buildingCategories);
    }

    @Override
    public BuildingCategory getChild(int id) {
        return null;
    }
}
