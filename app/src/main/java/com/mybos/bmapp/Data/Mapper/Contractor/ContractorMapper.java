package com.mybos.bmapp.Data.Mapper.Contractor;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class ContractorMapper extends BaseMapper {

    public void get(Context context,int buildingId,String selectedCategory,String keySearch,int page,SuccessCallback<ContractorList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CASE_CONTRACTOR),Integer.toString(buildingId))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyBodyBuilder urlData = new VolleyBodyBuilder();
                    urlData.setParameter("f", selectedCategory).
                            setParameter("k", keySearch).
                            setParameter("p", page).
                            setParameter("s", 50);
                    Log.e("check_get_contractor",url+"--page:"+page);
                    VolleyRequest.shared(mContext).post(url, header, urlData.build(), (response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONArray contractorJsonList = ((JSONObject) object).optJSONArray("data");
                                    ContractorList list = new ContractorList();
                                    for (int i = 0; i < contractorJsonList.length(); i++) {
                                        Contractor contractor = new Contractor();
                                        JSONObject contractorJson = (JSONObject) contractorJsonList.get(i);
                                        contractor.setCompany(contractorJson.optString("company"));
                                        contractor.setAddress(contractorJson.optString("address"));
                                        contractor.setPreferred(contractorJson.optInt("preferred"));
                                        contractor.setPhone(contractorJson.optString("phone"));
                                        contractor.setMobile(contractorJson.optString("mobile"));
                                        contractor.setFax((contractorJson.has("fax") && !contractorJson.isNull("fax"))?contractorJson.optString("fax"):null);
                                        contractor.setEmail(contractorJson.optString("email"));
                                        contractor.setFacebook((contractorJson.has("facebook") && !contractorJson.isNull("facebook"))?contractorJson.optString("facebook"):null);
                                        contractor.setWebsite((contractorJson.has("website") && !contractorJson.isNull("website"))?contractorJson.optString("website"):null);
                                        contractor.setGoogle((contractorJson.has("google") && !contractorJson.isNull("google"))?contractorJson.optString("google"):null);
                                        contractor.setIndustry(contractorJson.optInt("industry"));
                                        contractor.setId(contractorJson.optInt("id"));
                                        contractor.setInfo((contractorJson.has("info") && !contractorJson.isNull("info"))?contractorJson.optString("info"):null);
                                        contractor.setCategory(contractorJson.optInt("category"));
                                        contractor.setCategoryName(contractorJson.optString("categoryName"));

                                        JSONObject mainContactJson = contractorJson.optJSONObject("mainContact");
                                        if (mainContactJson != null) {
                                            Contractor.MainContact contact = new Contractor.MainContact();

                                            contact.setMain(mainContactJson.optInt("main"));
                                            contact.setNumber(mainContactJson.optString("number"));
                                            contact.setName(mainContactJson.optString("name"));
                                            contact.setEmail(mainContactJson.optString("email"));
                                            contact.setId(mainContactJson.optInt("id"));
                                            contact.setContractor(mainContactJson.optInt("contractor"));

                                            contractor.setMainContact(contact);
                                        }
                                        list.addContrator(contractor);
                                        ContractorInfoCollector.addToContractorList(contractor);
                                    }
                                    Logger.shared().o(list);
                                    onSuccess.onSuccess(list);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException|InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void getInsurances(Context context,int buildingId, int contractorId, SuccessCallback<ArrayList<Contractor>> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex()).clearInsurances();
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CONTRACTOR_DETAIL),Integer.toString(buildingId), String.valueOf(contractorId))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url,header,(response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONObject contractorJson = ((JSONObject) object).optJSONObject("data");
                                    ArrayList<Contractor> list = new ArrayList<>();
                                    Contractor contractor = new Contractor();

                                    JSONArray insurancesJson = contractorJson.optJSONArray("insurances");
                                    for (int j = 0; j < insurancesJson.length(); j++) {
                                        JSONObject insuranceObject = (JSONObject) insurancesJson.get(j);
                                        if (insuranceObject != null) {
                                            Contractor.ContractorInsurance Insurance = new Contractor.ContractorInsurance();

                                            Insurance.setAdded(insuranceObject.optString("added"));
                                            Insurance.setContractor(insuranceObject.optInt("id"));
                                            Insurance.setName(insuranceObject.optString("name"));
                                            Insurance.setExpire(insuranceObject.optString("expire"));

                                            JSONObject AttachmentJson = insuranceObject.optJSONObject("attachment");
                                            if (AttachmentJson != null) {
                                                Contractor.FileAttachment fileAttachment = new Contractor.FileAttachment();
                                                fileAttachment.setAdded(AttachmentJson.optString("added"));
                                                fileAttachment.setDetail(AttachmentJson.optString("detail"));
                                                fileAttachment.setExpire(AttachmentJson.optString("expire"));
                                                fileAttachment.setFileType(AttachmentJson.optString("file_type"));
                                                fileAttachment.setFileName(AttachmentJson.optString("file_name"));
                                                fileAttachment.setID(AttachmentJson.optInt("id"));
                                                fileAttachment.setName(AttachmentJson.optString("name"));
                                                fileAttachment.setSize(AttachmentJson.optInt("size"));
                                                fileAttachment.setSized(AttachmentJson.optString("sized"));
                                                fileAttachment.setSubject(AttachmentJson.optString("subject"));
                                                fileAttachment.setType(AttachmentJson.optInt("type"));
                                                fileAttachment.setURL(AttachmentJson.optString("url"));
                                                Insurance.setAttachment(fileAttachment);
                                            }
                                            else {
                                                Contractor.FileAttachment fileAttachment = new Contractor.FileAttachment();
                                                Insurance.setAttachment(fileAttachment);
                                            }

                                            contractor.setInsurances(Insurance);
                                            ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex()).setInsurances(Insurance);
                                        }
                                    }
                                    list.add(contractor);

                                    Logger.shared().o(list);
                                    onSuccess.onSuccess(list);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException|InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void getDocuments(Context context,int buildingId, int contractorId, SuccessCallback<ArrayList<Contractor>> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex()).clearDocuments();
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CONTRACTOR_DETAIL),Integer.toString(buildingId), String.valueOf(contractorId))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url,header,(response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONObject contractorJson = ((JSONObject) object).optJSONObject("data");
                                    ArrayList<Contractor> list = new ArrayList<>();
                                    Contractor contractor = new Contractor();

                                    JSONArray documentsJson = contractorJson.optJSONArray("documents");
                                    for (int j = 0; j < documentsJson.length(); j++) {
                                        JSONObject documentObject = (JSONObject) documentsJson.get(j);
                                        if (documentObject != null) {
                                            Contractor.ContractorDocument Document = new Contractor.ContractorDocument();

                                            Contractor.FileAttachment fileAttachment = new Contractor.FileAttachment();
                                            fileAttachment.setAdded(documentObject.optString("added"));
                                            fileAttachment.setDetail(documentObject.optString("detail"));
                                            fileAttachment.setExpire(documentObject.optString("expire"));
                                            fileAttachment.setFileType(documentObject.optString("file_type"));
                                            fileAttachment.setFileName(documentObject.optString("file_name"));
                                            fileAttachment.setID(documentObject.optInt("id"));
                                            fileAttachment.setName(documentObject.optString("name"));
                                            fileAttachment.setSize(documentObject.optInt("size"));
                                            fileAttachment.setSized(documentObject.optString("sized"));
                                            fileAttachment.setSubject(documentObject.optString("subject"));
                                            fileAttachment.setType(documentObject.optInt("type"));
                                            fileAttachment.setURL(documentObject.optString("url"));
                                            Document.setAttachment(fileAttachment);

                                            contractor.setDocuments(Document);
                                            ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex()).setDocuments(Document);
                                        }
                                    }
                                    list.add(contractor);

                                    Logger.shared().o(list);
                                    onSuccess.onSuccess(list);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException|InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public void getHistories(Context context,int buildingId, int contractorId, SuccessCallback<ArrayList<Contractor>> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            try {
                ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex()).clearHistories();
                if(null != mContext){
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.BUILDING_CONTRACTOR_HISTORY),Integer.toString(buildingId), String.valueOf(contractorId))
                            .build();
                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url,header,(response)->{
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONObject){
                                if (((JSONObject) object).optInt("state") == 200){
                                    JSONArray contractorJson = ((JSONObject) object).optJSONArray("data");
                                    ArrayList<Contractor> list = new ArrayList<>();
                                    Contractor contractor = new Contractor();

                                    for (int j = 0; j < contractorJson.length(); j++) {
                                        JSONObject historyObject = (JSONObject) contractorJson.get(j);
                                        if (historyObject != null) {
                                            Contractor.ContractorHistory contractorHistory = new Contractor.ContractorHistory();
                                            contractorHistory.setAdded(historyObject.optString("added"));
                                            contractorHistory.setArea(historyObject.getInt("area"));
                                            contractorHistory.setAreaText(historyObject.getString("area_text"));
                                            contractorHistory.setBuilding(historyObject.optInt("building"));
                                            contractorHistory.setCompletionDate(historyObject.getString("completion_date"));
                                            contractorHistory.setDetail(historyObject.optString("detail"));
                                            contractorHistory.setDetail(historyObject.optString("due"));
                                            contractorHistory.setHistoryNote(historyObject.getString("history_note"));
                                            contractorHistory.setID(historyObject.optInt("id"));
                                            contractorHistory.setIsDuplicate(historyObject.optInt("is_duplicate"));
                                            contractorHistory.setIsRead(historyObject.optInt("is_read"));
                                            contractorHistory.setIsReport(historyObject.optInt("is_report"));
                                            contractorHistory.setLoggedBy(historyObject.optString("logged_by"));
                                            contractorHistory.setNumber(historyObject.optInt("number"));
                                            contractorHistory.setPriority(historyObject.optInt("priority"));
                                            contractorHistory.setPriorityText(historyObject.optString("priority_text"));
                                            contractorHistory.setSStatus(historyObject.optInt("sStatus"));
                                            contractorHistory.setStarred(historyObject.optInt("starred"));
                                            contractorHistory.setStart(historyObject.optString("start"));
                                            contractorHistory.setStatus(historyObject.optInt("status"));
                                            contractorHistory.setStatusText(historyObject.optString("status_text"));
                                            contractorHistory.setSubStatus(historyObject.optString("sub_status"));
                                            contractorHistory.setSubject(historyObject.optString("subject"));
                                            contractorHistory.setSync(historyObject.optInt("sync"));
                                            contractorHistory.setType(historyObject.optInt("type"));
                                            contractorHistory.setTypeText(historyObject.optString("type_text"));
                                            contractorHistory.setUpdate(historyObject.optString("updated"));
                                            contractorHistory.setWorkOrder(historyObject.optString("work_order"));

                                            contractor.setHistories(contractorHistory);
                                            ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex()).setHistories(contractorHistory);
                                        }
                                    }
                                    list.add(contractor);

                                    Logger.shared().o(list);
                                    onSuccess.onSuccess(list);
                                }else{
                                    onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                                }
                            }else {
                                throw new InvalidResponse();
                            }
                        }catch (JSONException|InvalidResponse e){
                            onFailure.onFailure(e);
                        }
                    },onFailure::onFailure);
                }else {
                    throw new InternalError();
                }
            }catch(InternalError error){
                onFailure.onFailure(error);
            }
        });
    }

    public static Map<String,Object> buildRawJSONDataFromContractor(Contractor contractor){
        return new VolleyBodyBuilder()
                .setParameter("company",contractor.getCompany())
                .setParameter("address",contractor.getAddress())
                .setParameter("preferred",contractor.getPreferred())
                .setParameter("phone",contractor.getPhone())
                .setParameter("fax",contractor.getFax())
                .setParameter("email",contractor.getEmail())
                .setParameter("facebook",contractor.getFacebook())
                .setParameter("website",contractor.getWebsite())
                .setParameter("google",contractor.getGoogle())
                .setParameter("industry",contractor.getIndustry())
                .setParameter("id",contractor.getId())
                .setParameter("info",contractor.getInfo())
                .setParameter("category",contractor.getCategory())
                .setParameter("categoryName",contractor.getCategoryName())
                .setParameter("subject",contractor.getSubject())
                .setParameter("message",contractor.getMessage())
                .export();
    }

    public static Map<String,Object> buildRawJSONDataFromMainContact(Contractor.MainContact mainContact){
        return new VolleyBodyBuilder()
                .setParameter("contractor", mainContact.getContractor())
                .setParameter("email", mainContact.getEmail())
                .setParameter("id", mainContact.getId())
                .setParameter("main", mainContact.getMain())
                .setParameter("name", mainContact.getName())
                .setParameter("number", mainContact.getNumber())
                .export();
    }
}
