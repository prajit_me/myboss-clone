package com.mybos.bmapp.Data.Model.Contractor;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Component.SpinnerAdapter.SpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

import java.util.ArrayList;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class Contractor extends BaseModel implements SpinnerItemInterface<Integer>,Parcelable {
    private String company = "";
    private String address = "";
    private int preferred = 0;
    private String phone = "";
    private String mobile = "";
    private String fax = "";
    private String email = "";
    private String facebook = "";
    private String website = "";
    private String google = "";
    private int industry = 0;
    private int id = 0;
    private String info = "";
    private String categoryName = "";
    private int category = 0;

    private String subject = "";
    private String message = "";

    private MainContact mainContact;
    private ArrayList<ContractorInsurance> Insurances = new ArrayList<>();
    private ArrayList<ContractorDocument> Documents = new ArrayList<>();
    private ArrayList<ContractorHistory> Histories = new ArrayList<>();

    public Contractor(){}

    protected Contractor(Parcel in) {
        company = in.readString();
        address = in.readString();
        preferred = in.readInt();
        phone = in.readString();
        mobile = in.readString();
        fax = in.readString();
        email = in.readString();
        facebook = in.readString();
        website = in.readString();
        google = in.readString();
        industry = in.readInt();
        id = in.readInt();
        info = in.readString();
        categoryName = in.readString();
        category = in.readInt();
        subject = in.readString();
        message = in.readString();
    }

    public static final Creator<Contractor> CREATOR = new Creator<Contractor>() {
        @Override
        public Contractor createFromParcel(Parcel in) {
            return new Contractor(in);
        }

        @Override
        public Contractor[] newArray(int size) {
            return new Contractor[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setPreferred(int preferred) {
        this.preferred = preferred;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public void setIndustry(int industry) {
        this.industry = industry;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setMainContact(MainContact mainContact) {
        this.mainContact = mainContact;
    }

    public String getCompany() {
        return company;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public int getPreferred() {
        return preferred;
    }

    public String getPhone() {
        return phone;
    }

    public String getMobile() {
        return mobile;
    }

    public String getFax() {
        return fax;
    }

    public String getEmail() {
        return email;
    }

    public String getFacebook() {
        return facebook;
    }

    public String getWebsite() {
        return website;
    }

    public String getGoogle() {
        return google;
    }

    public int getIndustry() {
        return industry;
    }

    public String getInfo() {
        return info;
    }

    public int getCategory() {
        return category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void clearInsurances() {
        Insurances.clear();
    }

    public void setInsurances(ArrayList<ContractorInsurance> Insurances) {
        this.Insurances.addAll(Insurances);
    }

    public void setInsurances(ContractorInsurance Insurance) {
        this.Insurances.add(Insurance);
    }

    public ArrayList<ContractorInsurance> getInsurances() {
        return Insurances;
    }

    public void clearDocuments() {
        Documents.clear();
    }

    public void setDocuments(ArrayList<ContractorDocument> documents) {
        Documents = documents;
    }

    public void setDocuments(ContractorDocument document) {
        Documents.add(document);
    }

    public ArrayList<ContractorDocument> getDocuments() {
        return Documents;
    }

    public void clearHistories() {
        Histories.clear();
    }

    public void setHistories(ArrayList<ContractorHistory> histories) {
        Histories = histories;
    }

    public void setHistories(ContractorHistory histories) {
        Histories.add(histories);
    }

    public ArrayList<ContractorHistory> getHistories() {
        return Histories;
    }

    @Override
    public Integer getSpinnerItemId() {
        return id;
    }

    @Override
    public String getTittle() {
        return getCompany();
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public MainContact getMainContact() {
        return mainContact;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(company);
        dest.writeString(address);
        dest.writeInt(preferred);
        dest.writeString(phone);
        dest.writeString(fax);
        dest.writeString(email);
        dest.writeString(facebook);
        dest.writeString(website);
        dest.writeString(google);
        dest.writeInt(industry);
        dest.writeInt(id);
        dest.writeString(info);
        dest.writeString(categoryName);
        dest.writeInt(category);
        dest.writeString(subject);
        dest.writeString(message);
    }

    public static class MainContact{
        private int main;
        private String number;
        private String name;
        private String email;
        private int id;
        private int contractor;

        public void setId(int id) {
            this.id = id;
        }

        public void setMain(int main) {
            this.main = main;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public void setName(String name) {
            this.name = name;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public void setContractor(int contractor) {
            this.contractor = contractor;
        }

        public int getContractor() {
            return contractor;
        }

        public String getNumber() {
            return number;
        }

        public String getEmail() {
            return email;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }

        public int getMain() {
            return main;
        }
    }

    public static class ContractorInsurance {
        private String Added = "";
        private FileAttachment Attachment;
        private int Contractor = 0;
        private String Expire = "";
        private String Name = "";

        public void setAdded(String added) {
            Added = added;
        }

        public String getAdded() {
            return Added;
        }

        public void setAttachment(FileAttachment attachment) {
            Attachment = attachment;
        }

        public FileAttachment getAttachment() {
            return Attachment;
        }

        public void setContractor(int contractor) {
            Contractor = contractor;
        }

        public int getContractor() {
            return Contractor;
        }

        public void setExpire(String expire) {
            Expire = expire;
        }

        public String getExpire() {
            return Expire;
        }

        public void setName(String name) {
            Name = name;
        }

        public String getName() {
            return Name;
        }
    }

    public static class ContractorDocument {
        private FileAttachment Attachment;

        public void setAttachment(FileAttachment attachment) {
            Attachment = attachment;
        }

        public FileAttachment getAttachment() {
            return Attachment;
        }
    }

    public static class ContractorHistory {
        private String Added;
        private int Area;
        private String AreaText;
        private int Building;
        private String CompletionDate;
        private String Detail;
        private String Due;
        private String HistoryNote;
        private int ID;
        private int IsDuplicate;
        private int IsRead;
        private int IsReport;
        private String LoggedBy;
        private int Number;
        private int Priority;
        private String PriorityText;
        private int SStatus;
        private int Starred;
        private String Start;
        private int Status;
        private String StatusText;
        private String SubStatus;
        private String Subject;
        private int Sync;
        private int Type;
        private String TypeText;
        private String Update;
        private String WorkOrder;

        public String getAdded() {
            return Added;
        }

        public void setAdded(String added) {
            Added = added;
        }

        public int getArea() {
            return Area;
        }

        public void setArea(int area) {
            Area = area;
        }

        public String getAreaText() {
            return AreaText;
        }

        public void setAreaText(String areaText) {
            AreaText = areaText;
        }

        public int getBuilding() {
            return Building;
        }

        public void setBuilding(int building) {
            Building = building;
        }

        public String getCompletionDate() {
            return CompletionDate;
        }

        public void setCompletionDate(String completionDate) {
            CompletionDate = completionDate;
        }

        public String getDetail() {
            return Detail;
        }

        public void setDetail(String detail) {
            Detail = detail;
        }

        public String getDue() {
            return Due;
        }

        public void setDue(String due) {
            Due = due;
        }

        public String getHistoryNote() {
            return HistoryNote;
        }

        public void setHistoryNote(String historyNote) {
            HistoryNote = historyNote;
        }

        public int getID() {
            return ID;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getIsDuplicate() {
            return IsDuplicate;
        }

        public void setIsDuplicate(int isDuplicate) {
            IsDuplicate = isDuplicate;
        }

        public int getIsRead() {
            return IsRead;
        }

        public void setIsRead(int isRead) {
            IsRead = isRead;
        }

        public int getIsReport() {
            return IsReport;
        }

        public void setIsReport(int isReport) {
            IsReport = isReport;
        }

        public String getLoggedBy() {
            return LoggedBy;
        }

        public void setLoggedBy(String loggedBy) {
            LoggedBy = loggedBy;
        }

        public int getNumber() {
            return Number;
        }

        public void setNumber(int number) {
            Number = number;
        }

        public int getPriority() {
            return Priority;
        }

        public void setPriority(int priority) {
            Priority = priority;
        }

        public String getPriorityText() {
            return PriorityText;
        }

        public void setPriorityText(String priorityText) {
            PriorityText = priorityText;
        }

        public int getSStatus() {
            return SStatus;
        }

        public void setSStatus(int SStatus) {
            this.SStatus = SStatus;
        }

        public int getStarred() {
            return Starred;
        }

        public void setStarred(int starred) {
            Starred = starred;
        }

        public String getStart() {
            return Start;
        }

        public void setStart(String start) {
            Start = start;
        }

        public int getStatus() {
            return Status;
        }

        public void setStatus(int status) {
            Status = status;
        }

        public String getStatusText() {
            return StatusText;
        }

        public void setStatusText(String statusText) {
            StatusText = statusText;
        }

        public String getSubStatus() {
            return SubStatus;
        }

        public void setSubStatus(String subStatus) {
            SubStatus = subStatus;
        }

        public String getSubject() {
            return Subject;
        }

        public void setSubject(String subject) {
            Subject = subject;
        }

        public int getSync() {
            return Sync;
        }

        public void setSync(int sync) {
            Sync = sync;
        }

        public int getType() {
            return Type;
        }

        public void setType(int type) {
            Type = type;
        }

        public String getTypeText() {
            return TypeText;
        }

        public void setTypeText(String typeText) {
            TypeText = typeText;
        }

        public String getUpdate() {
            return Update;
        }

        public void setUpdate(String update) {
            Update = update;
        }

        public String getWorkOrder() {
            return WorkOrder;
        }

        public void setWorkOrder(String workOrder) {
            WorkOrder = workOrder;
        }
    }

    public static class FileAttachment {
        private String Added = "";
        private String Detail = "";
        private String Expire = "";
        private String FileName = "";
        private String FileType = "";
        private int ID = 0;
        private String Name = "";
        private int Size = 0;
        private String Sized = "";
        private String Subject = "";
        private int Type = 0;
        private String URL = "";

        public void setAdded(String Added) {
            this.Added = Added;
        }

        public String getAdded() {
            return this.Added;
        }

        public void setDetail(String Detail) {
            this.Detail = Detail;
        }

        public String getDetail() {
            return this.Detail;
        }

        public void setExpire(String Expire) {
            this.Expire = Expire;
        }

        public String getExpire() {
            return this.Expire;
        }

        public void setFileName(String FileName) {
            this.FileName = FileName;
        }

        public String getFileName() {
            return FileName;
        }

        public void setFileType(String FileType) {
            this.FileType = FileType;
        }

        public String getFileType() {
            return FileType;
        }

        public void setID(int ID) {
            this.ID = ID;
        }

        public int getID() {
            return ID;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getName() {
            return Name;
        }

        public void setSize(int Size) {
            this.Size = Size;
        }

        public int getSize() {
            return this.Size;
        }

        public void setSized(String Sized) {
            this.Sized = Sized;
        }

        public String getSized() {
            return this.Sized;
        }

        public void setSubject(String Subject) {
            this.Subject = Subject;
        }

        public String getSubject() {
            return this.Subject;
        }

        public void setType(int Type) {
            this.Type = Type;
        }

        public int getType() {
            return this.Type;
        }

        public void setURL(String URL) {
            this.URL = URL;
        }

        public String getURL() {
            return URL;
        }
    }

}
