package com.mybos.bmapp.Data.Model.Inspection;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Functional.PhotoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 18/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionArea extends BaseModel {
    int id;
    int inspection;
    String name;
    List<InspectionItem> items;
    public InspectionArea(){
        items = new ArrayList<>();
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setInspection(int inspection) {
        this.inspection = inspection;
    }

    public void setName(String name) {
        this.name = name;
    }
    public void addItem(InspectionItem item){
        items.add(item);
    }

    public String getName() {
        return name;
    }

    public List<InspectionItem> getItems() {
        return items;
    }

    public void ClearData(){
        for (InspectionItem item: items
             ) {
            item.setComment("");
            item.setIsChecked(2);
            item.setPhotoPath("");
            item.setImage(null);
        }
    }
}
