package com.mybos.bmapp.Data.Functional;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;
import com.mybos.bmapp.Utils.BitmapUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

/**
 * Created by EmLaAi on 08/04/2018.
 *
 * @author EmLaAi
 */
public class PhotoModel extends BaseModel {
    Uri photoUri;
    Bitmap data;
    String name;
    String mimetype;

    public String getName() {
        return name;
    }

    public String getMimetype() {
        return mimetype;
    }
    public String getBase64Image(){
        return BitmapUtils.toBase64(data);
    }

    public PhotoModel(Uri photoUri, String name, String mime){
        this.photoUri = photoUri;
        this.name = name;
        this.mimetype = mime;
    }
    public PhotoModel(Bitmap bitmap,String name,String mime){
        this.data = compressImage(bitmap);
        this.name = name;
        this.mimetype = "image/jpeg";
    }
    public VolleyMultipartBuilder.MultipartFile createMultipartFile(){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Log.e("check_image",stream.toByteArray().toString());
//        if (mimetype.equals("Content-Type: image/jpeg")){
//            Logger.shared().i("---JPG----");
//            data.compress(Bitmap.CompressFormat.JPEG,100,stream);
//        }else{
//            data.compress(Bitmap.CompressFormat.PNG,100,stream);
//        }
        data.compress(Bitmap.CompressFormat.JPEG,100,stream);
        return new VolleyMultipartBuilder.MultipartFile(name,stream.toByteArray(),"image/jpeg");
    }

    private Bitmap compressImage(Bitmap image) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//Compression quality, here 100 means no compression, the storage of compressed data to baos
        int options = 90;
        while (baos.toByteArray().length / 1024 > 500) {  //Loop if compressed picture is greater than 400kb, than to compression
            baos.reset();//Reset baos is empty baos
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//The compression options%, storing the compressed data to the baos
            options -= 10;//Every time reduced by 10
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//The storage of compressed data in the baos to ByteArrayInputStream
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//The ByteArrayInputStream data generation
        return bitmap;
    }
}
