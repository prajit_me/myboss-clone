package com.mybos.bmapp.Data.Mapper.Resident;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Model.Resident.Resident;
import com.mybos.bmapp.Data.Response.ResidentApartmentResponse;
import com.mybos.bmapp.Data.Response.ResidentListResponse;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentMapper extends BaseMapper {
    public void get(Context context, int buildingId, SearchTableQueryBuilder searchTableQueryBuilder, SuccessCallback<ResidentListResponse> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.resident(VolleyUrlBuilder.URLPATH.RESIDENT_LIST),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = searchTableQueryBuilder.buildPostBody();
                Log.e("check_url",url+body);
                VolleyRequest.shared(mContext).post(url,header,body,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                int maximum = ((JSONObject) object).optInt("total");
                                List<Resident> temp = new ArrayList<>();
                                JSONArray listResidentJSOn = ((JSONObject) object).getJSONArray("data");
                                for (int i = 0; i < listResidentJSOn.length(); i++) {
                                    JSONObject residentJSON = listResidentJSOn.optJSONObject(i);
                                    Resident resident = new Resident();
                                    ResidentMapper.parseJSON(resident,residentJSON);
                                    temp.add(resident);
                                }
                                ResidentListResponse residentListResponse = new ResidentListResponse(maximum,temp);
                                onSuccess.onSuccess(residentListResponse);
                            }else{
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getResidentDetail(Context context,int buildingId,int apartmentId,int residentId,
                                  SuccessCallback<ResidentApartmentResponse> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.apartment(VolleyUrlBuilder.URLPATH.APARTMENT_DETAIL)
                                ,Integer.toString(buildingId),Integer.toString(apartmentId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            JSONArray residentsJSON = ((JSONObject) object).optJSONArray("residents");
                            for (int i = 0; i < residentsJSON.length(); i++) {
                                JSONObject residentJson = residentsJSON.getJSONObject(i);
                                if (residentJson.optInt("id") == residentId){
                                    ResidentApartmentResponse mResponse = new ResidentApartmentResponse();
                                    ResidentMapper.parseJSON(mResponse,residentJson);
                                    onSuccess.onSuccess(mResponse);
                                    return;
                                }
                            }
                            onSuccess.onSuccess(new ResidentApartmentResponse());
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public static void parseJSON(@NotNull Resident resident,@NotNull JSONObject object){
        resident.setUid(object.optInt("uid"));
        resident.setName(object.optString("name"));
        resident.setMain(object.optInt("main"));
        resident.setEmail(object.has("email") && !object.isNull("email")?object.optString("email"):null);
        resident.setLot(object.optInt("lot"));
        resident.setMobile(object.has("mobile") && !object.isNull("mobile")?object.optString("mobile"):null);
        resident.setTypeText(object.optString("typeText"));
        resident.setType(object.optInt("type"));
        resident.setApartment(object.optString("apartment"));
        resident.setMember(object.optInt("member"));
        resident.setLogin(object.optInt("login"));
        resident.setUserName(object.optString("username"));
        resident.setLastName(object.optString("last_name"));
        resident.setId(object.optInt("id"));
        resident.setFirstName(object.optString("first_name"));
        resident.setPhone(object.has("phone") && !object.isNull("phone")?object.optString("phone"):null);
        resident.setSecondNumber(object.optString("second_number"));
        resident.setMemberText(object.optString("memberText"));
        resident.setBuilding(object.optInt("building"));
    }

    public static void parseJSON(ResidentApartmentResponse response,JSONObject object){
        if (object.optJSONObject("photo") != null) {
            if (object.optJSONObject("photo").has("url") && !object.optJSONObject("photo").isNull("url")){
                response.setUrl(object.optJSONObject("photo").optString("url"));
            }else {
                response.setUrl(null);
            }
        }
        response.setEmergencyContact(object.has("emergency") && !object.isNull("emergency")?object.optString("emergency"):null);
        response.setEmergencyContactPhone(object.has("emergency_phone") && !object.isNull("emergency_phone")?object.optString("emergency_phone"):null);
    }
}
