package com.mybos.bmapp.Data.Response;

import com.mybos.bmapp.Data.Model.Resident.Resident;

import java.util.List;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentListResponse {
    private int maximum;
    private List<Resident> residents;
    public ResidentListResponse(int maximum,List<Resident> residents){
        this.maximum = maximum;
        this.residents = residents;
    }

    public int getMaximum() {
        return maximum;
    }

    public List<Resident> getResidents() {
        return residents;
    }
}
