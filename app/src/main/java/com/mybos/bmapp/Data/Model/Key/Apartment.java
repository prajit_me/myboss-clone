package com.mybos.bmapp.Data.Model.Key;

import com.mybos.bmapp.Data.Base.BaseModel;

import java.util.ArrayList;

public class Apartment extends BaseModel {

    private String apartment_Id;
    private int apartment_state;
    private String keyId;
    private String building;
    private String key;
    private String key_status;
    private String card_no;
    private String type;
    private String location;
    private String description;
    private String photo;
    private String comment;
    private String owner;
    private String is_request;
    private String locationText;
    private String typeText;
    private String ownerData;

    private static ArrayList<Apartment> apartment_keys_M = new ArrayList<>();
    private static ArrayList<Apartment> apartment_keys_O = new ArrayList<>();
    private static ArrayList<Apartment> apartment_keys = new ArrayList<>();
    private static ArrayList<Apartment> apartments = new ArrayList<>();


    public static ArrayList<Apartment> getApartments() {
        return apartments;
    }

    public static void setApartments(ArrayList<Apartment> apartments) {
        Apartment.apartments = apartments;
    }

    public static ArrayList<Apartment> getApartment_keys_M() {
        return apartment_keys_M;
    }

    public static void setApartment_keys_M(ArrayList<Apartment> apartment_keys) {
        Apartment.apartment_keys_M = apartment_keys;
    }

    public static ArrayList<Apartment> getApartment_keys_O() {
        return apartment_keys_O;
    }

    public static void setApartment_keys_O(ArrayList<Apartment> apartment_keys_O) {
        Apartment.apartment_keys_O = apartment_keys_O;
    }

    public static ArrayList<Apartment> getApartment_keys() {
        return apartment_keys;
    }

    public static void setApartment_keys(ArrayList<Apartment> apartment_keys) {
        Apartment.apartment_keys = apartment_keys;
    }


    public String getApartment_Id() {
        return apartment_Id;
    }

    public void setApartment_Id(String apartment_Id) {
        this.apartment_Id = apartment_Id;
    }

    public int getApartment_state() {
        return apartment_state;
    }

    public void setApartment_state(int apartment_state) {
        this.apartment_state = apartment_state;
    }

    public String getKeyId() {
        return keyId;
    }

    public void setKeyId(String keyId) {
        this.keyId = keyId;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getCard_no() {
        return card_no;
    }

    public void setCard_no(String card_no) {
        this.card_no = card_no;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getIs_request() {
        return is_request;
    }

    public void setIs_request(String is_request) {
        this.is_request = is_request;
    }

    public String getLocationText() {
        return locationText;
    }

    public void setLocationText(String locationText) {
        this.locationText = locationText;
    }

    public String getTypeText() {
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public String getOwnerData() {
        return ownerData;
    }

    public void setOwnerData(String ownerData) {
        this.ownerData = ownerData;
    }

    public String getKey_status() {
        return key_status;
    }

    public void setKey_status(String key_status) {
        this.key_status = key_status;
    }
}
