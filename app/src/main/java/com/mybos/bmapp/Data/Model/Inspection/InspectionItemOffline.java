package com.mybos.bmapp.Data.Model.Inspection;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Base.BaseModel;

import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

/**
 * Created by EmLaAi on 18/04/2018.
 *
 * @author EmLaAi
 */
@RealmClass
public class InspectionItemOffline implements Parcelable, RealmModel {
    int id;
    int area;
    String name;
    int inspection;
    String comment;
    String photoPath;
    private int isChecked;

    public InspectionItemOffline (){}

    protected InspectionItemOffline(Parcel in) {
        id = in.readInt();
        area = in.readInt();
        name = in.readString();
        inspection = in.readInt();
        comment = in.readString();
        photoPath = in.readString();
        isChecked = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(area);
        dest.writeString(name);
        dest.writeInt(inspection);
        dest.writeString(comment);
        dest.writeString(photoPath);
        dest.writeInt(isChecked);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InspectionItemOffline> CREATOR = new Creator<InspectionItemOffline>() {
        @Override
        public InspectionItemOffline createFromParcel(Parcel in) {
            return new InspectionItemOffline(in);
        }

        @Override
        public InspectionItemOffline[] newArray(int size) {
            return new InspectionItemOffline[size];
        }
    };

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInspection(int inspection) {
        this.inspection = inspection;
    }

    public int getInspection() {
        return inspection;
    }

    public String getName() {
        return name;
    }


    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }


    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public int getId() {
        return id;
    }


    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public void copyFromInspectionItem(InspectionItem item){
        this.id = item.getId();
        this.area = item.getArea();
        this.comment = item.getComment();
        this.inspection = item.getInspection();
        this.name = item.getName();
        this.photoPath = item.getPhotoPath();
        this.isChecked = item.getIsChecked();

    }

    public int getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(int isChecked) {
        this.isChecked = isChecked;
    }
}
