package com.mybos.bmapp.Data.Mapper.Attachment;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentFragment;
import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentFile;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadInvoice;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadQuote;
import com.mybos.bmapp.Data.Model.Cases.SelectedFileInvoiceQuotes;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;
import com.mybos.bmapp.Utils.DateUtils;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.File;
import java.util.Date;
import java.util.Map;

/**
 * Created by EmLaAi on 08/04/2018.
 *
 * @author EmLaAi
 */
public class AttachmentMapper extends BaseMapper {
    public void uploadPhoto(Context context, int buildingId, int caseId, PhotoModel photo,
                            SuccessCallback<AttachmentFile> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.attachment(VolleyUrlBuilder.URLPATH.ATTACHMENT_PHOTO),Integer.toString(buildingId),Integer.toString(caseId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                builder.addString("building",Integer.toString(buildingId));
                builder.addFile("file",photo.createMultipartFile());
                VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                AttachmentFile file = new AttachmentFile();
                                JSONObject fileJSON = ((JSONObject) object).getJSONObject("file");
                                parseFileJson(file,fileJSON);
                                onSuccess.onSuccess(file);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);

            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void uploadDocument(Context context, int buildingId, int caseId, File documentFile,
                            SuccessCallback<AttachmentFile> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.attachment(VolleyUrlBuilder.URLPATH.ATTACHMENT_DOC),Integer.toString(buildingId),Integer.toString(caseId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                builder.addString("building",Integer.toString(buildingId));
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    if (AttachmentFragment.selectedByteFile==null){
                        Log.e("check_null","null");
                    }
                    builder.addFile("file",new VolleyMultipartBuilder.MultipartFile(AttachmentFragment.selectedFileName, AttachmentFragment.selectedByteFile, AttachmentFragment.selectedFileMime));
                }

                VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                AttachmentFile file = new AttachmentFile();
                                JSONObject fileJSON = ((JSONObject) object).getJSONObject("file");
                                parseFileJson(file,fileJSON);
                                onSuccess.onSuccess(file);
                            }else if (((JSONObject) object).optInt("state") == 400){
                                onSuccess.onSuccess(null);
                            }
                            else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);

            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void uploadQuote(Context context, int buildingId, int caseId,Contractor contractor, Double value,PhotoModel model,
                            SuccessCallback<AttachmentUploadQuote> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.attachment(VolleyUrlBuilder.URLPATH.ATTACHMENT_QUOTE),Integer.toString(buildingId),Integer.toString(caseId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                builder.addString("contractor",Integer.toString(contractor.getId()));
                builder.addString("amount",Double.toString(value));
                builder.addString("type","quote");
                if (null != model) {
                    builder.addFile("file", model.createMultipartFile());
                }else if (SelectedFileInvoiceQuotes.getBytes() != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        builder.addFile("file",new VolleyMultipartBuilder.MultipartFile(SelectedFileInvoiceQuotes.getName(), SelectedFileInvoiceQuotes.getBytes(), SelectedFileInvoiceQuotes.getMime()));
                    }
                }
                VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                AttachmentUploadQuote quote = new AttachmentUploadQuote();
                                quote.setAmount(value);
                                quote.setType("quote");
                                quote.setDate(DateUtils.getDateString(new Date()));
                                quote.setContractor(contractor);
                                JSONObject fileJson = ((JSONObject) object).optJSONObject("file");

                                if (null != fileJson){
                                    AttachmentFile file = new AttachmentFile();
                                    parseFileJson(file,fileJson);
                                    quote.setFile(file);
                                }

                                onSuccess.onSuccess(quote);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void uploadInvoice(Context context, int buildingId, int caseId,
                              String invoiceNumber, Contractor contractor, Double value , String notes, PhotoModel model,
                              SuccessCallback<AttachmentUploadInvoice> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.attachment(VolleyUrlBuilder.URLPATH.ATTACHMENT_INVOICE),Integer.toString(buildingId),Integer.toString(caseId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().build();
                VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
                builder.addString("contractor",Integer.toString(contractor.getId()));
                builder.addString("number",invoiceNumber);
                builder.addString("amount",Double.toString(value));
                builder.addString("type","invoice");
                if (null != notes){
                    builder.addString("comment",notes);
                }
                if (null != model){
                    builder.addFile("file",model.createMultipartFile());
                }else if (SelectedFileInvoiceQuotes.getBytes() != null){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        builder.addFile("file",new VolleyMultipartBuilder.MultipartFile(SelectedFileInvoiceQuotes.getName(), SelectedFileInvoiceQuotes.getBytes(), SelectedFileInvoiceQuotes.getMime()));
                    }
                }
                VolleyRequest.shared(mContext).postForm(url,header,builder,response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            AttachmentUploadInvoice invoice = new AttachmentUploadInvoice();
                            invoice.setContractor(contractor);
                            invoice.setNumber(invoiceNumber);
                            invoice.setAmount(value);
                            invoice.setComment(notes);
                            invoice.setType("invoice");
                            invoice.setDate(DateUtils.getDateString(new Date()));


                            JSONObject fileJSON = ((JSONObject) object).optJSONObject("file");
                            if (null != fileJSON) {
                                AttachmentFile file = new AttachmentFile();
                                parseFileJson(file, fileJSON);
                                invoice.setFile(file);
                            }

                            onSuccess.onSuccess(invoice);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void parseFileJson(AttachmentFile file,JSONObject object){
        file.setUrl(object.optString("url"));
        file.setSized(object.optString("sized"));
        file.setAdded(object.optString("added"));
        file.setName(object.optString("name"));
        file.setId(object.optInt("id"));
        file.setExpire(object.optString("expire"));
        file.setSize(object.optInt("size"));
        file.setDetail(object.optString("detail"));
        file.setFileName(object.optString("file_name"));
        file.setType(object.optInt("type"));
        file.setFileType(object.optString("file_type"));
        file.setSubject(object.optString("subject"));
    }

    public String buildPath(String path){
        path = path.replace("/storage/emulated/0/","");
        int indexCut = path.indexOf(":");
        path = path.substring(indexCut+1,path.length());
        path = "/storage/emulated/0/"+path;
        Log.e("result",path);
        return "/storage/emulated/0/document/1800.xlsx";
    }

    public String getNameFromPath(String name){
        int indextCut = name.lastIndexOf(":");
        if (indextCut != -1){
            name = name.substring(indextCut+1,name.length());
        }
        return name;
    }

}
