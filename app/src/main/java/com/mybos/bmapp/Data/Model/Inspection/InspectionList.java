package com.mybos.bmapp.Data.Model.Inspection;

import com.mybos.bmapp.Data.Base.BaseModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 17/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionList extends BaseModel {
    private List<Inspection> list;
    private static InspectionList offlineList = new InspectionList();
    private static InspectionList offlineListPrivate = new InspectionList();
    public InspectionList(){
        list = new ArrayList<>();
    }

    public static InspectionList getOfflineList() {
        return offlineList;
    }

    public static void setOfflineList(InspectionList offlineList) {
        InspectionList.offlineList = offlineList;
    }

    public static InspectionList getOfflineListPrivate() {
        return offlineListPrivate;
    }

    public static void setOfflineListPrivate(InspectionList offlineListPrivate) {
        InspectionList.offlineListPrivate = offlineListPrivate;
    }

    public void addInspection(Inspection inspection){
        list.add(inspection);
    }

    public List<Inspection> getAll() {
        return list;
    }
}
