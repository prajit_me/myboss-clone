package com.mybos.bmapp.Data.Model.Library;

import com.mybos.bmapp.Data.Base.BaseModel;

import java.util.ArrayList;

public class LibraryFileOrFolder extends BaseModel {

    private String name = "";
    private String id = "";
    private String type = "";
    private String inside = "";
    private String size = "";
    private String url = "";
    private String added = "";

    public static ArrayList<LibraryFileOrFolder> listFileFolderCurrentPage = new ArrayList<>();

    public static ArrayList<LibraryFileOrFolder> getListFileFolderCurrentPage() {
        return listFileFolderCurrentPage;
    }

    public static void setListFileFolderCurrentPage(ArrayList<LibraryFileOrFolder> listFileFolderCurrentPage) {
        LibraryFileOrFolder.listFileFolderCurrentPage = listFileFolderCurrentPage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInside() {
        return inside;
    }

    public void setInside(String inside) {
        this.inside = inside;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAdded() {
        return added;
    }

    public void setAdded(String added) {
        this.added = added;
    }
}
