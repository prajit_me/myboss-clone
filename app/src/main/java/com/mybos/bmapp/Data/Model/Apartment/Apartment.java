package com.mybos.bmapp.Data.Model.Apartment;

import com.mybos.bmapp.Component.SpinnerAdapter.SpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class Apartment extends BaseModel implements SpinnerItemInterface<String> {
    private String status;
    private int isHotel;
    private String id;
    private String lot;
    private String uid;
    private int building;

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setIsHotel(int isHotel) {
        this.isHotel = isHotel;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public String getId() {
        return id;
    }

    @Override
    public String getSpinnerItemId() {
        return id;
    }

    @Override
    public String getTittle() {
        return id;
    }
}
