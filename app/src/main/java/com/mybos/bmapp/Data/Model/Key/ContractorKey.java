package com.mybos.bmapp.Data.Model.Key;

import java.util.ArrayList;

public class ContractorKey {

    private String companyName;
    private String address;
    private String preferred;
    private String phone;
    private String fax;
    private String email;
    private String facebook;
    private String website;
    private String google;
    private String industry;
    private String id;
    private String info;
    private String categoryName;
    private String category;
    private String mc_name;
    private String mc_number;


    private static ArrayList<ContractorKey> contractors = new ArrayList<>();
    private static String selectedContractor = "";

    public static ArrayList<ContractorKey> getContractors() {
        return contractors;
    }

    public static void setContractors(ArrayList<ContractorKey> contractors) {
        ContractorKey.contractors = contractors;
    }

    public static String getSelectedContractor() {
        return selectedContractor;
    }

    public static void setSelectedContractor(String selectedContractor) {
        ContractorKey.selectedContractor = selectedContractor;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getGoogle() {
        return google;
    }

    public void setGoogle(String google) {
        this.google = google;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPreferred() {
        return preferred;
    }

    public void setPreferred(String preferred) {
        this.preferred = preferred;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getMc_name() {
        return mc_name;
    }

    public void setMc_name(String mc_name) {
        this.mc_name = mc_name;
    }

    public String getMc_number() {
        return mc_number;
    }

    public void setMc_number(String mc_number) {
        this.mc_number = mc_number;
    }

    public static ContractorKey getObjectFromNameCopany(String name){
        for (ContractorKey con: ContractorKey.getContractors()
             ) {
            if (con.getCompanyName().equals(name)){
                return con;
            }
        }
        return null;
    }
}
