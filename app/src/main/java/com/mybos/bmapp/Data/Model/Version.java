package com.mybos.bmapp.Data.Model;

import android.util.Log;
import android.widget.Toast;

import java.util.Date;

import io.realm.Realm;
import io.realm.RealmObject;

public class Version extends RealmObject {

    private Date dayShow;

    public Date getDayShow() {
        return dayShow;
    }

    public void setDayShow(Date dayShow) {
        this.dayShow = dayShow;
    }

    public static void save(Version version) {
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction((mRealm)->{
                mRealm.where(Version.class).findAll().deleteAllFromRealm();
                realm.insert(version);
            });
        }
    }

    public static Version get(){
        try(Realm realm = Realm.getDefaultInstance()){
            return realm.where(Version.class).findFirst();
        }
    }
}
