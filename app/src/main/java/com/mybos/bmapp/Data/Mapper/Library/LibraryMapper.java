package com.mybos.bmapp.Data.Mapper.Library;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Library.LibraryFileOrFolder;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Map;

public class LibraryMapper extends BaseMapper {

    public void getLibraryList(Context context, int buildingId, String id, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder(2).parseUrl(VolleyUrlBuilder.URLPATH.library(VolleyUrlBuilder.URLPATH.LIBRARY_GET_LIST),
                        Integer.toString(buildingId),id).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                url = url+"?t=manual&p=1&s=50&k=&f=undefined&o=undefined&ot=undefined";
                Log.e("check","library:"+url);
                VolleyRequest.shared(mContext).get(url, header, (response) -> {
                    try {

                        ArrayList<LibraryFileOrFolder> fofList = new ArrayList<>();
                        Object object = new JSONTokener(response).nextValue();
                        String state = ((JSONObject) object).getString("state");
                        if (state.equals("200")) {
                            JSONArray data = ((JSONObject) object).getJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject inspectionJSON = data.getJSONObject(i);
                                LibraryFileOrFolder libraryFileOrFolder = new LibraryFileOrFolder();
                                libraryFileOrFolder.setName(inspectionJSON.optString("name"));
                                libraryFileOrFolder.setId(inspectionJSON.optString("id"));
                                libraryFileOrFolder.setType(inspectionJSON.optString("type"));
                                libraryFileOrFolder.setInside(inspectionJSON.optString("inside"));
                                if (inspectionJSON.has("url")){
                                    libraryFileOrFolder.setUrl(inspectionJSON.optString("url"));
                                }
                                if (inspectionJSON.has("added")){
                                    libraryFileOrFolder.setAdded(inspectionJSON.optString("added"));
                                }
                                fofList.add(libraryFileOrFolder);
                            }
                            LibraryFileOrFolder.setListFileFolderCurrentPage(new ArrayList<>(fofList));
                        }
                        onSuccess.onSuccess("success");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
