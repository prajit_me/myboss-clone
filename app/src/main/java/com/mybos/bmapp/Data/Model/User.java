package com.mybos.bmapp.Data.Model;

import com.mybos.bmapp.Data.Base.BaseModel;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by EmLaAi on 12/01/2018.
 *
 * @author EmLaAi
 */

public class User extends RealmObject {
    private int id;
    private String name;
    private String group;
    private String role;
    private int old;

    //MARK : getter and setter method
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getOld() {
        return old;
    }

    public String getGroup() {
        return group;
    }

    public String getRole() {
        return role;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOld(int old) {
        this.old = old;
    }

    public void setRole(String role) {
        this.role = role;
    }

    /**
     * static function to save new user into realm database
     * @param user user need to save
     */
    public static void save(User user){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction((mRealm)->{
                mRealm.where(User.class).findAll().deleteAllFromRealm();
                realm.insert(user);
            });
        }
    }

    /**
     * static function to delete all user saved in database
     */
    public static void delete(){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.beginTransaction();
            realm.where(User.class).findAll().deleteAllFromRealm();
            realm.commitTransaction();
        }
    }

    /**
     * get User object from database
     * @return saved user
     */
    public static User get(){
        try(Realm realm = Realm.getDefaultInstance()){
            return realm.where(User.class).findFirst();
        }
    }
}
