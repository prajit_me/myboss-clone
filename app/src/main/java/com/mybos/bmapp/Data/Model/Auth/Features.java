package com.mybos.bmapp.Data.Model.Auth;

import android.content.Context;

import androidx.annotation.IntDef;
import androidx.annotation.NavigationRes;

import com.mybos.bmapp.Services.PreferenceManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 16/01/2018.
 *
 * @author EmLaAi
 */

public class Features {
    public static class TYPES{
        public static final int CASES = 0;
        public static final int CALENDAR = 1;
        public static final int MAINTENANCE_SCHEDULE = 2;
        public static final int BUILDING_APARTMENT = 3;
        public static final int BUILDING_ASSET = 4;
        public static final int BUILDING_BUDGET = 5;
        public static final int BUILDING_BY_LAW = 6;
        public static final int BUILDING_CATEGORY = 7;
        public static final int BUILDING_CHART_REPORT = 8;
        public static final int BUILDING_EC = 9;
        public static final int BUILDING_INSPECTION = 10;
        public static final int BUILDING_INVENTORY = 11;
        public static final int BUILDING_INVOICE_APPROVAL = 12;
        public static final int BUILDING_LIBRARY = 13;
        public static final int BUILDING_MANAGEMENT_REPORT = 14;
        public static final int BUILDING_METER = 15;
        public static final int BUILDING_PARKING_BREACH = 16;
        public static final int BUILDING_STAFF = 17;
        public static final int BUILDING_TIME_MACHINE = 18;
        public static final int BUILDING_WIKI = 19;
        public static final int RESIDENT = 20;
        public static final int KEY = 21;
        public static final int PARCEL = 22;
        public static final int CONTRACTOR = 23;
        public static final int CALCULATOR = 24;
        public static final int COMMUNITY_PORTAL = 25;
        public static final int BROADCAST = 26;
        public static final int SHIFT_LOG = 27;
        public static final int CHAT = 28;
        public static final int BUILDING = 29;
        public static final int CLOUD = 30;

        @IntDef({CASES,CALENDAR,MAINTENANCE_SCHEDULE,BUILDING_APARTMENT,BUILDING_ASSET,
                BUILDING_BUDGET,BUILDING_BY_LAW,BUILDING_CATEGORY,BUILDING_CHART_REPORT,
                BUILDING_EC,BUILDING_INSPECTION,BUILDING_INVENTORY,BUILDING_INVOICE_APPROVAL,
                BUILDING_LIBRARY,BUILDING_MANAGEMENT_REPORT,BUILDING_METER,BUILDING_PARKING_BREACH,
                BUILDING_STAFF,BUILDING_TIME_MACHINE,BUILDING_WIKI,RESIDENT,KEY,PARCEL,CONTRACTOR,CALCULATOR,COMMUNITY_PORTAL,BROADCAST,SHIFT_LOG,CHAT,BUILDING,CLOUD})
        @Retention(RetentionPolicy.SOURCE)
        public @interface FeatureHint{}

        public static int[] allValues = new int[]{CASES,CALENDAR,MAINTENANCE_SCHEDULE,BUILDING_APARTMENT,BUILDING_ASSET,
        BUILDING_BUDGET,BUILDING_BY_LAW,BUILDING_CATEGORY,BUILDING_CHART_REPORT,
        BUILDING_EC,BUILDING_INSPECTION,BUILDING_INVENTORY,BUILDING_INVOICE_APPROVAL,
        BUILDING_LIBRARY,BUILDING_MANAGEMENT_REPORT,BUILDING_METER,BUILDING_PARKING_BREACH,
        BUILDING_STAFF,BUILDING_TIME_MACHINE,BUILDING_WIKI,RESIDENT,KEY,PARCEL,CONTRACTOR,CALCULATOR,COMMUNITY_PORTAL,BROADCAST,SHIFT_LOG,CHAT,BUILDING,CLOUD};

        public static @FeatureHint Integer getFeature(Integer rawValue){
            if (null == rawValue){
                return null;
            }
            for(int feature:allValues){
                if (feature == rawValue){
                    return feature;
                }
            }
            return null;
        }
    }

    public static Features shared(Context context){
        if (null == instance){
            instance = new Features(context);
        }
        return instance;
    }
    private static Features instance;
    private List<Integer> featureList = new ArrayList<>();
    private Features(Context context){
        featureList.clear();
        for (Integer feature:TYPES.allValues){
            Boolean value = PreferenceManager.getFeatureValue(context,feature);
            if (value){
                featureList.add(feature);
            }
        }
    }
    public void parse(Context context,List<Integer> features){
        featureList.clear();
        PreferenceManager.clearFeature(context);
        for (Integer rawFeature:features){
            Integer feature = TYPES.getFeature(rawFeature);
            if (feature != null){
                PreferenceManager.saveFeature(context,feature);
                featureList.add(feature);
            }
        }
    }

    public Boolean hasFeature(@TYPES.FeatureHint Integer key){
        return featureList.contains(key);
    }
}
