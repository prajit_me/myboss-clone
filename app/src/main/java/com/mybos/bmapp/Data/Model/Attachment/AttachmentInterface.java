package com.mybos.bmapp.Data.Model.Attachment;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public interface AttachmentInterface {
    String getAttachmentTitle();
    String getSubTitle();
    String getLink();
    String getFileType();
    String getType();
}
