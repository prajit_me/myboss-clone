package com.mybos.bmapp.Data.Mapper.Weather;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Response.WeatherResponse;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Utils.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

/**
 * Created by EmLaAi on 01/04/2018.
 *
 * @author EmLaAi
 */
public class WeatherMapper extends BaseMapper {

    private final String WEATHER_URL_HEADER = "https://api.openweathermap.org/data/2.5/weather?APPID=1aa3932d536e5bc7c72dc51fa2f9b2f8&units=metric&";

    public void getWeatherForecast(Context context, Building building, SuccessCallback<WeatherResponse> onSuccess, FailureCallback onFailure){

        this.executeBackground(context,mContext->{
            if (null != mContext){
                String searchText = building.getSuburd() + "," + building.getState();
                String url = WEATHER_URL_HEADER;
                if (building.getwCode().equals("null")){
                    url=url+"q="+building.getSuburd()+","+building.getCountry();
                }else{
                    url=url+"id="+building.getwCode();
                }
                Log.e("check_weather",url);
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header, response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            String CurrentStatus = ((JSONObject) object).optJSONArray("weather").getJSONObject(0).optString("description");
                            CurrentStatus = StringUtils.firstCapitalized(CurrentStatus);

                            String statusText = CurrentStatus;
                            String temp = ((JSONObject) object).optJSONObject("main")
                                    .optString("temp");
                            if(temp.contains(".")) {
                                temp = temp.substring(0, temp.indexOf("."));
                            }
                            WeatherResponse weatherResponse = new WeatherResponse();
                            weatherResponse.status = statusText;
                            weatherResponse.temp = temp;
                            onSuccess.onSuccess(weatherResponse);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse|NullPointerException e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);

            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
