package com.mybos.bmapp.Data.Mapper.BuildingCase;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Asset.Asset;
import com.mybos.bmapp.Data.Model.Asset.AssetsList;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingAssetMapper extends BaseMapper {
    public void get(Context context,int buildingId,SuccessCallback<AssetsList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.BUILDING_ASSET),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                JSONArray assetArrayJSON = ((JSONObject) object).optJSONArray("data");
                                AssetsList assetsList = new AssetsList();
                                for (int i = 0; i < assetArrayJSON.length(); i++) {
                                    JSONObject assetJSON = assetArrayJSON.getJSONObject(i);
                                    Asset asset = new Asset();
                                    asset.setId(assetJSON.optInt("id"));
                                    asset.setBuilding(assetJSON.optInt("building"));
                                    asset.setBarcode(assetJSON.has("barcode") && !assetJSON.isNull("barcode")?assetJSON.optString("barcode"):null);
                                    asset.setName(assetJSON.optString("name"));
                                    asset.setDescription(assetJSON.optString("description"));
                                    asset.setCategory(assetJSON.optInt("category"));
                                    asset.setLocation(assetJSON.optString("location"));
                                    asset.setSerial(assetJSON.optString("serial"));
                                    asset.setMake(assetJSON.has("make") && !assetJSON.isNull("make")?assetJSON.optString("make"):null);
                                    asset.setModel(assetJSON.has("model") && !assetJSON.isNull("model")?assetJSON.optString("model"):null);
                                    asset.setValue(assetJSON.optInt("value"));
                                    asset.setAdded(assetJSON.optString("added"));
                                    asset.setWarantyTitle(assetJSON.has("warranty_title") && !assetJSON.isNull("warranty_title")?assetJSON.optString("warranty_title"):null);
                                    asset.setWarantyExpire(assetJSON.has("warranty_expire") && !assetJSON.isNull("warranty_expire")?assetJSON.optString("warranty_expire"):null);
                                    asset.setWarantyDocument(assetJSON.has("warranty_document") && !assetJSON.isNull("warranty_document")?assetJSON.optString("warranty_document"):null);
                                    asset.setUpdated(assetJSON.has("updated") && !assetJSON.isNull("updated")?assetJSON.optString("updated"):null);
                                    asset.setCategoryName(assetJSON.optString("categoryName"));
                                    assetsList.addAsset(asset);
                                }
                                onSuccess.onSuccess(assetsList);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void get(Context context,int buildingId,int categoryId,SuccessCallback<AssetsList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingAsset(VolleyUrlBuilder.URLPATH.BUILDING_ASSET_PER_CATEGORY),Integer.toString(buildingId),Integer.toString(categoryId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,response->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray){
                            AssetsList assetsList = new AssetsList();
                            for (int i = 0; i < ((JSONArray) object).length(); i++) {
                                JSONObject assetJSON = ((JSONArray) object).getJSONObject(i);
                                Asset asset = new Asset();
                                asset.setId(assetJSON.optInt("id"));
                                asset.setBuilding(assetJSON.optInt("building"));
                                asset.setBarcode(assetJSON.has("barcode") && !assetJSON.isNull("barcode")?assetJSON.optString("barcode"):null);
                                asset.setName(assetJSON.optString("name"));
                                asset.setDescription(assetJSON.optString("description"));
                                asset.setCategory(assetJSON.optInt("category"));
                                asset.setLocation(assetJSON.optString("location"));
                                asset.setSerial(assetJSON.optString("serial"));
                                asset.setMake(assetJSON.has("make") && !assetJSON.isNull("make")?assetJSON.optString("make"):null);
                                asset.setModel(assetJSON.has("model") && !assetJSON.isNull("model")?assetJSON.optString("model"):null);
                                asset.setValue(assetJSON.optInt("value"));
                                asset.setAdded(assetJSON.optString("added"));
                                asset.setWarantyTitle(assetJSON.has("warranty_title") && !assetJSON.isNull("warranty_title")?assetJSON.optString("warranty_title"):null);
                                asset.setWarantyExpire(assetJSON.has("warranty_expire") && !assetJSON.isNull("warranty_expire")?assetJSON.optString("warranty_expire"):null);
                                asset.setWarantyDocument(assetJSON.has("warranty_document") && !assetJSON.isNull("warranty_document")?assetJSON.optString("warranty_document"):null);
                                asset.setUpdated(assetJSON.has("updated") && !assetJSON.isNull("updated")?assetJSON.optString("updated"):null);
                                asset.setCategoryName(assetJSON.optString("categoryName"));
                                assetsList.addAsset(asset);
                            }
                            onSuccess.onSuccess(assetsList);
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
