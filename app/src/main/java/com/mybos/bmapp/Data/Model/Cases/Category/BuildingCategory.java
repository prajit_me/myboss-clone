package com.mybos.bmapp.Data.Model.Cases.Category;

import com.mybos.bmapp.Component.SpinnerAdapter.TreeLikeSpinnerItemInterface;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public interface BuildingCategory {
    int ROOT_ID = -3;
    int ALL_ID = -2;
    int BACK_ID = -1;
    public boolean isParent();
    void setName(String name);
    String getName();
    void setId(int id);
    int getId();
    BuildingCategory getChild(int id);
}
