package com.mybos.bmapp.Data.Model.Attachment;

import com.mybos.bmapp.Data.Base.BaseModel;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class AttachmentFile extends BaseModel implements AttachmentInterface {
    private int id;
    private int type;
    private String name;
    private String fileName;
    private String sized;
    private int size;
    private String added;
    private String fileType;
    private String url;
    private String subject;
    private String detail;
    private String expire;

    public void setId(int id) {
        this.id = id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void setSized(String sized) {
        this.sized = sized;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setExpire(String expire) {
        this.expire = expire;
    }

    @Override
    public String getAttachmentTitle() {
        return name;
    }

    @Override
    public String getSubTitle() {
        return "";
    }

    @Override
    public String getLink() {
        return url;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return String.valueOf(type);
    }

    public String getFileName() {
        return fileName;
    }

    public int getSize() {
        return size;
    }

    public String getSized() {
        return sized;
    }

    public String getAdded() {
        return added;
    }

    public String getFileType() {
        return fileType;
    }

    public String getSubject() {
        return subject;
    }

    public String getDetail() {
        return detail;
    }

    public String getExpire() {
        return expire;
    }
}
