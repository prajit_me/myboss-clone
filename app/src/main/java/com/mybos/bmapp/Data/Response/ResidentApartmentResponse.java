package com.mybos.bmapp.Data.Response;


import androidx.annotation.StringDef;

import com.mybos.bmapp.Data.Base.BaseModel;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentApartmentResponse extends BaseModel {
    private String url = "";
    private String emergencyContact = "";
    private String emergencyContactPhone = "";

    public void setUrl(String url) {
        this.url = url;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public void setEmergencyContactPhone(String emergencyContactPhone) {
        this.emergencyContactPhone = emergencyContactPhone;
    }

    public String getUrl() {
        return url;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public String getEmergencyContactPhone() {
        return emergencyContactPhone;
    }
}
