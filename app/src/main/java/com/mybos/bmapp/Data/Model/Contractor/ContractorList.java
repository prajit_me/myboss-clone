package com.mybos.bmapp.Data.Model.Contractor;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.Data.Model.Filter.TypeList;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class ContractorList extends BaseModel
        implements KeepTrackSelectedItemList<Integer>,
                    Parcelable,
                    SpinnerSourceCompatible {
    List<Contractor> contractorList = new ArrayList<>();
    List<Integer> selectedId = new ArrayList<>();

    private static ContractorList contractorListApply = new ContractorList();

    public ContractorList(){}

    public ContractorList(ContractorList ob){
        contractorList = new ArrayList<>(ob.getContractorList());
        selectedId = new ArrayList<>(ob.getSelectedId());
    }

    protected ContractorList(Parcel in) {
        contractorList = in.createTypedArrayList(Contractor.CREATOR);
        in.readList(selectedId,Integer.class.getClassLoader());
    }

    public static final Creator<ContractorList> CREATOR = new Creator<ContractorList>() {
        @Override
        public ContractorList createFromParcel(Parcel in) {
            return new ContractorList(in);
        }

        @Override
        public ContractorList[] newArray(int size) {
            return new ContractorList[size];
        }
    };

    public void addContrator(Contractor contractor){
        this.contractorList.add(contractor);
    }


    @Override
    public void selectId(Integer id) {
        if (selectedId.contains(id)){
            return;
        }else {
            selectedId.add(id);
        }

    }

    @Override
    public void deselectId(Integer id) {
        if (selectedId.contains((Integer)id)){
            selectedId.remove((Integer)id);
        }
    }

    @Override
    public List<BaseModel> getSelectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Contractor contractor:contractorList){
            if (selectedId.contains(contractor.getId())){
                returnValue.add(contractor);
            }
        }
        return returnValue;
    }

    public List<Integer> getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(List<Integer> selectedId) {
        this.selectedId = selectedId;
    }

    public void setSelectedId(RealmList<Integer> realmList) {
        this.selectedId = new ArrayList<>();
        for (Integer id:realmList
        ) {
            selectedId.add(id);
        }
    }

    @Override
    public List<BaseModel> getUnselectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Contractor contractor:contractorList){
            if (!selectedId.contains(contractor.getId())){
                returnValue.add(contractor);
            }
        }
        return returnValue;
    }

    public List<String> getSelectedName(){
        List<String> nameList = new ArrayList<>();
        for (Contractor contractor:contractorList){
            if(selectedId.contains(contractor.getId())) {
                nameList.add(contractor.getCompany());
            }
        }
        return nameList;
    }
    public Contractor getSelectedItemAt(int position){
        if (position >= this.selectedId.size()){return null;}
        int selectedId = this.selectedId.get(position);
        return getContractor(selectedId);
    }
    public Contractor getContractor(int id){
        for (int i = 0; i < contractorList.size(); i++) {
            if (contractorList.get(i).getId() == id){
                return contractorList.get(i);
            }
        }
        return null;
    }

    public List<Contractor> getAll(){
        return contractorList;
    }

    public boolean checkSelected(int id){
        return selectedId.contains(id);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(contractorList);
        dest.writeList(selectedId);
    }

    @Override
    public List<String> getSource() {
        return null;
    }

    @Override
    public int count() {
        return contractorList.size();
    }

    @Override
    public BaseModel getItem(int position) {
        return contractorList.get(position);
    }

    @Override
    public String getTitle(int position) {
        return contractorList.get(position).getCompany();
    }

    public static ContractorList getContractorListApply() {
        return contractorListApply;
    }

    public static void setContractorListApply(ContractorList contractorListApply) {
        ContractorList.contractorListApply = contractorListApply;
    }

    public List<Contractor> getContractorList() {
        return contractorList;
    }

    public void setContractorList(List<Contractor> contractorList) {
        this.contractorList = contractorList;
    }
}
