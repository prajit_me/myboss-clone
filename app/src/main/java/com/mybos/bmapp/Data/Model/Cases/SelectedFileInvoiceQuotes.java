package com.mybos.bmapp.Data.Model.Cases;

import com.mybos.bmapp.Data.Base.BaseModel;


public class SelectedFileInvoiceQuotes extends BaseModel {

    private static String mime;
    private static String name;
    private static byte[] bytes;

    public static void clearData(){
        mime = null;
        name = null;
        bytes = null;
    }

    public static String getMime() {
        return mime;
    }

    public static void setMime(String mime) {
        SelectedFileInvoiceQuotes.mime = mime;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        SelectedFileInvoiceQuotes.name = name;
    }

    public static byte[] getBytes() {
        return bytes;
    }

    public static void setBytes(byte[] bytes) {
        SelectedFileInvoiceQuotes.bytes = bytes;
    }
}
