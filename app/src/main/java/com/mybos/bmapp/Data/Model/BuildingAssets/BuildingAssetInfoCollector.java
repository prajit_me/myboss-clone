package com.mybos.bmapp.Data.Model.BuildingAssets;

import java.util.ArrayList;

public class BuildingAssetInfoCollector {

    private static ArrayList<BuildingAsset> BuildingAssetList = new ArrayList<>();

    private static BuildingAsset SelectedBuildingAsset = new BuildingAsset();

    private static ArrayList<BuildingAssetHistory> AssetHistoryList = new ArrayList<>();

    public static void setAssetHistoryList(ArrayList<BuildingAssetHistory> assetHistoryList) {
        AssetHistoryList = assetHistoryList;
    }

    public static ArrayList<BuildingAssetHistory> getAssetHistoryList() {
        return AssetHistoryList;
    }

    public static void setBuildingAssetList(ArrayList<BuildingAsset> buildingAssetList) {
        BuildingAssetList = buildingAssetList;
    }

    public static void addToBuildingAssetList(BuildingAsset buildingAsset) {
        BuildingAssetList.add(buildingAsset);
    }

    public static ArrayList<BuildingAsset> getBuildingAssetList() {
        return BuildingAssetList;
    }

    public static void setSelectedBuildingAsset(BuildingAsset selectedBuildingAsset) {
        SelectedBuildingAsset = selectedBuildingAsset;
    }

    public static BuildingAsset getSelectedBuildingAsset() {
        return SelectedBuildingAsset;
    }

}
