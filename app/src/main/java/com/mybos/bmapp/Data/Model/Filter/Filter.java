package com.mybos.bmapp.Data.Model.Filter;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;

import java.util.ArrayList;
import java.util.List;

public class Filter extends BaseModel implements KeepTrackSelectedItemList<String>, SpinnerSourceCompatible {

    private String name = "";
    private String id = "";
    private ArrayList children = new ArrayList<Filter>();

    private static ArrayList<Filter> AssetsFilterList = new ArrayList<>();
    private static ArrayList<Filter> CaseFilterList = new ArrayList<>();
    private static ArrayList<Filter> ContractorsFilterList = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList getChildren() {
        return children;
    }

    public void setChildren(ArrayList children) {
        this.children = children;
    }

    public static ArrayList<Filter> getAssetsFilterList() {
        return AssetsFilterList;
    }

    public static void setAssetsFilterList(ArrayList<Filter> assetsFilterList) {
        AssetsFilterList = assetsFilterList;
    }

    public static ArrayList<Filter> getContractorsFilterList() {
        return ContractorsFilterList;
    }

    public static void setContractorsFilterList(ArrayList<Filter> contractorsFilterList) {
        ContractorsFilterList = contractorsFilterList;
    }


    public static ArrayList<Filter> getCaseFilterList() {
        return CaseFilterList;
    }

    public static void setCaseFilterList(ArrayList<Filter> caseFilterList) {
        CaseFilterList = caseFilterList;
    }

    @Override
    public void selectId(String id) {

    }

    @Override
    public void deselectId(String id) {

    }

    @Override
    public List<BaseModel> getSelectedItem() {
        return null;
    }

    @Override
    public List<BaseModel> getUnselectedItem() {
        return null;
    }

    @Override
    public List<String> getSource() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public BaseModel getItem(int position) {
        return null;
    }

    @Override
    public String getTitle(int position) {
        return null;
    }
}
