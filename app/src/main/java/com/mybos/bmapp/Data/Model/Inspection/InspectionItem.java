package com.mybos.bmapp.Data.Model.Inspection;

import android.net.Uri;

import com.mybos.bmapp.Activity.Inspection.InspectionInfoCollector;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Functional.PhotoModel;

/**
 * Created by EmLaAi on 18/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionItem extends BaseModel {
    int id;
    int area;
    String name;
    int inspection;
    String comment="";
    PhotoModel image = null;
    String photoPath="";
    private int isChecked = 0;

    public int getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(int isChecked) {
        this.isChecked = isChecked;
    }

    public void onCheckClick() {
        if (isChecked == 2) {
            isChecked = 1;
        }
        else if ((isChecked == 1)){
            isChecked = 2;
        }
        if (isChecked == 0){
            isChecked = 1;
        }
        InspectionInfoCollector.updateInspectionItemList(this);
    }

    private boolean isFinish = false;

    public void setId(int id) {
        this.id = id;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setInspection(int inspection) {
        this.inspection = inspection;
    }


    public int getInspection() {
        return inspection;
    }


    public String getName() {
        return name;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public void setImage(PhotoModel image) {
        this.image = image;
    }

    public String getComment() {
        return comment;
    }

    public PhotoModel getImage() {
        return image;
    }

    public boolean isFinish() {
        if (isChecked == 1) {
            isFinish = true;
        }
        else {
            isFinish = false;
        }
        return isFinish;
    }

    public int getId() {
        return id;
    }


    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

}
