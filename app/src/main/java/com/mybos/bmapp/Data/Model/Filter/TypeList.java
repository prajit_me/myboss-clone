package com.mybos.bmapp.Data.Model.Filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseType;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

public class TypeList extends BaseModel implements KeepTrackSelectedItemList<Integer>, Parcelable, SpinnerSourceCompatible {
    List<Type> typelist = new ArrayList<>();
    List<Integer> selectedId = new ArrayList<>();

    private static TypeList typelistApply = new TypeList();
    private static TypeList statuslistApply = new TypeList();

    public TypeList(){

    }

    public TypeList(TypeList ob){
        typelist = new ArrayList<>(ob.getTypelist());
        selectedId = new ArrayList<>(ob.getSelectedId());
    }

    public TypeList(List<BuildingCaseType> list){
        if (null!=list){
            for (BuildingCaseType type:list
                 ) {
                Type newType = new Type(type.getName(),type.getId());
                typelist.add(newType);
            }
        }
    }

    protected TypeList(Parcel in) {
        typelist = in.createTypedArrayList(Type.CREATOR);
        in.readList(selectedId,Integer.class.getClassLoader());
    }

    public static final Creator<TypeList> CREATOR = new Creator<TypeList>() {
        @Override
        public TypeList createFromParcel(Parcel in) {
            return new TypeList(in);
        }

        @Override
        public TypeList[] newArray(int size) {
            return new TypeList[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public void selectId(Integer id) {
        if (selectedId.contains(id)){
            return;
        }else {
            selectedId.add(id);
        }
    }

    @Override
    public void deselectId(Integer id) {
        if (selectedId.contains((Integer)id)){
            selectedId.remove((Integer)id);
        }
    }

    @Override
    public List<BaseModel> getSelectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Type type:typelist){
            if (selectedId.contains(type.getId())){
                returnValue.add(type);
            }
        }
        return returnValue;
    }


    @Override
    public List<BaseModel> getUnselectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Type type:typelist){
            if (!selectedId.contains(type.getId())){
                returnValue.add(type);
            }
        }
        return returnValue;
    }

    public List<String> getSelectedName(){
        List<String> nameList = new ArrayList<>();
        for (Type type:typelist){
            if(selectedId.contains(type.getId())) {
                nameList.add(type.getName());
            }
        }
        return nameList;
    }

    @Override
    public List<String> getSource() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public BaseModel getItem(int position) {
        return null;
    }

    @Override
    public String getTitle(int position) {
        return null;
    }

    public List<Type> getTypelist() {
        return typelist;
    }

    public void setTypelist(List<Type> typelist) {
        this.typelist = typelist;
    }

    public List<Integer> getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(List<Integer> selectedId) {
        this.selectedId = selectedId;
    }

    public void setSelectedId(RealmList<Integer> realmList) {
        this.selectedId = new ArrayList<>();
        for (Integer id:realmList
        ) {
            selectedId.add(id);
        }
    }

    public static TypeList getTypelistApply() {
        return typelistApply;
    }

    public static void setTypelistApply(TypeList typelistApply) {
        TypeList.typelistApply = typelistApply;
    }

    public static TypeList getStatuslistApply() {
        return statuslistApply;
    }

    public static void setStatuslistApply(TypeList statuslistApply) {
        TypeList.statuslistApply = statuslistApply;
    }
}
