package com.mybos.bmapp.Data.Model.Auth;

import android.content.Context;

import androidx.annotation.StringDef;


import com.mybos.bmapp.Services.PreferenceManager;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 16/01/2018.
 *
 * @author EmLaAi
 */

public class Permission {
    public static class Types{
        public static final String Case = "case";
        public static final String Maintenance = "maintenance";
        public static final String Resident = "resident";
        public static final String Key = "key";
        public static final String Parcel = "parcel";
        public static final String Contractor = "contractor";
        public static final String Apartment = "apartment";
        public static final String Asset = "asset";
        public static final String Budget = "budget";
        public static final String Law = "law";
        public static final String Category = "category";
        public static final String Inspection = "inspection";
        public static final String Inventory = "inventory";
        public static final String Library = "library";
        public static final String Meter = "meter";
        public static final String Parking = "parking";
        public static final String Booking = "booking";
        public static final String Staff = "staff";

        @StringDef({Case,Maintenance,Resident,Key,Parcel,Contractor,Apartment,Asset,Budget,Law,Category,Inspection,Inventory,Library,Meter,Parking,Booking,Staff})
        @Retention(RetentionPolicy.SOURCE)
        public @interface PermissionHint{}
        public static List<String> allPermissionValue = Arrays.asList(Case,Maintenance,Resident,Key,Parcel,Contractor,Apartment,Asset,
                Budget,Law,Category,Inspection,Inventory,Library,Meter,Parking,Booking,Staff);
        public static @PermissionHint String getPermission(String value){
            for (String permission:allPermissionValue) {
                if (permission.equals(value)){
                    return permission;
                }
            }
            return null;
//            if (Types.Case.equals(value)){
//                return Types.Case;
//            }else if(Types.Maintenance.equals(value)){
//                return Types.Maintenance;
//            }
//            else if(Types.Resident.equals(value)){
//                return Types.Resident;
//            }
//            else if(Types.Key.equals(value)){
//                return Types.Key;
//            }
//            else if(Types.Parcel.equals(value)){
//                return Types.Parcel;
//            }
//            else if(Types.Contractor.equals(value)){
//                return Types.Contractor;
//            }
//            else if(Types.Apartment.equals(value)){
//                return Types.Apartment;
//            }
//            else if(Types.Asset.equals(value)){
//                return Types.Asset;
//            }
//            else if(Types.Budget.equals(value)){
//                return Types.Budget;
//            }
//            else if(Types.Law.equals(value)){
//                return Types.Law;
//            }
//            else if(Types.Category.equals(value)){
//                return Types.Category;
//            }
//            else if(Types.Inspection.equals(value)){
//                return Types.Inspection;
//            }
//            else if(Types.Inventory.equals(value)){
//                return Types.Inventory;
//            }
//            else if(Types.Library.equals(value)){
//                return Types.Library;
//            }
//            else if(Types.Meter.equals(value)){
//                return Types.Meter;
//            }
//            else if(Types.Parking.equals(value)){
//                return Types.Parking;
//            }
//            else if(Types.Booking.equals(value)){
//                return Types.Booking;
//            }
//            else if(Types.Staff.equals(value)){
//                return Types.Staff;
//            }
//            return null;
        }
    }


    public static class Item{
        String key;
        boolean isView = false,isEdit = false,isDelete = false;
        public Item(@Permission.Types.PermissionHint String key,String value){
            this.key = key;
            if (value.length() > 0) {
                Character i = value.charAt(0);
                isView = (i == '1');
                value = value.substring(1);
            }
            if (value.length() > 0 ){
                Character i = value.charAt(0);
                isEdit = (i == '1');
                value = value.substring(1);
            }
            if (value.length() > 0 ){
                Character i = value.charAt(0);
                isDelete = (i == '1');
            }
        }
        public String getValue(){
            String returnValue = "";
            returnValue += isView ? "1" : "0";
            return "";
        }

        public boolean isView() {
            return isView;
        }

        public boolean isEdit() {
            return isEdit;
        }

        public boolean isDelete() {
            return isDelete;
        }
    }
    public static Permission shared(Context context){
        if (null == instance){
            instance = new Permission(context);
        }
        return instance;
    }
    private static Permission instance;
    private Permission(Context context){
        permissionList.clear();
        for (String permission:Types.allPermissionValue){
            String value = PreferenceManager.getPermissionValue(context,permission);
            if (null != value){
                Item item = new Item(permission,value);
                permissionList.add(item);
            }
        }
    }
    private List<Item> permissionList = new ArrayList<>();
    public void parse(Context context, Map<String,String> value){
        permissionList.clear();
        PreferenceManager.clearPermission(context);
        for(Map.Entry<String,String> set:value.entrySet()){
            String permissionName = Types.getPermission(set.getKey());
            if(permissionName != null){
                Item item = new Item(permissionName,set.getValue());
                PreferenceManager.savePermission(context,permissionName,item.getValue());
                permissionList.add(item);
            }

        }
    }
    public Item getPermission(@Types.PermissionHint String key){
        for (Item item:permissionList){
            if (key.equals(item.key)){
                return item;
            }
        }
        return null;
    }
}
