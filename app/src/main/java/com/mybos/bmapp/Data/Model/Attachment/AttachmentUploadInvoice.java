package com.mybos.bmapp.Data.Model.Attachment;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;

import java.text.NumberFormat;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class AttachmentUploadInvoice extends BaseModel implements AttachmentInterface {
    private String number;
    private Double amount;
    private String comment;
    private String type = "invoice";
    private String date;
    private Contractor contractor;
    private AttachmentFile file;

    public void setNumber(String number) {
        this.number = number;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setFile(AttachmentFile file) {
        this.file = file;
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    @Override
    public String getAttachmentTitle() {
        return contractor != null ? contractor.getCompany() : "";
    }

    @Override
    public String getSubTitle() {
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
        numberFormat.setMaximumFractionDigits(0);
        return numberFormat.format(amount);
    }

    @Override
    public String getLink() {
        return file != null ? file.getLink() : "";
    }

    @Override
    public String getFileType() {
        return this.file != null ? this.file.getFileType() : null;
    }

    public Double getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    public String getComment() {
        return comment;
    }

    public String getNumber() {
        return number;
    }

    public AttachmentFile getFile() {
        return file;
    }

    public Contractor getContractor() {
        return contractor;
    }
}
