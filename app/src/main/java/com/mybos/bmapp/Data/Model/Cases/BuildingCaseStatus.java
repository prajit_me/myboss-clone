package com.mybos.bmapp.Data.Model.Cases;

import com.mybos.bmapp.Data.Base.BaseModel;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseStatus extends BaseModel {
    private String name;
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
