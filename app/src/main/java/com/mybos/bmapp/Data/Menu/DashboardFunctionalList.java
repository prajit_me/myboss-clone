package com.mybos.bmapp.Data.Menu;

import android.content.Context;
import android.graphics.drawable.Drawable;

import androidx.annotation.IntDef;
import androidx.annotation.IntegerRes;

import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.R;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by EmLaAi on 01/04/2018.
 *
 * @author EmLaAi
 */
public class DashboardFunctionalList extends BaseModel {
    public static class ItemId{
        public static final int CASE_ID = 0;
        public static final int INSPECTION_ID = 1;
        public static final int RESIDENT_ID = 2;
        public static final int CONTRACTOR_ID = 3;
        public static final int LIBRARY_ID = 4;
        public static final int CALENDAR_ID = 5;
        public static final int BROADCAST_ID = 6;
        public static final int KEY_ID = 7;
        public static final int ASSET_ID = 8;
        @IntDef({CASE_ID,INSPECTION_ID,RESIDENT_ID,
                CONTRACTOR_ID, LIBRARY_ID, CALENDAR_ID,
                BROADCAST_ID, KEY_ID, ASSET_ID})
        @Retention(RetentionPolicy.SOURCE)
        public @interface ItemEnumId{}
    }
    public class Item{
        int id;
        Drawable icon;
        String title;

        private Item(){ }

        public Item(@ItemId.ItemEnumId int id, Drawable drawable, String title){
            this.id = id;
            this.icon = drawable;
            this.title = title;
        }

        public Drawable getIcon() {
            return icon;
        }

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }
    }
    List<Item> itemList;
    public DashboardFunctionalList(Context context){
        itemList = new ArrayList<>();
                if (Offlinemode.checkNetworkConnection(context)){
                    Item cases = new Item(ItemId.CASE_ID,context.getDrawable(R.drawable.ic_build_black_24dp),context.getString(R.string.left_menu_item_cases));
                    Item inspection = new Item(ItemId.INSPECTION_ID,context.getDrawable(R.drawable.ic_inspection),context.getString(R.string.left_menu_item_inspections));
                    Item resident = new Item(ItemId.RESIDENT_ID,context.getDrawable(R.drawable.ic_person_black_24dp),context.getString(R.string.left_menu_item_residents));

                    Item contractor = new Item(ItemId.CONTRACTOR_ID,context.getDrawable(R.drawable.ic_contractor),context.getString(R.string.left_menu_item_contractors));
                    Item library = new Item(ItemId.LIBRARY_ID,context.getDrawable(R.drawable.ic_library),context.getString(R.string.left_menu_item_library));
                    Item calendar = new Item(ItemId.CALENDAR_ID,context.getDrawable(R.drawable.ic_calendar),context.getString(R.string.left_menu_item_calendar));

                    Item broadcast = new Item(ItemId.BROADCAST_ID,context.getDrawable(R.drawable.ic_broadcast),context.getString(R.string.left_menu_item_broadcast));
                    Item key = new Item(ItemId.KEY_ID,context.getDrawable(R.drawable.ic_key),context.getString(R.string.left_menu_item_key));
                    Item asset = new Item(ItemId.ASSET_ID,context.getDrawable(R.drawable.ic_asset),context.getString(R.string.left_menu_item_asset));

                    itemList.addAll(Arrays.asList(cases, inspection, resident,
                            contractor, library, calendar,
                            broadcast, key, asset));
                }else{
                    Item cases = new Item(ItemId.CASE_ID,context.getDrawable(R.drawable.ic_build_black_24dp_gray),context.getString(R.string.left_menu_item_cases));
                    Item inspection = new Item(ItemId.INSPECTION_ID,context.getDrawable(R.drawable.ic_inspection),context.getString(R.string.left_menu_item_inspections));
                    Item resident = new Item(ItemId.RESIDENT_ID,context.getDrawable(R.drawable.ic_person_black_24dp_gray),context.getString(R.string.left_menu_item_residents));

                    Item contractor = new Item(ItemId.CONTRACTOR_ID,context.getDrawable(R.drawable.ic_contractor_gray),context.getString(R.string.left_menu_item_contractors));
                    Item library = new Item(ItemId.LIBRARY_ID,context.getDrawable(R.drawable.ic_library_gray),context.getString(R.string.left_menu_item_library));
                    Item calendar = new Item(ItemId.CALENDAR_ID,context.getDrawable(R.drawable.ic_calendar_gray),context.getString(R.string.left_menu_item_calendar));

                    Item broadcast = new Item(ItemId.BROADCAST_ID,context.getDrawable(R.drawable.ic_broadcast_gray),context.getString(R.string.left_menu_item_broadcast));
                    Item key = new Item(ItemId.KEY_ID,context.getDrawable(R.drawable.ic_key_gray),context.getString(R.string.left_menu_item_key));
                    Item asset = new Item(ItemId.ASSET_ID,context.getDrawable(R.drawable.ic_asset_gray),context.getString(R.string.left_menu_item_asset));

                    itemList.addAll(Arrays.asList(cases, inspection, resident,
                            contractor, library, calendar,
                            broadcast, key, asset));
                }
            }

    public Item getItem(int position){
        if (position < itemList.size()){
            return itemList.get(position);
        }
        return null;
    }
}
