package com.mybos.bmapp.Data.Model.Cases;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by EmLaAi on 07/04/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseJobArea extends BaseModel implements SpinnerSourceCompatible {
    public static final int PRIVATE_LOT_ID = 0;
    public static final int COMMON_ASSET_ID = 1;
    public static final int COMMON_NON_ASSET_ID = 2;
    List<JobAreaItem> items;
    public BuildingCaseJobArea(Context context){
        items = new ArrayList<>();
        JobAreaItem privatelot = new JobAreaItem(PRIVATE_LOT_ID,context.getString(R.string.new_case_job_area_private_lot));
        JobAreaItem commonAsset = new JobAreaItem(COMMON_ASSET_ID,context.getString(R.string.new_case_job_area_common_asset));
        JobAreaItem commonNonAsset = new JobAreaItem(COMMON_NON_ASSET_ID,context.getString(R.string.new_case_job_area_common_non_asset));
        items.addAll(Arrays.asList(commonAsset,commonNonAsset,privatelot));
    }

    public int getPositionForId(int id){
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).id == id){
                return i;
            }
        }
        return 0;
    }
    @Override
    public List<String> getSource() {
        List<String> names = new ArrayList<>();
        for (JobAreaItem item:items){
            names.add(item.name);
        }
        return names;
    }

    @Override
    public int count() {
        return items.size();
    }

    @Override
    public BaseModel getItem(int position) {
        return items.get(position);
    }

    @Override
    public String getTitle(int position) {
        return items.get(position).name;
    }

    public class JobAreaItem extends BaseModel{
        int id;
        String name;
        JobAreaItem(int id,String name){
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }
    }
}
