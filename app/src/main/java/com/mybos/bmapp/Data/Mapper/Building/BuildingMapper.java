package com.mybos.bmapp.Data.Mapper.Building;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * Created by EmLaAi on 28/02/2018.
 *
 * @author EmLaAi
 */

public class BuildingMapper extends BaseMapper {
    private static BuildingList USER_BUILDING_LIST;

    public void get(Context context,SuccessCallback<BuildingList> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if(null != mContext){
                String url = new VolleyUrlBuilder(2).parseUrl(VolleyUrlBuilder.URLPATH.building(VolleyUrlBuilder.URLPATH.BUILDING)).build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        USER_BUILDING_LIST = new BuildingList();
                        ArrayList<String> listName = new ArrayList<>();
                        BuildingList tempBuildingList = new BuildingList();
                        if (object instanceof JSONObject){
                            if(((JSONObject) object).getInt("state") == 200){
                                BuildingList list = new BuildingList();
                                JSONArray data = ((JSONObject) object).getJSONArray("building");
                                for (int i = 0; i < data.length(); i++) {
                                    Building building = new Building();
                                    building.setState(data.getJSONObject(i).optString("state"));
                                    building.setCountry(data.getJSONObject(i).optString("country"));
                                    building.setName(data.getJSONObject(i).optString("name"));
                                    building.setPhotoName(data.getJSONObject(i).optString("photoName"));
                                    building.setPostCode(data.getJSONObject(i).optString("postcode"));
                                    building.setAddress(data.getJSONObject(i).optString("address"));
                                    building.setUid(data.getJSONObject(i).optString("uid"));
                                    building.setPrStatus(data.getJSONObject(i).optInt("pr_status"));
                                    building.setLogin(data.getJSONObject(i).optInt("login"));
                                    building.setPcStatus(data.getJSONObject(i).optInt("pc_status"));
                                    building.setStatus(data.getJSONObject(i).optInt("status"));
                                    building.setId(data.getJSONObject(i).optInt("id"));
                                    building.setTimezone(data.getJSONObject(i).optString("timezone"));
                                    if (!data.getJSONObject(i).optString("photo").equals("false")){
                                        building.setPhoto(data.getJSONObject(i).optString("photo"));
                                    }
                                    building.setOwner(data.getJSONObject(i).optInt("owner"));
                                    building.setLogo(data.getJSONObject(i).optString("logo"));
                                    building.setSuburd(data.getJSONObject(i).optString("suburb"));
                                    building.setPlan(data.getJSONObject(i).optString("plan"));
                                    building.setwCode(data.getJSONObject(i).optString("wCode"));
                                    listName.add(building.getName());
                                    tempBuildingList.addBuilding(building);
                                }
                                // sort list
                                Collections.sort(listName);
                                for (String name:listName
                                     ) {
                                    for (Building building:tempBuildingList.getBuildingList()
                                         ) {
                                        if (building.getName().equals(name)){
                                            list.addBuilding(building);
                                            USER_BUILDING_LIST.addBuilding(building);
                                            break;
                                        }
                                    }
                                }
                                onSuccess.onSuccess(list);
                            }else{
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),
                                        ((JSONObject) object).has("message")?((JSONObject) object).getString("message"):"There isn't message"));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException | InvalidResponse  e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public static BuildingList getUserBuildingList(){
        return USER_BUILDING_LIST;
    }

    public void changeBuilding(Context context,int buildingId,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.building(VolleyUrlBuilder.URLPATH.BUILDING_INFO),
                                Integer.toString(buildingId))
                        .build();
                Map<String,String> headers = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).put(url,headers,null,
                        (response)-> onSuccess.onSuccess(null),
                        onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getVersionFromStore(Context context,SuccessCallback<String> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String storeVersion = "--";
                VersionChecker versionChecker = new VersionChecker();
                try {
                    storeVersion = versionChecker.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                onSuccess.onSuccess(storeVersion);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public class VersionChecker extends AsyncTask<String, String, String> {

        private String newVersion;

        @Override
        protected String doInBackground(String... params) {

            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=com.mybos.bmapp&hl=en")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;
        }
    }

}


