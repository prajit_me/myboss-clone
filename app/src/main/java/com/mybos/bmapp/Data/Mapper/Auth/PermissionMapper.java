package com.mybos.bmapp.Data.Mapper.Auth;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Auth.Permission;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by EmLaAi on 17/01/2018.
 *
 * @author EmLaAi
 */

public class PermissionMapper extends BaseMapper {
    public void get(int buildingId,Context context,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
//        Thread thread = new Thread(new MapperRunabble(context){
//            @Override
//            public void run() {
//                Context mContext = contextWeakReference.get();
//                if (null != mContext){
//                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
//                    String url = new VolleyUrlBuilder()
//                            .parseUrl(VolleyUrlBuilder.URLPATH.auth(VolleyUrlBuilder.URLPATH.PERMISSIONS),Integer.toString(buildingId))
//                            .build();
//                    Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
//                    VolleyRequest.shared(mContext).get(url,header,(response)->{
//                        Logger.shared().o(response);
//                    },onFailure::onFailure);
//                }
//            }
//        });
//        thread.start();
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.auth(VolleyUrlBuilder.URLPATH.PERMISSIONS),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url, header, (response) -> {
                    try {
                        Object obj = new JSONTokener(response).nextValue();
                        if (obj instanceof JSONObject){
                            Map<String,String> parseJson = new HashMap<>();
                            Iterator<String> iterator = ((JSONObject) obj).keys();
                            while (iterator.hasNext()){
                                String key = iterator.next();
                                String value = ((JSONObject) obj).getString(key);
                                parseJson.put(key,value);
                            }
                            Permission.shared(mContext).parse(mContext,parseJson);
                        }else{
                            throw new InvalidResponse();
                        }
                        onSuccess.onSuccess(null);
                    } catch (JSONException | InvalidResponse e) {
                        e.printStackTrace();
                        onFailure.onFailure(e);
                    }
                }, onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
