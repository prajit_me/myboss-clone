package com.mybos.bmapp.Data.Model.BuildingAssets;

public class BuildingAssetHistory {

    private String Added = "";
    private int Area = 0;
    private String AreaText = "";
    private int BuildingID = 0;
    private String CompletionDate = "";
    private String Detail = "";
    private String Due = "";
    private String HistoryNote = "";
    private String History_Note = "";
    private int HistoryID = 0;
    private int isDuplicate = 0;
    private int isRead = 0;
    private int isReport = 0;
    private String LoggedBy = "";
    private int HistoryNumber = 0;
    private int Priority = 0;
    private String PriorityText = "";
    private int sStatus = 0;
    private int Starred = 0;
    private String HistoryStart = "";
    private int Status = 0;
    private String StatusText = "";
    private String SubStatus = "";
    private String Subject = "";
    private int Sync = 0;
    private int HistoryType = 0;
    private String TypeText = "";
    private String Updated = "";
    private String WorkOrder = "";

    public String getAdded() {
        return Added;
    }

    public void setAdded(String added) {
        Added = added;
    }

    public int getArea() {
        return Area;
    }

    public void setArea(int area) {
        Area = area;
    }

    public String getAreaText() {
        return AreaText;
    }

    public void setAreaText(String areaText) {
        AreaText = areaText;
    }

    public int getBuildingID() {
        return BuildingID;
    }

    public void setBuildingID(int buildingID) {
        BuildingID = buildingID;
    }

    public String getCompletionDate() {
        return CompletionDate;
    }

    public void setCompletionDate(String completionDate) {
        CompletionDate = completionDate;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getDue() {
        return Due;
    }

    public void setDue(String due) {
        Due = due;
    }

    public String getHistoryNote() {
        return HistoryNote;
    }

    public void setHistoryNote(String historyNote) {
        HistoryNote = historyNote;
    }

    public String getHistory_Note() {
        return History_Note;
    }

    public void setHistory_Note(String history_Note) {
        History_Note = history_Note;
    }

    public int getHistoryID() {
        return HistoryID;
    }

    public void setHistoryID(int historyID) {
        HistoryID = historyID;
    }

    public int getIsDuplicate() {
        return isDuplicate;
    }

    public void setIsDuplicate(int isDuplicate) {
        this.isDuplicate = isDuplicate;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getIsReport() {
        return isReport;
    }

    public void setIsReport(int isReport) {
        this.isReport = isReport;
    }

    public String getLoggedBy() {
        return LoggedBy;
    }

    public void setLoggedBy(String loggedBy) {
        LoggedBy = loggedBy;
    }

    public int getHistoryNumber() {
        return HistoryNumber;
    }

    public void setHistoryNumber(int historyNumber) {
        HistoryNumber = historyNumber;
    }

    public int getPriority() {
        return Priority;
    }

    public void setPriority(int priority) {
        Priority = priority;
    }

    public String getPriorityText() {
        return PriorityText;
    }

    public void setPriorityText(String priorityText) {
        PriorityText = priorityText;
    }

    public int getsStatus() {
        return sStatus;
    }

    public void setsStatus(int sStatus) {
        this.sStatus = sStatus;
    }

    public int getStarred() {
        return Starred;
    }

    public void setStarred(int starred) {
        Starred = starred;
    }

    public String getHistoryStart() {
        return HistoryStart;
    }

    public void setHistoryStart(String historyStart) {
        HistoryStart = historyStart;
    }

    public int getStatus() {
        return Status;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public String getSubStatus() {
        return SubStatus;
    }

    public void setSubStatus(String subStatus) {
        SubStatus = subStatus;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public int getSync() {
        return Sync;
    }

    public void setSync(int sync) {
        Sync = sync;
    }

    public int getHistoryType() {
        return HistoryType;
    }

    public void setHistoryType(int historyType) {
        HistoryType = historyType;
    }

    public String getTypeText() {
        return TypeText;
    }

    public void setTypeText(String typeText) {
        TypeText = typeText;
    }

    public String getUpdated() {
        return Updated;
    }

    public void setUpdated(String updated) {
        Updated = updated;
    }

    public String getWorkOrder() {
        return WorkOrder;
    }

    public void setWorkOrder(String workOrder) {
        WorkOrder = workOrder;
    }
}
