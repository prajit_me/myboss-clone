package com.mybos.bmapp.Data.Mapper.Auth;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Auth.Features;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 27/02/2018.
 *
 * @author EmLaAi
 */

public class FeaturesMapper extends BaseMapper {

    public void get(int buildingId, Context context, SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        this.executeBackground(context,(mContext)->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.auth(VolleyUrlBuilder.URLPATH.FEATURES),Integer.toString(buildingId))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).get(url,header,(response)->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONArray){
                            List<Integer> features = new ArrayList<>();
                            for (int i = 0; i < ((JSONArray) object).length(); i++) {
                                Integer item = ((JSONArray) object).getInt(i);
                                features.add(item);
                            }
                            Logger.shared().o(features);
                            Features.shared(mContext).parse(mContext,features);
                            onSuccess.onSuccess(null);
                        }else{
                            throw new InvalidResponse();
                        }
                    }catch (JSONException | InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
