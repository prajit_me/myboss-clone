package com.mybos.bmapp.Data.Mapper.BuildingCase;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Cases.EmailContractor.EmailContractor;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 16/04/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseEmailContractorMapper extends BaseMapper {
    public void sendEmail(Context context, int building, String caseId,
                          EmailContractor emailContractor, List<Contractor> selectedContractor,
                          SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingEmail(VolleyUrlBuilder.URLPATH.BUILDING_CASE_EMAIL_CONTRACTOR),Integer.toString(building),caseId)
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyBodyBuilder builder = new VolleyBodyBuilder();
                Map<String,Object> rawEmailJson = BuildingCaseEmailContractorMapper.buildEmailRawJSON(emailContractor);
                List<Map<String,Object>> rawListContractor = new ArrayList<>();
                for (Contractor contractor:selectedContractor){
                    Map<String,Object> rawMainContact = ContractorMapper.buildRawJSONDataFromMainContact(contractor.getMainContact());
                    Map<String,Object> rawContractor = ContractorMapper.buildRawJSONDataFromContractor(contractor);
                    rawContractor.put("mainContact",rawMainContact);
                    rawListContractor.add(rawContractor);
                }
                builder.addSubRawJSON("wo",rawEmailJson);
                builder.addSubRawJSONList("contractors",rawListContractor);
                VolleyRequest.shared(mContext).post(url,header,builder.build(),response->{
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                onSuccess.onSuccess(null);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException|InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public static Map<String,Object> buildEmailRawJSON(EmailContractor emailContractor){
        return new VolleyBodyBuilder()
                .setParameter("dueBy", emailContractor.getDueByString())
                .setParameter("emailType", emailContractor.getId())
                .setParameter("enableDescription", emailContractor.getEnableDescription())
                .export();
    }
}
