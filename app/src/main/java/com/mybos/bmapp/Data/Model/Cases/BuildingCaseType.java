package com.mybos.bmapp.Data.Model.Cases;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;

import java.util.List;

/**
 * Created by EmLaAi on 29/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseType extends BaseModel {
    private String name;
    private int id;
    private int order;
    private int building;

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}
