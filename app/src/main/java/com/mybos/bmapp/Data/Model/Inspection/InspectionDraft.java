package com.mybos.bmapp.Data.Model.Inspection;

import com.mybos.bmapp.Utils.DateUtils;

import java.text.ParseException;
import java.util.Date;

public class InspectionDraft implements Comparable<InspectionDraft> {

    private String apartment = "";
    private String draft = "";
    private String fname = "";
    private String id = "";
    private String inspection = "";
    private String inspectionId = "";
    private String lname = "";
    private String manager = "";
    private String name = "";
    private String time = "";
    private String type = "";
    private String typeText = "";

    public String getApartment() {
        return apartment;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getDraft() {
        return draft;
    }

    public void setDraft(String draft) {
        this.draft = draft;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInspection() {
        return inspection;
    }

    public void setInspection(String inspection) {
        this.inspection = inspection;
    }

    public String getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(String inspectionId) {
        this.inspectionId = inspectionId;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeText() {
        return typeText;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    @Override
    public int compareTo(InspectionDraft o) {
        if (getTime() == null || o.getTime() == null) {
            return 0;
        }
        Date date1 = new Date();
        Date date2 = new Date();
        try {
            date1 = DateUtils.convertStringToDate(getTime());
            date2 = DateUtils.convertStringToDate(o.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date2.compareTo(date1);
    }
}
