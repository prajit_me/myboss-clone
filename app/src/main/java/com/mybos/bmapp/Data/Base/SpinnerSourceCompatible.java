package com.mybos.bmapp.Data.Base;

import java.util.List;

/**
 * Created by EmLaAi on 31/03/2018.
 *
 * @author EmLaAi
 */
public interface SpinnerSourceCompatible {
    List<String> getSource();
    int count();
    BaseModel getItem(int position);
    String getTitle(int position);
}
