package com.mybos.bmapp.Data.Model.BuildingAssets;

public class AssetFile {

    private String Added = "";
    private String Detail = "";
    private String Expire = "";
    private String FileName = "";
    private String FileType = "";
    private int AssetFileID = 0;
    private String AssetFileName = "";
    private int AssetFileSize = 0;
    private String AssetFileSized = "";
    private String Subject = "";
    private int AssetFileType = 0;
    private String URL = "";

    public AssetFile() {}

    public String getAdded() {
        return Added;
    }

    public void setAdded(String added) {
        Added = added;
    }

    public String getDetail() {
        return Detail;
    }

    public void setDetail(String detail) {
        Detail = detail;
    }

    public String getExpire() {
        return Expire;
    }

    public void setExpire(String expire) {
        Expire = expire;
    }

    public String getFileName() {
        return FileName;
    }

    public void setFileName(String fileName) {
        FileName = fileName;
    }

    public String getFileType() {
        return FileType;
    }

    public void setFileType(String fileType) {
        FileType = fileType;
    }

    public int getAssetFileID() {
        return AssetFileID;
    }

    public void setAssetFileID(int assetFileID) {
        AssetFileID = assetFileID;
    }

    public String getAssetFileName() {
        return AssetFileName;
    }

    public void setAssetFileName(String assetFileName) {
        AssetFileName = assetFileName;
    }

    public int getAssetFileSize() {
        return AssetFileSize;
    }

    public void setAssetFileSize(int assetFileSize) {
        AssetFileSize = assetFileSize;
    }

    public String getAssetFileSized() {
        return AssetFileSized;
    }

    public void setAssetFileSized(String assetFileSized) {
        AssetFileSized = assetFileSized;
    }

    public String getSubject() {
        return Subject;
    }

    public void setSubject(String subject) {
        Subject = subject;
    }

    public int getAssetFileType() {
        return AssetFileType;
    }

    public void setAssetFileType(int assetFileType) {
        AssetFileType = assetFileType;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
