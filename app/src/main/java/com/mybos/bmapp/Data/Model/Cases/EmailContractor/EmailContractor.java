package com.mybos.bmapp.Data.Model.Cases.EmailContractor;

import android.content.Context;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by EmLaAi on 14/04/2018.
 *
 * @author EmLaAi
 */
public class EmailContractor extends BaseModel {
    public static class TypeList implements SpinnerSourceCompatible{
        List<TypeListItem> items;
        public TypeList(Context context){
            items = new ArrayList<>();
            TypeListItem wo = new TypeListItem("wo",context.getString(R.string.email_contractor_type_work_order));
            TypeListItem quote = new TypeListItem("quote",context.getString(R.string.email_contractor_type_quote));
            TypeListItem caseEmail = new TypeListItem("case",context.getString(R.string.email_contractor_type_case));
            items.addAll(Arrays.asList(wo,quote,caseEmail));
        }

        @Override
        public List<String> getSource() {
            List<String> returnValue = new ArrayList<>();
            for (TypeListItem item:items){
                returnValue.add(item.name);
            }
            return returnValue;
        }

        @Override
        public int count() {
            return items.size();
        }

        @Override
        public BaseModel getItem(int position) {
            return items.get(position);
        }

        @Override
        public String getTitle(int position) {
            return items.get(position).name;
        }
    }
    public static class TypeListItem extends BaseModel{
        String id,name;
        public TypeListItem(String id,String name){
            this.id = id;
            this.name = name;
        }
    }
    Date dueBy;
    Boolean enableDescription;
    TypeListItem type;
    public void setDueBy(Date dueBy) {
        this.dueBy = dueBy;
    }
    public void setType(TypeListItem item){
        this.type = item;
    }

    public void setEnableDescription(Boolean enableDescription) {
        this.enableDescription = enableDescription;
    }

    public String getId() {
        return type.id;
    }

    public String getDueByString() {
        return DateUtils.getDateString(dueBy,"yyyy-MM-dd'T'HH:mm:ssZZZZZ");
    }

    public Boolean getEnableDescription() {
        return enableDescription;
    }
}
