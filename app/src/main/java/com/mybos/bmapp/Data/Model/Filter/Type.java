package com.mybos.bmapp.Data.Model.Filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Component.SpinnerAdapter.SpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

public class Type extends BaseModel implements SpinnerItemInterface<Integer>, Parcelable {
    private String name="";
    private int id;

    public Type(){
    }

    public Type(String name, int id){
        this.name = name;
        this.id = id;
    }

    protected Type(Parcel in) {
        name = in.readString();
        id = in.readInt();
    }

    public static final Creator<Type> CREATOR = new Creator<Type>() {
        @Override
        public Type createFromParcel(Parcel in) {
            return new Type(in);
        }

        @Override
        public Type[] newArray(int size) {
            return new Type[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(id);
    }

    @Override
    public Integer getSpinnerItemId() {
        return Integer.valueOf(id);
    }

    @Override
    public String getTittle() {
        return getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
