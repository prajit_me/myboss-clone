package com.mybos.bmapp.Data.Model.Inspection;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class InspectionOfflinetoSave implements Parcelable,RealmModel {

    public InspectionOfflinetoSave(){}

    private int buildingId;
    private String apartmentId;
    private int inspectionId;
    private boolean convert;
    private RealmList<InspectionItemOffline> UpdatedInspectionItemList;


    protected InspectionOfflinetoSave(Parcel in) {
        buildingId = in.readInt();
        apartmentId = in.readString();
        inspectionId = in.readInt();
        convert = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(buildingId);
        dest.writeString(apartmentId);
        dest.writeInt(inspectionId);
        dest.writeByte((byte) (convert ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InspectionOfflinetoSave> CREATOR = new Creator<InspectionOfflinetoSave>() {
        @Override
        public InspectionOfflinetoSave createFromParcel(Parcel in) {
            return new InspectionOfflinetoSave(in);
        }

        @Override
        public InspectionOfflinetoSave[] newArray(int size) {
            return new InspectionOfflinetoSave[size];
        }
    };

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public int getInspectionId() {
        return inspectionId;
    }

    public void setInspectionId(int inspectionId) {
        this.inspectionId = inspectionId;
    }

    public boolean isConvert() {
        return convert;
    }

    public void setConvert(boolean convert) {
        this.convert = convert;
    }

    public String getApartmentId() {
        return apartmentId;
    }

    public void setApartmentId(String apartmentId) {
        this.apartmentId = apartmentId;
    }

    public RealmList<InspectionItemOffline> getUpdatedInspectionItemList() {
        return UpdatedInspectionItemList;
    }

    public void setUpdatedInspectionItemList(RealmList<InspectionItemOffline> updatedInspectionItemList) {
        UpdatedInspectionItemList = updatedInspectionItemList;
    }
}
