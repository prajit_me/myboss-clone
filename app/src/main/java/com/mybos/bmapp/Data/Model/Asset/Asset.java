package com.mybos.bmapp.Data.Model.Asset;

import com.mybos.bmapp.Component.SpinnerAdapter.SpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class Asset extends BaseModel implements SpinnerItemInterface<Integer> {
    private int id;
    private int building;
    private String barcode;
    private String name;
    private String description;
    private int category;
    private String location;
    private String serial;
    private String make;
    private String model;
    private int value;
    private String added;
    private String warantyTitle;
    private String warantyExpire;
    private String warantyDocument;
    private String updated;
    private String categoryName;

    public void setId(int id) {
        this.id = id;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setWarantyExpire(String warantyExpire) {
        this.warantyExpire = warantyExpire;
    }

    public void setWarantyDocument(String warantyDocument) {
        this.warantyDocument = warantyDocument;
    }

    public void setWarantyTitle(String warantyTitle) {
        this.warantyTitle = warantyTitle;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public Integer getSpinnerItemId() {
        return id;
    }

    @Override
    public String getTittle() {
        return this.name;
    }
}
