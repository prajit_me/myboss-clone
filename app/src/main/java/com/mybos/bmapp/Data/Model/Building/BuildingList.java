package com.mybos.bmapp.Data.Model.Building;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.RealmClass;

/**
 * Created by EmLaAi on 15/01/2018.
 *
 * @author EmLaAi
 */
@RealmClass
public class BuildingList implements Parcelable,RealmModel {

    private RealmList<Building> list;
    private Integer selectedIndex = BuildingInfoCollection.getSelectedBuilding();
    private Integer selectedId;

    public BuildingList(){
        list = new RealmList<>();
    }

    protected BuildingList(Parcel in) {

        if (in.readByte() == 0) {
            selectedIndex = null;
        } else {
            selectedIndex = in.readInt();
        }
        if (in.readByte() == 0) {
            selectedId = null;
        } else {
            selectedId = in.readInt();
        }
    }

    public static final Creator<BuildingList> CREATOR = new Creator<BuildingList>() {
        @Override
        public BuildingList createFromParcel(Parcel in) {
            return new BuildingList(in);
        }

        @Override
        public BuildingList[] newArray(int size) {
            return new BuildingList[size];
        }
    };

    public void addBuilding(Building building){
        list.add(building);
    }

    public void replaceList(List<Building> list){
        for (Building building:list) {
            if (!checkBuildingInList(building)){
                this.list.add(building);
            }
        }
        update();
    }

    public boolean checkBuildingInList(Building building){
        if (this.list == null){return true;}
        for (int i = 0; i < this.list.size(); i++) {
            if(Objects.requireNonNull(this.list.get(i)).getId() == building.getId()){
                return true;
            }
        }
        return false;
    }

    public void setInitIndex(int index){
        if (null == selectedIndex){
            setSelectedIndex(index);
        }
    }

    public void setSelectedIndex(int index){
        try {
            this.selectedIndex = index;
            if (list.get(selectedIndex) != null) {
                this.selectedId = Objects.requireNonNull(list.get(this.selectedIndex)).getId();
            }
        }
        catch (NullPointerException e){}
    }

    public Integer getSelectedIndex() {
        return selectedIndex;
    }

    public Building getSelectedBuilding(){
        Building SelectedBuilding = new Building();
        if (list != null && list.size() > 0){
            SelectedBuilding = list.get(selectedIndex);
        }

        return SelectedBuilding;
    }

    public List<Building> getBuildingList() {
        return list;
    }

    public void update(){
        for (int i = 0; i < list.size(); i++) {
            try {
                if (Objects.requireNonNull(list.get(i)).getId() == selectedId) {
                    selectedIndex = i;
                    return;
                }
            }
            catch(NullPointerException e){}
        }
        selectedIndex = 0;
    }

    public boolean isNull(){
        boolean isNullList;
        try {
            if (list.size() == 0) {
                isNullList = true;
            }
            else {
                isNullList = false;
            }
        }
        catch (NullPointerException e){
            isNullList = true;
        }

        return isNullList;
    }

    public List<String> getBuildingListName(){
        List<String> returnValue = new ArrayList<>();
        if (list != null) {
            for (Building building : list) {
                returnValue.add(building.getName());
            }
        }

        return returnValue;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(list);
        if (selectedIndex == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(selectedIndex);
        }
        if (selectedId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(selectedId);
        }
    }

    public static void save(BuildingList buildingList){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction(mRealm->{
                mRealm.delete(Building.class);
                mRealm.delete(BuildingList.class);
                mRealm.insertOrUpdate(buildingList);
            });
        }
    }

    public static BuildingList get(){
        try (Realm realm = Realm.getDefaultInstance()){
            BuildingList buildingList = realm.where(BuildingList.class).findFirst();
            return null != buildingList ? realm.copyFromRealm(buildingList) : null;
        }
    }
}
