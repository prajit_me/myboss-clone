package com.mybos.bmapp.Data.Mapper.Calendar;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Data.Model.Calendar.Booking;
import com.mybos.bmapp.Data.Model.Calendar.File;
import com.mybos.bmapp.Data.Model.Calendar.Maintenance;
import com.mybos.bmapp.Data.Model.Calendar.MaintenanceCommnet;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.Data.Model.Calendar.Reminder;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.Map;

public class CalendarMapper extends BaseMapper {

    public void getReminder(Context context, int buildingId, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String param = "/reminder?m="+Reminder.getCurMonth()+"&y="+Reminder.getCurYear();
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_GET_EVENT),
                                    Integer.toString(buildingId))
                            .build();
                    url = url + param;
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONArray) {
                                ArrayList<Reminder> reminders = new ArrayList<>();
                                int Size = ((JSONArray) object).length();
                                for (int i = 0; i < Size; i++) {
                                    JSONObject CurrentObject = ((JSONArray) object).getJSONObject(i);
                                    Reminder reminder = new Reminder();
                                    reminder.setId(CurrentObject.optString("id"));
                                    reminder.setBuilding(CurrentObject.optString("building"));
                                    reminder.setComment(CurrentObject.optString("comment"));
                                    reminder.setTitle(CurrentObject.optString("title"));
                                    reminder.setDate(CurrentObject.optString("date"));
                                    reminder.setTime(CurrentObject.optString("time"));
                                    reminder.setDashboard(CurrentObject.optString("dashboard"));
                                    reminder.setFired(CurrentObject.optString("fired"));

                                    reminders.add(reminder);
                                }
                                Reminder.setReminders(reminders);
                                onSuccess.onSuccess("Success");
                            } else {
                                throw new InvalidResponse();
                            }
                        } catch (JSONException | InvalidResponse e) {
                            onFailure.onFailure(e);
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }

    public void getBooking(Context context, int buildingId, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String param = "/booking?m="+Reminder.getCurMonth()+"&y="+Reminder.getCurYear();
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_GET_EVENT),
                                    Integer.toString(buildingId))
                            .build();
                    url = url + param;
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONArray) {
                                ArrayList<Booking> bookings = new ArrayList<>();
                                int Size = ((JSONArray) object).length();
                                for (int i = 0; i < Size; i++) {
                                    JSONObject CurrentObject = ((JSONArray) object).getJSONObject(i);
                                    Booking booking = new Booking();
                                    booking.setId(CurrentObject.optString("id"));
                                    booking.setCalendar(CurrentObject.optString("calendar"));
                                    booking.setDate(CurrentObject.optString("date"));
                                    booking.setSlot_from(CurrentObject.optString("slot_from"));
                                    booking.setSlot_to(CurrentObject.optString("slot_to"));
                                    booking.setApartment(CurrentObject.optString("apartment"));
                                    booking.setPrice(CurrentObject.optString("price"));
                                    booking.setInvoice(CurrentObject.optString("invoice"));
                                    booking.setTransaction(CurrentObject.optString("transaction"));
                                    booking.setComment(CurrentObject.optString("comment"));
                                    booking.setStatus(CurrentObject.optString("status"));
                                    booking.setBooker_fname(CurrentObject.optString("booker_fname"));
                                    booking.setBooker_lname(CurrentObject.optString("booker_lname"));
                                    booking.setBooker_mobile(CurrentObject.optString("booker_mobile"));
                                    booking.setBooker_phone(CurrentObject.optString("booker_phone"));
                                    booking.setBooker_email(CurrentObject.optString("booker_email"));
                                    booking.setBooker_comment(CurrentObject.optString("booker_comment"));
                                    booking.setToken(CurrentObject.optString("token"));
                                    booking.setPaid(CurrentObject.optString("paid"));
                                    booking.setBuilding(CurrentObject.optString("building"));
                                    booking.setCalendarName(CurrentObject.optString("calendarName"));
                                    booking.setStatusText(CurrentObject.optString("statusText"));
                                    booking.setBuildingName(CurrentObject.optString("buildingName"));

                                    bookings.add(booking);
                                }
                                Booking.setBookings(bookings);
                                onSuccess.onSuccess("Success");
                            } else {
                                throw new InvalidResponse();
                            }
                        } catch (JSONException | InvalidResponse e) {
                            onFailure.onFailure(e);
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }

    public void getMaintenance(Context context, int buildingId, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String param = "/maintenance?m="+Reminder.getCurMonth()+"&y="+Reminder.getCurYear();
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_GET_EVENT),
                                    Integer.toString(buildingId))
                            .build();
                    url = url + param;
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        try {
                            Object object = new JSONTokener(response).nextValue();
                            if (object instanceof JSONArray) {
                                ArrayList<Maintenance> maintenances = new ArrayList<>();
                                int Size = ((JSONArray) object).length();
                                for (int i = 0; i < Size; i++) {
                                    JSONObject CurrentObject = ((JSONArray) object).getJSONObject(i);
                                    Maintenance maintenance = new Maintenance();
                                    //get Photo
                                    ArrayList<File> photoFiles = new ArrayList<>();
                                    JSONArray jsonPhotos =CurrentObject.getJSONArray("photos");
                                    for (int j=0;j<jsonPhotos.length();j++){
                                        JSONObject jsonPhoto = jsonPhotos.getJSONObject(j);
                                        File photoFile = new File();
                                        photoFile.setId(jsonPhoto.optString("id"));
                                        photoFile.setType(jsonPhoto.optString("type"));
                                        photoFile.setName(jsonPhoto.optString("name"));
                                        photoFile.setFile_name(jsonPhoto.optString("file_name"));
                                        photoFile.setSized(jsonPhoto.optString("sized"));
                                        photoFile.setSize(jsonPhoto.optString("size"));
                                        photoFile.setAdded(jsonPhoto.optString("added"));
                                        photoFile.setFile_type(jsonPhoto.optString("file_type"));
                                        photoFile.setUrl(jsonPhoto.optString("url"));
                                        photoFile.setSubject(jsonPhoto.optString("subject"));
                                        photoFile.setDetail(jsonPhoto.optString("detail"));
                                        photoFile.setExpire(jsonPhoto.optString("expire"));
                                        photoFiles.add(photoFile);
                                    }
                                    maintenance.setPhotos(new ArrayList<>(photoFiles));
                                    //get doc
                                    ArrayList<File> docFiles = new ArrayList<>();
                                    JSONArray jsonDocs =CurrentObject.getJSONArray("documents");
                                    for (int j=0;j<jsonPhotos.length();j++){
                                        JSONObject jsonDoc = jsonPhotos.getJSONObject(j);
                                        File docFile = new File();
                                        docFile.setId(jsonDoc.optString("id"));
                                        docFile.setType(jsonDoc.optString("type"));
                                        docFile.setName(jsonDoc.optString("name"));
                                        docFile.setFile_name(jsonDoc.optString("file_name"));
                                        docFile.setSized(jsonDoc.optString("sized"));
                                        docFile.setSize(jsonDoc.optString("size"));
                                        docFile.setAdded(jsonDoc.optString("added"));
                                        docFile.setFile_type(jsonDoc.optString("file_type"));
                                        docFile.setUrl(jsonDoc.optString("url"));
                                        docFile.setSubject(jsonDoc.optString("subject"));
                                        docFile.setDetail(jsonDoc.optString("detail"));
                                        docFile.setExpire(jsonDoc.optString("expire"));
                                        docFiles.add(docFile);
                                    }
                                    maintenance.setDocuments(new ArrayList<>(docFiles));
                                    // get Lists
                                    //contractors
                                    ArrayList<String> listContractor = new ArrayList<>();
                                    JSONArray contractors = CurrentObject.getJSONArray("contractors");
                                        for (int c=0;c<contractors.length();c++) {
                                            listContractor.add(contractors.optString(c));
                                        }
                                    maintenance.setContractors(new ArrayList<>(listContractor));
                                    //assets
                                    ArrayList<String> listAssets = new ArrayList<>();
                                    JSONArray assets = CurrentObject.getJSONArray("assets");
                                    for (int c=0;c<assets.length();c++) {
                                        listContractor.add(assets.optString(c));
                                    }
                                    maintenance.setAssets(new ArrayList<>(listAssets));
                                    //assetText
                                    ArrayList<String> listAssetsText = new ArrayList<>();
                                    JSONArray assetText = CurrentObject.getJSONArray("assetText");
                                    for (int c=0;c<assetText.length();c++) {
                                        listContractor.add(assetText.optString(c));
                                    }
                                    maintenance.setAssets(new ArrayList<>(listAssetsText));
                                    //contractorText
                                    ArrayList<String> listContractorText = new ArrayList<>();
                                    JSONArray contractorTexts = CurrentObject.getJSONArray("assetText");
                                    for (int c=0;c<contractorTexts.length();c++) {
                                        listContractor.add(contractorTexts.optString(c));
                                    }
                                    maintenance.setAssets(new ArrayList<>(listContractorText));

                                    maintenance.setId(CurrentObject.optString("id"));
                                    maintenance.setType_text(CurrentObject.optString("type_text"));
                                    maintenance.setTitle(CurrentObject.optString("title"));
                                    maintenance.setSubject(CurrentObject.optString("subject"));
                                    maintenance.setDetail(CurrentObject.optString("detail"));
                                    maintenance.setStart(CurrentObject.optString("start"));
                                    maintenance.setArea(CurrentObject.optString("area"));
                                    maintenance.setAreaText(CurrentObject.optString("areaText"));
                                    maintenance.setEnd(CurrentObject.optString("end"));
                                    maintenance.setType(CurrentObject.optString("type"));
                                    maintenance.setAuto_wo(CurrentObject.optString("auto_wo"));
                                    maintenance.setAuto_time(CurrentObject.optString("auto_time"));
                                    maintenance.setLogged_by(CurrentObject.optString("logged_by"));
                                    maintenance.setActive(CurrentObject.optString("active"));
                                    maintenances.add(maintenance);
                                }
                                Maintenance.setMaintenances(maintenances);
                                onSuccess.onSuccess("Success");
                            } else {
                                throw new InvalidResponse();
                            }
                        } catch (JSONException | InvalidResponse e) {
                            onFailure.onFailure(e);
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }

    public void getDateMaintenance(Context context, int buildingId, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_GET_DATE_MAINTENANCE),
                        Integer.toString(buildingId)).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String param = "{\"m\":"+Reminder.getCurMonth()+",\"y\":"+Reminder.getCurYear()+"}";
                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject) {
                            for (String date: SelectedDay.getAllDayOfCurrentMonth()) {
                                if (!((JSONObject) object).isNull(date)){
                                    JSONArray jsondate = ((JSONObject) object).getJSONArray(date);
                                    for (int d=0;d<jsondate.length();d++){
                                        JSONObject currentJson = jsondate.getJSONObject(d);
                                        JSONObject jsonHistory = currentJson.getJSONObject("history");
                                        JSONObject jsonData = currentJson.getJSONObject("data");

                                        MaintenancePerDay maintenancePerDay = new MaintenancePerDay();
                                        maintenancePerDay.setDate_ogrinal(jsonHistory.optString("day"));
                                        maintenancePerDay.setDate(date);
                                        maintenancePerDay.setId(jsonData.optString("id"));
                                        maintenancePerDay.setStatusText(jsonHistory.optString("statusText"));
                                        maintenancePerDay.setTitle(jsonData.optString("title"));
                                        maintenancePerDay.setDateStart(jsonData.optString("start"));
                                        maintenancePerDay.setDateEnd(jsonData.optString("end"));
                                        maintenancePerDay.setType_text(jsonData.optString("type_text"));
                                        maintenancePerDay.setDetail(jsonData.optString("detail"));

                                        //get Documents
                                        ArrayList<File> documents = new ArrayList<>();
                                        JSONArray jsonDocs = jsonHistory.getJSONArray("documents");
                                        for (int doc=0;doc<jsonDocs.length();doc++){
                                            JSONObject currentDoc = jsonDocs.getJSONObject(doc);
                                            File filedoc = new File();
                                            filedoc.setUrl(currentDoc.optString("url"));
                                            filedoc.setName(currentDoc.optString("name"));
                                            filedoc.setAdded(currentDoc.optString("added"));
                                            documents.add(filedoc);
                                        }
                                        maintenancePerDay.setDocuments(new ArrayList<>(documents));
                                        //get Invoices
                                        ArrayList<File> invoices = new ArrayList<>();
                                        JSONArray jsonInvoices = jsonHistory.getJSONArray("invoices");
                                        for (int in=0;in<jsonInvoices.length();in++){
                                            JSONObject currentInv = jsonInvoices.getJSONObject(in);
                                            JSONObject attach = currentInv.getJSONObject("attachment");
                                            File fileInv = new File();
                                            fileInv.setUrl(attach.optString("url"));
                                            fileInv.setName(attach.optString("name"));
                                            fileInv.setAdded(attach.optString("added"));

                                            invoices.add(fileInv);
                                        }
                                        maintenancePerDay.setInvoices(invoices);

                                        ArrayList<MaintenanceCommnet> commnets = new ArrayList<>();
                                        JSONArray jsonComment = jsonHistory.getJSONArray("comments");
                                        for (int c=0;c<jsonComment.length();c++){
                                            JSONObject currentComment = jsonComment.getJSONObject(c);
                                            MaintenanceCommnet commnet = new MaintenanceCommnet();
                                            commnet.setComment(currentComment.optString("comment"));
                                            commnet.setWriterName(currentComment.optString("writerName"));
                                            commnet.setTime(currentComment.optString("time"));
                                            commnet.setId_comment(currentComment.optString("id"));
                                            //add to list comment
                                            commnets.add(commnet);
                                        }
                                        maintenancePerDay.setComments(new ArrayList<>(commnets));
                                        MaintenancePerDay.getMaintenancePerDays().add(maintenancePerDay);
                                    }
                                }
                            }
                        }
                        onSuccess.onSuccess("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void changeDateReminder(Context context, int buildingId,String RemindarId , String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_CHANGE_DATE_REMINDER),
                        Integer.toString(buildingId), RemindarId).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void changeStatusBooking(Context context, int buildingId,String bookingId , String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_CHANGE_STATUS_BOOKING),
                        Integer.toString(buildingId), bookingId).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void changeStatusMaintenance(Context context, int buildingId,String MaintenanceId,String dateOgrin, String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_CHANGE_STATUS_MAINTENANCE),
                        Integer.toString(buildingId), MaintenanceId,dateOgrin).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                Log.e("Log  _changeSM",url);
                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void changeCommentBooking(Context context, int buildingId,String bookingId , String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_CHANGE_COMMENT_BOOKING),
                        Integer.toString(buildingId), bookingId).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();

                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void addCommentMaintenance(Context context, int buildingId,String MaintenanceId,String dateOgrin, String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_ADD_COMMENT_MAINTENANCE),
                        Integer.toString(buildingId), MaintenanceId,dateOgrin).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                            if (state.equals("200")){
                                JSONObject jsonComment = ((JSONObject) object).getJSONObject("data");
                                MaintenanceCommnet maintenanceCommnet = new MaintenanceCommnet();
                                maintenanceCommnet.setId(jsonComment.optString("event"));
                                maintenanceCommnet.setDay(jsonComment.optString("day"));
                                maintenanceCommnet.setTime(jsonComment.optString("time"));
                                maintenanceCommnet.setTime(jsonComment.optString("time"));
                                maintenanceCommnet.setWriter(jsonComment.optString("writer"));
                                maintenanceCommnet.setWriter(jsonComment.optString("writer"));
                                maintenanceCommnet.setComment(jsonComment.optString("comment"));
                                maintenanceCommnet.setComment(jsonComment.optString("comment"));
                                maintenanceCommnet.setWriterName(jsonComment.optString("writerName"));
                                maintenanceCommnet.setId_comment(jsonComment.optString("id"));

                                MaintenancePerDay.getMaintenancePerDaySelected().getComments().add(maintenanceCommnet);
                            }
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void removeCommentMaintenance(Context context, int buildingId,String MaintenanceId,String dateOgrin, String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_REMOVE_COMMENT_MAINTENANCE),
                        Integer.toString(buildingId), MaintenanceId,dateOgrin).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).delete(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");

                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void moveMaintenance(Context context, int buildingId,String maintenanceId,String dateOgrin , String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            if (null != mContext) {
                String url = new VolleyUrlBuilder().parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_MOVE_MAINTENANCE),
                        Integer.toString(buildingId), maintenanceId,dateOgrin).build();
                Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                VolleyRequest.shared(mContext).post(url, header, param, (response) -> {
                    try {
                        Object object = new JSONTokener(response).nextValue();
                        String state = "";
                        if (object instanceof JSONObject) {
                            state = ((JSONObject) object).optString("state");
                        }
                        onSuccess.onSuccess(state);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, onFailure::onFailure);
            } else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void getConvert(Context context, int buildingId,String maintenanceId, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        this.executeBackground(context, (mContext) -> {
            try {
                if (null != mContext) {
                    String url = new VolleyUrlBuilder()
                            .parseUrl(VolleyUrlBuilder.URLPATH.calendar(VolleyUrlBuilder.URLPATH.CALENDAR_GET_CONVERT_MAINTENANCE),
                                    Integer.toString(buildingId),maintenanceId)
                            .build();
                    Map<String, String> header = new VolleyHeaderBuilder().buildDefaultJson();
                    VolleyRequest.shared(mContext).get(url, header, (response) -> {
                        String stringRespone = response.toString();
                        try {
                            JSONObject jsonObject = new JSONObject(stringRespone);
                            String state = jsonObject.optString("state");
                            if (state.equals("200")){
                                String id = jsonObject.optString("id");
                                onSuccess.onSuccess(id);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, onFailure::onFailure);
                } else {
                    throw new InternalError();
                }
            } catch (InternalError error) {
                onFailure.onFailure(error);
            }
        });
    }
}
