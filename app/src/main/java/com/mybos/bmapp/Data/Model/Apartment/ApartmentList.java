package com.mybos.bmapp.Data.Model.Apartment;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;
import com.mybos.bmapp.Data.Base.SpinnerSourceCompatible;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class ApartmentList extends BaseModel implements KeepTrackSelectedItemList<String>,SpinnerSourceCompatible {
    private List<Apartment> apartments;
    private List<String> selectedApartmets = new ArrayList<>();
    public ApartmentList(){
        apartments = new ArrayList<>();
    }

    public void addAparment(Apartment apartment){
        this.apartments.add(apartment);
    }

    @Override
    public void selectId(String id) {
        for (int i = 0;i < selectedApartmets.size();i++){
            if (selectedApartmets.get(i).equals(id)){
                return;
            }
        }
        selectedApartmets.add(id);
    }

    @Override
    public void deselectId(String id) {
        for (int i = 0; i < selectedApartmets.size();i++){
            if (selectedApartmets.get(i).equals(id)){
                selectedApartmets.remove(i);
                return;
            }
        }
    }

    @Override
    public List<BaseModel> getSelectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Apartment apartment:apartments){
            if (selectedApartmets.contains(apartment.getId())){
                returnValue.add(apartment);
            }
        }
        return returnValue;
    }

    @Override
    public List<BaseModel> getUnselectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Apartment apartment:apartments){
            if (!selectedApartmets.contains(apartment.getId())){
                returnValue.add(apartment);
            }
        }
        return returnValue;
    }

    public List<String> getSelectedApartmets() {
        return selectedApartmets;
    }

    @Override
    public List<String> getSource() {
        List<String> returnValue = new ArrayList<>();
        for (Apartment apartment:apartments){
            returnValue.add(apartment.getTittle());
        }
        return returnValue;
    }

    @Override
    public int count() {
        return apartments.size();
    }

    @Override
    public BaseModel getItem(int position) {
        return apartments.get(position);
    }

    @Override
    public String getTitle(int position) {
        return apartments.get(position).getTittle();
    }

    public List<Apartment> getApartments() {
        return apartments;
    }
}
