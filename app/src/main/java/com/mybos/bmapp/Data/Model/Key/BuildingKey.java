package com.mybos.bmapp.Data.Model.Key;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Building.Building;

import java.util.ArrayList;

public class BuildingKey extends BaseModel {

    private String Apartment = "";
    private String BuildingID = "";
    private String CardNo = "";
    private String Comment = "";
    private String Description = "";
    private String KeyID = "";
    private String isRequest = "";
    private String KeyName = "";
    private String Location = "";
    private String LocationText = "";
    private String KeyOwner = "";
    private String KeyOwnerData = "";
    private String KeyPhoto = "";
    private String KeyStatus = "";
    private String StatusText = "";
    private String KeyType = "";
    private String TypeText = "";

    private static ArrayList<BuildingKey> buildingKeys;

    public String getApartment() {
        return Apartment;
    }

    public void setApartment(String apartment) {
        Apartment = apartment;
    }

    public String getBuildingID() {
        return BuildingID;
    }

    public void setBuildingID(String buildingID) {
        BuildingID = buildingID;
    }

    public String getCardNo() {
        return CardNo;
    }

    public void setCardNo(String cardNo) {
        CardNo = cardNo;
    }

    public String getComment() {
        return Comment;
    }

    public void setComment(String comment) {
        Comment = comment;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getKeyID() {
        return KeyID;
    }

    public void setKeyID(String keyID) {
        KeyID = keyID;
    }

    public String getIsRequest() {
        return isRequest;
    }

    public void setIsRequest(String isRequest) {
        this.isRequest = isRequest;
    }

    public String getKeyName() {
        return KeyName;
    }

    public void setKeyName(String keyName) {
        KeyName = keyName;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getLocationText() {
        return LocationText;
    }

    public void setLocationText(String locationText) {
        LocationText = locationText;
    }

    public String getKeyOwner() {
        return KeyOwner;
    }

    public void setKeyOwner(String keyOwner) {
        KeyOwner = keyOwner;
    }

    public String getKeyOwnerData() {
        return KeyOwnerData;
    }

    public void setKeyOwnerData(String keyOwnerData) {
        KeyOwnerData = keyOwnerData;
    }

    public String getKeyPhoto() {
        return KeyPhoto;
    }

    public void setKeyPhoto(String keyPhoto) {
        KeyPhoto = keyPhoto;
    }

    public String getKeyStatus() {
        return KeyStatus;
    }

    public void setKeyStatus(String keyStatus) {
        KeyStatus = keyStatus;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public String getKeyType() {
        return KeyType;
    }

    public void setKeyType(String keyType) {
        KeyType = keyType;
    }

    public String getTypeText() {
        return TypeText;
    }

    public void setTypeText(String typeText) {
        TypeText = typeText;
    }

    public static ArrayList<BuildingKey> getBuildingKeys() {
        return buildingKeys;
    }

    public static void setBuildingKeys(ArrayList<BuildingKey> buildingKeys) {
        BuildingKey.buildingKeys = buildingKeys;
    }
}
