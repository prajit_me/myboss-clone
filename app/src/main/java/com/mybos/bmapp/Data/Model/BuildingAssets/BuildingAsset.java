package com.mybos.bmapp.Data.Model.BuildingAssets;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;

import java.util.ArrayList;

public class BuildingAsset extends BaseModel {

    private String Added = "";
    private int Barcode = 0;
    private int BuildingID = 0;
    private int Category = 0;
    private String CategoryName = "";
    private ArrayList<String> ContractorText = new ArrayList<>();
    private ArrayList<Contractor> Contractors = new ArrayList<>();
    private String Description = "";
    private ArrayList<AssetFile> Documents = new ArrayList<>();
    private int AssetID = 0;
    private String Location = "";
    private String Make = "";
    private String Model = "";
    private String Name = "";
    private ArrayList<AssetFile> Photos = new ArrayList<>();
    private int Serial = 0;
    private String Update = "";
    private int AssetValue = 0;
    private ArrayList<AssetFile> WarrantyDocument = new ArrayList<>();
    private String WarrantyExpire = "";
    private String WarrantyTitle = "";

    public BuildingAsset() {}

    public String getAdded() {
        return Added;
    }

    public void setAdded(String added) {
        Added = added;
    }

    public int getBarcode() {
        return Barcode;
    }

    public void setBarcode(int barcode) {
        Barcode = barcode;
    }

    public int getBuildingID() {
        return BuildingID;
    }

    public void setBuildingID(int buildingID) {
        BuildingID = buildingID;
    }

    public int getCategory() {
        return Category;
    }

    public void setCategory(int category) {
        Category = category;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public ArrayList<String> getContractorText() {
        return ContractorText;
    }

    public void addToContractorText(String contractorText) {
        ContractorText.add(contractorText);
    }

    public void setContractorText(ArrayList<String> contractorText) {
        ContractorText = contractorText;
    }

    public ArrayList<Contractor> getContractors() {
        return Contractors;
    }

    public void addToContractors(Contractor contractor) {
        Contractors.add(contractor);
    }

    public void setContractors(ArrayList<Contractor> contractors) {
        Contractors = contractors;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public ArrayList<AssetFile> getDocuments() {
        return Documents;
    }

    public void addToDocuments(AssetFile document) {
        Documents.add(document);
    }

    public void setDocuments(ArrayList<AssetFile> documents) {
        Documents = documents;
    }

    public int getAssetID() {
        return AssetID;
    }

    public void setAssetID(int assetID) {
        AssetID = assetID;
    }

    public String getLocation() {
        return Location;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public String getMake() {
        return Make;
    }

    public void setMake(String make) {
        Make = make;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public ArrayList<AssetFile> getPhotos() {
        return Photos;
    }

    public void addToPhotos(AssetFile photo) {
        Photos.add(photo);
    }

    public void setPhotos(ArrayList<AssetFile> photos) {
        Photos = photos;
    }

    public int getSerial() {
        return Serial;
    }

    public void setSerial(int serial) {
        Serial = serial;
    }

    public String getUpdate() {
        return Update;
    }

    public void setUpdate(String update) {
        Update = update;
    }

    public int getAssetValue() {
        return AssetValue;
    }

    public void setAssetValue(int assetValue) {
        AssetValue = assetValue;
    }

    public ArrayList<AssetFile> getWarrantyDocument() {
        return WarrantyDocument;
    }

    public void addToWarrantyDocument(AssetFile warrantyDocument) {
        WarrantyDocument.add(warrantyDocument);
    }

    public void setWarrantyDocument(ArrayList<AssetFile> warrantyDocument) {
        WarrantyDocument = warrantyDocument;
    }

    public String getWarrantyExpire() {
        return WarrantyExpire;
    }

    public void setWarrantyExpire(String warrantyExpire) {
        WarrantyExpire = warrantyExpire;
    }

    public String getWarrantyTitle() {
        return WarrantyTitle;
    }

    public void setWarrantyTitle(String warrantyTitle) {
        WarrantyTitle = warrantyTitle;
    }
}
