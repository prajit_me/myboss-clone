package com.mybos.bmapp.Data.Mapper.Firebase;

import android.content.Context;
import android.os.Build;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseMapper;
import com.mybos.bmapp.Error.InternalError;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.Map;

public class FirebaseMapper extends BaseMapper {

    public void activeDevice(Context context, String token,
                       BaseMapper.SuccessCallback<Void> onSuccess, BaseMapper.FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder(2)
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.FIREBASE_ACTIVE_DEVICE))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder()
                        .setParameter("token",token)
                        .setParameter("device", Build.MODEL)
                        .setParameter("os","android")
                        .setParameter("version",String.valueOf(Build.VERSION.SDK_INT))
                        .build();
                VolleyRequest.shared(mContext).put(url,header,body, response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                onSuccess.onSuccess(null);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException |InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }

    public void dologOutNRemoveToken(Context context, String token,
                                     BaseMapper.SuccessCallback<Void> onSuccess, BaseMapper.FailureCallback onFailure){
        this.executeBackground(context,mContext->{
            if (null != mContext){
                String url = new VolleyUrlBuilder()
                        .parseUrl(VolleyUrlBuilder.URLPATH.buildingCase(VolleyUrlBuilder.URLPATH.LOGOUT))
                        .build();
                Map<String,String> header = new VolleyHeaderBuilder().buildDefaultJson();
                String body = new VolleyBodyBuilder()
                        .setParameter("token",token)
                        .build();
                VolleyRequest.shared(mContext).post(url,header,body, response->{
                    try{
                        Object object = new JSONTokener(response).nextValue();
                        if (object instanceof JSONObject){
                            if (((JSONObject) object).optInt("state") == 200){
                                Log.e("check_logout","ok");
                                onSuccess.onSuccess(null);
                            }else {
                                onFailure.onFailure(processError(((JSONObject) object).optInt("state"),((JSONObject) object).getString("message")));
                            }
                        }else {
                            throw new InvalidResponse();
                        }
                    }catch (JSONException |InvalidResponse e){
                        onFailure.onFailure(e);
                    }
                },onFailure::onFailure);
            }else {
                onFailure.onFailure(new InternalError());
            }
        });
    }
}
