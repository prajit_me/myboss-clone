package com.mybos.bmapp.Data.Base;

import android.content.Context;
import android.os.Looper;
import android.os.Message;
import android.os.Process;

import com.mybos.bmapp.Error.InterfaceServerFailureException;
import com.mybos.bmapp.Error.ServerFailure;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;

import java.lang.ref.WeakReference;
import java.util.Map;

/**
 * Created by EmLaAi on 17/01/2018.
 *
 * @author EmLaAi
 */

public class BaseMapper {

    protected void executeBackground(Context context,BackgroundJob job){
        try {
            Thread thread = new Thread(new MapperRunabble(context) {
                @Override
                public void run() {
                    Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
                    Context mContext = contextWeakReference.get();
                    job.execute(mContext);
                }
            });
            thread.start();
        }
        catch (OutOfMemoryError e){
            System.gc();
            executeBackground(context, job);
        }
    }


    protected static abstract class MapperRunabble implements Runnable{
        protected WeakReference<Context> contextWeakReference;
        protected MapperRunabble(Context context){
            contextWeakReference = new WeakReference<Context>(context);
        }
    }

    public InterfaceServerFailureException processError(Integer code, String message){
        return new ServerFailure(message);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception e);
    }
    public interface BackgroundJob{
        void execute(Context context);
    }
}
