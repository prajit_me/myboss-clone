package com.mybos.bmapp.Data.Model.Cases;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentFile;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadInvoice;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadQuote;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public class BuildingCase extends BaseModel implements Parcelable {

    private int id,number,building;
    private String added;
    private String subject;
    private String detail;
    private int type;
    private int priority;
    private String start;
    private String due;
    private int status;
    private Integer subStatus;
    private int area;
    private Integer workOrder;
    private int isRead;
    private int isReport;
    private String loggedBy;
    private int isDuplicate;
    private String completionDate;
    private String updated;
    private String historyNote;
    private String starred;
    private String typeText;
    private String statusText;
    private String areaText;
    private String priorityText;
    private int hasVideo;
    private int hasAttachment;
    private int woSent;
    private ArrayList<String> contractorList = new ArrayList<>();

    private int sStatus;

    private static  BuildingCase selectedBuildingCase = new BuildingCase();

    private List<Integer> assets = new ArrayList<>();
    private List<String> assetData = new ArrayList<>();

    private List<Integer> contractors = new ArrayList<>();
    private List<String> contractorDatas = new ArrayList<>();

    private List<String> apartments = new ArrayList<>();

    private List<AttachmentFile> docs = new ArrayList<>();
    private List<AttachmentFile> photos = new ArrayList<>();
    private List<AttachmentUploadQuote> quotes = new ArrayList<>();
    private List<AttachmentUploadInvoice> invoices = new ArrayList<>();

    public BuildingCase(){}

    protected BuildingCase(Parcel in) {
        id = in.readInt();
        number = in.readInt();
        building = in.readInt();
        added = in.readString();
        subject = in.readString();
        detail = in.readString();
        type = in.readInt();
        priority = in.readInt();
        start = in.readString();
        due = in.readString();
        status = in.readInt();
        if (in.readByte() == 0) {
            subStatus = null;
        } else {
            subStatus = in.readInt();
        }
        area = in.readInt();
        if (in.readByte() == 0) {
            workOrder = null;
        } else {
            workOrder = in.readInt();
        }
        isRead = in.readInt();
        isReport = in.readInt();
        loggedBy = in.readString();
        isDuplicate = in.readInt();
        completionDate = in.readString();
        updated = in.readString();
        historyNote = in.readString();
        starred = in.readString();
        typeText = in.readString();
        statusText = in.readString();
        areaText = in.readString();
        priorityText = in.readString();
        hasVideo = in.readInt();
        hasAttachment = in.readInt();
        woSent = in.readInt();
        sStatus = in.readInt();
        assetData = in.createStringArrayList();
        contractorDatas = in.createStringArrayList();
        apartments = in.createStringArrayList();
    }

    public static final Creator<BuildingCase> CREATOR = new Creator<BuildingCase>() {
        @Override
        public BuildingCase createFromParcel(Parcel in) {
            return new BuildingCase(in);
        }

        @Override
        public BuildingCase[] newArray(int size) {
            return new BuildingCase[size];
        }
    };

    public static BuildingCase getSelectedBuildingCase() {
        return selectedBuildingCase;
    }

    public static void setSelectedBuildingCase(BuildingCase selectedBuildingCase) {
        BuildingCase.selectedBuildingCase = selectedBuildingCase;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public void setBuilding(int building) {
        this.building = building;
    }

    public void setAdded(String added) {
        this.added = added;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSubStatus(Integer subStatus) {
        this.subStatus = subStatus;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public void setWorkOrder(Integer workOrder) {
        this.workOrder = workOrder;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public void setIsReport(int isReport) {
        this.isReport = isReport;
    }

    public void setLoggedBy(String loggedBy) {
        this.loggedBy = loggedBy;
    }

    public void setIsDuplicate(int isDuplicate) {
        this.isDuplicate = isDuplicate;
    }

    public void setCompletionDate(String completionDate) {
        this.completionDate = completionDate;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    public void setHistoryNote(String historyNote) {
        this.historyNote = historyNote;
    }

    public void setStarred(String starred) {
        this.starred = starred;
    }

    public void setTypeText(String typeText) {
        this.typeText = typeText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public void setAreaText(String areaText) {
        this.areaText = areaText;
    }

    public void setPriorityText(String priorityText) {
        this.priorityText = priorityText;
    }

    public void setHasVideo(int hasVideo) {
        this.hasVideo = hasVideo;
    }

    public void setHasAttachment(int hasAttachment) {
        this.hasAttachment = hasAttachment;
    }

    public void setWoSent(int woSent) {
        this.woSent = woSent;
    }

    public void setsStatus(int sStatus) {
        this.sStatus = sStatus;
    }

    public void addAssets(int assetId){
        this.assets.add(assetId);
    }

    public void setAssets(List<Integer> assets) {
        this.assets = new ArrayList<>();
        this.assets = assets;
    }



    public void setApartments(List<String> apartments) {
        this.apartments = new ArrayList<>();
        this.apartments = apartments;
    }

    public void addAssetData(String assetData){
        this.assetData.add(assetData);
    }
    public void addContractors(int contractor){
        this.contractors.add(contractor);
    }
    public void addContractorData(String contractorData){
        this.contractorDatas.add(contractorData);
    }
    public void addApartment(String apartment){
        this.apartments.add(apartment);
    }
    public void addAttachmentFile(AttachmentFile file){
        this.photos.add(file);
    }
    public void addDocsFile(AttachmentFile file){
        this.docs.add(file);
    }
    public void addAttachmentQuote(AttachmentUploadQuote quote){
        this.quotes.add(quote);
    }

    public void addAttachmentInvoice(AttachmentUploadInvoice invoice){
        this.invoices.add(invoice);
    }

    public int getId() {
        return id;
    }

    public int getBuilding() {
        return building;
    }

    public int getStatus() {
        return status;
    }

    public Integer getSubStatus() {
        return subStatus;
    }

    public String getAdded() {
        return added;
    }

    public String getStart() {
        return start;
    }

    public String getDue() {
        return due;
    }

    public Integer getWorkOrder() {
        return workOrder;
    }

    public int getIsRead() {
        return isRead;
    }

    public int getIsDuplicate() {
        return isDuplicate;
    }

    public int getIsReport() {
        return isReport;
    }

    public String getLoggedBy() {
        return loggedBy;
    }

    public String getCompletionDate() {
        return completionDate;
    }

    public int getHasAttachment() {
        return hasAttachment;
    }

    public int getHasVideo() {
        return hasVideo;
    }

    public String getUpdated() {
        return updated;
    }

    public String getStarred() {
        return starred;
    }

    public String getTypeText() {
        return typeText;
    }

    public String getStatusText() {
        return statusText;
    }

    public String getAreaText() {
        return areaText;
    }

    public String getPriorityText() {
        return priorityText;
    }

    public List<Integer> getAssets() {
        return assets;
    }

    public List<String> getAssetData() {
        return assetData;
    }

    public String getSubject() {
        return subject;
    }

    public String getDetail() {
        return detail;
    }

    public String getHistoryNote() {
        return historyNote;
    }

    public int getNumber() {
        return number;
    }

    public int getsStatus() {
        return sStatus;
    }

    public int getPriority() {
        return priority;
    }

    public int getType() {
        return type;
    }

    public Date getStartDate() {
        return DateUtils.parseDate(this.start,"yyyy-MM-dd");
    }

    public Date getDueDate() {
        return DateUtils.parseDate(this.due,"yyyy-MM-dd");
    }

    public int getArea() {
        return area;
    }

    public ArrayList<String> getContractorList() {
        return contractorList;
    }

    public void setContractorList(ArrayList<String> contractorList) {
        this.contractorList = contractorList;
    }

    public List<Integer> getAssetsSelectedList() {
        return assets;
    }

    public List<Integer> getContractorsSelectedList() {
        return contractors;
    }

    public List<String> getApartmentsSelectedList() {
        return apartments;
    }

    public List<AttachmentFile> getPhotos() {
        return photos;
    }

    public List<AttachmentUploadQuote> getQuotes() {
        return quotes;
    }

    public List<AttachmentUploadInvoice> getInvoices() {
        return invoices;
    }

    public String getAllApartmentString() {
        StringBuilder returnString = new StringBuilder();
        for (String apartment:apartments){
            if (returnString.toString().equals("")){
                returnString.append(apartment);
            }else{
                returnString.append("\n").append(apartment);
            }
        }
        return returnString.toString();
    }

    public String getAllAssetString(){
        StringBuilder returnString = new StringBuilder();
        for (String asset:assetData){
            if (returnString.toString().equals("")) {
                returnString.append(asset);
            }else{
                returnString.append("\n").append(asset);
            }
        }
        return returnString.toString();
    }
    public String getAllContractorString(){
        StringBuilder returnString = new StringBuilder();
        for (String contractor:contractorDatas){
            if (returnString.toString().equals("")){
                returnString.append(contractor);
            }else {
                returnString.append("\n").append(contractor);
            }
        }
        return returnString.toString();
    }

    public boolean valadiate(){
        if (this.subject == null || this.subject.trim().length() == 0){
            return false;
        }
//        if (this.detail == null || this.detail.trim().length() == 0){
//            return false;
//        }
        if (this.area == BuildingCaseJobArea.COMMON_ASSET_ID) {
            if (assets.size() == 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(number);
        dest.writeInt(building);
        dest.writeString(added);
        dest.writeString(subject);
        dest.writeString(detail);
        dest.writeInt(type);
        dest.writeInt(priority);
        dest.writeString(start);
        dest.writeString(due);
        dest.writeInt(status);
        if (subStatus == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(subStatus);
        }
        dest.writeInt(area);
        if (workOrder == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(workOrder);
        }
        dest.writeInt(isRead);
        dest.writeInt(isReport);
        dest.writeString(loggedBy);
        dest.writeInt(isDuplicate);
        dest.writeString(completionDate);
        dest.writeString(updated);
        dest.writeString(historyNote);
        dest.writeString(starred);
        dest.writeString(typeText);
        dest.writeString(statusText);
        dest.writeString(areaText);
        dest.writeString(priorityText);
        dest.writeInt(hasVideo);
        dest.writeInt(hasAttachment);
        dest.writeInt(woSent);
        dest.writeInt(sStatus);
        dest.writeStringList(assetData);
        dest.writeStringList(contractorDatas);
        dest.writeStringList(apartments);
    }

    public List<AttachmentFile> getDocs() {
        return docs;
    }

    public void setDocs(List<AttachmentFile> docs) {
        this.docs = docs;
    }
}
