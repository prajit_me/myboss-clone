package com.mybos.bmapp.Data.Model.Asset;

import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Base.KeepTrackSelectedItemList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 30/03/2018.
 *
 * @author EmLaAi
 */
public class AssetsList extends BaseModel implements KeepTrackSelectedItemList<Integer> {
    private List<Asset> assetList;
    private List<Asset> allAsset;
    private List<Integer> selectedId = new ArrayList<>();
    public AssetsList(){
        this.assetList = new ArrayList<>();
        this.allAsset = new ArrayList<>();
    }

    public void addAsset(Asset asset){
        this.assetList.add(asset);
        this.allAsset.add(asset);
    }

    public void addNewAssetList(List<Asset> assets){
        this.assetList = new ArrayList<>();
        this.assetList.addAll(assets);
    }

    public void reset(){
        this.assetList = new ArrayList<>();
        this.assetList.addAll(allAsset);
    }

    public List<Asset> getAssetList() {
        return assetList;
    }

    @Override
    public void selectId(Integer id) {
        if (selectedId.contains(id)){
            return;
        }else {
            selectedId.add(id);
        }
    }

    @Override
    public void deselectId(Integer id) {
        if (selectedId.contains((Integer)id)){
            selectedId.remove((Integer)id);
        }
    }

    @Override
    public List<BaseModel> getSelectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Asset asset:assetList){
            if (selectedId.contains(asset.getId())){
                returnValue.add(asset);
            }
        }
        return returnValue;
    }

    @Override
    public List<BaseModel> getUnselectedItem() {
        List<BaseModel> returnValue = new ArrayList<>();
        for (Asset asset:assetList){
            if (!selectedId.contains(asset.getId())){
                returnValue.add(asset);
            }
        }
        return returnValue;
    }

    public List<Integer> getSelectedId() {
        return selectedId;
    }
}
