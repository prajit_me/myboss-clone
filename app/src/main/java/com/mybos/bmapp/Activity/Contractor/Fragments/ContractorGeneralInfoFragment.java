package com.mybos.bmapp.Activity.Contractor.Fragments;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContractorGeneralInfoFragment extends Fragment {

    private TextView tvContractorName, tvContractorAddress, tvContractorPhone, tvContractorMobile, tvContractorEmail, tvContractWebsite,
            tvMainContactName, tvMainContactPhone, tvMainContactEmail;
    private ImageView ivContractorPhone, ivContractorMobile, ivContactorEmail, ivMainContactPhone, ivMainContactEmail;
    private Contractor SelectedContractor = ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex());
    private final int CALL_PHONE_PERMISSION = 1000;
    private String savedNumber;

    private void showInfo() {
        tvContractorName.setText(SelectedContractor.getCompany());
        tvContractorAddress.setText(SelectedContractor.getAddress());
        tvContractorPhone.setText(SelectedContractor.getPhone());
        tvContractorMobile.setText(SelectedContractor.getMobile());
        tvContractorEmail.setText(SelectedContractor.getEmail());
        tvContractWebsite.setText(SelectedContractor.getWebsite());

        tvMainContactName.setText(SelectedContractor.getMainContact().getName());
        tvMainContactPhone.setText(SelectedContractor.getMainContact().getNumber());
        tvMainContactEmail.setText(SelectedContractor.getMainContact().getEmail());
    }

    public void callNumber(String number){
        if (number == null){
            return;
        }
        String uriNumber = "tel:" + number.replace("\\s+","");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uriNumber));
        if(ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            savedNumber = number;
            ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.CALL_PHONE},CALL_PHONE_PERMISSION);
        }else {
            startActivity(intent);
        }
    }

    public void sendEmail(String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
        if (intent.resolveActivity(this.getActivity().getPackageManager()) != null){
            startActivity(intent);
        }else {
            Toast.makeText(this.getActivity(), getString(R.string.ALertNoEmailApp), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CALL_PHONE_PERMISSION){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                callNumber(savedNumber);
            }else {
                Toast.makeText(this.getActivity(), getString(R.string.AlertPermissionGrantFailure), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void startup() {
        //  SHOW CONTRACTOR INFO
        showInfo();
    }

    public ContractorGeneralInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contractor_general_info, container, false);

        //  GET REFERENCES
        tvContractorName = rootView.findViewById(R.id.tvContractorContactName_ContractorGeneralInfoFragment);
        tvContractorAddress = rootView.findViewById(R.id.tvContractorAddress_ContractorGeneralInfoFragment);
        tvContractorPhone = rootView.findViewById(R.id.tvContractorPhone_ContractorGeneralInfoFragment);
        tvContractorMobile = rootView.findViewById(R.id.tvContractorMobile_ContractorGeneralInfoFragment);
        tvContractorEmail = rootView.findViewById(R.id.tvContractorEmail_ContractorGeneralInfoFragment);
        tvContractWebsite = rootView.findViewById(R.id.tvContractorWebsite_ContractorGeneralInfoFragment);
        tvMainContactName = rootView.findViewById(R.id.tvContractorMainContactName_ContractorGeneralInfoFragment);
        tvMainContactPhone = rootView.findViewById(R.id.tvContractorMainContactPhone_ContractorGeneralInfoFragment);
        tvMainContactEmail = rootView.findViewById(R.id.tvContractorMainContactEmail_ContractorGeneralInfoFragment);

        ivContractorPhone = rootView.findViewById(R.id.ivContractorPhone_ContractorGeneralInfoFragment);
        ivContractorMobile = rootView.findViewById(R.id.ivContractorMobile_ContractorGeneralInfoFragment);
        ivContactorEmail = rootView.findViewById(R.id.ivContractorEmail_ContractorGeneralInfoFragment);
        ivMainContactPhone = rootView.findViewById(R.id.ivContractorMainContactPhone_ContractorGeneralInfoFragment);
        ivMainContactEmail = rootView.findViewById(R.id.ivContractorMainContactEmail_ContractorGeneralInfoFragment);

        //  SET LISTENER
        ContractorContactListener ContactListener = new ContractorContactListener();
        ivContractorPhone.setOnClickListener(ContactListener);
        ivContractorMobile.setOnClickListener(ContactListener);
        ivContactorEmail.setOnClickListener(ContactListener);
        ivMainContactPhone.setOnClickListener(ContactListener);
        ivMainContactEmail.setOnClickListener(ContactListener);

        //  START UP
        startup();

        return rootView;
    }

    private class ContractorContactListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.ivContractorPhone_ContractorGeneralInfoFragment:
                    callNumber(SelectedContractor.getPhone());
                    break;
                case R.id.ivContractorMobile_ContractorGeneralInfoFragment:
                    callNumber(SelectedContractor.getMobile());
                    break;
                case R.id.ivContractorEmail_ContractorGeneralInfoFragment:
                    sendEmail(SelectedContractor.getEmail());
                    break;
                case R.id.ivContractorMainContactPhone_ContractorGeneralInfoFragment:
                    callNumber(SelectedContractor.getMainContact().getNumber());
                    break;
                case R.id.ivContractorMainContactEmail_ContractorGeneralInfoFragment:
                    sendEmail(SelectedContractor.getMainContact().getEmail());
                    break;
            }
        }
    }

}
