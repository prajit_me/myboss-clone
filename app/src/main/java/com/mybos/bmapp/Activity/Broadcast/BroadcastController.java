package com.mybos.bmapp.Activity.Broadcast;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.Broadcast.BroadcastMapper;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Broadcast.Groups;
import com.mybos.bmapp.Data.Model.Broadcast.User;

import java.util.ArrayList;

public class BroadcastController {

    public void getBroadcastListGroup(Context context, int buildingId, SuccessCallback<Groups> onSuccess, FailureCallback onFailure) {
        Groups.getGroupsWithDefaultGroups();
        BroadcastMapper mapper = new BroadcastMapper();
        mapper.getGroups(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getBroadcastListUser(Context context, int buildingId, int groupId, SuccessCallback<User> onSuccess, FailureCallback onFailure){
        BroadcastMapper mapper = new BroadcastMapper();
        mapper.getUsers(context, buildingId, groupId, onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getBroadcastCredit(Context context, int buildingId, SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure){
        BroadcastMapper mapper = new BroadcastMapper();
        mapper.getCredit(context, buildingId, onSuccess::onSuccess, onFailure::onFailure);
    }

    public void sendContentEmailBroadcast(Context context, int buildinId, String subject, String content,SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure){
        BroadcastMapper mapper = new BroadcastMapper();
        mapper.sendContentEmail(context, buildinId, subject, content, onSuccess::onSuccess, onFailure::onFailure);
    }

    public void sendEmail(Context context, int buildinId, SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure){
        BroadcastMapper mapper = new BroadcastMapper();
        Broadcast.setStat_send(0);
        mapper.sendMail(context, buildinId, onSuccess::onSuccess, onFailure::onFailure);
    }

    public void sendSMS(Context context, int buildinId, String composeSMS, SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure){
        BroadcastMapper mapper = new BroadcastMapper();
        Broadcast.setStat_send(0);
        mapper.sendSMS(context, buildinId, composeSMS, onSuccess::onSuccess, onFailure::onFailure);
    }

    public void purchaseSMS(Context context, int buildinId, String data, SuccessCallback<Broadcast> onSuccess, FailureCallback onFailure){
        BroadcastMapper mapper = new BroadcastMapper();
        Broadcast.setStat_send(0);
        mapper.purchaseSMS(context, buildinId, data, onSuccess::onSuccess, onFailure::onFailure);
    }



    public interface SuccessCallback<T> {
        void onSuccess(T value);
    }

    public interface FailureCallback {
        void onFailure(Exception error);
    }
}
