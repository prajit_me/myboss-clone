package com.mybos.bmapp.Activity.BuildingCase.NewCase;

import android.app.TaskStackBuilder;
import android.content.Intent;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.*;
import com.mybos.bmapp.Activity.BuildingCase.Adapter.EditBuildingCasePagerAdapter;
import com.mybos.bmapp.Activity.BuildingCase.Adapter.NewBuildingCasePagerAdapter;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListActivity;
import com.mybos.bmapp.Activity.BuildingCase.EmailContractor.EmailContractorActivity;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.ConfirmEmailContractorDialog.ConfirmEmailContractorDialog;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.List;

public class NewBuildingCaseActivity extends a_base_toolbar implements AttachmentFragment.AttachmentEventListener {
    public static final String CASE_ID_KEY = "NewBuildingCaseActivity.CASE_ID_KEY";
    NewBuildingCaseController controller = new NewBuildingCaseController();
    private String caseId = "new";
    private final String Error = "Error";
    private ContractorList contractorList = new ContractorList();
    private BuildingCase buildingCase;
    private ViewPager viewPager;
    private int currentPage = 0;
    private NewBuildingCasePagerAdapter viewPagerAdapter = new NewBuildingCasePagerAdapter(this,getSupportFragmentManager());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_new_building_case);

        //IMPORTANT: setup drawer with drop down building list
        String caseId = getIntent().getStringExtra(CASE_ID_KEY);
        if (null != caseId){
            this.caseId = caseId;
        }

        if (null != getSupportActionBar()) {
            super.updateToolbarTitle(getString(R.string.new_case_activity_title));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        viewPager = findViewById(R.id.emailContractorPager);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(this.currentPage);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                NewBuildingCaseActivity.this.currentPage = position;
//                NewBuildingCasePagerAdapter adapter = (NewBuildingCasePagerAdapter) viewPager.getAdapter();
//                adapter.getCaseDetailFragment().setupData(buildingCase,contractorList);
//                adapter.getAttachmentFragment().setup(buildingCase,contractorList);
                dismissKeyboard();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout topTabBar = findViewById(R.id.topTabBar);
        topTabBar.setupWithViewPager(viewPager);

        if (null == this.buildingCase || null == contractorList){
            getInitData();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.new_case_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.barSave){
            if (buildingCase.valadiate() && contractorList.getSelectedId().size() > 0){
                displayLoadingScreen();
                Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
                controller.saveBuildingCase(this,selectedBuilding.getId(),contractorList,buildingCase,Void->{
                    dismissLoadingScreen();
                    ConfirmEmailContractorDialog dialog = ConfirmEmailContractorDialog.getInstance();
                    dialog.setAcceptClick(()->{
                        Intent main = new Intent(this,DashboardActivity.class);
                        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

                        Intent caseList = new Intent(this,CaseListActivity.class);
                        Intent emailContractor = new Intent(this, EmailContractorActivity.class);
                        emailContractor.putExtra(EmailContractorActivity.CASE_ID_KEY,Integer.toString(buildingCase.getId()));
                        TaskStackBuilder builder = TaskStackBuilder.create(this);
                        builder.addNextIntent(main);
                        builder.addNextIntent(caseList);
                        builder.addNextIntent(emailContractor);
                        builder.startActivities();
                        finish();
                    });
                    dialog.setCancelClick(this::navigateToCaseList);
                    try {
                        dialog.show(getSupportFragmentManager(), "TAG");
                    }
                    catch (IllegalStateException e){}

                },error -> {
                    dismissLoadingScreen();
                    if (Offlinemode.checkNetworkConnection(NewBuildingCaseActivity.this)) {
                        Log.e("onOptionsItemSelected",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(NewBuildingCaseActivity.this);
                    }

                });
            }else {
                Toast.makeText(this, getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            }

        }
        return false;
    }

    public void getInitData(){
        this.displayLoadingScreen();
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getBuildingCaseDetail(this,selectedBuilding.getId(),caseId,(buildingCase)->{
            this.buildingCase = buildingCase;
            Log.e("check","now on newbuilding case");
            getContractorList(selectedBuilding.getId(),1);

        },(error)->{
            this.dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(NewBuildingCaseActivity.this)) {
                Log.e("getInitData", error.toString());
            }else {
                Offlinemode.showWhenOfflineMode(NewBuildingCaseActivity.this);
            }

        });

    }


    private void getContractorList(int buildingId,int page){
        controller.getContractorList(this, buildingId, "all", page, (contractorList) -> {
            if (contractorList.getContractorList().size()==0){
                this.dismissLoadingScreen();
                showContractorList();
            }else {
                for (Contractor c: contractorList.getContractorList()
                ) {
                    this.contractorList.addContrator(c);
                }
                getContractorList(buildingId,page+1);
            }
        }, (error) -> {
            this.dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(NewBuildingCaseActivity.this)) {
                Log.e("getInitData", error.toString());
            } else {
                Offlinemode.showWhenOfflineMode(NewBuildingCaseActivity.this);
            }
        });
    }

    private void showContractorList(){
        List<Integer> selectedContractor = this.buildingCase.getContractorsSelectedList();
        for (Integer contractorId:selectedContractor){
            this.contractorList.selectId(contractorId);
        }
        if (null != viewPager){
            NewBuildingCasePagerAdapter adapter = (NewBuildingCasePagerAdapter) viewPager.getAdapter();
            if (null != adapter){
                adapter.getCaseDetailFragment().setupData(this.buildingCase,this.contractorList);
                adapter.getAttachmentFragment().setup(this.buildingCase,this.contractorList);
                adapter.getAttachmentFragment().addListener(this);
                if (this.currentPage == 0){
                    adapter.getCaseDetailFragment().initData();
                }
            }
        }
    }


    @Override
    public void didAddAttachment() {
        NewBuildingCasePagerAdapter adapter = (NewBuildingCasePagerAdapter) viewPager.getAdapter();
        if (null != adapter){
            adapter.getCaseDetailFragment().updateUI();
        }
    }
}
