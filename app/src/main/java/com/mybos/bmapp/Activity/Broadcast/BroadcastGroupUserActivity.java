package com.mybos.bmapp.Activity.Broadcast;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Data.Model.Broadcast.Groups;
import com.mybos.bmapp.R;

import java.util.ArrayList;

public class BroadcastGroupUserActivity extends a_base_toolbar {

    ListView lvContactGroup;
    ListView lvCustomGroup;
    public static final String GROUP_NAME = "group_name";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_group_user);
        updateToolbarTitle("Contact group");

        lvContactGroup = findViewById(R.id.fragment1);
        lvCustomGroup = findViewById(R.id.fragment2);
        this.setItemsAndClickForList();

    }

    private void setItemsAndClickForList(){

        ArrayList<String> listContactgroup = new ArrayList<>();
        for(int i = 0; i < 7; i++) {
            listContactgroup.add(Groups.getGroups().get(i).getName());
        }
        ArrayList<String> listCustomgroup = new ArrayList<>();
        for(int i = 7; i < Groups.getGroups().size(); i++) {
            listCustomgroup.add(Groups.getGroups().get(i).getName());
        }
        ArrayAdapter adapter1 = new ArrayAdapter(BroadcastGroupUserActivity.this,
                android.R.layout.simple_expandable_list_item_1, listCustomgroup);
        lvCustomGroup.setAdapter(adapter1);
        ArrayAdapter adapter2 = new ArrayAdapter(BroadcastGroupUserActivity.this,
                android.R.layout.simple_expandable_list_item_1, listContactgroup);
        lvContactGroup.setAdapter(adapter2);

        lvContactGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(BroadcastGroupUserActivity.this, BroadcastActivity.class);
                intent.putExtra(GROUP_NAME, listContactgroup.get(i));
                startActivity(intent);
                finish();
            }
        });
        lvCustomGroup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(BroadcastGroupUserActivity.this, BroadcastActivity.class);
                intent.putExtra(GROUP_NAME, listCustomgroup.get(i));
                startActivity(intent);
                finish();
            }
        });
    }

}
