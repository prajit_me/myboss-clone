package com.mybos.bmapp.Activity.Calendar.CalendarCustom.Listener;

import java.util.Calendar;
import java.util.List;

public interface OnSelectDateListener {
    void onSelect(List<Calendar> calendar);
}
