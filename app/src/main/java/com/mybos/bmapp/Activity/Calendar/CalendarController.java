package com.mybos.bmapp.Activity.Calendar;

import android.content.Context;

import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Data.Mapper.Calendar.CalendarMapper;

public class CalendarController {

    public void getReminder (Context context, int buildingId, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.getReminder(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getBooking (Context context, int buildingId, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.getBooking(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getMaintenance (Context context, int buildingId, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.getMaintenance(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getDateMaintenance (Context context, int buildingId, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.getDateMaintenance(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void changeDateReminder (Context context, int buildingId,String reminderId, String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.changeDateReminder(context, buildingId,reminderId,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void changeStatusBooking (Context context, int buildingId,String bookingId, String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.changeStatusBooking(context, buildingId,bookingId,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void changeCommentBooking (Context context, int buildingId,String bookingId, String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.changeCommentBooking(context, buildingId,bookingId,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void changeStatusMaintenance(Context context, int buildingId,String maintenanceId,String dateOgrin,String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.changeStatusMaintenance(context, buildingId,maintenanceId,dateOgrin,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void addCommentMaintenance(Context context, int buildingId,String maintenanceId,String dateOgrin, String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.addCommentMaintenance(context, buildingId,maintenanceId,dateOgrin,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void removeCommentMaintenance(Context context, int buildingId,String maintenanceId,String dateOgrin, String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.removeCommentMaintenance(context, buildingId,maintenanceId,dateOgrin,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void moveMaintenance(Context context, int buildingId,String maintenanceId,String dateOgirn, String param, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.moveMaintenance(context, buildingId,maintenanceId,dateOgirn,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getConvert (Context context, int buildingId,String maintenanceId, KeysController.SuccessCallback<String> onSuccess, KeysController.FailureCallback onFailure) {
        CalendarMapper mapper = new CalendarMapper();
        mapper.getConvert(context, buildingId,maintenanceId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public interface SuccessCallback<T> {
        void onSuccess(T value);
    }

    public interface FailureCallback {
        void onFailure(Exception error);
    }
}
