package com.mybos.bmapp.Activity.Filter;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.Fragment.CaseListFragment;
import com.mybos.bmapp.Component.SpinnerAdapter.NullableKeepTrackSelectedSpinnerAdapter;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Data.Model.Filter.Priority;
import com.mybos.bmapp.Data.Model.Filter.Prioritylist;
import com.mybos.bmapp.Data.Model.Filter.Type;
import com.mybos.bmapp.Data.Model.Filter.TypeList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.List;

public class FilterCaseActivity extends a_base_toolbar {

    Spinner spiContractor,spiPriority,spiType,spiStatus;
    private static ContractorList contractorList;
    private static Prioritylist prioritylist;
    private static TypeList typeList;
    private static TypeList statusList;
    FlexboxLayout tagContractor,tagPriorityCloud,tagTypeCloud,tagStatusCloud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_case);

        contractorList = new ContractorList(ContractorList.getContractorListApply());
        prioritylist = new Prioritylist(Prioritylist.getPriorityListApply());
        typeList = new TypeList(TypeList.getTypelistApply());
        statusList = new TypeList(TypeList.getStatuslistApply());

        super.updateToolbarTitle(getString(R.string.case_filter_title));

        //get list type at one time with contractor
        setupContent();
        setupOptionBtn();
    }

    private void setupOptionBtn(){
        findViewById(R.id.btnOption).setVisibility(View.VISIBLE);
        findViewById(R.id.btnOption).setBackground(getDrawable(R.drawable.ic_done));
        findViewById(R.id.btnOption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CaseListFragment.loadCaseList = true;
                ContractorList.setContractorListApply(new ContractorList(contractorList));
                Prioritylist.setPriorityListApply(new Prioritylist(prioritylist));
                TypeList.setTypelistApply(new TypeList(typeList));
                TypeList.setStatuslistApply(new TypeList(statusList));

                Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
                FilterStoreRealm newFilter = new FilterStoreRealm();
                newFilter.setBuildingId(selectedBuilding.getId());
                newFilter.setSelectedFCContractor(contractorList.getSelectedId());
                newFilter.setSelectedFCPriority(prioritylist.getSelectedId());
                newFilter.setSelectedFCType(typeList.getSelectedId());
                newFilter.setSelectedFCStatus(statusList.getSelectedId());
                ListFilterStoreRealm temp = ListFilterStoreRealm.get();
                if (null!=temp){
                    if (temp.getFilterByBuildingId(selectedBuilding.getId()) != null){
                        temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCContractor(contractorList.getSelectedId());
                        temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCPriority(prioritylist.getSelectedId());
                        temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCType(typeList.getSelectedId());
                        temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCStatus(statusList.getSelectedId());
                    }else {
                        temp.addNewFilter(newFilter);
                    }

                }else{
                    temp = new ListFilterStoreRealm();
                    temp.addNewFilter(newFilter);

                }
                ListFilterStoreRealm.save(temp);
                FilterCaseActivity.this.finish();
            }
        });
    }

    private void setupContent(){
        tagContractor = findViewById(R.id.tagContractorCloud);
        tagPriorityCloud = findViewById(R.id.tagPriorityCloud);
        tagTypeCloud = findViewById(R.id.tagTypeCloud);
        tagStatusCloud = findViewById(R.id.tagStatusCloud);

        spiContractor = findViewById(R.id.spiContractor);
        spiPriority = findViewById(R.id.spiPriority);
        spiType = findViewById(R.id.spiType);
        spiStatus = findViewById(R.id.spiStatus);

        tagContractor.removeAllViews();
        tagPriorityCloud.removeAllViews();
        tagTypeCloud.removeAllViews();
        tagStatusCloud.removeAllViews();


        View view = new View(this);
        float densiry = this.getResources().getDisplayMetrics().density;
        view.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagContractor.addView(view);
        if (null != ContractorList.getContractorListApply()) {
            if(null != ContractorList.getContractorListApply().getSelectedId()) {
                for (int id : ContractorList.getContractorListApply().getSelectedId()) {
                    contractorList.selectId(id);
                }
            }
        }
        List<BaseModel> selectedContractorModel = contractorList.getSelectedItem();
        for (BaseModel item : selectedContractorModel) {
            if (item instanceof Contractor) {
                addContractorTag((Contractor) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapter = new NullableKeepTrackSelectedSpinnerAdapter(this, contractorList);
        spiContractor.setAdapter(adapter);
        spiContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spiContractor.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiContractor.getAdapter();
                Contractor contractor = (Contractor) mAdapter.getItem(position);
                if (null != contractor) {
                    mAdapter.selectItem(contractor);
                    addContractorTag(contractor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        View view2 = new View(this);
//        float densiry2 = this.getResources().getDisplayMetrics().density;
        view2.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagPriorityCloud.addView(view2);
        if (null != Prioritylist.getPriorityListApply()) {
            if (null != Prioritylist.getPriorityListApply().getSelectedId()) {
                for (int id : Prioritylist.getPriorityListApply().getSelectedId()) {
                    prioritylist.selectId(id);
                }
            }
        }
        List<BaseModel> selectedContractorModelpri = prioritylist.getSelectedItem();
        for (BaseModel item : selectedContractorModelpri) {
            if (item instanceof Priority) {
                addPriorityTag((Priority) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapterPri = new NullableKeepTrackSelectedSpinnerAdapter(this, prioritylist);
        spiPriority.setAdapter(adapterPri);
        spiPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spiPriority.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiPriority.getAdapter();
                Priority priority = (Priority) mAdapter.getItem(position);
                if (null != priority) {
                    mAdapter.selectItem(priority);
                    addPriorityTag(priority);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        View view3 = new View(this);
//        float densiry2 = this.getResources().getDisplayMetrics().density;
        view3.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagTypeCloud.addView(view3);
        if (null != TypeList.getTypelistApply()) {
            if (null != TypeList.getTypelistApply().getSelectedId()) {
                for (int id : TypeList.getTypelistApply().getSelectedId()) {
                    typeList.selectId(id);
                }
            }
        }
        List<BaseModel> selectedContractorModeltype = typeList.getSelectedItem();
        for (BaseModel item : selectedContractorModeltype) {
            if (item instanceof Type) {
                addTypeTag((Type) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapterType = new NullableKeepTrackSelectedSpinnerAdapter(this, typeList);
        spiType.setAdapter(adapterType);
        spiType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spiType.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiType.getAdapter();
                Type type = (Type) mAdapter.getItem(position);
                if (null != type) {
                    mAdapter.selectItem(type);
                    addTypeTag(type);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        View view4 = new View(this);
        view4.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagStatusCloud.addView(view4);
        if (null != TypeList.getStatuslistApply()) {
            if (null != TypeList.getStatuslistApply().getSelectedId()) {
                for (int id : TypeList.getStatuslistApply().getSelectedId()) {
                    statusList.selectId(id);
                }
            }
        }
        List<BaseModel> selectedContractorModelStatus = statusList.getSelectedItem();
        for (BaseModel item : selectedContractorModelStatus) {
            if (item instanceof Type) {
                addStatusTag((Type) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapterStatus = new NullableKeepTrackSelectedSpinnerAdapter(this, statusList);
        spiStatus.setAdapter(adapterStatus);
        spiStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                spiStatus.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiStatus.getAdapter();
                Type type = (Type) mAdapter.getItem(position);
                if (null != type) {
                    mAdapter.selectItem(type);
                    addStatusTag(type);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void addContractorTag(Contractor contractor){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagContractor,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(contractor.getCompany());
        textView.setTag(contractor);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Contractor){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiContractor.getAdapter();
                mAdapter.deselectItem((Contractor)object);
                tagContractor.removeView(tag);
                contractorList.deselectId(((Contractor) object).getId());
            }
        });
        tagContractor.addView(tag);
    }

    public void addPriorityTag(Priority priority){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagPriorityCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(priority.getName());
        textView.setTag(priority);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Priority){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiPriority.getAdapter();
                mAdapter.deselectItem((Priority)object);
                tagPriorityCloud.removeView(tag);
                prioritylist.deselectId(((Priority) object).getId());
            }
        });
        tagPriorityCloud.addView(tag);
    }

    public void addTypeTag(Type type){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagTypeCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(type.getName());
        textView.setTag(type);
        textView.setOnClickListener(v -> {
            Log.e("check_click","click_remove");
            Object object = v.getTag();
            if (object instanceof Type){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiType.getAdapter();
                mAdapter.deselectItem((Type)object);
                tagTypeCloud.removeView(tag);
                typeList.deselectId(((Type) object).getId());
            }
        });
        tagTypeCloud.addView(tag);
    }

    public void addStatusTag(Type type){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagStatusCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(type.getName());
        textView.setTag(type);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Type){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spiStatus.getAdapter();
                mAdapter.deselectItem((Type)object);
                tagStatusCloud.removeView(tag);
                statusList.deselectId(((Type) object).getId());
            }
        });
        tagStatusCloud.addView(tag);
    }

    public static ContractorList getContractorList() {
        return contractorList;
    }

    public static void setContractorList(ContractorList contractorList) {
        FilterCaseActivity.contractorList = contractorList;
    }

    public static Prioritylist getPrioritylist() {
        return prioritylist;
    }

    public static void setPrioritylist(Prioritylist prioritylist) {
        FilterCaseActivity.prioritylist = prioritylist;
    }

    public static TypeList getTypeList() {
        return typeList;
    }

    public static void setTypeList(TypeList typeList) {
        FilterCaseActivity.typeList = typeList;
    }

    public static TypeList getStatusList() {
        return statusList;
    }

    public static void setStatusList(TypeList statusList) {
        FilterCaseActivity.statusList = statusList;
    }


    //end file
}