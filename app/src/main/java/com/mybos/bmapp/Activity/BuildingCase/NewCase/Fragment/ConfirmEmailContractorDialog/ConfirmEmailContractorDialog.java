package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.ConfirmEmailContractorDialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 14/04/2018.
 *
 * @author EmLaAi
 */
public class ConfirmEmailContractorDialog extends DialogFragment {
    public ConfirmEmailContractorDialog(){}
    private OnAcceptClick acceptClick;
    private OnCancelClick cancelClick;

    public static ConfirmEmailContractorDialog getInstance(){
        return new ConfirmEmailContractorDialog();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.a_new_case_d_confrim_email_contractor,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnYes).setOnClickListener(v -> {
            dismiss();
            if (null != acceptClick){
                acceptClick.handleAcceptClick();
            }
        });
        view.findViewById(R.id.btnNo).setOnClickListener(v -> {
            dismiss();
            if (null != cancelClick){
                cancelClick.handleCancelClick();
            }
        });
    }

    public void setAcceptClick(OnAcceptClick acceptClick){
        this.acceptClick = acceptClick;
    }
    public void setCancelClick(OnCancelClick cancelClick){
        this.cancelClick = cancelClick;
    }

    public interface OnAcceptClick{
        void handleAcceptClick();
    }
    public interface OnCancelClick{
        void handleCancelClick();
    }
}
