package com.mybos.bmapp.Activity.Keys;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.Keys.KeyMapper;
import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.Data.Model.Key.BuildingKey;
import com.mybos.bmapp.Data.Model.Key.ContractorKey;
import com.mybos.bmapp.Data.Model.Key.History;

import java.util.ArrayList;

public class KeysController {

    public void getPrivateKeys (Context context, int buildingId, SuccessCallback<ArrayList<Apartment>> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.getPrivateKeyList(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getContractorKeys (Context context, int buildingId, SuccessCallback<ArrayList<BuildingKey>> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.getContractorKeyList(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getApartmentKey (Context context, int buildingId,String param, SuccessCallback<Apartment> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.getKeyApartment(context, buildingId,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getKeyHistory (Context context, int buildingId,SuccessCallback<History> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.getKeyHistory(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void getContractorSignIn (Context context, int buildingId, SuccessCallback<ContractorKey> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.getContractorSignIn(context, buildingId,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void postSignOut (Context context, int buildingId,String keyId, String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.postSignOut(context, buildingId,keyId,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public void postSignIn (Context context, int buildingId,String keyId, String param, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        KeyMapper mapper = new KeyMapper();
        mapper.postSignIn(context, buildingId,keyId,param,onSuccess::onSuccess, onFailure::onFailure);
    }

    public interface SuccessCallback<T> {
        void onSuccess(T value);
    }

    public interface FailureCallback {
        void onFailure(Exception error);
    }
}
