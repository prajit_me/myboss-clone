package com.mybos.bmapp.Activity.BuildingCase.EditCase;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseMapper;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;

/**
 * Created by EmLaAi on 23/04/2018.
 *
 * @author EmLaAi
 */
public class EditCaseController {

    public void getContractorList(Context context, int buildingId, String selectedCategory,int page, SuccessCallback<ContractorList> onSuccess, FailureCallback onError){
        ContractorMapper mapper = new ContractorMapper();
        mapper.get(context,buildingId,selectedCategory,"",page,onSuccess::onSuccess,onError::onFailure);
    }
    public void getBuildingCaseDetail(Context context, int buildingId, String caseId , SuccessCallback<BuildingCase> onSuccess, FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.get(context,buildingId,caseId,onSuccess::onSuccess,onError::onFailure);
    }

    public void getMessage(Context context, int buildingId, String caseId , SuccessCallback<String> onSuccess, FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.getMessage(context,buildingId,caseId,onSuccess::onSuccess,onError::onFailure);
    }

    public void sendMessage(Context context, int buildingId, String caseId,String comment, SuccessCallback<String> onSuccess, FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.sendMessage(context,buildingId,caseId,comment,onSuccess::onSuccess,onError::onFailure);
    }

    public void updateMessage(Context context, int buildingId, String caseId,String commentId,String comment, SuccessCallback<String> onSuccess, FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.updateMessage(context,buildingId,caseId,commentId,comment,onSuccess::onSuccess,onError::onFailure);
    }

    public void deleteMessage(Context context, int buildingId, String caseId,String commentId, SuccessCallback<String> onSuccess, FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.deleteMessage(context,buildingId,caseId,commentId,onSuccess::onSuccess,onError::onFailure);
    }



    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
