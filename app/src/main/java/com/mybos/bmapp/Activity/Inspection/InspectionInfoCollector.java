package com.mybos.bmapp.Activity.Inspection;

import android.util.Log;
import android.widget.Toast;

import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItem;

import java.util.ArrayList;

public class InspectionInfoCollector {

    private static ArrayList<InspectionItem> UpdatedInspectionItemList = new ArrayList<>();

    public static void clearInspectionItemList() {
        UpdatedInspectionItemList = new ArrayList<>();
    }

    public static ArrayList<InspectionItem> getUpdatedInspectionItemList() {
        return UpdatedInspectionItemList;
    }

    public static void addToUpdatedInspectionItemList(InspectionItem SelectedInspectionItem) {
        UpdatedInspectionItemList.add(SelectedInspectionItem);
    }

    public static void setUpdatedInspectionItemList(ArrayList<InspectionItem> updatedInspectionItemList) {
        UpdatedInspectionItemList = updatedInspectionItemList;
    }

    public static InspectionItem getInspection(InspectionItem inspectionItemFinding){
        InspectionItem item = inspectionItemFinding;
        for (InspectionItem inspectionI:UpdatedInspectionItemList
             ) {
            if (inspectionI.getId()==inspectionItemFinding.getId()){
                item = inspectionI;
            }
        }
        return item;
    }

    public static void updateInspectionItemList(InspectionItem SelectedInspectionItem) {
        if (UpdatedInspectionItemList.contains(SelectedInspectionItem)) {
            for (InspectionItem item: UpdatedInspectionItemList
                 ) {
                if(item.getId()==SelectedInspectionItem.getId()){
                    item.setComment(SelectedInspectionItem.getComment());
                    item.setImage(SelectedInspectionItem.getImage());
//                    item.setIsChecked(SelectedInspectionItem.getIsChecked()); checking is need it
                }
            }
        }
        else {
            UpdatedInspectionItemList.add(SelectedInspectionItem);
        }
    }
}
