package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mybos.bmapp.R;

public class KeySpinnerSignInAdapter extends BaseAdapter {

    private Context context;
    private String title;
    private String value;

    public KeySpinnerSignInAdapter(Context context, String title, String value) {
        this.context = context;
        this.title = title;
        this.value = value;
    }


    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.key_signin_spinner_line, null);

        TextView tvNameSignInA = view.findViewById(R.id.tvNameSignInA);
        TextView tvValueSigninA = view.findViewById(R.id.tvValueSigninA);

        tvNameSignInA.setText(title);
        tvValueSigninA.setText(value);


        return view;
    }
}
