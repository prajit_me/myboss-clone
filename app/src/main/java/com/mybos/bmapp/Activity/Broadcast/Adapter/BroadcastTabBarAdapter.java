package com.mybos.bmapp.Activity.Broadcast.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.R;

import java.util.Arrays;
import java.util.List;

public class BroadcastTabBarAdapter extends FragmentPagerAdapter {

    private @SearchTableQueryBuilder.SectionHint.BroadcastSectionHint
    List<Integer> sections = Arrays.asList(SearchTableQueryBuilder.SectionHint.BroadcastEmail,
            SearchTableQueryBuilder.SectionHint.BroadcastSMS);
    private Context context;

    public BroadcastTabBarAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return new Fragment();
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        int section = sections.get(position);
        if (section == SearchTableQueryBuilder.SectionHint.BroadcastEmail){
            return context.getString(R.string.broadcast_list_section_email);
        }
        if (section == SearchTableQueryBuilder.SectionHint.BroadcastSMS){
            return context.getString(R.string.broadcast_list_section_sms);
        }
        return super.getPageTitle(position);
    }
}

