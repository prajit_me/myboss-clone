package com.mybos.bmapp.Activity.Resident.ResidentList;

import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListActivity;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Filter.FilterCaseActivity;
import com.mybos.bmapp.Activity.Filter.FilterStoreRealm;
import com.mybos.bmapp.Activity.Filter.ListFilterStoreRealm;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Activity.Resident.Adapter.ResidentRecyclerAdapter;
import com.mybos.bmapp.Activity.Filter.FilterFragment;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Data.Model.Filter.Prioritylist;
import com.mybos.bmapp.Data.Model.Filter.TypeList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.List;

public class ResidentListActivity extends a_base_toolbar {
    private RecyclerView rclResidentList;
    private int currentPage = 1;
    private String keyword="";
    private ResidentController controller = new ResidentController();
    private EditText etSearch;
    private String section = ALL;
    private Button btnTagFilter;
    private boolean checkInside = false;

    public static final String ALL = "all";
    public static final String OWNER = "owner";
    public static final String TENANT = "tenant";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_resident_list);

        //IMPORTANT: setup drawer with drop down building list
        setupRecycleView();
        setupSearchView();
        setupBtnFilter();
        getRealmData();
        super.updateToolbarTitle(getString(R.string.resident_list_title));
    }

    public void getRealmData(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        ListFilterStoreRealm listFilterRealm = ListFilterStoreRealm.get();
        if (null!=listFilterRealm && null!=listFilterRealm.getFilterByBuildingId(selectedBuilding.getId())){
            FilterStoreRealm filterRealm = listFilterRealm.getFilterByBuildingId(selectedBuilding.getId());
            if (null!=filterRealm.getSelectedFilterResident())
            {
                section = filterRealm.getSelectedFilterResident();
                addTag(filterRealm.getSelectedFilterResidentName(),false);

            }

        }
//        getResidentList();
    }

    public void setupBtnFilter(){
        findViewById(R.id.btnFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterFragment dialog = new FilterFragment(1);
                dialog.show(getSupportFragmentManager(),"Filter");
            }
        });
    }

    public void setupRecycleView(){
        rclResidentList = findViewById(R.id.rclResident);
        rclResidentList.setLayoutManager(new LinearLayoutManager(this));
        ResidentRecyclerAdapter adapter = new ResidentRecyclerAdapter();
        adapter.setup(rclResidentList);
        rclResidentList.setAdapter(adapter);
        adapter.setOnNeedLoadMoreListener(this::getResidentList);
    }

    public void setupSearchView(){
        btnTagFilter = findViewById(R.id.btnTagFilter);
        etSearch = findViewById(R.id.etSearchView);
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                v.clearFocus();
                dismissKeyboard();
                performSearch();
                return true;
            }
            return false;
        });
    }

    public void addTag(String name,boolean save){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        getResidentList();
        btnTagFilter.setText(name);
        btnTagFilter.setVisibility(View.VISIBLE);
        btnTagFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentPage = 1;
                btnTagFilter.setVisibility(View.GONE);
                section = ALL;
                getResidentList();
                ListFilterStoreRealm temp2 = ListFilterStoreRealm.get();
                if (null!=temp2){
                    if (temp2.getFilterByBuildingId(selectedBuilding.getId()) != null){
                        temp2.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterResident(null);
                    }
                }
                ListFilterStoreRealm.save(temp2);
            }

        });
        //save Realm
        if (save){
            ListFilterStoreRealm temp = ListFilterStoreRealm.get();
            FilterStoreRealm newFilter = new FilterStoreRealm();
            newFilter.setBuildingId(selectedBuilding.getId());
            newFilter.setSelectedFilterResident(section);
            newFilter.setSelectedFilterResidentName(name);
            if (null!=temp){
                if (temp.getFilterByBuildingId(selectedBuilding.getId()) != null){
                    temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterResident(section);
                    temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterResidentName(name);
                }else {
                    temp.addNewFilter(newFilter);
                }

            }else{
                temp = new ListFilterStoreRealm();
                temp.addNewFilter(newFilter);

            }
            ListFilterStoreRealm.save(temp);
        }

    }

    public void getResidentList(){
        if (checkInside){
            return;
        }
        checkInside = true;
        Log.e("check","get_resident"+currentPage);
        SearchTableQueryBuilder searchTableQueryBuilder = new SearchTableQueryBuilder();
        searchTableQueryBuilder.setCurrentPage(this.currentPage);
        searchTableQueryBuilder.setKeyword(this.keyword);
        searchTableQueryBuilder.setSection(section);
        final String check = keyword;
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.get(this,selectedBuilding.getId(),searchTableQueryBuilder,response->{
            if (null != check && !check.equals(this.keyword)){
                return;
            }
            if (null == check && null != keyword){
                return;
            }
            if (currentPage == 1 ){
                ((ResidentRecyclerAdapter) rclResidentList.getAdapter()).reset();
            }
            ResidentRecyclerAdapter adapter = (ResidentRecyclerAdapter) rclResidentList.getAdapter();
            adapter.setMaximum(response.getMaximum());
            adapter.updateModel(response.getResidents());
            adapter.notifyDataSetChanged();
            this.currentPage += 1;
            checkInside = false;
        },error -> {
            if (isVisible) {
                if (Offlinemode.checkNetworkConnection(ResidentListActivity.this)){
                    Log.e("getResidentList",error.toString());
                }else{
                    Intent intent = new Intent(ResidentListActivity.this, DashboardActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    public void performSearch(){
        this.currentPage = 1;
        this.keyword = etSearch.getText().toString();
        getResidentList();
//        ((ResidentRecyclerAdapter) rclResidentList.getAdapter()).reset();
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }


    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

}
