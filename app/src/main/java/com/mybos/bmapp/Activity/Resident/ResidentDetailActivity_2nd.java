package com.mybos.bmapp.Activity.Resident;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Activity.Resident.ResidentDetail.Component.ResidentBasicInfoComponent;
import com.mybos.bmapp.Activity.Resident.ResidentDetail.Component.ResidentEmergencyComponent;
import com.mybos.bmapp.Activity.Resident.ResidentDetail.ResidentDetailController;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Resident.Resident;
import com.mybos.bmapp.R;
import com.bumptech.glide.Glide;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class ResidentDetailActivity_2nd extends a_base_toolbar {

    public static final String RESIDENT_KEY = "ResidentDetailActivity.RESIDENT_KEY";
    public final int CALL_PHONE_PERMISSION = 1000;
    private ResidentBasicInfoComponent viewBaisInfo;
    private ResidentEmergencyComponent viewEmergency;
    private ImageView imgAvartar;
    private Resident resident;
    private String savedNumber;
    private TextView tvApartmentId;
    private ResidentDetailController controller = new ResidentDetailController();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resident_detail_2nd);


        resident = getIntent().getParcelableExtra(RESIDENT_KEY);
        imgAvartar = findViewById(R.id.imgAvatar);
        tvApartmentId = findViewById(R.id.tvApartmentId);
        setupResidentBasicInfo();
        getExtraDetail();
        if (null != getSupportActionBar()) {
            updateToolbarTitle(resident.getFullName());
        }
    }

    public void setupResidentBasicInfo(){
        viewBaisInfo = findViewById(R.id.viewBasicInfo);
        viewBaisInfo.applyInfo(resident);
        tvApartmentId.setText("Apartment "+resident.getApartment());
        viewBaisInfo.setEmailClickListener(()-> sendEmail(resident.getEmail()));
        viewBaisInfo.setHomeNumberClickListener(()-> callNumber(resident.getPhone()));
        viewBaisInfo.setMobileNumberClickListener(()->this.callNumber(resident.getMobile()));
    }

    public void setupEmergencyInfo(String contact,String phone){
        viewEmergency = findViewById(R.id.viewEmergency);
        viewEmergency.applyInfo(contact,phone);
        viewEmergency.setListener(()->this.callNumber(phone));
    }

    public void getExtraDetail(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getExtraDetailResident(this,selectedBuilding.getId(),resident.getUid(),resident.getId(),response->{
            setupEmergencyInfo(response.getEmergencyContact(),response.getEmergencyContactPhone());
            if (response.getUrl().length() == 0){
                //do nothing
            }
            else {
                Glide.with(this).load(response.getUrl()).into(imgAvartar);
            }
        },error -> {
            if (isVisible){
                if (Offlinemode.checkNetworkConnection(ResidentDetailActivity_2nd.this)){
                    Log.e("getExtraDetail",error.toString());
                }else{
                    Offlinemode.showWhenOfflineMode(ResidentDetailActivity_2nd.this);
                }
            }
        });
    }

    public void callNumber(String number){
        if (number == null){
            return;
        }
        String uriNumber = "tel:" + number.replace("\\s+","");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uriNumber));
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            savedNumber = number;
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.CALL_PHONE},CALL_PHONE_PERMISSION);
        }else {
            startActivity(intent);
        }
    }

    public void sendEmail(String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
        if (intent.resolveActivity(getPackageManager()) != null){
            startActivity(intent);
        }else {
            Log.e("sendEmail",getString(R.string.ALertNoEmailApp));
            Toast.makeText(this, getString(R.string.ALertNoEmailApp), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CALL_PHONE_PERMISSION){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                callNumber(savedNumber);
            }else {
                Log.e("onRequestPermission",getString(R.string.AlertPermissionGrantFailure));
                Toast.makeText(this, getString(R.string.AlertPermissionGrantFailure), Toast.LENGTH_LONG).show();
            }
        }
    }
}
