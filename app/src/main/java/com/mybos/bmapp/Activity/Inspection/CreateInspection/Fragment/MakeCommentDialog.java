package com.mybos.bmapp.Activity.Inspection.CreateInspection.Fragment;


import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.mybos.bmapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MakeCommentDialog extends DialogFragment {
    private static final String COMMENT_KEY = "MakeCommentDialog.COMMENT_KEY";
    private String saveComment;
    private EditText txtComment;
    private OnSaveClick saveClick;

    public MakeCommentDialog() {
        // Required empty public constructor
    }

    public static MakeCommentDialog createInstance(String saveComment){
        MakeCommentDialog fragment = new MakeCommentDialog();
        Bundle bundle = new Bundle();
        bundle.putString(COMMENT_KEY,saveComment);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.a_make_inspection_d_make_comment, container, false);
        getDialog().setCanceledOnTouchOutside(true);
        if (null != getArguments()){
            saveComment = getArguments().getString(COMMENT_KEY);
        }
        txtComment = view.findViewById(R.id.txtComment);
        txtComment.setText(saveComment);

        view.findViewById(R.id.btnSave).setOnClickListener(v -> {
            String comment = txtComment.getText().toString();
            if (null != saveClick){
                saveClick.saveComment(comment);
            }
            this.dismiss();
        });
        view.findViewById(R.id.btnCancel).setOnClickListener(v -> this.dismiss());

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
        int width = (int) ( Resources.getSystem().getDisplayMetrics().widthPixels * 0.9);
        if (null != getDialog().getWindow()) {
            getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

    }

    public void setSaveClick(OnSaveClick saveClick) {
        this.saveClick = saveClick;
    }

    public interface OnSaveClick{
        void saveComment(String comment);
    }

}
