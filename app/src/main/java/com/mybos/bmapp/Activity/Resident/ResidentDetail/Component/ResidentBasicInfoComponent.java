package com.mybos.bmapp.Activity.Resident.ResidentDetail.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Resident.Resident;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentBasicInfoComponent extends ConstraintLayout {
    TextView txtHomeNumber,txtMobileNumber,txtEmail,txtECMember;
    private OnHomeNumberClickListener homeNumberClickListener;
    private OnMobileNumberClickListener mobileNumberClickListener;
    private OnEmailClickListener emailClickListener;
    public ResidentBasicInfoComponent(Context context) {
        super(context);
        additionalInit();
    }

    public ResidentBasicInfoComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public ResidentBasicInfoComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_resident_detail_c_resident_basic_info,this);
        txtHomeNumber = findViewById(R.id.txtHomeNumber);
        txtMobileNumber = findViewById(R.id.txtMobileNumber);
        txtEmail = findViewById(R.id.txtEmail);
        txtECMember = findViewById(R.id.txtECMember);
        findViewById(R.id.imgHomeNumber).setOnClickListener(v -> {
            if (homeNumberClickListener != null){
                homeNumberClickListener.homePhoneClick();
            }
        });
        findViewById(R.id.imgMobileNumber).setOnClickListener(v -> {
            if (mobileNumberClickListener != null){
                mobileNumberClickListener.mobileNumberClick();
            }
        });
        findViewById(R.id.imgEmail).setOnClickListener(v -> {
            if (emailClickListener != null){
                emailClickListener.emailClick();
            }
        });
    }

    public void setEmailClickListener(OnEmailClickListener emailClickListener) {
        this.emailClickListener = emailClickListener;
    }

    public void setHomeNumberClickListener(OnHomeNumberClickListener homeNumberClickListener) {
        this.homeNumberClickListener = homeNumberClickListener;
    }

    public void setMobileNumberClickListener(OnMobileNumberClickListener mobileNumberClickListener) {
        this.mobileNumberClickListener = mobileNumberClickListener;
    }

    public void applyInfo(Resident resident){
        txtHomeNumber.setText(resident.getPhone());
        txtMobileNumber.setText(resident.getMobile());
        txtEmail.setText(resident.getEmail());
        txtECMember.setText(resident.getMemberText());
    }

    public interface OnHomeNumberClickListener{
        void homePhoneClick();
    }

    public interface OnMobileNumberClickListener{
        void mobileNumberClick();
    }

    public interface OnEmailClickListener{
        void emailClick();
    }
}
