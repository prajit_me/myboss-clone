package com.mybos.bmapp.Activity.Dashboard.Controller;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.Auth.FeaturesMapper;
import com.mybos.bmapp.Data.Mapper.Building.BuildingMapper;
import com.mybos.bmapp.Data.Mapper.Auth.PermissionMapper;
import com.mybos.bmapp.Data.Mapper.Firebase.FirebaseMapper;
import com.mybos.bmapp.Data.Mapper.Weather.WeatherMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;
import com.mybos.bmapp.Data.Response.WeatherResponse;

/**
 * Created by EmLaAi on 16/01/2018.
 *
 * @author EmLaAi
 */

public class DashboardController {
    public void getPermission(int buildingId,Context context,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        PermissionMapper mapper = new PermissionMapper();
        mapper.get(buildingId,context,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void getFeatures(int buildingId,Context context,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        FeaturesMapper mapper = new FeaturesMapper();
        mapper.get(buildingId,context,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getBuildingList(Context context, SuccessCallback<BuildingList> onSuccess, FailureCallback onFailure){
        BuildingMapper mapper = new BuildingMapper();
        mapper.get(context,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void getWeatherInfo(Context context, Building building, SuccessCallback<WeatherResponse> onSuccess, FailureCallback onFailure){
        WeatherMapper mapper = new WeatherMapper();
        mapper.getWeatherForecast(context,building,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void getVersionFromStore(Context context, SuccessCallback<String> onSuccess, FailureCallback onFailure){
        BuildingMapper mapper = new BuildingMapper();
        mapper.getVersionFromStore(context,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void activeDevice(Context context, String token, SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        FirebaseMapper mapper = new FirebaseMapper();
        mapper.activeDevice(context,token,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void LogoutDevice(Context context, String token, SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        FirebaseMapper mapper = new FirebaseMapper();
        mapper.dologOutNRemoveToken(context,token,onSuccess::onSuccess,onFailure::onFailure);
    }


    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
