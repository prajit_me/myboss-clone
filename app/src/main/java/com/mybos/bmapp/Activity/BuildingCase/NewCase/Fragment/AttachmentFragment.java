package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment;


import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentDialog.Attachment3PhotoDialog;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentDialog.AttachmentPhotoDialog;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentDialog.AttachmentUploadDialog;
import com.mybos.bmapp.Activity.WebView.WebViewActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.RecycleAdapter.MultiSectionRecyclerAdapter;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentInterface;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Cases.SelectedFileInvoiceQuotes;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.BuildFileNameForUploadDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class AttachmentFragment extends BaseFragment implements Attachment3PhotoDialog.AttachmentUploadPhotosDialogListener ,AttachmentPhotoDialog.AttachmentUploadDialogListener, AttachmentUploadDialog.AttachmentUploadDialogListener, MultiSectionRecyclerAdapter.OnItemSelectedListener {
    public static final String CUSTOM_TAB_PACKAGE_NAME = "com.chrome.dev";
    Button btnAddPhoto, btnAddQuote,btnAddInvoice;
    RecyclerView rclPhotoAttachment, rclQuoteAttachment, rclInvoiceAttachment, rclAttachmentList, rclDocAttachment;
    ContractorList contractorList;
    BuildingCase buildingCase;
    AttachmentController controller = new AttachmentController();
    AttachmentEventListener listener;
    public static final int PICKFILE_RESULT_CODE = 1;
    private Uri fileUri;
    private String filePath;
    private ArrayList<String> filePaths;
    public final int REQUEST_PERMISSION = 99;
    Attachment3PhotoDialog dialog;
    public static byte[] selectedByteFile;
    public static String selectedFileName;
    public static String selectedFileMime;

    public AttachmentFragment() {
        // Required empty public constructor
    }

    public static AttachmentFragment newInstance(){
        AttachmentFragment fragment = new AttachmentFragment();
        return fragment;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (null != savedInstanceState){
            this.buildingCase = savedInstanceState.getParcelable("BuildingCase");
            this.contractorList = savedInstanceState.getParcelable("Contractor");
        }else if (BuildingCase.getSelectedBuildingCase()!=null){
            this.buildingCase = BuildingCase.getSelectedBuildingCase();
        }
        setupRecycleView();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("check","onResume");
        if (null != this.buildingCase && null != this.contractorList){
            setupRecycleView();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.e("check","onCreate");
        View view = inflater.inflate(R.layout.a_edit_new_case_f_attachment, container, false);
        rclPhotoAttachment = view.findViewById(R.id.rclPhotoAttachment);
        MultiSectionRecyclerAdapter PhotoAdapter = new MultiSectionRecyclerAdapter();
        PhotoAdapter.setListener(this);
        rclPhotoAttachment.setLayoutManager(new LinearLayoutManager(getContext()));
        rclPhotoAttachment.setAdapter(PhotoAdapter);

        view.findViewById(R.id.btnAddPhoto_AttachmentFragment).setOnClickListener(v -> {
            if (buildingCase.getPhotos().size()<3) {
                Attachment3PhotoDialog.setMaxImage(buildingCase.getPhotos().size());
                dialog = Attachment3PhotoDialog.createInstance(this.contractorList, AttachmentPhotoDialog.PHOTO_MODE);
                dialog.setListener(this);
                dialog.show(getFragmentManager(), "TAG");
            }else {
                Toast.makeText(getContext(), "You can't add more than three photos", Toast.LENGTH_SHORT).show();
            }
        });


        rclQuoteAttachment = view.findViewById(R.id.rclQuoteAttachment);
        MultiSectionRecyclerAdapter QuoteAdapter = new MultiSectionRecyclerAdapter();
        QuoteAdapter.setListener(this);
        rclQuoteAttachment.setLayoutManager(new LinearLayoutManager(getContext()));
        rclQuoteAttachment.setAdapter(QuoteAdapter);

        view.findViewById(R.id.btnAddQuote_AttachmentFragment).setOnClickListener(v -> {
            AttachmentUploadDialog dialog = AttachmentUploadDialog.createInstance(this.contractorList,AttachmentUploadDialog.QUOTE_MODE);
            dialog.setListener(this);
            dialog.show(getFragmentManager(),"TAG");
        });

        rclInvoiceAttachment = view.findViewById(R.id.rclInvoiceAttachment);
        MultiSectionRecyclerAdapter InvoiceAdapter = new MultiSectionRecyclerAdapter();
        InvoiceAdapter.setListener(this);
        rclInvoiceAttachment.setLayoutManager(new LinearLayoutManager(getContext()));
        rclInvoiceAttachment.setAdapter(InvoiceAdapter);

        view.findViewById(R.id.btnAddInvoice_AttachmentFragment).setOnClickListener(v -> {
            AttachmentUploadDialog dialog = AttachmentUploadDialog.createInstance(this.contractorList,AttachmentUploadDialog.INVOICE_MODE);
            dialog.setListener(this);
            dialog.show(getFragmentManager(),"TAG");
        });

        rclDocAttachment = view.findViewById(R.id.rclDocAttachment);
        MultiSectionRecyclerAdapter DocApdapter = new MultiSectionRecyclerAdapter();
        DocApdapter.setListener(this);
        rclDocAttachment.setLayoutManager(new LinearLayoutManager(getContext()));
        rclDocAttachment.setAdapter(DocApdapter);

        view.findViewById(R.id.btnAddDoc_AttachmentFragment).setOnClickListener(v -> {

            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
        });

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(getActivity(), listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION);
        }

        return view;
    }

    //Active Result Upload document
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case PICKFILE_RESULT_CODE:
                if (resultCode == -1) {

                    fileUri = data.getData();
                    File file = new File(fileUri.getPath());
                    InputStream inputStream = null;
                    try {
                        inputStream = getContext().getContentResolver().openInputStream(fileUri);
                        selectedByteFile = new byte[inputStream.available()];
                        inputStream.read(selectedByteFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Log.e("error_get_file",e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("error_get_file2",e.toString());
                    }

                    filePath = fileUri.getLastPathSegment();
                    Log.e("check_path",file.getAbsolutePath()+"--"+file.getPath());
                    ContentResolver cR = getContext().getContentResolver();
                    String mime = cR.getType(fileUri);
                    Log.e("check_mime",mime+"-"+fileUri.getLastPathSegment()+"--"+file.getName());
                    selectedFileMime = mime;
                    String name = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf(":")+1,file.getAbsolutePath().length()-1);
                    selectedFileName = BuildFileNameForUploadDocument.checkAndBuildFileName(name,mime);
                    Log.e("check_name",selectedFileName);
                    uploadDocument(new File(fileUri.getPath()));
                }
                break;

        }
    }

    public  String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public void setup(BuildingCase buildingCase, ContractorList contractor){
        this.buildingCase = buildingCase;
        this.contractorList = contractor;
        if (rclAttachmentList != null) {
            setupRecycleView();
        }
    }

    public void setupDocumentRecycleView(){
        MultiSectionRecyclerAdapter adapter = (MultiSectionRecyclerAdapter) rclDocAttachment.getAdapter();
        List<BaseModel> docs = new ArrayList<>();
        if(buildingCase!=null) {
            if (buildingCase.getDocs() != null) {
                docs = new ArrayList<>(buildingCase.getDocs());
            }
        }
        MultiSectionRecyclerAdapter.SectionList doc;

        rclDocAttachment.setVisibility(View.VISIBLE);
        doc = new MultiSectionRecyclerAdapter.SectionList(getString(R.string.case_attachment_section_title_document), docs);

        adapter.clearSource();
        adapter.addSection(doc);
        adapter.parseSource();
    }


    public void setupPhotoRecycleView(){
        MultiSectionRecyclerAdapter adapter = (MultiSectionRecyclerAdapter) rclPhotoAttachment.getAdapter();
        List<BaseModel> photos = new ArrayList<>();

        if(buildingCase!=null) {
            if (buildingCase.getPhotos() != null) {
                photos = new ArrayList<>(buildingCase.getPhotos());
            }
        }

        MultiSectionRecyclerAdapter.SectionList photo;

        rclPhotoAttachment.setVisibility(View.VISIBLE);
        photo = new MultiSectionRecyclerAdapter.SectionList(getString(R.string.case_attachment_section_title_photo), photos);


        adapter.clearSource();
        adapter.addSection(photo);
        adapter.parseSource();
    }

    public void setupQuoteRecycleView(){
        MultiSectionRecyclerAdapter adapter = (MultiSectionRecyclerAdapter) rclQuoteAttachment.getAdapter();
        List<BaseModel> Quotes = new ArrayList<>();

        if(buildingCase!=null) {
            if (buildingCase.getQuotes() != null) {
                Quotes = new ArrayList<>(buildingCase.getQuotes());
            }
        }

        MultiSectionRecyclerAdapter.SectionList Quote;

        rclQuoteAttachment.setVisibility(View.VISIBLE);
        Quote = new MultiSectionRecyclerAdapter.SectionList(getString(R.string.case_attachment_section_title_quote),Quotes);


        adapter.clearSource();
        adapter.addSection(Quote);
        adapter.parseSource();
    }

    public void setupInvoiceRecycleView(){
        MultiSectionRecyclerAdapter adapter = (MultiSectionRecyclerAdapter) rclInvoiceAttachment.getAdapter();
        List<BaseModel> Invoices = new ArrayList<>();

        if(buildingCase!=null) {
            if (buildingCase.getInvoices() != null) {
                Invoices = new ArrayList<>(buildingCase.getInvoices());
            }
        }

        MultiSectionRecyclerAdapter.SectionList Invoice;
        rclInvoiceAttachment.setVisibility(View.VISIBLE);
        Invoice = new MultiSectionRecyclerAdapter.SectionList(getString(R.string.case_attachment_section_title_invoice),Invoices);

        adapter.clearSource();
        adapter.addSection(Invoice);
        adapter.parseSource();
    }
    
    public void setupRecycleView(){
        setupPhotoRecycleView();
        setupQuoteRecycleView();
        setupInvoiceRecycleView();
        setupDocumentRecycleView();
    }

    public void uploadDocument(File docFile){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.uploadDocument(getContext(),selectedBuilding.getId(),buildingCase.getId(),docFile,file->{
            if (null == file){
                Toast.makeText(getContext(), "File's Type is not Supported, Please Select the file on your device", Toast.LENGTH_LONG).show();
            }else {
                buildingCase.addDocsFile(file);
                setupDocumentRecycleView();
                selectedByteFile = null;//clear data
                selectedFileName = null;
            }
        },error -> {
            if (Offlinemode.checkNetworkConnection(getContext())){
                Log.e("uploadDocument",error.toString());
            }else{
                Offlinemode.showWhenOfflineMode(getContext());
            }
        });
    }

    @Override
    public void uploadPhoto(PhotoModel photoModel) {
            Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
            displayLoadingScreen();
            controller.uploadPhoto(getContext(), selectedBuilding.getId(), buildingCase.getId(), photoModel, file -> {
                dismissLoadingScreen();
                buildingCase.addAttachmentFile(file);
                setupPhotoRecycleView();
            }, error -> {
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("uploadPhoto",error.toString());
                }else{
                    Offlinemode.showWhenOfflineMode(getContext());
                }
            });
    }

    @Override
    public void uploadQuote(Double value, Contractor contractor, PhotoModel photoModel) {
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        displayLoadingScreen();
        controller.uploadQuote(getContext(),selectedBuilding.getId(),buildingCase.getId(),photoModel,contractor,value,quote->{
            dismissLoadingScreen();
            buildingCase.addAttachmentQuote(quote);
            contractorList.selectId(contractor.getId());
            if (null != listener){
                listener.didAddAttachment();
            }
            setupQuoteRecycleView();
            SelectedFileInvoiceQuotes.clearData();
        },error -> {
            dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(getContext())){
                Log.e("uploadQuote",error.toString());
            }else{
                Offlinemode.showWhenOfflineMode(getContext());
            }
        });
    }

    @Override
    public void uploadInvoice(String invoiceNumber, Double value, String notes, Contractor contractor, PhotoModel photoModel) {
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        displayLoadingScreen();
        controller.uploadInvoice(getContext(),selectedBuilding.getId(),buildingCase.getId(),photoModel,contractor,value,notes,invoiceNumber,invoice->{
            dismissLoadingScreen();
            buildingCase.addAttachmentInvoice(invoice);
            contractorList.selectId(contractor.getId());
            if (null != listener){
                listener.didAddAttachment();
            }
            setupInvoiceRecycleView();
            SelectedFileInvoiceQuotes.clearData();
        },error -> {
            dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(getContext())){
                Log.e("uploadInvoice",error.toString());
            }else{
                Offlinemode.showWhenOfflineMode(getContext());
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("Contractor",contractorList);
        outState.putParcelable("BuildingCase",buildingCase);
    }

    public void addListener(AttachmentEventListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(BaseModel baseModel) {
        if (baseModel instanceof AttachmentInterface){
            String url = ((AttachmentInterface) baseModel).getLink();
            String type = ((AttachmentInterface) baseModel).getType();
            String typefile = ((AttachmentInterface) baseModel).getFileType();
            Intent intent = new Intent(getContext(), WebViewActivity.class);
            intent.putExtra(WebViewActivity.ATTACHMENT_URL_KEY,url);
            intent.putExtra(WebViewActivity.ATTACHMENT_TYPE_KEY,type);
            intent.putExtra(WebViewActivity.ATTACHMENT_TYPE_FILE_KEY,typefile);
            startActivity(intent);
        }
    }

    @Override
    public void uploadPhotos(ArrayList<PhotoModel> uploadList) {
        displayLoadingScreen();
        for (int i=0; i <uploadList.size();i++) {
            int j = i;
            Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
            controller.uploadPhoto(getContext(), selectedBuilding.getId(), buildingCase.getId(), uploadList.get(i), file -> {
                buildingCase.addAttachmentFile(file);
                setupPhotoRecycleView();
                if (j == uploadList.size()-1){
                    dismissLoadingScreen();
                    dialog.dismiss();
                }
            }, error -> {
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())) {
                    Log.e("uploadPhoto", error.toString());
                    Toast.makeText(getContext(), "Upload photos failed!", Toast.LENGTH_SHORT).show();
                } else {
                    Offlinemode.showWhenOfflineMode(getContext());
                }
            });
        }

    }

    public interface AttachmentEventListener{
        void didAddAttachment();
    }

}
