package com.mybos.bmapp.Activity.Library;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Component.RecycleAdapter.AbstractScrollToLoadMoreRecyclerAdapter;
import com.mybos.bmapp.Data.Model.Library.LibraryFileOrFolder;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

public class LibraryListAdapter extends AbstractScrollToLoadMoreRecyclerAdapter<LibraryFileOrFolder> {
    @Override
    public int getItemLayout() {
        return R.layout.a_library_listsrow;
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new LibraryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof LibraryViewHolder ){
            LibraryFileOrFolder fof = sourceList.get(i);
            if (fof.getType().equals("0")){
                ((LibraryViewHolder) holder).ivFileType.setImageResource(R.drawable.ic_folder);
            } else if (fof.getUrl().contains(".doc")){
                ((LibraryViewHolder) holder).ivFileType.setImageResource(R.drawable.ic_doc);
            } else if (fof.getUrl().contains(".xls")){
                ((LibraryViewHolder) holder).ivFileType.setImageResource(R.drawable.ic_excel);
            } else if (fof.getUrl().contains(".pdf")){
                ((LibraryViewHolder) holder).ivFileType.setImageResource(R.drawable.ic_pdf);
            }

            ((LibraryViewHolder) holder).tvTitle.setText(fof.getName());

            if (!fof.getInside().equals("-")){
                String inside = fof.getInside();
                inside = inside.replaceAll("[A-Za-z]", "");
                inside = inside.replaceAll("\\s", "");
                String[] numberInside = inside.split(",");

                ((LibraryViewHolder) holder).tvtype.setText("Folder:");
                ((LibraryViewHolder) holder).tvtype2.setText("File:");
                ((LibraryViewHolder) holder).tvInfo.setText(numberInside[0]);
                ((LibraryViewHolder) holder).tvInfo2.setText(numberInside[1]);
            }else {
                ((LibraryViewHolder) holder).tvtype.setText("Added:");
                ((LibraryViewHolder) holder).tvInfo.setText(DateUtils.editDateTime2(fof.getAdded()));
            }

            ((LibraryViewHolder) holder).setListener(()->{
                Context context = ((LibraryViewHolder) holder).tvTitle.getContext();
                if (fof.getType().equals("0")){
                    Intent intent = new Intent(context, LibraryListActivity.class);
                    intent.putExtra(LibraryListActivity.TITLE,fof.getName());
                    intent.putExtra(LibraryListActivity.ID,fof.getId());
                    context.startActivity(intent);
                }else{
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(fof.getUrl()));
                    context.startActivity(browserIntent);
                }
            });

        }

    }

    class LibraryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView ivFileType;
        TextView tvTitle, tvtype, tvInfo, tvtype2, tvInfo2;
        OnItemClickListener listener;

        public LibraryViewHolder(View itemView) {
            super(itemView);
            ivFileType = itemView.findViewById(R.id.ivFileType);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvtype = itemView.findViewById(R.id.tvtype);
            tvInfo = itemView.findViewById(R.id.tvInfo);
            tvtype2 = itemView.findViewById(R.id.tvtype2);
            tvInfo2 = itemView.findViewById(R.id.tvInfo2);
            itemView.findViewById(R.id.cardViewLibrary).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (null != listener) {
                listener.onClick();
            }
        }

        public void setListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }
    public static interface OnItemClickListener{
        void onClick();
    }
}
