package com.mybos.bmapp.Activity.Base;


import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.FragmentManager;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListActivity;
import com.mybos.bmapp.Activity.Dashboard.Controller.DashboardController;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.LoginScreen.LoginActivity;
import com.mybos.bmapp.Component.AlertFragment.AlertFragment;
import com.mybos.bmapp.Component.LoadingFragment.LoadingFragment;
import com.mybos.bmapp.Data.Functional.AlertButton;
import com.mybos.bmapp.Data.Mapper.Error.ErrorMapper;
import com.mybos.bmapp.Error.LocalizeException;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.PreferenceManager;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {
    private static String ALERT_DIALOG_TAG = "ALERT_DIALOG_TAG";
    private static String LOADING_DIALOG_TAG = "LOADING_DIALOG_TAG";
    private View loadingView;
    private LoadingFragment loadingFragment;
    protected boolean isVisible;
    protected boolean needRemoveLoadingScreen = false;
    private DashboardController controller = new DashboardController();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        try {
            super.onResume();
            this.isVisible = true;
            if (needRemoveLoadingScreen) {
                if (null != loadingFragment) {
                    loadingFragment.dismiss();
                }
                needRemoveLoadingScreen = false;
            }
        } catch (OutOfMemoryError e) {
            System.gc();
            onResume();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            this.isVisible = false;
        } catch (OutOfMemoryError e) {
        }
    }

    /**
     * display a normal android type alert dialog
     *
     * @param title          alert title
     * @param message        alert message
     * @param positiveButton hold positive button info
     * @param negativeButton hold negative button info
     */
    public void displayAlert(String title, String message, @Nullable AlertButton positiveButton, @Nullable AlertButton negativeButton) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        if (null != positiveButton) {
            builder.setPositiveButton(positiveButton.getName(), (v, i) -> {
                positiveButton.getAction().onClick();
            });
        } else {
            builder.setPositiveButton(R.string.OK, null);
        }
        if (null != negativeButton) {
            builder.setNegativeButton(negativeButton.getName(), (v, i) -> {
                negativeButton.getAction().onClick();
            });
        }
        builder.show();
    }

    public void doLogout() {
        Log.e("check_logout", DashboardActivity.firebaseToken);
        controller.LogoutDevice(this, DashboardActivity.firebaseToken,
                (status) -> {
                    PreferenceManager.clearPreference(this);
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                },
                (error) -> {
                    Log.e("check_logout_error", error.toString());
                    Toast.makeText(this, "check_logout_error", Toast.LENGTH_SHORT).show();
                    PreferenceManager.clearPreference(this);
                    Intent intent = new Intent(this, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                });
    }

    public void navigateToRoot() {
        Intent intent = new Intent(this, DashboardActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void navigateToCaseList() {

        Intent main = new Intent(this, DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent caseList = new Intent(this, CaseListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(caseList);
        builder.startActivities();
        finish();
    }

    public void displayLoadingIndicator() {
        LayoutInflater inflater = getLayoutInflater();
        loadingView = inflater.inflate(R.layout.c_loading_indicator, null);
        ((ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content)).addView(loadingView);
    }

    public void removeLoadingIndicator() {
        if (null != loadingView) {
            ((ViewGroup) loadingView.getParent()).removeView(loadingView);
            loadingView = null;
        }
    }

    public void displayLoadingScreen() {
        try {
            loadingFragment = LoadingFragment.newInstance(null);
            loadingFragment.show(getSupportFragmentManager(), LOADING_DIALOG_TAG);
        } catch (IllegalStateException e) {
        }
    }

    public void dismissLoadingScreen() {
        if (isVisible) {
            if (null!=loadingFragment){
                loadingFragment.dismiss();
            }
        } else {
            needRemoveLoadingScreen = true;
        }

    }

    public void displayAlert(String title, String message, AlertFragment.AlertFragmentDatasource datasource) {
        try {
            FragmentManager fm = getSupportFragmentManager();
            AlertFragment fragment = AlertFragment.getInstanced(title, message, datasource);
            fragment.show(fm, ALERT_DIALOG_TAG);
        } catch (IllegalStateException e) {
        }
    }

    public void displayAlert(String title, Exception e, AlertFragment.AlertFragmentDatasource datasource) {
        if (null != datasource) {
            String message = e instanceof LocalizeException ? ((LocalizeException) e).getLocalizeMessage(this) : e.getMessage();
            AlertFragment fragment = AlertFragment.getInstanced(title, message, datasource);
            fragment.show(getSupportFragmentManager(), ALERT_DIALOG_TAG);
        } else {
            String message = e instanceof LocalizeException ? ((LocalizeException) e).getLocalizeMessage(this) : e.getMessage();
            AlertFragment fragment = AlertFragment.getInstanced(title, message, (alert) -> {
                List<Button> buttons = new ArrayList<>();

                Button ok = new Button(this);
                ok.setText(R.string.OK);
                ok.setOnClickListener((v) -> {
                    alert.dismiss();
                });
                buttons.add(ok);

                Button send = new Button(this);
                send.setText(R.string.Send);
                send.setOnClickListener((v) -> {
                    alert.dismiss();
                    ErrorMapper errorMapper = new ErrorMapper();
                    errorMapper.sendError(e, this, (Void) -> {
                    }, (error) -> {
                    });
                });

                return buttons;
            });
            try {
                fragment.show(getSupportFragmentManager(), ALERT_DIALOG_TAG);
            } catch (IllegalStateException IllStaExc) {
            }
        }
    }

    public void dismissKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != imm && null != getCurrentFocus()) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setupTapOutsideToDismiss(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener((v, event) -> {
                this.dismissKeyboard();
//                v.performClick();
                return false;
            });
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View childView = ((ViewGroup) view).getChildAt(i);
                setupTapOutsideToDismiss(childView);
            }
        }
    }
}
