package com.mybos.bmapp.Activity.Calendar.Activities;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarMaintenanceInfoAdapter;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;

public class CalendarMaintenanceInfoActivity extends a_base_toolbar {

    TextView tvDatetitle;
    TabLayout topTabBarMaintenanceInfo;
    ViewPager viewPagerMaintenance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_maintenance_info);
        updateToolbarTitle(MaintenancePerDay.getMaintenancePerDaySelected().getTitle());
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarMaintenanceInfoActivity.this, CalendarMaintenancesActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tvDatetitle = findViewById(R.id.tvDatetitle);
        topTabBarMaintenanceInfo = findViewById(R.id.topTabBarMaintenanceInfo);
        viewPagerMaintenance = findViewById(R.id.viewPagerMaintenance);

        String titleDate = SelectedDay.getDateOfweek()+", "+SelectedDay.getFullDate();
        tvDatetitle.setText(titleDate);

        viewPagerMaintenance.setAdapter(new CalendarMaintenanceInfoAdapter(this,getSupportFragmentManager()));
        topTabBarMaintenanceInfo.setupWithViewPager(viewPagerMaintenance);
    }
}
