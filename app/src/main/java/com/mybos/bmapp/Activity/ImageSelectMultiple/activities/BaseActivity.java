package com.mybos.bmapp.Activity.ImageSelectMultiple.activities;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Build.VERSION;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import com.mybos.bmapp.Activity.ImageSelectMultiple.views.CustomProgressDialog;
import java.util.ArrayList;
import java.util.List;

public class BaseActivity extends AppCompatActivity {
    public CustomProgressDialog progressDialog;
    public static final int SHOW_PROGRESS = 1;
    public static final int DISMISS_PROGRESS = 2;

    public BaseActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showProgressDialog(String message) {
        if (this.progressDialog == null) {
            this.progressDialog = new CustomProgressDialog(this);
        }

        this.progressDialog.setMessage(message);
        this.progressDialog.setCancelable(false);
        this.progressDialog.setCanceledOnTouchOutside(false);
        this.progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (this.progressDialog != null && this.progressDialog.isShowing()) {
            this.progressDialog.dismiss();
        }

    }

    public boolean hasStoragePermission(Context context) {
        int writePermissionCheck = ContextCompat.checkSelfPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        int readPermissionCheck = ContextCompat.checkSelfPermission(context, "android.permission.READ_EXTERNAL_STORAGE");
        return VERSION.SDK_INT < 23 || writePermissionCheck != -1 && readPermissionCheck != -1;
    }

    public boolean hasCameraPermission(Context context) {
        int cameraPermissionCheck = ContextCompat.checkSelfPermission(context, "android.permission.CAMERA");
        return VERSION.SDK_INT < 23 || cameraPermissionCheck != -1;
    }

    public boolean hasCallPermission(Context context) {
        int callPermissionCheck = ContextCompat.checkSelfPermission(context, "android.permission.CALL_PHONE");
        return VERSION.SDK_INT < 23 || callPermissionCheck != -1;
    }

    public void requestCameraPermissions(Activity activity, int requestCode) {
        ActivityCompat.requestPermissions(activity, new String[]{"android.permission.CAMERA"}, requestCode);
    }

    public void requestStoragePermissions(Activity activity, int requestCode) {
        int hasReadPermission = ContextCompat.checkSelfPermission(activity, "android.permission.READ_EXTERNAL_STORAGE");
        int hasWritePermission = ContextCompat.checkSelfPermission(activity, "android.permission.WRITE_EXTERNAL_STORAGE");
        List<String> permissions = new ArrayList();
        if (hasReadPermission != 0) {
            permissions.add("android.permission.READ_EXTERNAL_STORAGE");
        }

        if (hasWritePermission != 0) {
            permissions.add("android.permission.WRITE_EXTERNAL_STORAGE");
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(activity, (String[])permissions.toArray(new String[permissions.size()]), requestCode);
        }

    }

    public void requestCallPermissions(Activity activity, int requestCode) {
        int hasCallPermission = ContextCompat.checkSelfPermission(activity, "android.permission.CALL_PHONE");
        List<String> permissions = new ArrayList();
        if (hasCallPermission != 0) {
            permissions.add("android.permission.CALL_PHONE");
        }

        if (!permissions.isEmpty()) {
            ActivityCompat.requestPermissions(activity, (String[])permissions.toArray(new String[permissions.size()]), requestCode);
        }

    }

    public boolean validateGrantedPermissions(int[] grantResults) {
        boolean isGranted = true;
        if (grantResults != null && grantResults.length > 0) {
            for(int i = 0; i < grantResults.length; ++i) {
                if (grantResults[i] == -1) {
                    isGranted = false;
                    break;
                }
            }

            return isGranted;
        } else {
            isGranted = false;
            return isGranted;
        }
    }
}
