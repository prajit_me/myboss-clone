package com.mybos.bmapp.Activity.Inspection.CreateInspection.Fragment;


import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChoosePhotoSourceDialog extends DialogFragment {
    private OnCameraClick cameraClick;
    private OnLibraryClick libraryClick;

    public ChoosePhotoSourceDialog() {
        // Required empty public constructor
    }

    public static ChoosePhotoSourceDialog createInstance(){
        return new ChoosePhotoSourceDialog();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(true);
        View view = inflater.inflate(R.layout.a_make_inspection_f_choose_photo_source_dialog, container, false);
        view.findViewById(R.id.btnCamera).setOnClickListener(v -> {
            if (cameraClick != null){
                cameraClick.selectCameraSource();
            }
            this.dismiss();
        });
        view.findViewById(R.id.btnLibrary).setOnClickListener(v -> {
            if (libraryClick != null){
                libraryClick.selectLibrarySource();
            }
            this.dismiss();
        });
        return view;
    }

    public void setCameraClick(OnCameraClick cameraClick) {
        this.cameraClick = cameraClick;
    }

    public void setLibraryClick(OnLibraryClick libraryClick) {
        this.libraryClick = libraryClick;
    }

    public interface OnCameraClick{
        void selectCameraSource();
    }
    public interface OnLibraryClick{
        void selectLibrarySource();
    }

}
