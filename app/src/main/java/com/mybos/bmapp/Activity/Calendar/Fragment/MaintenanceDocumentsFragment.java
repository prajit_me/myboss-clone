package com.mybos.bmapp.Activity.Calendar.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarListDocAdapter;
import com.mybos.bmapp.R;

public class MaintenanceDocumentsFragment extends BaseFragment {

    private View view;
    ListView lvCalendarDocuments;

    public MaintenanceDocumentsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_documents, container, false);
        this.view = view;

        lvCalendarDocuments = view.findViewById(R.id.lvCalendarDocuments);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        lvCalendarDocuments.setAdapter(new CalendarListDocAdapter(getContext(),"doc"));
    }

}
