package com.mybos.bmapp.Activity.Resident.ResidentDetail;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.Resident.ResidentMapper;
import com.mybos.bmapp.Data.Response.ResidentApartmentResponse;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentDetailController {

    public void getExtraDetailResident(Context context, int buildingId, int apartmentId, int residentId,
                                       SuccessCallback<ResidentApartmentResponse> onSuccess,FailureCallback onFailure){
        ResidentMapper mapper = new ResidentMapper();
        mapper.getResidentDetail(context,buildingId,apartmentId,residentId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
