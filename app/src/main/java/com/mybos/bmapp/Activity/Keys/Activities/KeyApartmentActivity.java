package com.mybos.bmapp.Activity.Keys.Activities;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Keys.Activities.KeyList.KeyListActivity;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyApartmentAdaptor;
import com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.PrivateKeyListFragment;
import com.mybos.bmapp.R;

public class KeyApartmentActivity extends a_base_toolbar {

    TabLayout tblKeyApartment;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_apartment);
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KeyApartmentActivity.this, KeyListActivity.class);
                startActivity(intent);
                finish();
            }
        });
        updateToolbarTitle("Apartment "+getIntent().getStringExtra(PrivateKeyListFragment.KEY_SELECTED));

        viewPager = findViewById(R.id.viewPagerKeyApartment);
        tblKeyApartment = findViewById(R.id.topTabBarKeyApartment);
        viewPager.setAdapter(new KeyApartmentAdaptor(this,
                getIntent().getStringExtra(PrivateKeyListFragment.KEY_SELECTED),getSupportFragmentManager()));
        tblKeyApartment.setupWithViewPager(viewPager);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
