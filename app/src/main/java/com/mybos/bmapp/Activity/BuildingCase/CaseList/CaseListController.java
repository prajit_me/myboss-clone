package com.mybos.bmapp.Activity.BuildingCase.CaseList;

import android.content.Context;

import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseMapper;
import com.mybos.bmapp.Data.Response.BuildingCaseResponse;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public class CaseListController {
    public void getBuildingCaseList(Context context, int buildingId, SearchTableQueryBuilder searchTableQueryBuilder, SuccessCallback<BuildingCaseResponse> onSuccess, FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.getList(context,buildingId,searchTableQueryBuilder,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void deleteBuildingCase(Context context, int buildingId, int caseId, SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.delete(context,buildingId,caseId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
