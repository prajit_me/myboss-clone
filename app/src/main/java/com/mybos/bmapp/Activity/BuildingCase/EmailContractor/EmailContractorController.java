package com.mybos.bmapp.Activity.BuildingCase.EmailContractor;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseEmailContractorMapper;
import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseMapper;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Cases.EmailContractor.EmailContractor;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;

import java.util.List;

/**
 * Created by EmLaAi on 14/04/2018.
 *
 * @author EmLaAi
 */
public class EmailContractorController {
    public void getContractorList(Context context, int buildingId,String SelectCategory, int page, SuccessCallback<ContractorList> onSuccess, FailureCallback onError){
        ContractorMapper mapper = new ContractorMapper();
        mapper.get(context,buildingId,SelectCategory,"",page,onSuccess::onSuccess,onError::onFailure);
    }
    public void getBuildingCaseDetail(Context context, int buildingId, String caseId , SuccessCallback<BuildingCase> onSuccess, FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.get(context,buildingId,caseId,onSuccess::onSuccess,onError::onFailure);
    }

    public void sendEmailToContractor(Context context, int buildingId, String caseId, EmailContractor email, List<Contractor> selectedContractors, SuccessCallback<Void> onSuccess, FailureCallback onFailure){
        BuildingCaseEmailContractorMapper mapper = new BuildingCaseEmailContractorMapper();
        mapper.sendEmail(context,buildingId,caseId,email,selectedContractors,onSuccess::onSuccess,onFailure::onFailure);
    }

    interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    interface FailureCallback{
        void onFailure(Exception error);
    }
}
