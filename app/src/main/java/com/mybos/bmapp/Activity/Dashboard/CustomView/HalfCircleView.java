package com.mybos.bmapp.Activity.Dashboard.CustomView;

import android.view.View;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.mybos.bmapp.R;


public class HalfCircleView extends View {

    private Paint p = new Paint();
    private int[] gradientColors = new int[]{
            getResources().getColor(R.color.nav_start_gradient),
            getResources().getColor(R.color.nav_middle_gradient),
            getResources().getColor(R.color.nav_end_gradient)};

    public HalfCircleView(Context context) {
        super(context);
    }

    public HalfCircleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public HalfCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public HalfCircleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(getResources().getColor(R.color.transparent));
        p.setAntiAlias(true);
        p.setColor(getResources().getColor(R.color.nav_start_gradient));
        p.setShader(new LinearGradient(0,0,getWidth(),getHeight(),gradientColors,null, Shader.TileMode.MIRROR));
        RectF rectF = new RectF((getWidth() / 2) - getHeight(),-getHeight(),(getWidth() / 2) + getHeight(),getHeight());
        canvas.drawOval(rectF,p);
    }


}
