package com.mybos.bmapp.Activity.Base;


import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.mybos.bmapp.Component.AlertFragment.AlertFragment;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BaseFragment extends Fragment {
    private static String ALERT_DIALOG_TAG = "ALERT_DIALOG_TAG";
    private static String LOADING_DIALOG_TAG = "LOADING_DIALOG_TAG";

    public void displayAlert(String title, String message, AlertFragment.AlertFragmentDatasource datasource){
        if (null == getActivity()){
            return;
        }
        FragmentManager fm = getActivity().getSupportFragmentManager();
        AlertFragment fragment =  AlertFragment.getInstanced(title,message,datasource);
        fragment.show(fm,ALERT_DIALOG_TAG);
    }

    public void displayAlert(String title, Exception e, AlertFragment.AlertFragmentDatasource datasource){
        if (null == getActivity()){
            return;
        }
        if (null != datasource){
            AlertFragment fragment = AlertFragment.getInstanced(title,e.getLocalizedMessage(),datasource);
            fragment.show(getActivity().getSupportFragmentManager(),ALERT_DIALOG_TAG);
        }else{
            AlertFragment fragment = AlertFragment.getInstanced(title,e.getLocalizedMessage(),(alert)->{
                List<Button> buttons = new ArrayList<>();

                Button ok = new Button(getContext());
                ok.setText(R.string.OK);
                ok.setOnClickListener((v)->{
                    alert.dismiss();
                });
                buttons.add(ok);

                Button send = new Button(getContext());
                FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                send.setText(R.string.Send);
                send.setOnClickListener((v)->{
                    alert.dismiss();
                    crashlytics.recordException(new Exception(e.getMessage()));
                });

                return buttons;
            });
            try {
                fragment.show(getActivity().getSupportFragmentManager(), ALERT_DIALOG_TAG);
            }
            catch (IllegalStateException IllStageExc){}
        }
    }

    public void displayLoadingScreen(){
        if (getActivity() != null && getActivity() instanceof BaseActivity){
            ((BaseActivity) getActivity()).displayLoadingScreen();
        }
    }

    public void dismissLoadingScreen(){
        if (getActivity() != null && getActivity() instanceof BaseActivity){
            ((BaseActivity) getActivity()).dismissLoadingScreen();
        }
    }

    public void dismissKeyboard(){
        if (getActivity() == null){
            return;
        }
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (null != imm && null != getActivity().getCurrentFocus()) {
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setupTapOutsideToDismiss(View view){
        if (!(view instanceof EditText)){
            view.setOnTouchListener((v, event) -> {
                this.dismissKeyboard();
//                v.performClick();
                return false;
            });
        }
        if (view instanceof ViewGroup){
            for (int i = 0;i < ((ViewGroup) view).getChildCount();i++){
                View childView = ((ViewGroup) view).getChildAt(i);
                setupTapOutsideToDismiss(childView);
            }
        }
    }
}
