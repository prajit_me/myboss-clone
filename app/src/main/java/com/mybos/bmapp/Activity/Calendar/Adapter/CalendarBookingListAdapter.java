package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Calendar.Activities.CalendarBookingInfoActivity;
import com.mybos.bmapp.Data.Model.Calendar.Booking;
import com.mybos.bmapp.Data.Model.Calendar.DaysEvent;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;

public class CalendarBookingListAdapter extends BaseAdapter {

    private Context context;
    ArrayList<Booking> showList = new ArrayList<>();
    public static final String BOOKING_ID = "booking_id";

    public CalendarBookingListAdapter(Context context, String showLisString) {
        this.context = context;

        if (showLisString.equals("all")){
            showList = new ArrayList<>(DaysEvent.getDayEFromDateOfDayEs(SelectedDay.getDate()).getBooking());
        }else{
            for (Booking booking: DaysEvent.getDayEFromDateOfDayEs(SelectedDay.getDate()).getBooking()
            ) {
                if (showLisString.equals(booking.getStatusText())){
                    showList.add(booking);
                }
            }
        }
    }

    @Override
    public int getCount() {
        return showList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.calendar_a_booking, null);

        Booking booking = showList.get(i);

        TextView tvNameBooking = view.findViewById(R.id.tvNameBooking);
        ImageView ivStatusBooking = view.findViewById(R.id.ivStatusBooking);
        TextView tvStatusBooking = view.findViewById(R.id.tvStatusBooking);
        TextView tvTimeBooking = view.findViewById(R.id.tvTimeBooking);

        tvNameBooking.setText(booking.getCalendarName());
        String time = DateUtils.changeTimeformat(booking.getSlot_from())+"-"+DateUtils.changeTimeformat(booking.getSlot_to());
        tvTimeBooking.setText(time);

        if (booking.getStatusText().equals("Pending")){
            ivStatusBooking.setImageResource(R.drawable.calendar_event_pending);
            tvStatusBooking.setText(R.string.calendar_status_pending);
            tvStatusBooking.setTextColor(context.getResources().getColor(R.color.solid_blue));
        } else if(booking.getStatusText().equals("Confirm")){
            ivStatusBooking.setImageResource(R.drawable.calendar_event_complete);
            tvStatusBooking.setText(R.string.calendar_status_Approved);
            tvStatusBooking.setTextColor(context.getResources().getColor(R.color.calendar_status_complete));
        } else {
            ivStatusBooking.setImageResource(R.drawable.calendar_event_failed);
            tvStatusBooking.setText(R.string.calendar_status_Rejected);
            tvStatusBooking.setTextColor(context.getResources().getColor(R.color.calendar_status_failed));
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CalendarBookingInfoActivity.class);
                intent.putExtra(BOOKING_ID,booking.getId());
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });

        return view;
    }
}
