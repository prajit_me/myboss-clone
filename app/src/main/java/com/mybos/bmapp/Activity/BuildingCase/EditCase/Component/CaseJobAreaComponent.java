package com.mybos.bmapp.Activity.BuildingCase.EditCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Cases.BuildingCaseJobArea;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 24/04/2018.
 *
 * @author EmLaAi
 */
public class CaseJobAreaComponent extends ConstraintLayout {
    TextView txtJobArea,txtApartment,txtAsset,txtContractor;
    TextView txtJobAreaLabel,txtApartmentLabel,txtAssetLabel,txtContractorLabel;
    public CaseJobAreaComponent(Context context) {
        super(context);
        additionalInit();
    }

    public CaseJobAreaComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public CaseJobAreaComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }
    public void additionalInit(){
        inflate(getContext(), R.layout.a_edit_case_f_edit_case_detail_c_case_job_area,this);
        txtJobArea = findViewById(R.id.txtJobArea);
        txtJobAreaLabel = findViewById(R.id.txtJobAreaLabel);
        txtApartment = findViewById(R.id.txtApartment);
        txtApartmentLabel = findViewById(R.id.txtApartmentLabel);
        txtAsset = findViewById(R.id.txtAsset);
        txtAssetLabel = findViewById(R.id.txtAssetLabel);
        txtContractor = findViewById(R.id.txtContractor);
        txtContractorLabel = findViewById(R.id.txtContractorLabel);
    }

    public void setJobArea(String value,int id){
        if (id == BuildingCaseJobArea.COMMON_ASSET_ID){
            txtApartmentLabel.setVisibility(GONE);
            txtApartment.setVisibility(GONE);
            txtAssetLabel.setVisibility(VISIBLE);
            txtAsset.setVisibility(VISIBLE);
        }else if (id == BuildingCaseJobArea.COMMON_NON_ASSET_ID){
            txtApartmentLabel.setVisibility(GONE);
            txtApartment.setVisibility(GONE);
            txtAssetLabel.setVisibility(GONE);
            txtAsset.setVisibility(GONE);
        }else if (id == BuildingCaseJobArea.PRIVATE_LOT_ID){
            txtApartmentLabel.setVisibility(VISIBLE);
            txtApartment.setVisibility(VISIBLE);
            txtAssetLabel.setVisibility(GONE);
            txtAsset.setVisibility(GONE);
        }
        txtJobArea.setText(value);
    }

    public void setApartment(String value){
        txtApartment.setText(value);
    }
    public void setAsset(String value){
        txtAsset.setText(value);
    }
    public void setContractor(String value){
        txtContractor.setText(value);
    }
}
