package com.mybos.bmapp.Activity.Resident;

import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;

public class ResidentInfoCollection {

    private static String RESIDENT_ROOM_NUMBER = "";

    public static void setResidentRoomNumber(String RoomNumber) {
        RESIDENT_ROOM_NUMBER = RoomNumber;
    }

    public static String getResidentRoomNumber(){
        return RESIDENT_ROOM_NUMBER;
    }

    public static String getResidentInfo(){
        return "Apartment " + RESIDENT_ROOM_NUMBER
                + " - "
                + BuildingInfoCollection.getBuildingName() + " Apartment";
    }

}
