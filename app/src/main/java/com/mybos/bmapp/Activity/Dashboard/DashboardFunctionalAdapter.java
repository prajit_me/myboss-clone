package com.mybos.bmapp.Activity.Dashboard;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Data.Menu.DashboardFunctionalList;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 01/04/2018.
 *
 * @author EmLaAi
 */
public class DashboardFunctionalAdapter extends RecyclerView.Adapter<DashboardFunctionalAdapter.FunctionalViewHolder> {
    private OnItemClickListener listener;
    private DashboardFunctionalList list;
    public DashboardFunctionalAdapter(Context context,OnItemClickListener listener){
        list = new DashboardFunctionalList(context);
        this.listener = listener;
    }
    @NonNull
    @Override
    public FunctionalViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_a_dashboard_functional_box_item,parent,false);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = parent.getMeasuredHeight() / 3;
        layoutParams.width = parent.getMeasuredWidth() / 3;
        view.setLayoutParams(layoutParams);
        return new FunctionalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FunctionalViewHolder holder, int position) {
        DashboardFunctionalList.Item item = list.getItem(position);
        if (null != item){
            holder.iconFunctional.setImageDrawable(item.getIcon());
            holder.tvTitle.setText(item.getTitle());
        }
    }

    @Override
    public int getItemCount() {
        return 9;
    }

    class FunctionalViewHolder extends RecyclerView.ViewHolder{
        ImageView iconFunctional;
        TextView tvTitle;
        ConstraintLayout layout;

        FunctionalViewHolder(View itemView) {
            super(itemView);
            iconFunctional = itemView.findViewById(R.id.iconFunctional);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            layout = itemView.findViewById(R.id.layoutItem);
            itemView.setOnClickListener(v -> {
                DashboardFunctionalList.Item item = list.getItem(getAdapterPosition());
                if (null != item){
                    listener.didSelectItemId(item.getId());
                }
            });
        }
    }

    interface OnItemClickListener{
        void didSelectItemId(@DashboardFunctionalList.ItemId.ItemEnumId int id);
    }
}
