package com.mybos.bmapp.Activity.ImageSelectMultiple.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.Adapter;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;

import com.mybos.bmapp.Activity.ImageSelectMultiple.activities.GalleryActivity;
import com.mybos.bmapp.Activity.ImageSelectMultiple.views.AutoImageView;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Image;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Params;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Utils;
import com.squareup.picasso.Picasso;
import com.mybos.bmapp.R.id;
import com.mybos.bmapp.R.dimen;
import com.mybos.bmapp.R.drawable;
import com.mybos.bmapp.R.layout;
import java.util.ArrayList;

public class GalleryImagesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Image> list;
    Activity activity;
    int columnCount;
    private ArrayList<Long> selectedIDs;
    private int screenWidth;
    private View.OnClickListener onClickListener;
    Params params;

    public GalleryImagesAdapter(Activity activity, ArrayList<Image> list, int columnCount, Params params) {
        this.activity = activity;
        this.list = list;
        this.columnCount = columnCount;
        this.params = params;
        this.selectedIDs = new ArrayList();
        WindowManager wm = (WindowManager)activity.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        this.screenWidth = size.x;
    }

    public int getItemCount() {
        return this.list.size();
    }

    public long getItemId(int position) {
        return ((Image)this.list.get(position))._id;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = (LayoutInflater)this.activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = mInflater.inflate(layout.image_item, parent, false);
        GalleryImagesAdapter.ImageHolder dataObjectHolder = new GalleryImagesAdapter.ImageHolder(view);
        return dataObjectHolder;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        GalleryImagesAdapter.ImageHolder holder = (GalleryImagesAdapter.ImageHolder)viewHolder;
        Image entity = (Image)this.list.get(position);
        float height;
        if (entity.isPortraitImage) {
            height = Float.valueOf(this.activity.getResources().getDimension(dimen.image_height_landscape));
        } else {
            height = Float.valueOf(this.activity.getResources().getDimension(dimen.image_height_landscape));
        }

        if (holder.imageView != null) {
            Picasso.with(this.activity).load(entity.uri).placeholder(drawable.image_processing_full).into(holder.imageView);
        }

        if (this.selectedIDs.contains(entity._id)) {
            if (this.params.getLightColor() != 0) {
                holder.frameLayout.setForeground(new ColorDrawable(this.params.getLightColor()));
            }

            holder.selectedImageView.setVisibility(View.VISIBLE);
        } else {
            holder.frameLayout.setForeground((Drawable)null);
            holder.selectedImageView.setVisibility(View.INVISIBLE);
        }

        holder.setTag(id.image_id, entity._id);
        holder.parentLayout.setOnClickListener(this.onClickListener);
    }

    public void setSelectedItem(View parentView, long imageId) {
        if (this.selectedIDs.contains(imageId)) {
            this.selectedIDs.remove(imageId);
            ((FrameLayout)parentView.findViewById(id.frameLayout)).setForeground((Drawable)null);
            ((ImageView)parentView.findViewById(id.selectedImageView)).setVisibility(View.INVISIBLE);
        } else if (this.selectedIDs.size() < this.params.getPickerLimit()) {
            this.selectedIDs.add(imageId);
            if (this.params.getLightColor() != 0) {
                ((FrameLayout)parentView.findViewById(id.frameLayout)).setForeground(new ColorDrawable(this.params.getLightColor()));
            }

            ((ImageView)parentView.findViewById(id.selectedImageView)).setVisibility(View.VISIBLE);
        } else if (this.activity instanceof GalleryActivity) {
            ((GalleryActivity)this.activity).showLimitAlert("You can select only " + this.params.getPickerLimit() + " images at a time.");
        }

    }

    public void disableSelection() {
        this.selectedIDs.clear();
        this.notifyDataSetChanged();
    }

    public void setItems(ArrayList<Image> imagesList) {
        this.list.clear();
        this.list.addAll(imagesList);
    }

    public ArrayList<Long> getSelectedIDs() {
        return this.selectedIDs;
    }

    public void setOnHolderClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public class ImageHolder extends RecyclerView.ViewHolder {
        public RelativeLayout parentLayout;
        public FrameLayout frameLayout;
        public AutoImageView imageView;
        public ImageView selectedImageView;

        public ImageHolder(View v) {
            super(v);
            this.imageView = (AutoImageView)v.findViewById(id.imageView);
            this.selectedImageView = (ImageView)v.findViewById(id.selectedImageView);
            this.parentLayout = (RelativeLayout)v.findViewById(id.parentLayout);
            this.frameLayout = (FrameLayout)v.findViewById(id.frameLayout);
            if (GalleryImagesAdapter.this.params.getToolbarColor() != 0) {
                Utils.setViewBackgroundColor(GalleryImagesAdapter.this.activity, this.selectedImageView, GalleryImagesAdapter.this.params.getToolbarColor());
            }

        }

        public void setId(int position) {
            this.parentLayout.setId(position);
        }

        public void setTag(int resource_id, long id) {
            this.parentLayout.setTag(resource_id, id);
        }
    }
}