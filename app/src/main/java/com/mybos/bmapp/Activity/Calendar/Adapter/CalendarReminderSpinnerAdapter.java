package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mybos.bmapp.R;

public class CalendarReminderSpinnerAdapter extends BaseAdapter {

    private Context context;
    private String date = "Choose day";

    public CalendarReminderSpinnerAdapter(Context context, String date) {
        this.context = context;
        if(!date.equals("")){
            this.date = date;
        }

    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.calendar_spinner_reminder, null);

        TextView tvDateSelected = view.findViewById(R.id.tvDateSelected);
        tvDateSelected.setText(date);


        return view;
    }
}
