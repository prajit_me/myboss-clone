package com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Fragment;


import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Adapter.AddContractorListAdapter;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmailAddContractorDialogFragment extends DialogFragment {
    public static final String CONTRACTOR_LIST_KEY = "EmailAddContractorDialogFragment.CONTRACTOR_LIST_KEY";
    private ContractorList contractorList;
    private RecyclerView rclContractorList;
    private EditText etSearch;
    private OnSelectContractor listener;
    public EmailAddContractorDialogFragment() {
        // Required empty public constructor
    }

    public static EmailAddContractorDialogFragment createInstance(ContractorList contractorList){
        EmailAddContractorDialogFragment fragment = new EmailAddContractorDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(CONTRACTOR_LIST_KEY,contractorList);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.a_email_contractor_d_add_contractor, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDialog().setCanceledOnTouchOutside(true);
        if (getArguments() != null){
            this.contractorList = getArguments().getParcelable(CONTRACTOR_LIST_KEY);
            setupRecycleView(view);
            setupSearchView(view);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        int width = (int) ( Resources.getSystem().getDisplayMetrics().widthPixels * 0.9);
        int height = (int) (Resources.getSystem().getDisplayMetrics().heightPixels * 0.8);
        if (null != getDialog().getWindow()) {
            getDialog().getWindow().setLayout(width,height);
        }

    }

    public void setupRecycleView(View view){
        rclContractorList = view.findViewById(R.id.rclContractorList);
        rclContractorList.setLayoutManager(new LinearLayoutManager(getContext()));
        AddContractorListAdapter adapter = new AddContractorListAdapter(contractorList);
        adapter.setListener(contractor -> {
            dismiss();
            if (null != listener){
                listener.selectContractor(contractor);
            }
        });
        rclContractorList.setAdapter(adapter);

    }

    public void setupSearchView(View view){
        etSearch = view.findViewById(R.id.etSearch);
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                performSearch();
                etSearch.clearFocus();
                if (null == getActivity()){
                    return false;
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (null != imm){
                    imm.hideSoftInputFromWindow(etSearch.getWindowToken(),0);
                }
                return true;
            }
            return false;
        });
    }

    public void performSearch(){
        AddContractorListAdapter adapter = (AddContractorListAdapter) rclContractorList.getAdapter();
        String searchKey = etSearch.getText().toString();
        adapter.performFilter(searchKey);
    }

    public void setListener(OnSelectContractor listener) {
        this.listener = listener;
    }

    public interface OnSelectContractor{
        void selectContractor(Contractor contractor);
    }
}
