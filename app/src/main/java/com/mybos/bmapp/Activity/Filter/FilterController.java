package com.mybos.bmapp.Activity.Filter;

import android.content.Context;

import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseController;
import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCategoryMapper;
import com.mybos.bmapp.Data.Mapper.Filter.FilterMapper;
import com.mybos.bmapp.Data.Model.Cases.Category.BuildingCategory;

import java.util.List;

public class FilterController {

    public void getCategoryTree(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<String> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        FilterMapper mapper = new FilterMapper();
        mapper.getCategoryTree(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getContractorCategoryTree(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<String> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        FilterMapper mapper = new FilterMapper();
        mapper.getContractorFilterList(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
