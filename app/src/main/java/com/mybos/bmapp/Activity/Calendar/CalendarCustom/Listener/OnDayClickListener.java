package com.mybos.bmapp.Activity.Calendar.CalendarCustom.Listener;

import com.mybos.bmapp.Activity.Calendar.CalendarCustom.EventDay;


public interface OnDayClickListener {
    void onDayClick(EventDay eventDay);
}
