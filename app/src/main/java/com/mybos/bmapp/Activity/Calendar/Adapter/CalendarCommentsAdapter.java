package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Calendar.CalendarController;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Calendar.MaintenanceCommnet;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

public class CalendarCommentsAdapter extends BaseAdapter {

    private Context context;
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    CalendarController controller = new CalendarController();
    private OnItemClick listener;

    public CalendarCommentsAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return MaintenancePerDay.getMaintenancePerDaySelected().getComments().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.calendar_a_comment, null);

        MaintenanceCommnet maintenanceCommnet = MaintenancePerDay.getMaintenancePerDaySelected().getComments().get(i);

        TextView tvWriter = view.findViewById(R.id.tvWriter);
        TextView tvDateComment = view.findViewById(R.id.tvDateComment);
        TextView tvComment = view.findViewById(R.id.tvComment);
        Button btnMenu = view.findViewById(R.id.btnMenu);

        tvWriter.setText(maintenanceCommnet.getWriterName());
        tvComment.setText(maintenanceCommnet.getComment());
        String dateComment = DateUtils.editDateTime3(maintenanceCommnet.getTime());
        tvDateComment.setText(dateComment);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu commentPopupMenu = new PopupMenu(context, btnMenu);
                commentPopupMenu.getMenuInflater().inflate(R.menu.calendar_maintenance_comment_menu, commentPopupMenu.getMenu());
                commentPopupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem menuItem) {
                        String param = "{\"comment\":" + maintenanceCommnet.getId_comment() + "}";
                        String id = MaintenancePerDay.getMaintenancePerDaySelected().getId();
                        String dateOgrin = MaintenancePerDay.getMaintenancePerDaySelected().getDate_ogrinal();
                        controller.removeCommentMaintenance(context, selectedBuilding.getId(),id,dateOgrin, param,
                                state -> {
                                    MaintenancePerDay.getMaintenancePerDaySelected().getComments().remove(maintenanceCommnet);
                                    listener.didSelect(maintenanceCommnet);
                                },
                                error -> {
                                });
                        return false;
                    }
                });

                commentPopupMenu.show();
            }
        });

        return view;
    }
    public void setListenerdelete(OnItemClick listener) {
        this.listener = listener;
    }

    public interface OnItemClick {
        void didSelect(MaintenanceCommnet commnet);

    }

}
