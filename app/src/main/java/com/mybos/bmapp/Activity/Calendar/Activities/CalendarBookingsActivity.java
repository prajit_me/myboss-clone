package com.mybos.bmapp.Activity.Calendar.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarBookingListAdapter;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;

import java.util.ArrayList;

public class CalendarBookingsActivity extends a_base_toolbar {

    TextView tvBookingsDate;
    Spinner spViewBooking;
    ListView lvBooking;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_bookings);
        updateToolbarTitle("Bookings");

        tvBookingsDate = findViewById(R.id.tvBookingsDate);
        spViewBooking = findViewById(R.id.spViewBooking);
        lvBooking = findViewById(R.id.lvBooking);

        String dateTitle = SelectedDay.getDateOfweek()+", "+SelectedDay.getFullDate();
        tvBookingsDate.setText(dateTitle);

        ArrayList<String> itemsSpinnerView = new ArrayList<>();
        itemsSpinnerView.add("All bookings");
        itemsSpinnerView.add("Pending bookings");
        itemsSpinnerView.add("Approved bookings");
        itemsSpinnerView.add("Rejected bookings");

        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,itemsSpinnerView);
        spViewBooking.setAdapter(arrayAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        lvBooking.setAdapter(new CalendarBookingListAdapter(this,"all"));

        spViewBooking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spViewBooking.getSelectedItem().toString().equals("All bookings")){
                    lvBooking.setAdapter(new CalendarBookingListAdapter(CalendarBookingsActivity.this,"all"));
                }else if (spViewBooking.getSelectedItem().toString().equals("Pending bookings")){
                    lvBooking.setAdapter(new CalendarBookingListAdapter(CalendarBookingsActivity.this,"Pending"));
                }else if (spViewBooking.getSelectedItem().toString().equals("Approved bookings")){
                    lvBooking.setAdapter(new CalendarBookingListAdapter(CalendarBookingsActivity.this,"Confirm"));
                }else if (spViewBooking.getSelectedItem().toString().equals("Rejected bookings")){
                    lvBooking.setAdapter(new CalendarBookingListAdapter(CalendarBookingsActivity.this,"Reject"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
