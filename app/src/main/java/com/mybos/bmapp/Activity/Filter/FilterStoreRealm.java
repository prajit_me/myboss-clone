package com.mybos.bmapp.Activity.Filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class FilterStoreRealm implements Parcelable,RealmModel {

    private int buildingId;
    private String selectedFilterResident;
    private String selectedFilterResidentName;
    private String selectedFilterAsset;
    private String selectedFilterAssetName;
    private String selectedFilterContractor;
    private String selectedFilterContractorName;
    private RealmList <Integer> selectedFCContractor;
    private RealmList<Integer> selectedFCPriority;
    private RealmList <Integer> selectedFCType;
    private RealmList <Integer> selectedFCStatus;

    public FilterStoreRealm(){

    }

    FilterStoreRealm(Parcel in){
        buildingId = in.readInt();
        selectedFilterResident = in.readString();
        selectedFilterAsset = in.readString();
        selectedFilterContractor = in.readString();
        selectedFilterResidentName = in.readString();
        selectedFilterAssetName = in.readString();
        selectedFilterContractorName = in.readString();

        selectedFCContractor = new RealmList<>();
        selectedFCPriority = new RealmList<>();
        selectedFCType = new RealmList<>();
        selectedFCStatus = new RealmList<>();
    }


    public static final Creator<FilterStoreRealm> CREATOR = new Creator<FilterStoreRealm>() {
        @Override
        public FilterStoreRealm createFromParcel(Parcel in) {
            return new FilterStoreRealm(in);
        }

        @Override
        public FilterStoreRealm[] newArray(int size) {
            return new FilterStoreRealm[size];
        }
    };

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public String getSelectedFilterResident() {
        return selectedFilterResident;
    }

    public void setSelectedFilterResident(String selectedFilterResident) {
        this.selectedFilterResident = selectedFilterResident;
    }

    public String getSelectedFilterAsset() {
        return selectedFilterAsset;
    }

    public void setSelectedFilterAsset(String selectedFilterAsset) {
        this.selectedFilterAsset = selectedFilterAsset;
    }

    public String getSelectedFilterContractor() {
        return selectedFilterContractor;
    }

    public void setSelectedFilterContractor(String selectedFilterContractor) {
        this.selectedFilterContractor = selectedFilterContractor;
    }

    public String getSelectedFilterResidentName() {
        return selectedFilterResidentName;
    }

    public void setSelectedFilterResidentName(String selectedFilterResidentName) {
        this.selectedFilterResidentName = selectedFilterResidentName;
    }

    public String getSelectedFilterAssetName() {
        return selectedFilterAssetName;
    }

    public void setSelectedFilterAssetName(String selectedFilterAssetName) {
        this.selectedFilterAssetName = selectedFilterAssetName;
    }

    public String getSelectedFilterContractorName() {
        return selectedFilterContractorName;
    }

    public void setSelectedFilterContractorName(String selectedFilterContractorName) {
        this.selectedFilterContractorName = selectedFilterContractorName;
    }

    public RealmList<Integer> getSelectedFCContractor() {
        return selectedFCContractor;
    }

    public void setSelectedFCContractor(List<Integer> listSelected) {
        this.selectedFCContractor = new RealmList<>();
        for (Integer id:listSelected
             ) {
            this.selectedFCContractor.add(id);
        }
    }

    public RealmList<Integer> getSelectedFCPriority() {
        return selectedFCPriority;
    }

    public void setSelectedFCPriority(List<Integer> listSelected) {
        this.selectedFCPriority = new RealmList<>();
        for (Integer id:listSelected
        ) {
            this.selectedFCPriority.add(id);
        }
    }

    public RealmList<Integer> getSelectedFCType() {
        return selectedFCType;
    }

    public void setSelectedFCType(List<Integer> listSelected) {
        this.selectedFCType = new RealmList<>();
        for (Integer id:listSelected
        ) {
            this.selectedFCType.add(id);
        }
    }

    public RealmList<Integer> getSelectedFCStatus() {
        return selectedFCStatus;
    }

    public void setSelectedFCStatus(List<Integer> listSelected) {
        this.selectedFCStatus = new RealmList<>();
        for (Integer id:listSelected
        ) {
            this.selectedFCStatus.add(id);
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(buildingId);
        dest.writeString(selectedFilterResident);
        dest.writeString(selectedFilterAsset);
        dest.writeString(selectedFilterContractor);
        dest.writeString(selectedFilterResidentName);
        dest.writeString(selectedFilterAssetName);
        dest.writeString(selectedFilterContractorName);
    }

}
