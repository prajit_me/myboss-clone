package com.mybos.bmapp.Activity.Calendar.CalendarCustom.Utils;

import android.graphics.Typeface;
import android.widget.TextView;

import com.mybos.bmapp.R;

import java.util.Calendar;

public class DayColorsUtils {

    public static void setDayColors(TextView textView, int textColor, int typeface, int background) {
        if (textView == null) {
            return;
        }

        textView.setTypeface(null, typeface);
        textView.setTextColor(textColor);
        textView.setBackgroundResource(background);
    }

    public static void setSelectedDayColors(TextView dayLabel, CalendarProperties calendarProperties) {
//        setDayColors(dayLabel, calendarProperties.getSelectionLabelColor(), Typeface.NORMAL,
//                R.drawable.circle_today);
//        dayLabel.getBackground().setColorFilter(calendarProperties.getSelectionColor(),
//                android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public static void setCurrentMonthDayColors(Calendar day, Calendar today, TextView dayLabel,
                                                CalendarProperties calendarProperties) {
        if (today.equals(day)) {
            setDayColors(dayLabel, calendarProperties.getTodayLabelColor(), Typeface.BOLD,
                    R.drawable.calendar_today);
        } else if (calendarProperties.getHighlightedDays().contains(day)) {
            setDayColors(dayLabel, calendarProperties.getHighlightedDaysLabelsColor(),
                    Typeface.NORMAL, R.drawable.background_transparent);
        } else {
            setDayColors(dayLabel, calendarProperties.getDaysLabelsColor(), Typeface.NORMAL,
                    R.drawable.background_transparent);
        }
    }
}
