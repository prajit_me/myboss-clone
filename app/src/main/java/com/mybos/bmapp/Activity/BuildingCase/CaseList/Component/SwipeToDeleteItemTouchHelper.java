package com.mybos.bmapp.Activity.BuildingCase.CaseList.Component;

import android.graphics.Canvas;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.view.View;

import com.mybos.bmapp.Activity.BuildingCase.Adapter.CaseList.CaseListRecyclerAdapter;

/**
 * Created by EmLaAi on 26/03/2018.
 *
 * @author EmLaAi
 */

public class SwipeToDeleteItemTouchHelper extends ItemTouchHelper.SimpleCallback {
    private SwipeToDeleteListener listener;

    public SwipeToDeleteItemTouchHelper(int dragDirs, int swipeDirs,SwipeToDeleteListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if (null != listener){
            listener.onSwipe(viewHolder);
        }
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
//            super.onSelectedChanged(viewHolder, actionState);
        if (null != viewHolder){
            if(viewHolder instanceof CaseListRecyclerAdapter.CaseListRecyclerViewHolder) {
                final View foreground = ((CaseListRecyclerAdapter.CaseListRecyclerViewHolder) viewHolder).getForeground();
                getDefaultUIUtil().onSelected(foreground);
            }
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
//            super.onChildDrawOver(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        final View foreground = ((CaseListRecyclerAdapter.CaseListRecyclerViewHolder)viewHolder).getForeground();
        getDefaultUIUtil().onDrawOver(c,recyclerView,foreground,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
//            super.clearView(recyclerView, viewHolder);
        final View foreground = ((CaseListRecyclerAdapter.CaseListRecyclerViewHolder)viewHolder).getForeground();
        getDefaultUIUtil().clearView(foreground);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
//            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        final View foreground = ((CaseListRecyclerAdapter.CaseListRecyclerViewHolder)viewHolder).getForeground();
        getDefaultUIUtil().onDraw(c,recyclerView,foreground,dX,dY,actionState,isCurrentlyActive);
    }

    @Override
    public int getSwipeDirs(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        if (!(viewHolder instanceof CaseListRecyclerAdapter.CaseListRecyclerViewHolder)) return 0;
        return super.getSwipeDirs(recyclerView, viewHolder);
    }

    public interface SwipeToDeleteListener{
        void onSwipe(RecyclerView.ViewHolder viewHolder);
    }
}
