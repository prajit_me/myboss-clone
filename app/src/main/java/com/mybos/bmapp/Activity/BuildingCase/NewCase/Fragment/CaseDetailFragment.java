package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Component.BuildingCaseBasicInfoComponent;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Component.BuildingCaseJobAreaComponent;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Component.BuildingCaseNotesComponent;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Component.BuildingCaseNumberComponent;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Component.BuildingCaseSubjectComponent;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleSpinnerAdapter;
import com.mybos.bmapp.Component.SpinnerAdapter.NullableKeepTrackSelectedSpinnerAdapter;
import com.mybos.bmapp.Component.SpinnerAdapter.TreeLikeSpinnerAdapter;
import com.mybos.bmapp.Component.SpinnerAdapter.TreeLikeSpinnerItemInterface;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Apartment.Apartment;
import com.mybos.bmapp.Data.Model.Apartment.ApartmentList;
import com.mybos.bmapp.Data.Model.Asset.Asset;
import com.mybos.bmapp.Data.Model.Asset.AssetsList;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseJobArea;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseTypeList;
import com.mybos.bmapp.Data.Model.Cases.Category.BuildingCategory;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class CaseDetailFragment extends BaseFragment implements BuildingCaseSubjectComponent.OnSubjectUpdate, BuildingCaseSubjectComponent.OnDescriptionUpdate, BuildingCaseNotesComponent.OnNoteChange {
    private BuildingCase buildingCase;
    private ContractorList contractorList;
    private CaseDetailFragmentController controller = new CaseDetailFragmentController();

    private BuildingCaseStatusList statusList;
    private BuildingCaseTypeList typeList;
    private ApartmentList apartmentList;
    private AssetsList assetsList;
    private List<BuildingCategory> categoryList;

    private BuildingCaseNumberComponent buildingCaseNumberView;
    private BuildingCaseBasicInfoComponent buildingCaseBasicInfoComponent;
    private BuildingCaseJobAreaComponent buildingCaseJobAreaComponent;
    private BuildingCaseSubjectComponent buildingCaseSubjectComponent;
    private BuildingCaseNotesComponent buildingCaseNotesComponent;
    private ViewGroup container;
    private ScrollView scrollView;
    private BuildingCaseJobArea jobArea;
    private final String Error = "Error";
    private static boolean onTouch = false;

    Spinner dropdownContractor,dropdownApartment,dropdownAsset,dropdownCategory,dropdownJobArea;
    FlexboxLayout tagContractor,tagAsset,tagAppartment;
    ConstraintLayout viewAsset,viewApartment;

    public CaseDetailFragment() {
        // Required empty public constructor
    }
    public static CaseDetailFragment newInstance(){
        CaseDetailFragment fragment = new CaseDetailFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.container = container;
        View view = inflater.inflate(R.layout.a_new_case_f_case_detail, container, false);
        buildingCaseNumberView = view.findViewById(R.id.buildingCaseNumber);
        buildingCaseBasicInfoComponent = view.findViewById(R.id.buildingCaseBasicInfo);
        buildingCaseSubjectComponent = view.findViewById(R.id.buildingCaseSubject);
        buildingCaseNotesComponent = view.findViewById(R.id.buildingCaseNotes);
        scrollView = view.findViewById(R.id.scrollView);

        dropdownContractor = view.findViewById(R.id.dropdownContractor);
        dropdownApartment = view.findViewById(R.id.dropdownApartment);
        dropdownAsset = view.findViewById(R.id.dropdownAsset);
        dropdownCategory = view.findViewById(R.id.dropdownCategory);
        dropdownJobArea = view.findViewById(R.id.dropdownJobArea);

        tagContractor = view.findViewById(R.id.tagContractorCloud);
        tagAsset = view.findViewById(R.id.tagAssetCloud);
        tagAppartment = view.findViewById(R.id.tagAppartmentCloud);

        viewAsset = view.findViewById(R.id.viewAsset);
        viewApartment = view.findViewById(R.id.viewApartment);

        jobArea = new BuildingCaseJobArea(container.getContext());
        setupTapOutsideToDismiss(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (this.buildingCase != null && this.contractorList != null){
            if (null != assetsList) {
                this.assetsList.reset();
            }
            updateUI();
            updateJobAreaView(buildingCase.getArea());
        }
    }

    public void setupData(BuildingCase buildingCase, ContractorList contractorList){
        this.buildingCase = buildingCase;
        this.contractorList = contractorList;
    }

    public void initData(){
        Building seletedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        Map<String,Boolean> check = new HashMap<>();
        check.put("Error",false);
        check.put("Status",false);
        check.put("Type",false);
        check.put("Apartment",false);
        check.put("Asset",false);
        check.put("Category",false);
        displayLoadingScreen();
        controller.getBuildingCaseStatus(getContext(),seletedBuilding.getId(),(statusList)->{
            if (!check.get("Error")){
                this.statusList = statusList;
                check.put("Status",true);
                if (check.get("Status") && check.get("Type") && check.get("Apartment") && check.get("Asset") && check.get("Category")){
                    dismissLoadingScreen();
                    updateUI();
                }
            }
        },(error)->{
            if (!check.get("Error")){
                check.put("Error",true);
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("initData",error.toString());
                }else {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }
        });
        controller.getBuildingCaseType(getContext(),seletedBuilding.getId(),(typeList)->{
            if (!check.get("Error")){
                this.typeList = typeList;
                check.put("Type",true);
                if (check.get("Status") && check.get("Type") && check.get("Apartment") && check.get("Asset") && check.get("Category")){
                    dismissLoadingScreen();
                    updateUI();
                }
            }
        },(error)->{
            if (!check.get("Error")){
                check.put("Error",true);
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("initData",error.toString());
                }else {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }
        });
        controller.getAllBuildingApartment(getContext(),seletedBuilding.getId(),(apartmentList)->{
            if (!check.get("Error")){
                this.apartmentList = apartmentList;
                check.put("Apartment",true);
                if (check.get("Status") && check.get("Type") && check.get("Apartment") && check.get("Asset") && check.get("Category")){
                    dismissLoadingScreen();
                    updateUI();
                }
            }
        },(error)->{
            if (!check.get("Error")){
                check.put("Error",true);
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("initData",error.toString());
                }else {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }
        });
        controller.getAllAsset(getContext(),seletedBuilding.getId(),(assetsList)->{
            if (!check.get("Error")){
                this.assetsList = assetsList;
                check.put("Asset",true);
                if (check.get("Status") && check.get("Type") && check.get("Apartment") && check.get("Asset") && check.get("Category")){
                    dismissLoadingScreen();
                    updateUI();
                }
            }
        },(error)->{
            if (!check.get("Error")){
                check.put("Error",true);
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("initData",error.toString());
                }else {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }
        });
        controller.getCategoryTree(getContext(),seletedBuilding.getId(),(categoryList)->{
            if (!check.get("Error")){
                this.categoryList = categoryList;
                check.put("Category",true);
                if (check.get("Status") && check.get("Type") && check.get("Apartment") && check.get("Asset") && check.get("Category")){
                    dismissLoadingScreen();
                    updateUI();
                }
            }
        },(error)->{
            if (!check.get("Error")){
                check.put("Error",true);
                dismissLoadingScreen();
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("initData",error.toString());
                }else {
                    Intent intent = new Intent(getContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }
        });
    }

    public void updateUI(){
        buildingCaseNumberView.setCaseNumber(Integer.toString(buildingCase.getNumber()));
        buildingCaseNumberView.setupStatusDropDown(statusList,buildingCase.getsStatus(),status -> this.buildingCase.setsStatus(status.getId()));

        buildingCaseBasicInfoComponent.setupCreateDateCalendar(buildingCase.getStartDate(),date->{
            buildingCase.setStart(DateUtils.getDateString(date,"yyyy-MM-dd'T'HH:mm:ssZZZZZ"));
        });
        buildingCaseBasicInfoComponent.setupDueDateCalendar(buildingCase.getDueDate(),date -> {
            buildingCase.setDue(DateUtils.getDateString(date,"yyyy-MM-dd'T'HH:mm:ssZZZZZ"));
        });
        buildingCaseBasicInfoComponent.setupPriorityDropDown(buildingCase.getPriority(),priority -> buildingCase.setPriority(priority));
        buildingCaseBasicInfoComponent.setupTypeDropDown(typeList,buildingCase.getType(),type -> buildingCase.setType(type.getId()));

        setupContractor(contractorList,contractorList.getSelectedId());
        setupAssetDropdown(assetsList,buildingCase.getAssetsSelectedList());
        setupApartmentDropdown(apartmentList,buildingCase.getApartmentsSelectedList());
        setupCategoryList(categoryList,getContext());
        setupJobAreaDropDown(jobArea,buildingCase.getArea());

        buildingCaseSubjectComponent.setSubject(buildingCase.getSubject(),this);
        buildingCaseSubjectComponent.setDescription(buildingCase.getDetail(),this);

        buildingCaseNotesComponent.setNote(buildingCase.getHistoryNote(),this);
    }



    public void setupContractor(ContractorList contractorList, List<Integer> seletedContractors){
        tagContractor.removeAllViews();
        View view = new View(getContext());
        float densiry = getContext().getResources().getDisplayMetrics().density;
        view.setLayoutParams(new ViewGroup.LayoutParams(0,50 * (int) densiry));
        tagContractor.addView(view);
        if (null != seletedContractors) {
            for (int id : seletedContractors) {
                contractorList.selectId(id);
            }
        }
        List<BaseModel> selectedContractorModel = contractorList.getSelectedItem();
        for (BaseModel item:selectedContractorModel){
            if (item instanceof Contractor){
                addContractorTag((Contractor) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapter = new NullableKeepTrackSelectedSpinnerAdapter(getContext(),contractorList);
        dropdownContractor.setAdapter(adapter);
        dropdownContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dropdownContractor.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownContractor.getAdapter();
                Contractor contractor = (Contractor) mAdapter.getItem(position);
                if (null != contractor){
                    mAdapter.selectItem(contractor);
                    addContractorTag(contractor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addContractorTag(Contractor contractor){
        View tag = getLayoutInflater().inflate(R.layout.tag_item,tagContractor,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(contractor.getCompany());
        textView.setTag(contractor);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Contractor){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownContractor.getAdapter();
                mAdapter.deselectItem((Contractor)object);
                tagContractor.removeView(tag);
//                            contractorList.deselectId(((Contractor) object).getId());
            }
        });
        tagContractor.addView(tag);
//                    contractorList.selectId(contractor.getId());
    }

    public void setupCategoryList(List<BuildingCategory> categoryList, Context context){
        List<TreeLikeSpinnerItemInterface> source = new ArrayList<>();
        for(BuildingCategory category:categoryList){
            if (category instanceof TreeLikeSpinnerItemInterface){
                source.add((TreeLikeSpinnerItemInterface) category);
            }
        }
        TreeLikeSpinnerAdapter adapter = new TreeLikeSpinnerAdapter(getContext(),source);
        dropdownCategory.setAdapter(adapter);
        dropdownCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TreeLikeSpinnerAdapter clickAdapter = (TreeLikeSpinnerAdapter) dropdownCategory.getAdapter();
                BuildingCategory category = (BuildingCategory) clickAdapter.getItem(position);
                if (null != category){
                    Log.e("check","click_on_category");
                    if (category.getId() == BuildingCategory.ALL_ID){
                        Log.e("check","click_on_category_if");
                        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
                        controller.getAllAsset(getContext(),selectedBuilding.getId(), CaseDetailFragment.this::updateAssetList,error -> {
                            if(Offlinemode.checkNetworkConnection(getContext())){
                                Log.e("onItemSelected",error.toString());
                            }else {
                                Offlinemode.showWhenOfflineMode(getContext());
                            }
                        });
                    }else if(category.getId() == BuildingCategory.BACK_ID){
                        Log.e("check","click_on_category_back");
                    }else {
                        Log.e("check","click_on_category_else");
                        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
                        controller.getAsset(getContext(),selectedBuilding.getId(),category.getId(),CaseDetailFragment.this::updateAssetList,error -> {
                            if(Offlinemode.checkNetworkConnection(getContext())){
                                Log.e("onItemSelected",error.toString());
                            }else {
                                Offlinemode.showWhenOfflineMode(getContext());
                            }
                        });
                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setupAssetDropdown(AssetsList assetsList,List<Integer> selectedAsset){
        tagAsset.removeAllViews();
        View view = new View(getContext());
        float densiry = getContext().getResources().getDisplayMetrics().density;
        view.setLayoutParams(new ViewGroup.LayoutParams(0,50 * (int) densiry));
        tagAsset.addView(view);
        if (null != selectedAsset) {
            for (int id : selectedAsset) {
                assetsList.selectId(id);
            }
        }
        List<BaseModel> selectedAssetModel = assetsList.getSelectedItem();
        for (BaseModel item:selectedAssetModel){
            if (item instanceof Asset){
                addAssetTag((Asset) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapter = new NullableKeepTrackSelectedSpinnerAdapter(getContext(),assetsList);
        dropdownAsset.setAdapter(adapter);
        dropdownAsset.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dropdownAsset.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownAsset.getAdapter();
                Asset asset = (Asset) mAdapter.getItem(position);
                if (null != asset){
                    mAdapter.selectItem(asset);
                    addAssetTag(asset);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addAssetTag(Asset asset){
        View tag = getLayoutInflater().inflate(R.layout.tag_item,tagAsset,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(asset.getName());
        textView.setTag(asset);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Asset){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownAsset.getAdapter();
                mAdapter.deselectItem((Asset)object);
                tagAsset.removeView(tag);
                buildingCase.setAssets(assetsList.getSelectedId());
            }
        });
        tagAsset.addView(tag);
        buildingCase.setAssets(assetsList.getSelectedId());
    }

    public void setupApartmentDropdown(ApartmentList apartmentList,List<String> selectedApartment){
        tagAppartment.removeAllViews();
        View view = new View(getContext());
        float densiry = getContext().getResources().getDisplayMetrics().density;
        view.setLayoutParams(new ViewGroup.LayoutParams(0,50 * (int) densiry));
        tagAppartment.addView(view);
        if (null != selectedApartment){
            for (String id:selectedApartment){
                apartmentList.selectId(id);
            }
        }
        List<BaseModel> selectedApartmentModel = apartmentList.getSelectedItem();
        for (BaseModel item:selectedApartmentModel){
            if (item instanceof Apartment){
                addApartmentTag((Apartment) item);
            }
        }
        NullableKeepTrackSelectedSpinnerAdapter adapter = new NullableKeepTrackSelectedSpinnerAdapter(getContext(),apartmentList);
        dropdownApartment.setAdapter(adapter);
        dropdownApartment.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                onTouch = true;
                return false;
            }
        });
        dropdownApartment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (onTouch){
                    onTouch=false;
                    NullableKeepTrackSelectedSpinnerAdapter subAdapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownApartment.getAdapter();
                    Apartment apartment = (Apartment) subAdapter.getItem(position);
                    if (null != apartment){
                        subAdapter.selectItem(apartment);
                        addApartmentTag(apartment);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addApartmentTag(Apartment apartment){
        View tag = getLayoutInflater().inflate(R.layout.tag_item,tagAsset,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(apartment.getTittle());
        textView.setTag(apartment);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Apartment){
                NullableKeepTrackSelectedSpinnerAdapter subAdapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownApartment.getAdapter();
                subAdapter.deselectItem((Apartment)object);
                tagAppartment.removeView(tag);
                buildingCase.setApartments(apartmentList.getSelectedApartmets());
            }
        });
        tagAppartment.addView(tag);
        buildingCase.setApartments(apartmentList.getSelectedApartmets());
    }

    public void setupJobAreaDropDown(BuildingCaseJobArea jobArea,int selectedJobArea){
        SimpleTitleSpinnerAdapter adapter = new SimpleTitleSpinnerAdapter(getContext(),jobArea);
        dropdownJobArea.setAdapter(adapter);
        dropdownJobArea.setSelection(jobArea.getPositionForId(selectedJobArea));
        dropdownJobArea.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object object = dropdownJobArea.getAdapter().getItem(position);
                if (object instanceof BuildingCaseJobArea.JobAreaItem){
                    buildingCase.setArea(((BuildingCaseJobArea.JobAreaItem) object).getId());
                    updateJobAreaView(((BuildingCaseJobArea.JobAreaItem) object).getId());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void updateAssetList(AssetsList assetsList){
        this.assetsList.addNewAssetList(assetsList.getAssetList());
        NullableKeepTrackSelectedSpinnerAdapter adapter = (NullableKeepTrackSelectedSpinnerAdapter) dropdownAsset.getAdapter();
        if (null != adapter){
            adapter.changeMainSource(this.assetsList);
        }
    }

    public void updateJobAreaView(int id){
        if(id == BuildingCaseJobArea.PRIVATE_LOT_ID){
            viewApartment.setVisibility(View.VISIBLE);
            viewAsset.setVisibility(View.GONE);
        }else if (id == BuildingCaseJobArea.COMMON_ASSET_ID){
            viewAsset.setVisibility(View.VISIBLE);
            viewApartment.setVisibility(View.GONE);
        }else if (id == BuildingCaseJobArea.COMMON_NON_ASSET_ID){
            viewApartment.setVisibility(View.GONE);
            viewAsset.setVisibility(View.GONE);
        }

    }

    public void refreshView(){
        scrollView.setVisibility(View.GONE);
        scrollView.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateSubject(String newValue) {
        buildingCase.setSubject(newValue);
    }

    @Override
    public void updateDescription(String newValue) {
        buildingCase.setDetail(newValue);
    }

    @Override
    public void updateNotes(String notes) {
        buildingCase.setHistoryNote(notes);
    }

    interface CaseDetailFragmentListener{}
}
