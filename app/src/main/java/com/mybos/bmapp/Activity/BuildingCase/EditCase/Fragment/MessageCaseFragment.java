package com.mybos.bmapp.Activity.BuildingCase.EditCase.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.Adapter.MessageCaseAdapter;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.EditCaseController;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.Message;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class MessageCaseFragment extends BaseFragment {

    RecyclerView RCView;
    EditText edtMessage;
    ImageButton btnSend;

    EditCaseController controller = new EditCaseController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    public static MessageCaseFragment  createInstance(){
        MessageCaseFragment fragment = new MessageCaseFragment();
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.case_message_fragment, container, false);
        RCView = view.findViewById(R.id.RCView);
        RCView.setLayoutManager(new LinearLayoutManager(getContext()));
        edtMessage = view.findViewById(R.id.edtMessage);
        btnSend = view.findViewById(R.id.btnSend);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getMessages();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!edtMessage.getText().toString().isEmpty()){
                    displayLoadingScreen();
                    controller.sendMessage(getContext(),selectedBuilding.getId(), Message.getSelectedReId(),edtMessage.getText().toString(),
                            success->{
                                getMessages();
                                dismissLoadingScreen();
                                edtMessage.setText("");
                            },
                            error->{Log.e("send message: ",error.toString());});
                }
            }
        });
    }


    public void getMessages(){
        controller.getMessage(getContext(),selectedBuilding.getId(), Message.getSelectedReId(),
                resp->{
                    RCView.setAdapter(new MessageCaseAdapter(MessageCaseFragment.this));
                },
                error->{
                    Log.e("get Message:", error.toString());
                });
    }
}
