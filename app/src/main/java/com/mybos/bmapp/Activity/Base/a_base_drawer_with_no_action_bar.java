package com.mybos.bmapp.Activity.Base;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.mybos.bmapp.Activity.Assets.Activity.BuildingAssetActivity;
import com.mybos.bmapp.Activity.Broadcast.BroadcastActivity;
import com.mybos.bmapp.Activity.Broadcast.BroadcastController;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListActivity;
import com.mybos.bmapp.Activity.Calendar.Activities.CalendarActivity;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Inspection.InspectionList.InspectionListActivity;
import com.mybos.bmapp.Activity.Keys.Activities.KeyList.KeyListActivity;
import com.mybos.bmapp.Activity.Library.LibraryListActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Activity.Resident.ResidentList.ResidentListActivity;
import com.mybos.bmapp.Activity.Contractor.Activities.ContractorListActivity;
import com.mybos.bmapp.Data.Mapper.Building.BuildingMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.R;
import com.bumptech.glide.Glide;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.List;

public class a_base_drawer_with_no_action_bar extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    protected boolean check = false;
    protected DrawerLayout drawer;
    static int oldBuildingSelected = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.dashboard_title);
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle action = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.drawer_open,R.string.drawer_close);
//        drawer.addDrawerListener(action);
//        action.syncState();
    }

    @Override
    public void setContentView(int layoutResID) {
        // overide default set contain to inject child contain into base layout
        super.setContentView(R.layout.a_base_drawer_with_no_action_bar);
        getLayoutInflater().inflate(layoutResID,findViewById(R.id.content));

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle action = new ActionBarDrawerToggle(this,drawer,R.string.drawer_open,R.string.drawer_close);
        drawer.addDrawerListener(action);
        action.syncState();
        action.setHomeAsUpIndicator(getDrawable(R.drawable.ic_menu_white));

        NavigationView navigationView = findViewById(R.id.navigation);
        View navHeader = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        navigationView.setItemTextAppearance(R.style.MenuIconStyle);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.left_menu_home:
                navigateToRoot();
                break;
            case R.id.left_menu_cases:
                navigateToCaseList();
                break;
            case R.id.left_menu_inspection:
                navigateToInspectionList();
                break;
            case R.id.left_menu_resident:
                navigateToResidentList();
                break;
            case R.id.left_menu_logout:
                doLogout();
                break;
        }
        return false;
    }

    @Override
    public void doLogout(){
        super.doLogout();

    }

    public void advanceSetupDrawerMenu(){
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                drawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setupBuildingDropdownList();
            }
        });
    }

    public void setupBuildingDropdownList(){

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        Spinner spinner = drawer.findViewById(R.id.buildingSpinner);
        if (oldBuildingSelected==-1){
            oldBuildingSelected = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding().getId();
        }

        List<String> buildingNames = SingletonObjectHolder.getInstance().getBuildingList().getBuildingListName();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_title_only,R.id.tvTitle,buildingNames);
        if (null == spinner){
            return;
        }
        spinner.setAdapter(adapter);
        spinner.setDropDownVerticalOffset(spinner.getHeight());
        spinner.setSelection(SingletonObjectHolder.getInstance().getBuildingList().getSelectedIndex());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                SingletonObjectHolder.getInstance().getBuildingList().setSelectedIndex(position);
                BuildingInfoCollection.setSelectedBuilding(position);
                BuildingInfoCollection.setBuildingID(SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding().getId());
                if (null != view) {
                    TextView txtTitle = view.findViewById(R.id.tvTitle);
                    txtTitle.setText(null);
                }
                applyBuildingInfo(SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding());
                BuildingMapper mapper = new BuildingMapper();
                mapper.changeBuilding(a_base_drawer_with_no_action_bar.this,
                        SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding().getId(),
                        Void->{},e -> {});
                if (check&&(oldBuildingSelected!=SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding().getId())){
                    DashboardActivity.loading = true;
                    navigateToRoot();
                    oldBuildingSelected = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding().getId();
                }
                check = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void applyBuildingInfo(Building building){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ((TextView) drawer.findViewById(R.id.txtAddress)).setText(building.getAddress());

        ((TextView) drawer.findViewById(R.id.txtBuildingName)).setText(building.getName());
        BuildingInfoCollection.setBuildingName(building.getName());

        ImageView imageView = drawer.findViewById(R.id.imgNavBuilding);

        Glide.with(this).load(building.getPhoto()).into(imageView);
    }

    public void navigateToCaseList() {

        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent caseList = new Intent(this,CaseListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(caseList);
        builder.startActivities();
        finish();
    }

    public void navigateToResidentList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent resident = new Intent(this, ResidentListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(resident);
        builder.startActivities();
        finish();
    }

    public void navigateToInspectionList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent inspectionList = new Intent(this, InspectionListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(inspectionList);
        builder.startActivities();
        finish();
    }

    public void navigateToContractorList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent ContractorList = new Intent(this, ContractorListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(ContractorList);
        builder.startActivities();
        finish();
    }

    public void navigateToLibraryList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent LibraryList = new Intent(this, LibraryListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(LibraryList);
        builder.startActivities();
        finish();
    }

    public void navigateToAssetList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent AssetList = new Intent(this, BuildingAssetActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(AssetList);
        builder.startActivities();
        finish();
    }

    public void navigateToBroadcast(){
        BroadcastController controller = new BroadcastController();
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getBroadcastListGroup(this, selectedBuilding.getId(),groups->{
        }, error -> {
            if (Offlinemode.checkNetworkConnection(this)){
                Log.e("navigateToBroadcast",error.toString());
            }else {
                Toast.makeText(this, "disconnect!", Toast.LENGTH_SHORT).show();
            }
        });
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent BroadcastList = new Intent(this, BroadcastActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(BroadcastList);
        builder.startActivities();
        finish();
    }

    public void navigateToKey(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent Keylist = new Intent(this, KeyListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(Keylist);
        builder.startActivities();
        finish();
    }

    public void navigateToCalendar(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
        Intent Calendar = new Intent(this, CalendarActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(Calendar);
        builder.startActivities();
        finish();
    }
}
