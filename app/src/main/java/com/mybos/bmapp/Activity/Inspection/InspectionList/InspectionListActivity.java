package com.mybos.bmapp.Activity.Inspection.InspectionList;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;

import com.google.android.material.tabs.TabLayout;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.mybos.bmapp.Activity.Base.*;
import com.mybos.bmapp.Activity.Inspection.Adapter.InspectionListPagerAdapter;
import com.mybos.bmapp.Activity.Inspection.InspectionInfoCollector;
import com.mybos.bmapp.Activity.Offlinemode.ChangNetworkStateReciver;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Inspection.InspectionOfflinetoSaveList;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

public class InspectionListActivity extends a_base_toolbar {

    private static final int REQUEST_PERMISSION=99;
    BroadcastReceiver myBroadcast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_inspection_list);

        List<String> listPermissionsNeeded = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION);
        }

        //IMPORTANT: setup drawer with drop down building list

        super.updateToolbarTitle(getString(R.string.inspection_list_activity_title));

        IntentFilter intentFilter = new IntentFilter(ChangNetworkStateReciver.NETWORK_AVAILABLE_ACTION);
        myBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Offlinemode.checkNetworkConnection(InspectionListActivity.this)){
                    checkShowBtnSync(InspectionListActivity.this);
                    ((Button) findViewById(R.id.textButtonOffline)).setVisibility(View.INVISIBLE);
                }else{
                    ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.INVISIBLE);
                    ((Button) findViewById(R.id.textButtonOffline)).setVisibility(View.VISIBLE);
                }
            }
        };
        registerReceiver(myBroadcast,intentFilter);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new InspectionListPagerAdapter(this,getSupportFragmentManager()));

        TabLayout tabLayout = findViewById(R.id.topTabBar);
        tabLayout.setupWithViewPager(viewPager);

        InspectionInfoCollector.clearInspectionItemList();
    }

    private void checkShowBtnSync(Context context){
        if (InspectionOfflinetoSaveList.get()!=null){
            if (InspectionOfflinetoSaveList.get().getListInspectionOfflinetoSave().size() > 0) {
                String sizeList = String.valueOf(InspectionOfflinetoSaveList.get().getListInspectionOfflinetoSave().size());
                ((Button) findViewById(R.id.btnSyncingInspection)).setText("Tap to sync " +sizeList+" inspection");
                ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.VISIBLE);
                setupBtnSyncingInspection(context);
            }
        }else {
            ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.INVISIBLE);
        }
    }

    private void setupBtnSyncingInspection(Context context){
        ((Button) findViewById(R.id.btnSyncingInspection)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Offlinemode.dialogAsyncInspection(context);
                ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.INVISIBLE);
            }
        });
    }

}
