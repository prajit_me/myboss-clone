package com.mybos.bmapp.Activity.Assets.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseActivity;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseController;
import com.mybos.bmapp.Activity.Filter.FilterFragment;
import com.mybos.bmapp.Activity.Inspection.CreateInspection.Fragment.ChoosePhotoSourceDialog;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.SpinnerAdapter.NullableKeepTrackSelectedSpinnerAdapter;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.Data.Model.Cases.SelectedFileInvoiceQuotes;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Error.ImageEncodeError;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;
import com.mybos.bmapp.Utils.BuildFileNameForUploadDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewAssetActivity extends a_base_toolbar {

    EditText edtAssetName,editNewCategory,edtLocation,
            edtSerialNumber,editAssetValue,editDescription,
            editBarcode,editMake,editModel;
    Button spinnerNestUnder;
    Spinner spinnerServiceContractor;
    FlexboxLayout tagContractorCloud;
    TextView hintContractorService;
    private String selectedCategory = null;
    private static ContractorList contractorList = new ContractorList();
    private static final int PHOTO_LIBRARY_REQUEST_CODE = 1;
    private static final int PHOTO_TAKE_REQUEST_CODE = 2;
    public static final int PICKFILE_RESULT_CODE = 3;
    Uri currentUri;
    PhotoModel selectedPhoto = null;
    byte[] selectedFileByte = null;
    String currentPhotoPath,selectedFileName,selectedFileMime;
    NewBuildingCaseController newBuildingCaseController = new NewBuildingCaseController();
    AssetController assetController = new AssetController();
    public static final String TYPE_EDIT = "AsEdit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_asset);
        updateToolbarTitle(getIntent().getIntExtra(TYPE_EDIT,0)==0?"New asset":BuildingAssetInfoCollector.getSelectedBuildingAsset().getName());

        edtAssetName = findViewById(R.id.edtAssetName);
        editNewCategory = findViewById(R.id.editNewCategory);
        edtLocation = findViewById(R.id.edtLocation);
        edtSerialNumber = findViewById(R.id.edtSerialNumber);
        editAssetValue = findViewById(R.id.editAssetValue);
        editDescription = findViewById(R.id.editDescription);
        editBarcode = findViewById(R.id.editBarcode);
        editMake = findViewById(R.id.editMake);
        editModel = findViewById(R.id.editModel);
        spinnerNestUnder = findViewById(R.id.spinnerNestUnder);
        spinnerServiceContractor = findViewById(R.id.spinnerServiceContractor);
        tagContractorCloud = findViewById(R.id.tagContractorCloud);

        hintContractorService = findViewById(R.id.hintContractorService);

        spinnerNestUnder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterFragment dialog = new FilterFragment(2,1);
                dialog.show(getSupportFragmentManager(),"Filter");
            }
        });

        findViewById(R.id.btnOption).setBackground(getDrawable(R.drawable.ic_done));
        findViewById(R.id.btnOption).setVisibility(View.VISIBLE);
        findViewById(R.id.btnOption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getIntent().getIntExtra(TYPE_EDIT,0)==1){
                    try {
                        editAsset();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }else {
                    createNewAsset();
                }
            }
        });
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        getContractorList(selectedBuilding.getId(),1);
        setupTakePhotoCameraAndLibraryAction();
        if (getIntent().getIntExtra(TYPE_EDIT,0)==1){
            setUpForEditAsset();
        }
    }

    private void getContractorList(int buildingId,int page){
        newBuildingCaseController.getContractorList(this, buildingId, "all",page, (contractorList) -> {
            if (contractorList.getContractorList().size()==0){
                setupSnipperContractorService();
            }else {
                for (Contractor c: contractorList.getContractorList()
                ) {
                    this.contractorList.addContrator(c);
                }
                getContractorList(buildingId,page+1);
            }
        }, error -> {
            this.contractorList = new ContractorList();
        });
    }

    private void setupSnipperContractorService(){
        tagContractorCloud.removeAllViews();

        View view = new View(this);
        float densiry = this.getResources().getDisplayMetrics().density;
        view.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagContractorCloud.addView(view);
        if (getIntent().getIntExtra(TYPE_EDIT, 0) == 1) {
            if (BuildingAssetInfoCollector.getSelectedBuildingAsset().getContractors().size() != 0) {
                for (Contractor contractor : BuildingAssetInfoCollector.getSelectedBuildingAsset().getContractors()) {
                    contractorList.selectId(contractor.getId());
                }
            }
        } else {
            if (null != ContractorList.getContractorListApply()) {
                if (null != ContractorList.getContractorListApply().getSelectedId()) {
                    for (int id : ContractorList.getContractorListApply().getSelectedId()) {
                        contractorList.selectId(id);
                    }
                }
            }
        }
        List<BaseModel> selectedContractorModel = contractorList.getSelectedItem();
        for (BaseModel item : selectedContractorModel) {
            if (item instanceof Contractor) {
                addContractorTag((Contractor) item);
            }
        }
        spinnerServiceContractor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                edtAssetName.clearFocus();
                editNewCategory.clearFocus();
                return false;
            }
        });
        NullableKeepTrackSelectedSpinnerAdapter adapter = new NullableKeepTrackSelectedSpinnerAdapter(this, contractorList);
        spinnerServiceContractor.setAdapter(adapter);
        spinnerServiceContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("check_click", "click item");
                spinnerServiceContractor.setSelection(0);
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spinnerServiceContractor.getAdapter();
                Contractor contractor = (Contractor) mAdapter.getItem(position);
                if (null != contractor) {
                    mAdapter.selectItem(contractor);
                    addContractorTag(contractor);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void addContractorTag(Contractor contractor){
        hintContractorService.setVisibility(View.GONE);
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagContractorCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(contractor.getCompany());
        textView.setTag(contractor);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            if (object instanceof Contractor){
                NullableKeepTrackSelectedSpinnerAdapter mAdapter = (NullableKeepTrackSelectedSpinnerAdapter) spinnerServiceContractor.getAdapter();
                mAdapter.deselectItem((Contractor)object);
                tagContractorCloud.removeView(tag);
                contractorList.deselectId(((Contractor) object).getId());
                if (contractorList.getSelectedId().size()==0){
                    hintContractorService.setVisibility(View.VISIBLE);
                }
            }
        });
        tagContractorCloud.addView(tag);
    }

    public void setupTakePhotoCameraAndLibraryAction(){
        findViewById(R.id.btnAddPhoto).setOnClickListener(v -> {
            ChoosePhotoSourceDialog dialog = ChoosePhotoSourceDialog.createInstance();
            dialog.setCameraClick(()->{
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (intent.resolveActivity(NewAssetActivity.this.getPackageManager()) != null){
                                // Create the File where the photo should go
                                File photoFile = null;
                                try {
                                    photoFile = createImageFile();
                                } catch (IOException ex) {
                                    // Error occurred while creating the File
                                }
                                // Continue only if the File was successfully created
                                if (photoFile != null) {
                                    Uri photoURI = FileProvider.getUriForFile(NewAssetActivity.this,
                                            "com.mybos.bmapp.fileprovider",
                                            photoFile);
                                    currentUri = photoURI;
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                    startActivityForResult(intent,PHOTO_TAKE_REQUEST_CODE);
                                }
                            }
                    });

            dialog.setLibraryClick(()->{
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/jpeg,jpg,png");
                            if (intent.resolveActivity(NewAssetActivity.this.getPackageManager()) != null) {
                                startActivityForResult(intent, PHOTO_LIBRARY_REQUEST_CODE);
                            }
                    });
            dialog.show(getSupportFragmentManager(),"TAG");
        });

        findViewById(R.id.btnAddDoc).setOnClickListener(v -> {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.shared().i("-------------GET BACK ----------------");
        if (requestCode == PHOTO_LIBRARY_REQUEST_CODE){
            if (data != null){
                Uri photoUri = data.getData();
                if (NewAssetActivity.this != null && photoUri != null) {
                    String mime = NewAssetActivity.this.getContentResolver().getType(photoUri);
                    String name;
                    Cursor cursor = NewAssetActivity.this.getContentResolver().query(photoUri,null,null,null,null);
                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        name = cursor.getString(nameIndex);
                        cursor.close();
                    }else {
                        name = "photo" + (System.currentTimeMillis()/1000);
                    }
                    Logger.shared().i(mime);
                    try {
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(NewAssetActivity.this.getContentResolver(), photoUri);
                        ((TextView)findViewById(R.id.tvNamePhoto)).setText(name);
                        selectedPhoto = new PhotoModel(selectedImage,name,mime);
                    } catch (IOException e) {
                        Toast.makeText(NewAssetActivity.this, new ImageEncodeError().toString(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        if (requestCode == PHOTO_TAKE_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                try {
                    File file = new File(currentPhotoPath);
                    String name = file.getName();
                    String mime = "Content-Type: image/jpeg";
                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(NewAssetActivity.this.getContentResolver(),Uri.fromFile(file));
                    ((TextView)findViewById(R.id.tvNamePhoto)).setText(name);
                    selectedPhoto = new PhotoModel(selectedImage,name,mime);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        if (requestCode == PICKFILE_RESULT_CODE){
            if (resultCode == RESULT_OK){
                Uri fileUri = data.getData();
                InputStream inputStream = null;
                try {
                    inputStream = getContentResolver().openInputStream(fileUri);
                    selectedFileByte = new byte[inputStream.available()];
                    inputStream.read(selectedFileByte);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    Log.e("error_get_file",e.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e("error_get_file2",e.toString());
                }
                String filePath = fileUri.getLastPathSegment();
                File file = new File(buildPath(filePath));
                ContentResolver cR = getContentResolver();
                String mime = cR.getType(fileUri);
                String name = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/")+1);
                name = name.substring(name.lastIndexOf(":")+1);
                selectedFileName = BuildFileNameForUploadDocument.checkAndBuildFileName(name,mime);
                selectedFileMime = mime;

                ((TextView)findViewById(R.id.tvNameDoc)).setText(selectedFileName);
            }
        }
    }

    private void editAsset() throws UnsupportedEncodingException {
        if (edtAssetName.getText().toString().isEmpty() || (selectedCategory == null && editNewCategory.getText().toString().isEmpty())){
            Toast.makeText(this, R.string.AlertFillRequiredField, Toast.LENGTH_SHORT).show();
            return;
        }
        displayLoadingScreen();
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        VolleyMultipartBuilder builder = new VolleyMultipartBuilder();

        builder.addString("id",String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getAssetID()));
        builder.addString("building",String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getBuildingID()));
        builder.addString("name",URLEncoder.encode(edtAssetName.getText().toString(),"utf-8"));

        builder.addString("add",BuildingAssetInfoCollector.getSelectedBuildingAsset().getAdded());
        builder.addString("warranty_title",BuildingAssetInfoCollector.getSelectedBuildingAsset().getWarrantyTitle());
        builder.addString("warranty_expire",BuildingAssetInfoCollector.getSelectedBuildingAsset().getWarrantyExpire());

        if (!editNewCategory.getText().toString().isEmpty()){
            builder.addString("newCatName",URLEncoder.encode(editNewCategory.getText().toString(),"utf-8"));
        }
        if (selectedCategory != null){
            builder.addString("category",selectedCategory);
        }

        builder.addString("location", URLEncoder.encode(edtLocation.getText().toString(),"utf-8"));
        builder.addString("serial",URLEncoder.encode(edtSerialNumber.getText().toString(),"utf-8"));
        builder.addString("value",URLEncoder.encode(editAssetValue.getText().toString(),"utf-8"));
        builder.addString("description",URLEncoder.encode(editDescription.getText().toString(),"utf-8"));
        builder.addString("barcode",URLEncoder.encode(editBarcode.getText().toString(),"utf-8"));
        builder.addString("make",URLEncoder.encode(editMake.getText().toString(),"utf-8"));
        if (!editModel.getText().toString().isEmpty()){
            builder.addString("model",URLEncoder.encode(editModel.getText().toString(),"utf-8"));
        }
        for (int i = 0; i < contractorList.getSelectedId().size();i++){
            builder.addString("contractors["+i+"]",contractorList.getSelectedId().get(i).toString());
        }
        if (contractorList.getSelectedId().size()>1){
            builder.addString("contractor",contractorList.getSelectedId().get(contractorList.getSelectedId().size()-1).toString());
        }
        if (selectedPhoto != null){
            builder.addFile("photoWillUpload[0]",selectedPhoto.createMultipartFile());
        }
        if (selectedFileByte != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.addFile("documentWillUpload[0]", new VolleyMultipartBuilder.MultipartFile(selectedFileName, selectedFileByte, selectedFileMime));
            }
        }

        assetController.editAsset(NewAssetActivity.this,selectedBuilding.getId(),
                String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getAssetID()),builder,ok->{
                    Intent intent = new Intent(NewAssetActivity.this,BuildingAssetActivity.class);
                    startActivity(intent);
                    Toast.makeText(this, "Asset has been saved.", Toast.LENGTH_SHORT).show();
                    Log.e("edit_ok","okkkk");
                    dismissLoadingScreen();
                },error -> {
                    Toast.makeText(this, "Asset hasn't been saved.", Toast.LENGTH_SHORT).show();
                    dismissLoadingScreen();
                });
    }

    private void setUpForEditAsset(){
        edtAssetName.setText(BuildingAssetInfoCollector.getSelectedBuildingAsset().getName());
        selectedCategory = String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getCategory());
        spinnerNestUnder.setText(BuildingAssetInfoCollector.getSelectedBuildingAsset().getCategoryName());

        edtLocation.setText(BuildingAssetInfoCollector.getSelectedBuildingAsset().getLocation()=="null"?"":BuildingAssetInfoCollector.getSelectedBuildingAsset().getLocation());
        edtSerialNumber.setText(String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getSerial()));
        editAssetValue.setText(String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getAssetValue()));
        editDescription.setText(BuildingAssetInfoCollector.getSelectedBuildingAsset().getDescription()=="null"?"":BuildingAssetInfoCollector.getSelectedBuildingAsset().getDescription());
        editMake.setText(BuildingAssetInfoCollector.getSelectedBuildingAsset().getMake()=="null"?"":BuildingAssetInfoCollector.getSelectedBuildingAsset().getMake());
        editModel.setText(BuildingAssetInfoCollector.getSelectedBuildingAsset().getModel()=="null"?"":BuildingAssetInfoCollector.getSelectedBuildingAsset().getModel());
        editBarcode.setText(String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getBarcode())=="null"?"":String.valueOf(BuildingAssetInfoCollector.getSelectedBuildingAsset().getBarcode()));
    }

    private void createNewAsset(){
        if (edtAssetName.getText().toString().isEmpty() || (selectedCategory == null && editNewCategory.getText().toString().isEmpty())){
            Toast.makeText(this, R.string.AlertFillRequiredField, Toast.LENGTH_SHORT).show();
            return;
        }
        displayLoadingScreen();
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        VolleyMultipartBuilder builder = new VolleyMultipartBuilder();
        builder.addString("name",edtAssetName.getText().toString());
        if (!editNewCategory.getText().toString().isEmpty()){
            builder.addString("newCatName",editNewCategory.getText().toString());
        }
        if (selectedCategory != null){
            builder.addString("category",selectedCategory);
        }
        if (!edtLocation.getText().toString().isEmpty()){
            builder.addString("location",edtLocation.getText().toString());
        }
        if (!edtSerialNumber.getText().toString().isEmpty()){
            builder.addString("serial",edtSerialNumber.getText().toString());
        }
        if (!editAssetValue.getText().toString().isEmpty()){
            builder.addString("value",editAssetValue.getText().toString());
        }
        if (!editDescription.getText().toString().isEmpty()){
            builder.addString("description",editDescription.getText().toString());
        }
        if (!editBarcode.getText().toString().isEmpty()){
            builder.addString("barcode",editBarcode.getText().toString());
        }
        if (!editMake.getText().toString().isEmpty()){
            builder.addString("make",editMake.getText().toString());
        }
        if (!editModel.getText().toString().isEmpty()){
            builder.addString("model",editModel.getText().toString());
        }
        for (int i = 0; i < contractorList.getSelectedId().size();i++){
            builder.addString("contractors["+i+"]",contractorList.getSelectedId().get(i).toString());
        }
        if (contractorList.getSelectedId().size()>1){
            builder.addString("contractor",contractorList.getSelectedId().get(contractorList.getSelectedId().size()-1).toString());
        }
        if (selectedPhoto != null){
            builder.addFile("photoWillUpload[0]",selectedPhoto.createMultipartFile());
        }
        if (selectedFileByte != null){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                builder.addFile("documentWillUpload[0]", new VolleyMultipartBuilder.MultipartFile(selectedFileName, selectedFileByte, selectedFileMime));
            }
        }
        assetController.createNewAsset(NewAssetActivity.this,selectedBuilding.getId(),builder,id->{
            BuildingAsset buildingAsset = new BuildingAsset();
            buildingAsset.setAssetID(Integer.valueOf(id));
            buildingAsset.setBuildingID(selectedBuilding.getId());
            assetController.getSpecificBuildingAsset(this,
                    buildingAsset,
                    response -> {
                        dismissLoadingScreen();
                        BuildingAssetInfoCollector.setSelectedBuildingAsset(response);
                        Intent intent = new Intent(NewAssetActivity.this,BuildingAssetDetailActivity.class);
                        startActivity(intent);

                    },
                    error -> {});
        },error -> {
            dismissLoadingScreen();
            Log.e("Creating failed",error.toString());
            Toast.makeText(this, "Creating new asset failed.", Toast.LENGTH_SHORT).show();
        });

    }

    public String buildPath(String path){
        path = path.replace("/storage/emulated/0/","");
        int indexCut = path.indexOf(":");
        path = path.substring(indexCut+1,path.length());
        path = "/storage/emulated/0/"+path;
        Log.e("result",path);
        return path;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = this.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpeg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategoryNameNId(String selectedCategory,String id) {
        this.selectedCategory = id;
        Log.e("check_selected",id);
        spinnerNestUnder.setText(selectedCategory);
        findViewById(R.id.txtView32).setVisibility(View.INVISIBLE);
    }

    public String getNameFromPath(String name){
        int indextCut = name.lastIndexOf(":");
        if (indextCut != -1){
            name = name.substring(indextCut+1,name.length());
        }
        return name;
    }
}