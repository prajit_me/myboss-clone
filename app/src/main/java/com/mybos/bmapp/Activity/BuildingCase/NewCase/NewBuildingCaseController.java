package com.mybos.bmapp.Activity.BuildingCase.NewCase;

import android.content.Context;
import android.util.Log;

import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseMapper;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class NewBuildingCaseController {


    public void getContractorList(Context context, int buildingId, String SelectedCategory, int page, SuccessCallback<ContractorList> onSuccess,FailureCallback onError){
        ContractorMapper mapper = new ContractorMapper();
        mapper.get(context,buildingId,SelectedCategory,"",page,onSuccess::onSuccess,onError::onFailure);
    }
    public void getBuildingCaseDetail(Context context, int buildingId, String caseId , SuccessCallback<BuildingCase> onSuccess,FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.get(context,buildingId,caseId,onSuccess::onSuccess,onError::onFailure);
    }
    public void saveBuildingCase(Context context,int buildingId,ContractorList contractorList,BuildingCase buildingCase ,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.save(context,buildingId,contractorList,buildingCase,onSuccess::onSuccess,onFailure::onFailure);
    }



    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
