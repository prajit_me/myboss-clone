package com.mybos.bmapp.Activity.ImageSelectMultiple.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.TextureView;
import android.view.View.MeasureSpec;

public class AutoFitTextureView extends TextureView {
    private int mRatioWidth;
    private int mRatioHeight;

    public AutoFitTextureView(Context context) {
        this(context, (AttributeSet)null);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AutoFitTextureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mRatioWidth = 0;
        this.mRatioHeight = 0;
    }

    public void setAspectRatio(int width, int height) {
        if (width >= 0 && height >= 0) {
            this.mRatioWidth = width;
            this.mRatioHeight = height;
            this.requestLayout();
        } else {
            throw new IllegalArgumentException("Size cannot be negative.");
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (0 != this.mRatioWidth && 0 != this.mRatioHeight) {
            if (width < height * this.mRatioWidth / this.mRatioHeight) {
                this.setMeasuredDimension(width, width * this.mRatioHeight / this.mRatioWidth);
            } else {
                this.setMeasuredDimension(height * this.mRatioWidth / this.mRatioHeight, height);
            }
        } else {
            this.setMeasuredDimension(width, height);
        }

    }
}
