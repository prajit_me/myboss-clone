package com.mybos.bmapp.Activity.BuildingCase.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.CaseDetailFragment;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 28/03/2018.
 *
 * @author EmLaAi
 */
public class NewBuildingCasePagerAdapter extends FragmentPagerAdapter {
    private CaseDetailFragment caseDetailFragment;
    private AttachmentFragment attachmentFragment;
    private Context context;

    public NewBuildingCasePagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        caseDetailFragment = CaseDetailFragment.newInstance();
        attachmentFragment = AttachmentFragment.newInstance();
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return caseDetailFragment;
        }else {
            return attachmentFragment;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    public CaseDetailFragment getCaseDetailFragment() {
        return caseDetailFragment;
    }

    public AttachmentFragment getAttachmentFragment() {
        return attachmentFragment;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0 ){
            return context.getString(R.string.new_case_fragment_title);
        }else{
            return context.getString(R.string.case_attachment_fragment_title);
        }
    }
}
