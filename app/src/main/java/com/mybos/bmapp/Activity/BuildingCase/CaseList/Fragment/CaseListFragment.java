package com.mybos.bmapp.Activity.BuildingCase.CaseList.Fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.flexbox.FlexboxLayout;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.Adapter.CaseList.CaseListRecyclerAdapter;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListController;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.Component.SwipeToDeleteItemTouchHelper;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Filter.FilterStoreRealm;
import com.mybos.bmapp.Activity.Filter.ListFilterStoreRealm;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.SpinnerAdapter.NullableKeepTrackSelectedSpinnerAdapter;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Data.Model.Filter.Priority;
import com.mybos.bmapp.Data.Model.Filter.Prioritylist;
import com.mybos.bmapp.Data.Model.Filter.Type;
import com.mybos.bmapp.Data.Model.Filter.TypeList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CaseListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CaseListFragment extends BaseFragment {
    private static final String SECTION_PARAMETER = "CaseListFragment.SECTION_PARAMETER";
    private BuildingList buildingList = SingletonObjectHolder.getInstance().getBuildingList();
    private String section;
    private CaseListController controller = new CaseListController();
    private int currentPage = 1;
    private String keyword;
    private boolean checkInside = false;

    private RecyclerView caseList;
    private TextView searchEditText;
    private FlexboxLayout tagContractorCloud,tagPriorityCloud,tagTypeCloud,tagStatusCloud;
    public static boolean loadCaseList = false;

    public CaseListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param section section string to send to server
     * @return A new instance of fragment CaseListFragment.
     */
    public static CaseListFragment newInstance(String section) {
        CaseListFragment fragment = new CaseListFragment();
        Bundle args = new Bundle();
        args.putString(SECTION_PARAMETER, section);
        fragment.setArguments(args);
        loadCaseList = false;
        Log.e("check","onCreateView");
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getArguments()){
            this.section = getArguments().getString(SECTION_PARAMETER);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.a_case_list_f_case_list, container, false);
        setupRecyclerView(view);
        setupSearchView(view);
        searchEditText = view.findViewById(R.id.etSearchView);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
            setupTag();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (null!=getFragmentManager()&&caseList!=null){
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            this.currentPage = 1;
        }
    }



    public void setupRecyclerView(View view){
        caseList = view.findViewById(R.id.rclCases);
        tagContractorCloud = view.findViewById(R.id.tagContractorCloud);
        tagPriorityCloud = view.findViewById(R.id.tagPriorityCloud);
        tagTypeCloud = view.findViewById(R.id.tagTypeCloud);
        tagStatusCloud = view.findViewById(R.id.tagStatusCloud);

        caseList.setLayoutManager(new LinearLayoutManager(getContext()));
        CaseListRecyclerAdapter adapter = new CaseListRecyclerAdapter(caseList);
        caseList.setAdapter(adapter);
        adapter.setOnNeedLoadMoreListener(this::getBuildingCaseList);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new SwipeToDeleteItemTouchHelper(0,ItemTouchHelper.LEFT,this::removeItem));
        itemTouchHelper.attachToRecyclerView(caseList);
    }

    public void setupSearchView(View view){
        EditText searchView = view.findViewById(R.id.etSearchView);
        searchView.setOnEditorActionListener((textView,actionId,keyEvent)->{
            if(actionId == EditorInfo.IME_ACTION_SEARCH){
                performSearch();
                textView.clearFocus();
                if (null == getActivity()){
                    return false;
                }
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (null != imm) {
                    imm.hideSoftInputFromWindow(textView.getWindowToken(), 0);
                }
                return true;
            }
            return false;
        });
    }

    public void getBuildingCaseList(){
        if (checkInside){
            return;
        }else {
            checkInside = true;
        }
        Log.e("check","getBuildingCaseList"+currentPage);
        if (!isStateSaved() && isVisible() && !isRemoving() && buildingList != null){
            SearchTableQueryBuilder queryBuilder = new SearchTableQueryBuilder();
            queryBuilder.setCurrentPage(this.currentPage);
            queryBuilder.setOrderBy("date");
            queryBuilder.setOrderType("desc");
            queryBuilder.setKeyword(this.keyword);
            queryBuilder.setSection(this.section);
            queryBuilder.setPageSize(50);
            final String check = this.keyword;
            if (buildingList.getSelectedBuilding() != null) {
                int buildingId = buildingList.getSelectedBuilding().getId();
                controller.getBuildingCaseList(getContext(), buildingId, queryBuilder, (buildingCaseResponse) -> {
                    if (null != check && !check.equals(this.keyword)) {
                        return;
                    }
                    if (null == check && null != keyword) {
                        return;
                    }
                    List<BuildingCase> showListCase = new ArrayList<>();
                    for (int i = 0; i < buildingCaseResponse.getModel().size(); i++) {
                        boolean sameCon = false;
                        boolean sameType = false;
                        boolean samePrio = false;
                        boolean sameStatus = false;
                        if (ContractorList.getContractorListApply()==null||ContractorList.getContractorListApply().getSelectedId().size()==0){
                             sameCon = true;
                        }else {
                            sameCon = true;
                            for (String nameContractor: ContractorList.getContractorListApply().getSelectedName()
                                 ) {
                                if (!buildingCaseResponse.getModel().get(i).getContractorList().contains(nameContractor)){
                                    sameCon = false;
                                }
                            }
                        }
                        if (Prioritylist.getPriorityListApply()==null||Prioritylist.getPriorityListApply().getSelectedId().size()==0){
                            samePrio = true;
                        }else {
                            samePrio = true;
                            if (Prioritylist.getPriorityListApply().getSelectedId().size()>1){
                                samePrio = false;
                            }else {
                                if (Prioritylist.getPriorityListApply().getSelectedId().get(0)!=buildingCaseResponse.getModel().get(i).getPriority()){
                                    samePrio = false;
                                }
                            }
                        }
                        if (TypeList.getTypelistApply()==null||TypeList.getTypelistApply().getSelectedId().size()==0){
                            sameType = true;
                        }else {
                            sameType = true;
                            if (TypeList.getTypelistApply().getSelectedId().size()>1){
                                sameType = false;
                            }else {
                                if (TypeList.getTypelistApply().getSelectedId().get(0)!=buildingCaseResponse.getModel().get(i).getType()){
                                    sameType = false;
                                }
                            }
                        }
                        if (TypeList.getStatuslistApply()==null||TypeList.getStatuslistApply().getSelectedId().size()==0){
                            sameStatus = true;
                        }else {
                            sameStatus = true;
                            if (TypeList.getStatuslistApply().getSelectedId().size()>1){
                                sameStatus = false;
                            }else {
                                if (TypeList.getStatuslistApply().getSelectedId().get(0)!=buildingCaseResponse.getModel().get(i).getsStatus()){
                                    sameStatus = false;
                                }
                            }
                        }

                        if (sameCon && sameType && samePrio && sameStatus){
                            showListCase.add(buildingCaseResponse.getModel().get(i));
                        }
                    }
                    if (currentPage==1){
                        ((CaseListRecyclerAdapter) this.caseList.getAdapter()).reset();
                    }
                    ((CaseListRecyclerAdapter) caseList.getAdapter()).setMaximum(showListCase.size());
                    ((CaseListRecyclerAdapter) caseList.getAdapter()).updateModel(showListCase);
                    caseList.getAdapter().notifyDataSetChanged();
                    this.currentPage += 1;
                    checkInside = false;
                }, (e) -> {
                    if (!this.isStateSaved() || !this.isRemoving() || this.isVisible()) {
                        Log.e("getInitData_EditCaseA", e.toString());
                        Intent intent = new Intent(getContext(), DashboardActivity.class);
                        getContext().startActivity(intent);
                    }
                });
            }
        }else {
            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
            crashlytics.log(SingletonObjectHolder.getInstance().printInfo());
        }
    }

    public void removeItem(RecyclerView.ViewHolder viewHolder){
        BuildingCase deleteCase = ((CaseListRecyclerAdapter) caseList.getAdapter()).getBuildingCase(viewHolder.getAdapterPosition());
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.deleteBuildingCase(getContext(),selectedBuilding.getId(),deleteCase.getId(),(Void)->{
            String message = String.format(getString(R.string.case_item_deleted_message),deleteCase.getNumber());
            Toast.makeText(getContext(),message,Toast.LENGTH_SHORT).show();
        },(error)->{
            if (Offlinemode.checkNetworkConnection(getContext())) {
                if (!this.isStateSaved() || !this.isRemoving()) {
                    Log.e("RemoveItem",error.toString());
                }
            }else {
                Offlinemode.showWhenOfflineMode(getContext());
            }
        });
        ((CaseListRecyclerAdapter) caseList.getAdapter()).removePosition(viewHolder.getAdapterPosition());
    }

    public void performSearch(){
        this.currentPage = 1;
        this.keyword = searchEditText.getText().toString();
        getBuildingCaseList();
//        ((CaseListRecyclerAdapter) this.caseList.getAdapter()).reset();
    }

    public void setupTag(){
        currentPage = 1;
        if (loadCaseList){
            getBuildingCaseList();
        }
        tagContractorCloud.removeAllViews();
        tagPriorityCloud.removeAllViews();
        tagTypeCloud.removeAllViews();
        tagStatusCloud.removeAllViews();
        View view = new View(getContext());
        float densiry = this.getResources().getDisplayMetrics().density;
        view.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagContractorCloud.addView(view);
        if (null != ContractorList.getContractorListApply()&&ContractorList.getContractorListApply().getSelectedId().size()>0) {
//            for (int id : contractorList.getSelectedId()) {
//                contractorList.selectId(id);
//            }
            tagContractorCloud.setVisibility(View.VISIBLE);
            List<BaseModel> selectedContractorModel = ContractorList.getContractorListApply().getSelectedItem();
            for (BaseModel item : selectedContractorModel) {
                if (item instanceof Contractor) {
                    addContractorTag((Contractor) item);
                }
            }
        }

        View view2 = new View(getContext());
        view2.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagPriorityCloud.addView(view2);
        if (null != Prioritylist.getPriorityListApply()&&Prioritylist.getPriorityListApply().getSelectedId().size()>0) {
            tagPriorityCloud.setVisibility(View.VISIBLE);
            List<BaseModel> selectedPriorityModel = Prioritylist.getPriorityListApply().getSelectedItem();
            for (BaseModel item : selectedPriorityModel) {
                if (item instanceof Priority) {
                    addPriorityTag((Priority) item);
                }
            }
        }

        View view3 = new View(getContext());
        view3.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagTypeCloud.addView(view3);
        if (null != TypeList.getTypelistApply()&&TypeList.getTypelistApply().getSelectedId().size()>0) {
            tagTypeCloud.setVisibility(View.VISIBLE);
            List<BaseModel> selectedTypeModel = TypeList.getTypelistApply().getSelectedItem();
            for (BaseModel item : selectedTypeModel) {
                if (item instanceof Type) {
                    addTypeTag((Type) item);
                }
            }
        }

        View view4 = new View(getContext());
        view4.setLayoutParams(new ViewGroup.LayoutParams(0, 50 * (int) densiry));

        tagStatusCloud.addView(view4);
        if (null != TypeList.getStatuslistApply()&&TypeList.getStatuslistApply().getSelectedId().size()>0) {
            tagStatusCloud.setVisibility(View.VISIBLE);
            List<BaseModel> selectedStatusModel = TypeList.getStatuslistApply().getSelectedItem();
            for (BaseModel item : selectedStatusModel) {
                if (item instanceof Type) {
                    addStatusTag((Type) item);
                }
            }
        }

    }

    public void addContractorTag(Contractor contractor){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagContractorCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(contractor.getCompany());
        textView.setTag(contractor);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            tagContractorCloud.removeView(tag);

            ContractorList.getContractorListApply().deselectId(((Contractor) object).getId());
            removeOnFilter();
            if (ContractorList.getContractorListApply().getSelectedId().size()==0){
                tagContractorCloud.setVisibility(View.GONE);
            }
            currentPage = 1;
            getBuildingCaseList();
//            ((CaseListRecyclerAdapter) this.caseList.getAdapter()).reset();
        });
        tagContractorCloud.addView(tag);
//        Toast.makeText(this, contractorList.getSelectedId().toString(), Toast.LENGTH_SHORT).show();
    }

    public void addPriorityTag(Priority priority){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagPriorityCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(priority.getName());
        textView.setTag(priority);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            tagPriorityCloud.removeView(tag);

            Prioritylist.getPriorityListApply().deselectId(((Priority) object).getId());
            removeOnFilter();
            if (Prioritylist.getPriorityListApply().getSelectedId().size()==0){
                tagPriorityCloud.setVisibility(View.GONE);
            }
            currentPage = 1;
            getBuildingCaseList();
//            ((CaseListRecyclerAdapter) this.caseList.getAdapter()).reset();

        });
        tagPriorityCloud.addView(tag);
//        Toast.makeText(this, contractorList.getSelectedId().toString(), Toast.LENGTH_SHORT).show();
    }

    public void addTypeTag(Type type){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagTypeCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(type.getName());
        textView.setTag(type);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            tagTypeCloud.removeView(tag);

            TypeList.getTypelistApply().deselectId(((Type) object).getId());
            removeOnFilter();
            if (TypeList.getTypelistApply().getSelectedId().size()==0){
                tagTypeCloud.setVisibility(View.GONE);
            }
            currentPage = 1;
            getBuildingCaseList();
        });
        tagTypeCloud.addView(tag);
//        Toast.makeText(this, contractorList.getSelectedId().toString(), Toast.LENGTH_SHORT).show();
    }

    public void addStatusTag(Type type){
        View tag = getLayoutInflater().inflate(R.layout.tag_item_border_x_close,tagStatusCloud,false);
        TextView textView = tag.findViewById(R.id.tvTitle);
        textView.setText(type.getName());
        textView.setTag(type);
        textView.setOnClickListener(v -> {
            Object object = v.getTag();
            tagStatusCloud.removeView(tag);

            TypeList.getStatuslistApply().deselectId(((Type) object).getId());
            removeOnFilter();
            if (TypeList.getStatuslistApply().getSelectedId().size()==0){
                tagStatusCloud.setVisibility(View.GONE);
            }
            currentPage = 1;
            getBuildingCaseList();
//            ((CaseListRecyclerAdapter) this.caseList.getAdapter()).reset();
        });
        tagStatusCloud.addView(tag);
//        Toast.makeText(this, contractorList.getSelectedId().toString(), Toast.LENGTH_SHORT).show();
    }

    public void removeOnFilter(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        FilterStoreRealm newFilter = new FilterStoreRealm();
        newFilter.setBuildingId(selectedBuilding.getId());
        newFilter.setSelectedFCContractor(ContractorList.getContractorListApply().getSelectedId());
        newFilter.setSelectedFCPriority(Prioritylist.getPriorityListApply().getSelectedId());
        newFilter.setSelectedFCType(TypeList.getTypelistApply().getSelectedId());
        newFilter.setSelectedFCStatus(TypeList.getStatuslistApply().getSelectedId());
        ListFilterStoreRealm temp = ListFilterStoreRealm.get();
        if (null!=temp){
            if (temp.getFilterByBuildingId(selectedBuilding.getId()) != null){
                temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCContractor(ContractorList.getContractorListApply().getSelectedId());
                temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCPriority(Prioritylist.getPriorityListApply().getSelectedId());
                temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCType(TypeList.getTypelistApply().getSelectedId());
                temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFCStatus(TypeList.getStatuslistApply().getSelectedId());
            }else {
                temp.addNewFilter(newFilter);
            }

        }else{
            temp = new ListFilterStoreRealm();
            temp.addNewFilter(newFilter);

        }
        ListFilterStoreRealm.save(temp);
    }

}
