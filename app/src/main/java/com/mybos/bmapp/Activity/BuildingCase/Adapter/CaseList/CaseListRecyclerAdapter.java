package com.mybos.bmapp.Activity.BuildingCase.Adapter.CaseList;

import android.content.Intent;
import android.graphics.Color;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mybos.bmapp.Activity.BuildingCase.EditCase.EditCaseActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Event.OnNeedLoadMoreListener;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public class CaseListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_NORMAL = 0;
    private final int VIEW_LOADING = 1;

    private boolean isLoading = false;
    private boolean isEnd = false;
    private int maximumItems = 0;
    private int visibleItemThreshold = 2;
    private OnNeedLoadMoreListener loadMoreListener;
    private List<BuildingCase> buildingCaseList = new ArrayList<>();


    public CaseListRecyclerAdapter(RecyclerView recyclerView){
        final LinearLayoutManager linearLayout = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int total = linearLayout.getItemCount();
                int lastVisibleItemPosition = linearLayout.findLastVisibleItemPosition();
                if(!isLoading && total < (lastVisibleItemPosition + visibleItemThreshold)){
                    if(null != loadMoreListener){
                        loadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return position < buildingCaseList.size() ? VIEW_NORMAL : VIEW_LOADING;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_NORMAL) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_a_case_list_n_case_list_item, parent, false);
            return new CaseListRecyclerViewHolder(view);
        }else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.r_card_like_loading_more_placeholder,parent,false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CaseListRecyclerViewHolder){
            ((CaseListRecyclerViewHolder) holder).tvTitle.setText(buildingCaseList.get(position).getSubject());

            String StCaseNumber = " #" + Integer.toString(buildingCaseList.get(position).getNumber());
            ((CaseListRecyclerViewHolder) holder).tvCaseNumber.setText(StCaseNumber);
            ((CaseListRecyclerViewHolder) holder).setListener(()->{
                if (Offlinemode.checkNetworkConnection(((CaseListRecyclerViewHolder) holder).tvTitle.getContext())) {
                    BuildingCase selectCase = buildingCaseList.get(position);
                    Intent intent = new Intent(((CaseListRecyclerViewHolder) holder).tvTitle.getContext(), EditCaseActivity.class);
                    intent.putExtra(EditCaseActivity.CASE_KEY, Integer.toString(selectCase.getId()));
                    ((CaseListRecyclerViewHolder) holder).tvTitle.getContext().startActivity(intent);
                }else {
                    Offlinemode.showWhenOfflineMode(((CaseListRecyclerViewHolder) holder).tvTitle.getContext());
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return null != buildingCaseList ?( !isEnd? buildingCaseList.size() + 1 : buildingCaseList.size()): 0;
    }

    public void setOnNeedLoadMoreListener(OnNeedLoadMoreListener listener){
        this.loadMoreListener = listener;
    }
    public void setLoaded(){
        this.isLoading = false;
    }

    public void setMaximum(int maximum){
        if (this.maximumItems != maximum){
            this.maximumItems = maximum;
        }
    }

    public void updateModel(List<BuildingCase> cases){
        setLoaded();
        this.buildingCaseList.addAll(cases);
        if (this.buildingCaseList.size() >= maximumItems){
            isEnd = true;
        }
    }

    public BuildingCase getBuildingCase(int position){
        return buildingCaseList.get(position);
    }

    public void removePosition(int position){
        buildingCaseList.remove(position);
        notifyItemRemoved(position);
    }

    public void reset(){
        buildingCaseList.clear();
        isEnd = false;
    }

    public class CaseListRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CardView cardView;
        TextView tvTitle,tvCaseNumber;
        RelativeLayout viewBackground,viewForeground;
        OnItemSelectListener listener;
        CaseListRecyclerViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.lblResidentName);
            tvCaseNumber = itemView.findViewById(R.id.lblCaseNumber);
            viewBackground = itemView.findViewById(R.id.background);
            viewForeground = itemView.findViewById(R.id.foreground);
            cardView = itemView.findViewById(R.id.cardView);
            cardView.setCardElevation(0);
            cardView.setBackgroundColor(Color.TRANSPARENT);
            itemView.findViewById(R.id.cardView).setOnClickListener(this);
        }

        public View getForeground(){
            return  viewForeground;
        }

        @Override
        public void onClick(View v) {
            if (listener != null){
                listener.selectCase();
            }
        }

        public void setListener(OnItemSelectListener listener) {
            this.listener = listener;
        }
    }

    class LoadingViewHolder extends RecyclerView.ViewHolder{
        LoadingViewHolder(View itemView) {
            super(itemView);
        }
    }

    public interface OnItemSelectListener{
        void selectCase();
    }
}
