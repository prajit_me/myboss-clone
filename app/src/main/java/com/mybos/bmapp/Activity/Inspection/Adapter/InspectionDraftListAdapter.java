package com.mybos.bmapp.Activity.Inspection.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mybos.bmapp.Activity.Inspection.InspectionList.InspectionListController;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionDraft;
import com.mybos.bmapp.Data.Model.Inspection.InspectionList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class InspectionDraftListAdapter extends RecyclerView.Adapter<InspectionDraftListAdapter.InspectionDraftItemViewHolder> {

    private ArrayList<InspectionDraft> source;
    private ArrayList<InspectionDraft> displaySource;
    private OnItemClick listener;
    InspectionListController controller = new InspectionListController();

    public InspectionDraftListAdapter (ArrayList<InspectionDraft> listDraft){
        displaySource = new ArrayList<>(listDraft);
        this.source = listDraft;
    }

    public void setSource(ArrayList<InspectionDraft> source) {
        this.source = source;
        displaySource = source;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InspectionDraftItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.r_a_inspection_list_n_inspection_item,parent,false);
        return new InspectionDraftListAdapter.InspectionDraftItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InspectionDraftItemViewHolder holder, int position) {
        InspectionDraft inspectionDraft = displaySource.get(position);
        holder.tvTitle.setText(inspectionDraft.getName());

        String StCreateDate = DateUtils.editDateTime3(inspectionDraft.getTime());
        holder.tvCreateDate.setText(StCreateDate);
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        holder.cardView.setOnClickListener(v -> {
            Inspection inspection = new Inspection();
            inspection.setHistoryIdForDraft(inspectionDraft.getId());
            inspection.setType(Integer.valueOf(inspectionDraft.getType()));
            inspection.setId(Integer.valueOf(inspectionDraft.getInspectionId()));
            listener.didSelect(inspection);
        });
    }

    public void performSearch(String keyword){
        displaySource = new ArrayList<>();
        ArrayList<InspectionDraft> source = this.source;
        for (InspectionDraft inspection:source){
            if (inspection.getName().toLowerCase().contains(keyword.toLowerCase())){
                displaySource.add(inspection);
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return displaySource.size();
    }

    public void setListener(OnItemClick listener) {
        this.listener = listener;
    }

    public class InspectionDraftItemViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle,tvCreateDate;
        CardView cardView;
        public InspectionDraftItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.lblResidentName);
            tvCreateDate = itemView.findViewById(R.id.lblCreateDate);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    public interface OnItemClick{
        void didSelect(Inspection inspection);
    }
}
