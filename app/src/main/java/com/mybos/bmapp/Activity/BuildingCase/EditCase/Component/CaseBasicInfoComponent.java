package com.mybos.bmapp.Activity.BuildingCase.EditCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 24/04/2018.
 *
 * @author EmLaAi
 */
public class CaseBasicInfoComponent extends ConstraintLayout {
    TextView txtCreatedOn,txtDueDate,txtPriority,txtCaseType;
    public CaseBasicInfoComponent(Context context) {
        super(context);
        additionalInit();
    }

    public CaseBasicInfoComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public CaseBasicInfoComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(),R.layout.a_edit_case_f_edit_case_detail_c_basic_info,this);
        txtCreatedOn = findViewById(R.id.txtCreateOn);
        txtDueDate = findViewById(R.id.txtDueDate);
        txtPriority = findViewById(R.id.txtPriority);
        txtCaseType = findViewById(R.id.txtCaseType);
    }

    public void setCreatedOn(String value){
        txtCreatedOn.setText(value);
    }
    public void setDueDate(String value){
        txtDueDate.setText(value);
    }
    public void setCaseType(String value){
        txtCaseType.setText(value);
    }
    public void setPriority(String value){
        txtPriority.setText(value);
    }
}
