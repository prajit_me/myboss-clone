package com.mybos.bmapp.Activity.Broadcast.Dialog;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.R;

public class BroadcastSendCompleted extends DialogFragment {

    public BroadcastSendCompleted(){}

    public BroadcastSendCompleted getInstance() {
        return new BroadcastSendCompleted();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.broadcast_send_completed, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.findViewById(R.id.btnOKbr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
//                Intent intent = new Intent(, BroadcastActivity.class);
//                startActivity(intent);
//                finish();
            }
        });
    }
}
