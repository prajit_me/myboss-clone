package com.mybos.bmapp.Activity.BuildingCase.CaseList;

import android.content.Intent;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.*;
import com.mybos.bmapp.Activity.BuildingCase.Adapter.CaseListPagerAdapter;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.CaseDetailFragmentController;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseActivity;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseController;
import com.mybos.bmapp.Activity.Filter.FilterCaseActivity;
import com.mybos.bmapp.Activity.Filter.FilterFragment;
import com.mybos.bmapp.Activity.Filter.FilterStoreRealm;
import com.mybos.bmapp.Activity.Filter.ListFilterStoreRealm;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Data.Model.Filter.Priority;
import com.mybos.bmapp.Data.Model.Filter.Prioritylist;
import com.mybos.bmapp.Data.Model.Filter.TypeList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

public class CaseListActivity extends a_base_toolbar {

    NewBuildingCaseController newBuildingCaseController = new NewBuildingCaseController();
    CaseDetailFragmentController caseDetailFragmentController = new CaseDetailFragmentController();
    ProgressBar prgCaseLoading;
    boolean getType = false, getContractor = false, getStatus = false;
    private ContractorList contractorList= new ContractorList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_case_list);
        //IMPORTANT: setup drawer with drop down building list
        FloatingActionButton fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener((v) -> {
            if (Offlinemode.checkNetworkConnection(CaseListActivity.this)) {
                //clear case Attachment
                BuildingCase.setSelectedBuildingCase(null);
                Intent intent = new Intent(this, NewBuildingCaseActivity.class);
                startActivity(intent);
            } else {
                Offlinemode.showWhenOfflineMode(CaseListActivity.this);
            }
        });
        prgCaseLoading = findViewById(R.id.prgCaseLoading);
        this.setupBtnOption();
        super.updateToolbarTitle(getString(R.string.case_list_title));
    }

    @Override
    protected void onStart() {
        super.onStart();
//        setupView();
    }

    public void setupView() {
        ViewPager viewPager = findViewById(R.id.emailContractorPager);
        viewPager.setAdapter(new CaseListPagerAdapter(this, getSupportFragmentManager()));

        TabLayout topTabBar = findViewById(R.id.topTabBar);
        topTabBar.setupWithViewPager(viewPager);
    }

    public void setupBtnOption() {
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

        //get type
        caseDetailFragmentController.getBuildingCaseType(this, selectedBuilding.getId(), list -> {
            FilterCaseActivity.setTypeList(new TypeList(list.getTypeList()));
            getType = true;
            setupData();
        }, error -> {
            FilterCaseActivity.setTypeList(new TypeList());
            getType = true;
            setupData();
            Log.e("get_typeList_fail", "");
        });

        //get contractor
        getContractorList(selectedBuilding.getId(),1);

        //get status and priority
        caseDetailFragmentController.getBuildingCaseStatusForFilter(this, selectedBuilding.getId(), statusList -> {
            FilterCaseActivity.setStatusList(statusList);
            FilterCaseActivity.setPrioritylist(new Prioritylist());
            getStatus = true;
            setupData();
            },
                error -> {
                    FilterCaseActivity.setStatusList(new TypeList());
                    getStatus = true;
                    setupData();
                });
    }

    private void getContractorList(int buildingId,int page){
        newBuildingCaseController.getContractorList(this, buildingId, "all",page, (contractorList) -> {
            if (contractorList.getContractorList().size()==0){
                FilterCaseActivity.setContractorList(this.contractorList);
                getContractor = true;
                setupData();
            }else {
                for (Contractor c: contractorList.getContractorList()
                ) {
                    this.contractorList.addContrator(c);
                }
                getContractorList(buildingId,page+1);
            }
        }, error -> {
            FilterCaseActivity.setContractorList(new ContractorList());
            getContractor = true;
            setupData();
        });
    }

    public void setupData() {
        if (getType&&getContractor&&getStatus){
            Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
            ListFilterStoreRealm listFilterRealm = ListFilterStoreRealm.get();
            //get realm
            if (null != listFilterRealm && null != listFilterRealm.getFilterByBuildingId(selectedBuilding.getId())) {
                FilterStoreRealm filterRealm = listFilterRealm.getFilterByBuildingId(selectedBuilding.getId());
                ContractorList.setContractorListApply(new ContractorList(FilterCaseActivity.getContractorList()));
                ContractorList.getContractorListApply().setSelectedId(filterRealm.getSelectedFCContractor());

                Prioritylist.setPriorityListApply(new Prioritylist(FilterCaseActivity.getPrioritylist()));
                Prioritylist.getPriorityListApply().setSelectedId(filterRealm.getSelectedFCPriority());

                TypeList.setTypelistApply(new TypeList(FilterCaseActivity.getTypeList()));
                TypeList.getTypelistApply().setSelectedId(filterRealm.getSelectedFCType());

                TypeList.setStatuslistApply(new TypeList(FilterCaseActivity.getStatusList()));
                TypeList.getStatuslistApply().setSelectedId(filterRealm.getSelectedFCStatus());
            }else {
                ContractorList.setContractorListApply(new ContractorList(FilterCaseActivity.getContractorList()));

                Prioritylist.setPriorityListApply(new Prioritylist(FilterCaseActivity.getPrioritylist()));

                TypeList.setTypelistApply(new TypeList(FilterCaseActivity.getTypeList()));

                TypeList.setStatuslistApply(new TypeList(FilterCaseActivity.getStatusList()));
            }
            setupView();
            prgCaseLoading.setVisibility(View.GONE);
            findViewById(R.id.btnOption).setVisibility(View.VISIBLE);
            findViewById(R.id.btnOption).setBackground(getDrawable(R.drawable.ic_filter));
            findViewById(R.id.btnOption).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(CaseListActivity.this, FilterCaseActivity.class);
                    startActivity(intent);
                }
            });
        }

    }

}
