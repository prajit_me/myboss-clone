package com.mybos.bmapp.Activity.Keys.Activities.SignOut;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Keys.Activities.History.KeyHistoryActivity;
import com.mybos.bmapp.Activity.Keys.Activities.KeyContractorListActivity;
import com.mybos.bmapp.Activity.Keys.Adapter.KeySpinnerSignInAdapter;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Key.ContractorKey;
import com.mybos.bmapp.Data.Model.Key.KeyInOut;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;

public class KeySignOutActivity extends a_base_toolbar {

    Spinner spCompanySignOut, spTimeSignOut, spDateSignOut;
    EditText edtNameSignOut, edtMobile, edtReason;
    TextView tvSubTitleSignOut;
    Button btnSignOut;
    ImageView ivSignature;
    private int spCompanyTouch = 0;
    private int spDateTouch = 0;
    private int spTimeTouch = 0;
    private String dateExpiry = "";
    private String timeDue = "";
    private String imageSign = "";

    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    @Override
    protected void onStart() {
        super.onStart();
        spCompanyTouch = 0;
            spCompanySignOut.setAdapter(new KeySpinnerSignInAdapter(KeySignOutActivity.this,
                    "Company", ContractorKey.getSelectedContractor()));
            ContractorKey conSelected = ContractorKey.getObjectFromNameCopany(ContractorKey.getSelectedContractor());
            if(!ContractorKey.getSelectedContractor().equals("")){
                edtNameSignOut.setText(conSelected.getMc_name());
                edtMobile.setText(conSelected.getMc_number());
            }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ContractorKey.setSelectedContractor("");
        setContentView(R.layout.activity_key_sign_out);
        updateToolbarTitle("Sign out key");

        spCompanySignOut = findViewById(R.id.spCompanySignOut);
        spTimeSignOut = findViewById(R.id.spTimeSignOut);
        spDateSignOut = findViewById(R.id.spDateSignOut);
        ivSignature = findViewById(R.id.ivSignature);
        btnSignOut = findViewById(R.id.btnSignOut);
        tvSubTitleSignOut = findViewById(R.id.tvSubTitleSignOut);
        edtReason = findViewById(R.id.edtReason);
        edtNameSignOut = findViewById(R.id.edtNameSignOut);
        edtMobile = findViewById(R.id.edtMobile);

        tvSubTitleSignOut.setText(getIntent().getStringExtra("sub_title"));


        spTimeSignOut.setAdapter(new KeySpinnerSignInAdapter(KeySignOutActivity.this,
                getString(R.string.key_sign_in_time), "00:00"));
        spDateSignOut.setAdapter(new KeySpinnerSignInAdapter(KeySignOutActivity.this,
                getString(R.string.key_sign_in_date), "dd/mm/yyyy"));

        spCompanySignOut.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (spCompanyTouch < 1) {
                    Intent intent = new Intent(KeySignOutActivity.this, KeyContractorListActivity.class);
                    startActivity(intent);
                    spCompanyTouch = 1;
                    return true;
                }
                return true;
            }
        });

        spDateSignOut.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (spDateTouch < 1) {
                    Calendar c = Calendar.getInstance();
                    DatePickerDialog dialog = new DatePickerDialog(KeySignOutActivity.this, R.style.DatePickerTheme, (view1, year, month, dayOfMonth) -> {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, dayOfMonth);
                        Date updateDate = calendar.getTime();
                        spDateSignOut.setAdapter(new KeySpinnerSignInAdapter(KeySignOutActivity.this,
                                getString(R.string.key_sign_in_date), DateUtils.getDateString(updateDate)));
                        dateExpiry = DateUtils.getDateString(updateDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            spDateTouch = 0;
                        }
                    });
                    spDateTouch = 1;
                    return true;
                }
                return true;
            }
        });

        spTimeSignOut.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (spTimeTouch < 1) {
                    Calendar c = Calendar.getInstance();
                    TimePickerDialog dialog = new TimePickerDialog(KeySignOutActivity.this, R.style.DatePickerTheme, new TimePickerDialog.OnTimeSetListener() {
                        @Override
                        public void onTimeSet(TimePicker timePicker, int i, int i1) {
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(Calendar.HOUR_OF_DAY, i);
                            calendar.set(Calendar.MINUTE, i1);
                            Date updateDate = calendar.getTime();
                            spTimeSignOut.setAdapter(new KeySpinnerSignInAdapter(KeySignOutActivity.this,
                                    getString(R.string.key_sign_in_time), DateUtils.getDateString(updateDate, "hh:mma")));
                            timeDue = DateUtils.getDateString(updateDate, "hh:mma");
                        }
                    }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false);
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            spTimeTouch = 0;
                        }
                    });
                    spTimeTouch = 1;
                    return true;
                }
                return true;
            }
        });

        btnSignOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtNameSignOut.getText().toString().replace(" ", "").isEmpty() ||
                        edtNameSignOut.getText().toString().isEmpty()) {
                    Toast.makeText(KeySignOutActivity.this, "Name Can't be Empty", Toast.LENGTH_LONG).show();
                } else {
                    controller.postSignOut(KeySignOutActivity.this, selectedBuilding.getId(),
                            KeyInOut.getKeyIdSelected(), buildParam(),
                            state -> {

                                Log.e("postSignOut", state);
                                Log.e("postSignOut_key", KeyInOut.getKeyIdSelected());
                                finish();
                            },
                            error -> {
                                if (Offlinemode.checkNetworkConnection(KeySignOutActivity.this)){
                                    Log.e("btnSignOut",error.toString());
                                }else {
                                    Offlinemode.showWhenOfflineMode(KeySignOutActivity.this);
                                }
                            }
                    );
                }
            }
        });

        ivSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Function call for Digital Signature
                // Dialog Function
                dialog = new Dialog(KeySignOutActivity.this);
                // Removing the features of Normal Dialogs
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.key_signature);
                dialog.setCancelable(true);
                dialog_action();
            }
        });
    }

    private String buildParam() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\"print\":false,");
        if (!ContractorKey.getSelectedContractor().isEmpty()) {
            stringBuffer.append("\"company\":\"" + ContractorKey.getSelectedContractor() + "\",");
        }
        stringBuffer.append("\"name\":\"" + edtNameSignOut.getText().toString().replace("\n", "") + "\",");
        if (!edtMobile.getText().toString().isEmpty()) {
            stringBuffer.append("\"mobile\":\"" + edtMobile.getText().toString().replace("\n", "") + "\",");
        }
        if (!edtReason.getText().toString().isEmpty()) {
            stringBuffer.append("\"reason\":\"" + edtReason.getText().toString().replace("\n", "") + "\",");
        }
        if (!timeDue.isEmpty()) {
            stringBuffer.append("\"due\":\"" + timeDue + "\",");
        }
        if (!dateExpiry.isEmpty()) {
            stringBuffer.append("\"expiry\":\"" + dateExpiry + "\",");
        }
        // Create empty signature
        if (imageSign.isEmpty()) {
            Bitmap bitmap = BitmapFactory.decodeResource(KeySignOutActivity.this.getResources(),R.drawable.signature_empty);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            imageSign = Base64.encodeToString(byteArray, Base64.NO_WRAP);
        }
        stringBuffer.append("\"sign\":\"data:image/png;base64," + imageSign + "\"}");
        Log.e("param", stringBuffer.toString());
        return stringBuffer.toString();
    }

    public void dialog_action() {
        mContent = dialog.findViewById(R.id.linearLayout);
        mSignature = new Signature(getApplicationContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        // Dynamically generating Layout through java code
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = dialog.findViewById(R.id.clear);
        mGetSign = dialog.findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = dialog.findViewById(R.id.cancel);
        view = mContent;

        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("tag", "Panel Cleared");
                mSignature.clear();
            }
        });
        mGetSign.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                Log.v("tag", "Panel Saved");
                view.setDrawingCacheEnabled(true);
                mSignature.save(view);
                dialog.dismiss();
            }
        });
        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    Button mClear, mGetSign, mCancel;
    Dialog dialog;
    LinearLayout mContent;
    View view;
    Signature mSignature;
    Bitmap bitmap;
    ;


    ///Signature class
    private class Signature extends View {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public Signature(Context context, AttributeSet attrs) {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        @SuppressLint("WrongThread")
        public void save(View v) {
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
            }
            // convert to base64
            Canvas canvas = new Canvas(bitmap);
            v.draw(canvas);
            try {
                ivSignature.setImageBitmap(bitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                imageSign = Base64.encodeToString(byteArray, Base64.NO_WRAP);
            } catch (Exception e) {
                Log.v("log_tag", e.toString());
            }
        }

        public void clear() {
            path.reset();
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:
                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++) {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;
                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string) {
            Log.v("log_tag", string);
        }

        private void expandDirtyRect(float historicalX, float historicalY) {
            if (historicalX < dirtyRect.left) {
                dirtyRect.left = historicalX;
            } else if (historicalX > dirtyRect.right) {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top) {
                dirtyRect.top = historicalY;
            } else if (historicalY > dirtyRect.bottom) {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY) {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }

}
