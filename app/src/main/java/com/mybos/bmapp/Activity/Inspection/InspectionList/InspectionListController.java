package com.mybos.bmapp.Activity.Inspection.InspectionList;

import android.content.Context;

import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Mapper.Inspection.InspectionMapper;
import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionDraft;
import com.mybos.bmapp.Data.Model.Inspection.InspectionList;

import java.util.ArrayList;

/**
 * Created by EmLaAi on 17/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionListController {
    public void getInspectionList(Context context, int buildingId, int inspectionType, SearchTableQueryBuilder builder, SuccessCallback<InspectionList> onSuccess, FailureCallback onFailure){
        InspectionMapper mapper = new InspectionMapper();
        mapper.getList(context,buildingId,inspectionType,builder,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getInspectionDraft(Context context, int buildingId, SuccessCallback<ArrayList<InspectionDraft>> onSuccess, FailureCallback onFailure){
        InspectionMapper mapper = new InspectionMapper();
        mapper.getListDraft(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
