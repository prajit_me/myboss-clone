package com.mybos.bmapp.Activity.Calendar.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarMaintenanceListAdapter;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;

import java.util.ArrayList;

public class CalendarMaintenancesActivity extends a_base_toolbar {

    TextView tvDateMaintenance;
    Spinner spListMaintenance;
    ListView lvMaintenance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_maintenances);

        updateToolbarTitle("Maintenance");
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarMaintenancesActivity.this, CalendarDayEventActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tvDateMaintenance = findViewById(R.id.tvDateMaintenance);
        spListMaintenance = findViewById(R.id.spListMaintenance);
        lvMaintenance = findViewById(R.id.lvMaintenance);

        //create array and set for Spinner
        ArrayList<String> itemsSpinner = new ArrayList<>();
        itemsSpinner.add("All maintenance");
        itemsSpinner.add("Pending maintenance");
        itemsSpinner.add("Complete maintenance");
        itemsSpinner.add("Failed maintenance");
        ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_spinner_dropdown_item,itemsSpinner);
        spListMaintenance.setAdapter(arrayAdapter);

        //set adapter for listview
        lvMaintenance.setAdapter(new CalendarMaintenanceListAdapter(this,"all"));
        setListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //set title date
        String dateTitle = SelectedDay.getDateOfweek()+", "+SelectedDay.getFullDate();
        tvDateMaintenance.setText(dateTitle);

    }

    public void setListener(){
        spListMaintenance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spListMaintenance.getSelectedItem().toString().equals("All maintenance")){
                    lvMaintenance.setAdapter(new CalendarMaintenanceListAdapter(CalendarMaintenancesActivity.this,"all"));
                }else if (spListMaintenance.getSelectedItem().toString().equals("Pending maintenance")){
                    lvMaintenance.setAdapter(new CalendarMaintenanceListAdapter(CalendarMaintenancesActivity.this,""));
                }else if (spListMaintenance.getSelectedItem().toString().equals("Complete maintenance")){
                    lvMaintenance.setAdapter(new CalendarMaintenanceListAdapter(CalendarMaintenancesActivity.this,"Completed"));
                }else if (spListMaintenance.getSelectedItem().toString().equals("Failed maintenance")){
                    lvMaintenance.setAdapter(new CalendarMaintenanceListAdapter(CalendarMaintenancesActivity.this,"Failed"));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
}
