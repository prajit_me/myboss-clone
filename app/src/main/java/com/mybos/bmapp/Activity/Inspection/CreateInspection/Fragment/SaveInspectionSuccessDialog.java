package com.mybos.bmapp.Activity.Inspection.CreateInspection.Fragment;


import android.content.res.Resources;
import android.os.Bundle;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SaveInspectionSuccessDialog extends DialogFragment {


    public SaveInspectionSuccessDialog() {
        // Required empty public constructor
    }

    public static SaveInspectionSuccessDialog createInstance(){return new SaveInspectionSuccessDialog();}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(true);
        View view = inflater.inflate(R.layout.a_make_inspection_d_save_inspection_success, container, false);
        view.findViewById(R.id.btnOk).setOnClickListener(v->this.dismiss());
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        int width = (int) ( Resources.getSystem().getDisplayMetrics().widthPixels * 0.9);
        if (null != getDialog().getWindow()) {
            getDialog().getWindow().setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
        }

    }
}
