package com.mybos.bmapp.Activity.Base;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.widget.TextView;

import com.mybos.bmapp.R;

public class a_base_toolbar extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        // overide default set contain to inject child contain into base layout
        super.setContentView(R.layout.a_base_toolbar);
        getLayoutInflater().inflate(layoutResID,findViewById(R.id.content));
//        // setup drawer
        Toolbar toolbar = findViewById(R.id.mainToolbar);
        findViewById(R.id.btnBack).setOnClickListener(v -> this.finish());

        setSupportActionBar(toolbar);


        if (null != getSupportActionBar()) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    protected void updateToolbarTitle(String title){
        ((TextView) findViewById(R.id.txtToolbarTitle)).setText(title);
    }
}
