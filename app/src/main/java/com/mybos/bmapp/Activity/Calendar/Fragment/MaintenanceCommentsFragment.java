package com.mybos.bmapp.Activity.Calendar.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarCommentsAdapter;
import com.mybos.bmapp.Activity.Calendar.CalendarController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class MaintenanceCommentsFragment extends BaseFragment {

    private View view;
    ListView lvComments;
    EditText edtComment;
    Button btnSendComment;
    CalendarController controller = new CalendarController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    public MaintenanceCommentsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_comments, container, false);
        this.view = view;

        btnSendComment = view.findViewById(R.id.btnSendComment);
        edtComment = view.findViewById(R.id.edtComment);
        lvComments = view.findViewById(R.id.lvComments);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        lvComments.setAdapter(new CalendarCommentsAdapter(getContext()));
        setListenerForAdapter();
        setListener();
        btnSendComment.setVisibility(View.INVISIBLE);
        btnSendComment.setEnabled(false);
    }

    public void setListener() {
        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayLoadingScreen();
                String id = MaintenancePerDay.getMaintenancePerDaySelected().getId();
                String dateOgrin = MaintenancePerDay.getMaintenancePerDaySelected().getDate_ogrinal();
                String param = "{\"comment\":\"" + edtComment.getText().toString() + "\"}";
                controller.addCommentMaintenance(getContext(), selectedBuilding.getId(), id,dateOgrin,param,
                        addComment -> {
                            dismissLoadingScreen();
                            lvComments.setAdapter(new CalendarCommentsAdapter(getContext()));
                            setListenerForAdapter();
                        },
                        error -> {
                            dismissLoadingScreen();
                            if (Offlinemode.checkNetworkConnection(getContext())){
                                Log.e("btnSendComment",error.toString());
                            }else{
                                Offlinemode.showWhenOfflineMode(getContext());
                            }
                        });
            }
        });

        edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtComment.getText().toString().isEmpty()) {
                    btnSendComment.setVisibility(View.INVISIBLE);
                    btnSendComment.setEnabled(false);
                } else {
                    btnSendComment.setVisibility(View.VISIBLE);
                    btnSendComment.setEnabled(true);
                }
            }
        });

    }

    private void setListenerForAdapter(){
        CalendarCommentsAdapter calendarCommentsAdapter = (CalendarCommentsAdapter) lvComments.getAdapter();
        calendarCommentsAdapter.setListenerdelete(a ->{
            lvComments.setAdapter(new CalendarCommentsAdapter(getContext()));
            setListenerForAdapter();
        } );
    }
}
