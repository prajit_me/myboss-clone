package com.mybos.bmapp.Activity.Assets.Activity;

import android.content.Context;

import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Mapper.Assets.AssetMapper;
import com.mybos.bmapp.Data.Mapper.Attachment.AttachmentMapper;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentFile;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadInvoice;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;

import java.io.File;

public class AssetController {

    public void createNewAsset(Context context, int buildingId, VolleyMultipartBuilder builder,
                               AssetController.SuccessCallback<String> onSuccess, AssetController.FailureCallback onFailure){
        AssetMapper mapper = new AssetMapper();
        mapper.createAsset(context,buildingId,builder,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void editAsset(Context context, int buildingId, String assetId, VolleyMultipartBuilder builder,
                               AssetController.SuccessCallback<String> onSuccess, AssetController.FailureCallback onFailure){
        AssetMapper mapper = new AssetMapper();
        mapper.editAsset(context,buildingId,assetId,builder,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getSpecificBuildingAsset(Context context, BuildingAsset asset,
                                                AssetController.SuccessCallback<BuildingAsset> onSuccess, AssetController.FailureCallback onFailure){
        AssetMapper mapper = new AssetMapper();
        mapper.getSpecificBuildingAsset(context,asset,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
