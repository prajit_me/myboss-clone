package com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Fragment;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.R;

public class EmailDetailFragment extends BaseFragment {
    private static final String CASE_NUMBER_KEY = "EmailDetailFragment.CASE_NUMBER_KEY";
    private Contractor contractor;
    private String caseNumber;
    private OnRemoveClick removeListener;
    public EmailDetailFragment() {
        // Required empty public constructor
    }

    public static EmailDetailFragment createInstance(String caseNumber){
        EmailDetailFragment fragment = new EmailDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putString(CASE_NUMBER_KEY,caseNumber);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.a_email_contractor_f_email_detail,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (null != getArguments()) {
            caseNumber = getArguments().getString(CASE_NUMBER_KEY);
        }
        if (null != contractor){
            applyData();
        }
    }

    public void setContractor(Contractor contractor) {
        this.contractor = contractor;
    }

    public void applyData(){
        if (null != getView() && null != contractor) {
            ((TextView) getView().findViewById(R.id.tvCaseNumber)).setText(caseNumber);
            ((TextView) getView().findViewById(R.id.tvCompany)).setText(contractor.getCompany());
            ((TextView) getView().findViewById(R.id.tvEmail)).setText(contractor.getEmail());
            EditText etSubject = getView().findViewById(R.id.etSubject);
            etSubject.setText(contractor.getSubject());
            etSubject.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    contractor.setSubject(s.toString());
                }
            });

            EditText etDescription = getView().findViewById(R.id.etDescription);
            etDescription.setText(contractor.getMessage());
            etDescription.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    contractor.setMessage(s.toString());
                }
            });

            getView().findViewById(R.id.imgClose).setOnClickListener(v -> {
                if (null != removeListener && null != contractor){
                    removeListener.removeContractor(contractor);
                }
            });
        }
    }

    public void setRemoveListener(OnRemoveClick removeListener) {
        this.removeListener = removeListener;
    }

    public interface OnRemoveClick{
        void removeContractor(Contractor contractor);
    }


}
