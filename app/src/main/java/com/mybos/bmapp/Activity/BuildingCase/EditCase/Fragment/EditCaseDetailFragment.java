package com.mybos.bmapp.Activity.BuildingCase.EditCase.Fragment;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListActivity;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Component.CaseBasicInfoComponent;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Component.CaseDescriptionComponent;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Component.CaseJobAreaComponent;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Component.CaseNotesComponent;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Component.CaseNumberComponent;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

/**
 * A simple {@link Fragment} subclass.
 */
public class EditCaseDetailFragment extends BaseFragment {
    private static BuildingCase buildingCase;
    private static ContractorList contractorList;
    private EditCaseDetailController controller = new EditCaseDetailController();

    private static CaseNumberComponent caseNumberComponent;
    private static CaseBasicInfoComponent basicInfoComponent;
    private static CaseJobAreaComponent jobAreaComponent;
    private static CaseDescriptionComponent descriptionComponent;
    private static CaseNotesComponent notesComponent;
    private static BuildingCaseStatusList statusList;
    private final String StError = "Error";
    private boolean pause = false;

    public EditCaseDetailFragment() {
        // Required empty public constructor
    }

    public static EditCaseDetailFragment createInstance(){
        EditCaseDetailFragment fragment = new EditCaseDetailFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.a_edit_case_f_edit_case_detail, container, false);
        caseNumberComponent = view.findViewById(R.id.viewCaseNumber);
        basicInfoComponent = view.findViewById(R.id.viewBasicInfo);
        jobAreaComponent = view.findViewById(R.id.viewJobArea);
        descriptionComponent = view.findViewById(R.id.viewDescription);
        notesComponent = view.findViewById(R.id.viewNotes);
        return view;
    }

    public void setupData(BuildingCase buildingCase, ContractorList contractorList){
        this.buildingCase = buildingCase;
        this.contractorList = contractorList;
    }

    @Override
    public void onPause() {
        super.onPause();
        pause = true;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (pause){
            initData();
        }
    }

    public void initData(){
        if ( this.statusList!=null){
            updateUI();
            return;
        }
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        displayLoadingScreen();
        controller.getBuildingCaseStatus(getContext(),selectedBuilding.getId(),statusList->{
            dismissLoadingScreen();
            this.statusList = statusList;
            updateUI();
        },error -> {
            dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(getContext())) {
                Log.e("EditCaseDF_initData", error.toString());
            } else {
                Intent intent = new Intent(getContext(), CaseListActivity.class);
                getContext().startActivity(intent);
            }
        });
    }

    public void updateUI(){
        caseNumberComponent.setCaseNumber(Integer.toString(buildingCase.getNumber()));
        caseNumberComponent.setupStatusDropDown(statusList,buildingCase.getsStatus(),status -> {
            updateStatus(status.getId());
        });

        basicInfoComponent.setCaseType(buildingCase.getTypeText());
        basicInfoComponent.setPriority(buildingCase.getPriorityText());
        basicInfoComponent.setCreatedOn(DateUtils.getDateString(buildingCase.getStartDate()));
        basicInfoComponent.setDueDate(DateUtils.getDateString(buildingCase.getDueDate()));

        jobAreaComponent.setJobArea(buildingCase.getAreaText(),buildingCase.getArea());
        jobAreaComponent.setApartment(buildingCase.getAllApartmentString());
        jobAreaComponent.setAsset(buildingCase.getAllAssetString());
        jobAreaComponent.setContractor(buildingCase.getAllContractorString());

        descriptionComponent.setSubject(buildingCase.getSubject());
        descriptionComponent.setDescription(buildingCase.getDetail());

        notesComponent.setNotes(buildingCase.getHistoryNote().equals("null")?"":buildingCase.getHistoryNote());
    }

    public void updateStatus( int id){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        this.caseNumberComponent.displayLoading();
        this.controller.updateBuildingCaseStatus(getContext(),selectedBuilding.getId(),buildingCase.getId(),id,Void->{
            this.buildingCase.setsStatus(id);
            this.caseNumberComponent.hideLoading();
        },error -> {
            this.caseNumberComponent.hideLoading();
            if (isVisible() || !isStateSaved()){
                if (Offlinemode.checkNetworkConnection(getContext())){
                    Log.e("EditCaseDF_updateStatus",error.toString());
                }else {
                    Offlinemode.showWhenOfflineMode(getContext());
                }
            }

        });
    }
}
