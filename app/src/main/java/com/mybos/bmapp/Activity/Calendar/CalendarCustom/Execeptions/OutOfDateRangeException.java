package com.mybos.bmapp.Activity.Calendar.CalendarCustom.Execeptions;

public class OutOfDateRangeException extends Exception {
    public OutOfDateRangeException(String message) {
        super(message);
    }
}
