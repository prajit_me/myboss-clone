package com.mybos.bmapp.Activity.Keys.Fragment.ContractorKey;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Keys.Activities.History.KeyHistoryActivity;
import com.mybos.bmapp.Activity.Keys.Activities.SignIn.KeySignInActivity;
import com.mybos.bmapp.Activity.Keys.Activities.SignOut.KeySignOutActivity;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyContractorListRecyclerAdapter;
import com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.WithMangement.WithManagementKeyListFragment;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class ContractorListFragment extends BaseFragment {

    private EditText edtSearchContractorKey;
    private RecyclerView rclvListContractorKey;
    private View view;
    private ProgressBar progressBarContractor;

    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    public ContractorListFragment() {
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contractor_list, container, false);
        progressBarContractor = view.findViewById(R.id.progressBarContractor);
        this.view = view;
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        setupRecycleView(view);
        setupSearchView(view);

    }

    private void setupRecycleView(View view) {

        rclvListContractorKey = view.findViewById(R.id.rclKeyContractorList);
        rclvListContractorKey.setLayoutManager(new LinearLayoutManager(getContext()));
        controller.getContractorKeys(getContext(), selectedBuilding.getId(),
                apartment -> {
                    KeyContractorListRecyclerAdapter adapter = new KeyContractorListRecyclerAdapter();
                    adapter.setListener(a -> {
                        Intent intent = new Intent(getContext(), KeyHistoryActivity.class);
                        intent.putExtra(WithManagementKeyListFragment.SUB_TITLE,a.getKeyName());
                        startActivity(intent);
                    });
                    adapter.setListenerSign(b ->{
                        if(b.getKeyStatus().equals("1")){
                            Intent intent = new Intent(getContext(), KeySignOutActivity.class);
                            intent.putExtra(WithManagementKeyListFragment.SUB_TITLE,b.getKeyName());
                            startActivity(intent);
                        }else {
                            Intent intent = new Intent(getContext(), KeySignInActivity.class);
                            intent.putExtra(WithManagementKeyListFragment.SUB_TITLE,b.getKeyName());
                            startActivity(intent);
                        }

                    });
                    rclvListContractorKey.setAdapter(adapter);
                    progressBarContractor.setVisibility(View.INVISIBLE);
                    rclvListContractorKey.setVisibility(View.VISIBLE);
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(getContext())){
                        Log.e("getContractorKeys",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(getContext());
                    }
                });
    }

    public void setupSearchView(View view) {
        edtSearchContractorKey = view.findViewById(R.id.etSearchKeyContractor);
        edtSearchContractorKey.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                v.clearFocus();
                String keyWord = edtSearchContractorKey.getText().toString();
                KeyContractorListRecyclerAdapter adapter = (KeyContractorListRecyclerAdapter) rclvListContractorKey.getAdapter();
                adapter.performSearch(keyWord);
                dismissKeyboard();
                return true;
            }
            return false;
        });
    }

}
