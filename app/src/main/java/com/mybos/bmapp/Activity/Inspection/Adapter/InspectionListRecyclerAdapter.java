package com.mybos.bmapp.Activity.Inspection.Adapter;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionList;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 17/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionListRecyclerAdapter extends RecyclerView.Adapter<InspectionListRecyclerAdapter.InspectionItemViewHolder> {
    private InspectionList source;
    private List<Inspection> displaySource;
    private OnItemClick listener;

    public InspectionListRecyclerAdapter(InspectionList inspectionList){
        displaySource = new ArrayList<>(inspectionList.getAll());
        this.source = inspectionList;
    }

    public void setSource(InspectionList source) {
        this.source = source;
        displaySource = new ArrayList<>(source.getAll());
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InspectionItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.r_a_inspection_list_n_inspection_item,parent,false);
        return new InspectionItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InspectionItemViewHolder holder, int position) {
        Inspection inspection = displaySource.get(position);
        holder.tvTitle.setText(inspection.getName());

        String StCreateDate = " " + inspection.getAddedDateString();
        holder.tvCreateDate.setText(StCreateDate);
        holder.cardView.setOnClickListener(v -> {
            listener.didSelect(inspection);
        });
    }

    @Override
    public int getItemCount() {
        return displaySource.size();
    }

    public void performSearch(String keyword){
        displaySource = new ArrayList<>();
        List<Inspection> source = this.source.getAll();
        for (Inspection inspection:source){
            if (inspection.getName().toLowerCase().contains(keyword.toLowerCase())){
                displaySource.add(inspection);
            }
        }
        notifyDataSetChanged();
    }

    public void setListener(OnItemClick listener) {
        this.listener = listener;
    }

    public class InspectionItemViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle,tvCreateDate;
        CardView cardView;
        public InspectionItemViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.lblResidentName);
            tvCreateDate = itemView.findViewById(R.id.lblCreateDate);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }

    public interface OnItemClick{
        void didSelect(Inspection inspection);
    }
}
