package com.mybos.bmapp.Activity.Calendar.Activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarReminderSpinnerAdapter;
import com.mybos.bmapp.Activity.Calendar.Adapter.ReminderListAdapter;
import com.mybos.bmapp.Activity.Calendar.CalendarController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Calendar.DaysEvent;
import com.mybos.bmapp.Data.Model.Calendar.Reminder;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class CalendarReminderInfoActivity extends a_base_toolbar {

    TextView tvReminderName,tvReminderDate,tvReminderTime,tvReminderComment;
    EditText edtComment;
    Button btnSendComment;
    Spinner SpinnerSelectDateReminder;
    Reminder reminder;

    CalendarController controller = new CalendarController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    private static String changeToDate = "";
    private int spDateTouch = 0;
    private boolean changedDate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_reminder_info);
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = null;
                if (changedDate){
                    intent = new Intent(CalendarReminderInfoActivity.this, CalendarActivity.class);
                    changedDate = false;
                }else {
                    intent = new Intent(CalendarReminderInfoActivity.this, CalendarRemindersActivity.class);
                }
                startActivity(intent);
                finish();
            }
        });

        tvReminderName = findViewById(R.id.tvReminderName);
        tvReminderDate = findViewById(R.id.tvReminderDate);
        tvReminderTime = findViewById(R.id.tvReminderTime);
        tvReminderComment = findViewById(R.id.tvReminderComment);
        edtComment = findViewById(R.id.edtComment);
        btnSendComment = findViewById(R.id.btnSendComment);
        SpinnerSelectDateReminder = findViewById(R.id.SpinnerSelectDateReminder);
        setListener();

        btnSendComment.setVisibility(View.INVISIBLE);
        btnSendComment.setEnabled(false);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = null;
        if (changedDate){
            intent = new Intent(CalendarReminderInfoActivity.this, CalendarActivity.class);
            changedDate = false;
        }else {
            intent = new Intent(CalendarReminderInfoActivity.this, CalendarRemindersActivity.class);
        }
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

       reminder = Reminder.getReminderFromIdOfDate(getIntent().getStringExtra(ReminderListAdapter.REMINDER_ID)
                , SelectedDay.getDate());
       if(reminder == null){
           return;
       }

        updateToolbarTitle(reminder.getTitle());

        SpinnerSelectDateReminder.setAdapter(new CalendarReminderSpinnerAdapter(CalendarReminderInfoActivity.this,""));

        tvReminderName.setText(reminder.getTitle());
        tvReminderDate.setText(reminder.getDate());
        edtComment.setText(reminder.getComment());
        tvReminderTime.setText(DateUtils.changeTimeformat2(reminder.getTime()));
        tvReminderComment.setText(reminder.getComment());
    }

    private void setListener(){
        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayLoadingScreen();
                String param = buildParamForChangeDate(reminder.getTitle(),reminder.getDate(),reminder.getTime(),
                        edtComment.getText().toString(),reminder.getId(),reminder.getBuilding());
                controller.changeDateReminder(CalendarReminderInfoActivity.this,selectedBuilding.getId(),reminder.getId(),param,
                        a->{
                            if (a.equals("200")){
                                tvReminderComment.setText(edtComment.getText().toString());
                                reminder.setComment(edtComment.getText().toString());
                                if (changeToDate!=""){
                                    changedDate = true;
                                    changeToDate = "";
                                }
                                dismissLoadingScreen();
                            }else {
                            }
                        },
                        error -> {
                            if (Offlinemode.checkNetworkConnection(CalendarReminderInfoActivity.this)){
                                Log.e("btnSendComment",error.toString());
                            }else{
                                Offlinemode.showWhenOfflineMode(CalendarReminderInfoActivity.this);
                            }
                        });
            }
        });

        edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtComment.getText().toString().isEmpty()){
                    btnSendComment.setVisibility(View.INVISIBLE);
                    btnSendComment.setEnabled(false);
                }else {
                    btnSendComment.setVisibility(View.VISIBLE);
                    btnSendComment.setEnabled(true);
                }
            }
        });

        SpinnerSelectDateReminder.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (spDateTouch < 1) {
                    Calendar c = Calendar.getInstance();
                    DatePickerDialog dialog = new DatePickerDialog(CalendarReminderInfoActivity.this, R.style.DatePickerTheme, (view1, year, month, dayOfMonth) -> {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, dayOfMonth);
                        Date updateDate = calendar.getTime();
                        SpinnerSelectDateReminder.setAdapter(new CalendarReminderSpinnerAdapter(CalendarReminderInfoActivity.this,
                                DateUtils.getDateString(updateDate,"dd-MM-YYYY")));
                        changeToDate = DateUtils.getDateString(updateDate,"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                    dialog.show();
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            spDateTouch = 0;
                        }
                    });
                    spDateTouch = 1;
                    return true;
                }
                return true;
            }
        });

    }

    private static String buildParamForChangeDate(String nameR, String date,String time, String comment,String id, String building){
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\"id\":"+id+",");
        stringBuffer.append("\"building\":"+building+",");
        stringBuffer.append("\"title\":\""+nameR+"\",");
        stringBuffer.append("\"date\":\""+date+"\",");
        stringBuffer.append("\"time\":\""+time+"\",");
        stringBuffer.append("\"comment\":\""+comment+"\",");
        stringBuffer.append("\"dashboard\":null,");
        stringBuffer.append("\"fired\":0,");
            stringBuffer.append("\"change\":\""+changeToDate+"\"}");
        Log.e("check_param",stringBuffer.toString());
        return stringBuffer.toString();
    }
}
