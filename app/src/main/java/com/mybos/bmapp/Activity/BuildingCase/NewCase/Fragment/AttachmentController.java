package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment;

import android.content.Context;

import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Mapper.Attachment.AttachmentMapper;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentFile;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadInvoice;
import com.mybos.bmapp.Data.Model.Attachment.AttachmentUploadQuote;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;

import java.io.File;
import java.nio.file.Files;

/**
 * Created by EmLaAi on 10/04/2018.
 *
 * @author EmLaAi
 */
public class AttachmentController {
    public  void  uploadPhoto(Context context, int buildingId, int caseId, PhotoModel model, SuccessCallback<AttachmentFile> onSuccess, FailureCallback onFailure){
        AttachmentMapper mapper = new AttachmentMapper();
        mapper.uploadPhoto(context,buildingId,caseId,model,onSuccess::onSuccess,onFailure::onFailure);
    }

    public  void  uploadDocument(Context context, int buildingId, int caseId, File documentFile, SuccessCallback<AttachmentFile> onSuccess, FailureCallback onFailure){
        AttachmentMapper mapper = new AttachmentMapper();
        mapper.uploadDocument(context,buildingId,caseId,documentFile,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void uploadQuote(Context context, int buildingId, int caseId, PhotoModel model , Contractor contractor,Double value,
                            SuccessCallback<AttachmentUploadQuote> onSuccess,FailureCallback onFailure){
        AttachmentMapper mapper = new AttachmentMapper();
        mapper.uploadQuote(context,buildingId,caseId,contractor,value,model,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void uploadInvoice(Context context, int buildingId, int caseId,
                              PhotoModel model, Contractor contractor, Double value,String notes,String invoiceNumber,
                              SuccessCallback<AttachmentUploadInvoice> onSuccess,FailureCallback onFailure){
        AttachmentMapper mapper = new AttachmentMapper();
        mapper.uploadInvoice(context,buildingId,caseId,invoiceNumber,contractor,value,notes,model,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
