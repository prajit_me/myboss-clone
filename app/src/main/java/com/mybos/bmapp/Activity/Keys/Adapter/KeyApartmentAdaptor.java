package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.OtherKeys.OtherKeyListFragment;
import com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.WithMangement.WithManagementKeyListFragment;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.R;

import java.util.Arrays;
import java.util.List;

public class KeyApartmentAdaptor extends FragmentPagerAdapter {

    private @SearchTableQueryBuilder.SectionHint.KeyApartmentSectionHint
    List<Integer> sections = Arrays.asList(SearchTableQueryBuilder.SectionHint.KeyApartmentWManagement,
            SearchTableQueryBuilder.SectionHint.KeyApartmentOther);
    private Context context;
    private WithManagementKeyListFragment fmManagement;
    private OtherKeyListFragment fmOther;

    public KeyApartmentAdaptor(Context context,String apartmentid, FragmentManager fm) {
        super(fm);
        this.context = context;
        fmManagement = new WithManagementKeyListFragment();
        fmManagement.setApartmentid(apartmentid);
        fmOther = new OtherKeyListFragment();
        fmOther.setApartmentid(apartmentid);
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return fmManagement;
        }else if(position == 1){
            return fmOther;
        }
        return null;
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        int section = sections.get(position);
        if (section == SearchTableQueryBuilder.SectionHint.KeyApartmentWManagement){
            return context.getString(R.string.key_apartment_management);
        }
        if (section == SearchTableQueryBuilder.SectionHint.KeyApartmentOther){
            return context.getString(R.string.key_apartment_other);
        }
        return super.getPageTitle(position);
    }
}
