package com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.WithMangement;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Keys.Activities.History.KeyHistoryActivity;
import com.mybos.bmapp.Activity.Keys.Activities.SignIn.KeySignInActivity;
import com.mybos.bmapp.Activity.Keys.Activities.SignOut.KeySignOutActivity;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyApartmentManagementAdapter;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;


public class WithManagementKeyListFragment extends BaseFragment {

    ListView lvKeyManagement;
    ProgressBar progressBarApartment;
    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    private static String apartmentid;
    public static final String SUB_TITLE = "sub_title";

    public WithManagementKeyListFragment() {
    }

    public static String getApartmentid() {
        return apartmentid;
    }

    public static void setApartmentid(String apartmentid) {
        WithManagementKeyListFragment.apartmentid = apartmentid;
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_with_management_key_list, container, false);
        lvKeyManagement = view.findViewById(R.id.lvKeyManagement);
        progressBarApartment = view.findViewById(R.id.progressBarApartment);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        controller.getApartmentKey(getContext(), selectedBuilding.getId(), buildParam(),
                apartment -> {
                    getApartmentManage();
                    lvKeyManagement.setAdapter(new KeyApartmentManagementAdapter(getContext(),
                            R.layout.key_apartment_management_adaptor));
                    KeyApartmentManagementAdapter adapter = (KeyApartmentManagementAdapter) lvKeyManagement.getAdapter();
                    adapter.setListenerBTNHistory(a->{
                        Intent intent = new Intent(getContext(), KeyHistoryActivity.class);
                        intent.putExtra(SUB_TITLE,"Apartment "+a.getKey());
                        startActivity(intent);
                    });
                    adapter.setListenerBTNSign(a ->{
                        Log.e("state",a.getKey_status());
                        Intent intent = new Intent();
                        if (a.getKey_status().equals("2")) {
                            intent = new Intent(getContext(), KeySignInActivity.class);
                        }else if(a.getKey_status().equals("1")){
                            intent = new Intent(getContext(), KeySignOutActivity.class);
                        }
                        intent.putExtra(SUB_TITLE,"Apartment "+a.getKey());
                        startActivity(intent);
                    });
                    progressBarApartment.setVisibility(View.INVISIBLE);
                    lvKeyManagement.setVisibility(View.VISIBLE);
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(getContext())){
                        Log.e("getApartmentKey",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(getContext());
                    }
                });
    }

    private String buildParam() {
        return "{\"apartment\":\"" + getApartmentid() + "\"}";
    }

    private void getApartmentManage() {
        Apartment.setApartment_keys_M(new ArrayList<>());
        for (Apartment apartment : Apartment.getApartment_keys()
        ) {
            if (apartment.getLocation().equals("1") || apartment.getLocation().equals("0"))
                Apartment.getApartment_keys_M().add(apartment);
        }
    }

}
