package com.mybos.bmapp.Activity.BuildingCase.EditCase;

import android.content.Intent;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.mybos.bmapp.Activity.Base.*;
import com.mybos.bmapp.Activity.BuildingCase.Adapter.EditBuildingCasePagerAdapter;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseActivity;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Auth.Permission;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.List;

public class EditCaseActivity extends a_base_toolbar implements AttachmentFragment.AttachmentEventListener{
    public static final String CASE_KEY = "EditCaseActivity.CASE_KEY";
    private EditCaseController controller = new EditCaseController();
    public static String caseId = "new";
    private ContractorList contractorList= new ContractorList();
    private BuildingCase buildingCase;
    private ViewPager viewPager;
    private int currentPage = 0;
    private Boolean loadingInit = false;
    private  Boolean enableLoading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_edit_case);

        //IMPORTANT: setup drawer with drop down building list

        if (null != getSupportActionBar()) {
            updateToolbarTitle(getString(R.string.edit_case_title));
        }
        String caseId = getIntent().getStringExtra(CASE_KEY);
        if (null != caseId){
            this.caseId = caseId;
        }
        enableLoading = true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (buildingCase == null || contractorList == null){
            getInitData();
        }
    }

    private void setUpView(){
        EditBuildingCasePagerAdapter adapter = new EditBuildingCasePagerAdapter(this,getSupportFragmentManager());
        viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(this.currentPage);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                EditCaseActivity.this.currentPage = position;
//                NewBuildingCasePagerAdapter adapter = (NewBuildingCasePagerAdapter) viewPager.getAdapter();
//                adapter.getCaseDetailFragment().setupData(buildingCase,contractorList);
//                adapter.getAttachmentFragment().setup(buildingCase,contractorList);
                dismissKeyboard();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        TabLayout topTabBar = findViewById(R.id.topTabBar);
        topTabBar.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (null != buildingCase && null != contractorList){
            EditBuildingCasePagerAdapter adapter = (EditBuildingCasePagerAdapter) viewPager.getAdapter();
            if (null != adapter){
                adapter.getCaseDetailFragment().updateUI();
            }
        }
    }

    public void getInitData(){
        if (enableLoading){
            this.displayLoadingScreen();
            enableLoading = false;
            loadingInit = true;
        }
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getBuildingCaseDetail(this,selectedBuilding.getId(),caseId,(buildingCase)->{
            this.buildingCase = buildingCase;
            BuildingCase.setSelectedBuildingCase(buildingCase);
            getContractorList(selectedBuilding.getId(),1);
        },(error)->{
            if (loadingInit){
                this.dismissLoadingScreen();
                loadingInit = false;
            }
            Log.e("getInitData_EditCaseA",error.toString());
            if (Offlinemode.checkNetworkConnection(this)){
                Log.e("offlineMod",error.toString());
            }else {
                Intent intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getContractorList(int buildingId,int page){
        controller.getContractorList(this, buildingId, "all", page, (contractorList) -> {
            for (Contractor c : contractorList.getContractorList()
            ) {
                this.contractorList.addContrator(c);
            }
            if (contractorList.getContractorList().size() == 0) {
                if (loadingInit) {
                    this.dismissLoadingScreen();
                    loadingInit = false;
                }
                showContractorList();
            } else {
                getContractorList(buildingId, page + 1);
            }
        }, (error) -> {
            if (loadingInit) {
                this.dismissLoadingScreen();
                loadingInit = false;
            }
            Log.e("getInitData_EditCaseA", error.toString());
            if (Offlinemode.checkNetworkConnection(this)) {
                Log.e("offlineMod", error.toString());
            } else {
                Intent intent = new Intent(this, DashboardActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showContractorList(){
        List<Integer> selectedContractor = this.buildingCase.getContractorsSelectedList();
        for (Integer contractorId:selectedContractor){
            this.contractorList.selectId(contractorId);
        }
        setUpView();
        if (null != viewPager){
            EditBuildingCasePagerAdapter adapter = (EditBuildingCasePagerAdapter) viewPager.getAdapter();
            if (null != adapter){
                adapter.getCaseDetailFragment().setupData(this.buildingCase,this.contractorList);
                adapter.getAttachmentFragment().setup(this.buildingCase,this.contractorList);
                adapter.getAttachmentFragment().addListener(this);
                if (this.currentPage == 0){
                    adapter.getCaseDetailFragment().initData();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_case_menu,menu);
        if (Permission.shared(this).getPermission(Permission.Types.Case) != null){
            if (Permission.shared(this).getPermission(Permission.Types.Case).isEdit()){
                menu.findItem(R.id.barEdit).setEnabled(true);
                menu.findItem(R.id.barEdit).setVisible(true);
            }else{
                menu.findItem(R.id.barEdit).setEnabled(false);
                menu.findItem(R.id.barEdit).setVisible(false);
            }
        }else{
            menu.findItem(R.id.barEdit).setEnabled(false);
            menu.findItem(R.id.barEdit).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.barEdit){
            Intent intent = new Intent(this, NewBuildingCaseActivity.class);
            intent.putExtra(NewBuildingCaseActivity.CASE_ID_KEY,Integer.toString(buildingCase.getId()));
            startActivity(intent);
        }
        return false;
    }

    @Override
    public void didAddAttachment() {
        EditBuildingCasePagerAdapter adapter = (EditBuildingCasePagerAdapter) viewPager.getAdapter();
        if (null != adapter){
            adapter.getCaseDetailFragment().updateUI();
        }
    }
}
