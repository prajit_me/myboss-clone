package com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyPrivateListRecyclerAdapter;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivateKeyListFragment extends BaseFragment {

    private RecyclerView rclvPrivateKey;
    private EditText editTextSearchPrivateKey;
    private int loaded = 0;
    private  View view;
    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    public static final String KEY_SELECTED = "key_selected";

    public PrivateKeyListFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_private_key_list, container, false);
        this.view = view;

        rclvPrivateKey = view.findViewById(R.id.rclKeyPrivateList);

        rclvPrivateKey.setLayoutManager(new LinearLayoutManager(getContext()));
        KeyPrivateListRecyclerAdapter adapter = new KeyPrivateListRecyclerAdapter();
        adapter.setup(rclvPrivateKey);
        rclvPrivateKey.setAdapter(adapter);

        return view;
    }



    @Override
    public void onStart() {
        super.onStart();
        if(loaded == 0){
            setupRecycleView(view);
            setupSearchView(view);
            loaded = 1;
        }
    }

    public void setupRecycleView(View view) {
        controller.getPrivateKeys(getContext(), selectedBuilding.getId(),
                apartment -> {
                    rclvPrivateKey.setAdapter(new KeyPrivateListRecyclerAdapter());
                    KeyPrivateListRecyclerAdapter adapter = (KeyPrivateListRecyclerAdapter) rclvPrivateKey.getAdapter();
                    adapter.setMaximum(adapter.getShowKey().size());
                    adapter.updateModel(adapter.getShowKey());
                    adapter.notifyDataSetChanged();
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(getContext())){
                        Log.e("getPrivateKeys",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(getContext());
                    }
                });
    }

    public void setupSearchView(View view) {
        editTextSearchPrivateKey = view.findViewById(R.id.etSearchKeyPrivate);
        editTextSearchPrivateKey.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                v.clearFocus();
                String keyWord = editTextSearchPrivateKey.getText().toString();
                rclvPrivateKey.setAdapter(new KeyPrivateListRecyclerAdapter());
                KeyPrivateListRecyclerAdapter adapter = (KeyPrivateListRecyclerAdapter) rclvPrivateKey.getAdapter();
                adapter.performSearch(keyWord);
                adapter.setMaximum(adapter.getShowKey().size());
                adapter.updateModel(adapter.getShowKey());
                adapter.notifyDataSetChanged();
                return true;
            }
            return false;
        });
    }

}
