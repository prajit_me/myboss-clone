package com.mybos.bmapp.Activity.Assets.Fragment;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.BuildingAssets.AssetFile;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.StringUtils;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssetWarrantyInfoFragment extends Fragment {

    private LinearLayout lnlyAssetWarrantyContainer;
    private BuildingAsset SelectedAsset = BuildingAssetInfoCollector.getSelectedBuildingAsset();
    private final int MIN_HEIGHT = 80, TEXT_SIZE = 15;
    private final String ORANGE_TEXT_COLOR = "#f19c0f";

    private ImageView getWarrantyType(String WarrantyType) {
        ImageView ivWarrantyType = new ImageView(this.getActivity());
        LinearLayout.LayoutParams lpTypeParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpTypeParams.setMargins(40, 5, 20, 5);
        ivWarrantyType.setLayoutParams(lpTypeParams);
        lpTypeParams.height = 80;
        lpTypeParams.width = 70;
        ivWarrantyType.setMinimumHeight(MIN_HEIGHT);
        ivWarrantyType.setMinimumWidth(MIN_HEIGHT);


        if (WarrantyType.contains("pdf")) {
            ivWarrantyType.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ic_pdf));
        }
        else {
            ivWarrantyType.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ic_doc));
        }

        return ivWarrantyType;
    }

    private TextView getWarrantyName(String WarrantyName) {
        TextView tvWarrantyName = new TextView(this.getActivity());

        LinearLayout.LayoutParams lpNameParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpNameParams.setMargins(20, 10, 5, 0);
        tvWarrantyName.setLayoutParams(lpNameParams);

        tvWarrantyName.setGravity(Gravity.CENTER|Gravity.START);
        tvWarrantyName.setMinHeight(MIN_HEIGHT);
        tvWarrantyName.setText(WarrantyName);
        tvWarrantyName.setTextSize(TEXT_SIZE);
        tvWarrantyName.setTextColor(Color.BLACK);
        tvWarrantyName.setTypeface(tvWarrantyName.getTypeface(), Typeface.BOLD);

        return tvWarrantyName;
    }

    private LinearLayout getAddedDate(String AddedDate) {
        LinearLayout lnlyAddedDate = new LinearLayout(this.getActivity());
        lnlyAddedDate.setOrientation(LinearLayout.HORIZONTAL);

        //  ADDED LABEL
        TextView tvAddedLabel = new TextView(this.getActivity());
        LinearLayout.LayoutParams lpDateLabelParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpDateLabelParams.setMargins(20, 20, 5, 5);
        tvAddedLabel.setLayoutParams(lpDateLabelParams);
        tvAddedLabel.setMinHeight(MIN_HEIGHT);
        tvAddedLabel.setText("Updated: ");
        tvAddedLabel.setTextSize(TEXT_SIZE);
        tvAddedLabel.setTextColor(Color.LTGRAY);

        //  ADDED DATE
        TextView tvAddedDate = new TextView(this.getActivity());
        LinearLayout.LayoutParams lpAddedDateParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpAddedDateParams.setMargins(5, 20, 5, 5);
        tvAddedDate.setLayoutParams(lpAddedDateParams);
        tvAddedDate.setMinHeight(MIN_HEIGHT);
        tvAddedDate.setText(StringUtils.getDate(AddedDate.substring(0, 10)));
        tvAddedDate.setTextSize(TEXT_SIZE);
        tvAddedDate.setTextColor(Color.parseColor(ORANGE_TEXT_COLOR));
        tvAddedDate.setTypeface(tvAddedDate.getTypeface(), Typeface.BOLD);

        lnlyAddedDate.addView(tvAddedLabel);
        lnlyAddedDate.addView(tvAddedDate);

        return lnlyAddedDate;
    }

    private LinearLayout getWarrantyNameAndDate(String WarrantyName, String WarrantyDate) {
        LinearLayout lnlyNameAndDate = new LinearLayout(this.getActivity());
        LinearLayout.LayoutParams lpWarrantyNameAndDateParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpWarrantyNameAndDateParams.setMargins(5, 5, 5, 5);
        lnlyNameAndDate.setLayoutParams(lpWarrantyNameAndDateParams);
        lnlyNameAndDate.setOrientation(LinearLayout.VERTICAL);

        lnlyNameAndDate.addView(getWarrantyName(WarrantyName));
        lnlyNameAndDate.addView(getAddedDate(WarrantyDate));

        return lnlyNameAndDate;
    }

    private LinearLayout getWarrantyInfo(AssetFile CurrentAssetFile) {
        LinearLayout lnlyWarrantyInfo = new LinearLayout(this.getActivity());
        lnlyWarrantyInfo.setOrientation(LinearLayout.HORIZONTAL);
        lnlyWarrantyInfo.setGravity(Gravity.CENTER|Gravity.START);

        String FileName = CurrentAssetFile.getAssetFileName();
        lnlyWarrantyInfo.addView(getWarrantyType(FileName.substring(FileName.indexOf("."))));
        lnlyWarrantyInfo.addView(getWarrantyNameAndDate(FileName.substring(0, FileName.indexOf(".")), CurrentAssetFile.getAdded()));

        return lnlyWarrantyInfo;
    }

    private ImageView getViewButton() {
        ImageView ivView = new ImageView(this.getActivity());
        LinearLayout.LayoutParams btnViewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ivView.setLayoutParams(btnViewParams);
        ivView.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ic_next));

        return ivView;
    }

    private void linkToAssetFile(String FileURL) {
        Intent InsuranceIntent = new Intent(Intent.ACTION_VIEW);
        InsuranceIntent.setData(Uri.parse(FileURL));
        startActivity(InsuranceIntent);
    }

    private RelativeLayout getWarrantyListCell(int CellID, AssetFile CurrentAssetFile) {
        RelativeLayout WarrantyListCell = new RelativeLayout(this.getActivity());
        RelativeLayout.LayoutParams WarrantyListCellParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        WarrantyListCellParams.setMargins(5, 10, 5, 10);
        WarrantyListCell.setLayoutParams(WarrantyListCellParams);
        WarrantyListCell.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.bg_corner_radius_8));
        WarrantyListCell.setGravity(Gravity.CENTER|Gravity.START);
        WarrantyListCell.setId(CellID);
        WarrantyListCell.setOnClickListener(new AssetFileSelectionListener());

        LinearLayout lnlyWarrantyInfo = getWarrantyInfo(CurrentAssetFile);
        RelativeLayout.LayoutParams lnlyWarrantyInfoParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lnlyWarrantyInfoParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        lnlyWarrantyInfo.setLayoutParams(lnlyWarrantyInfoParams);

        ImageView ivView = getViewButton();
        RelativeLayout.LayoutParams ivViewParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ivViewParams.setMargins(10,10,20,10);
        ivViewParams.addRule(RelativeLayout.CENTER_VERTICAL);
        ivViewParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        ivView.setLayoutParams(ivViewParams);

        WarrantyListCell.addView(lnlyWarrantyInfo);
        WarrantyListCell.addView(ivView);

        return WarrantyListCell;
    }

    private TextView getNoWarrantyFile() {
        TextView tvNoDocumentFile = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvNoDocumentFileParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvNoDocumentFileParams.setMargins(5, 5, 5, 5);
        tvNoDocumentFile.setLayoutParams(tvNoDocumentFileParams);

        tvNoDocumentFile.setGravity(Gravity.CENTER);
        tvNoDocumentFile.setMinHeight(MIN_HEIGHT * 2);
        tvNoDocumentFile.setText("No warranty was found.");
        tvNoDocumentFile.setTextSize(TEXT_SIZE);
        tvNoDocumentFile.setTextColor(Color.BLACK);
        tvNoDocumentFile.setTypeface(tvNoDocumentFile.getTypeface(), Typeface.ITALIC);

        return tvNoDocumentFile;
    }

    private void getWarrantyList() {
        int Size = SelectedAsset.getWarrantyDocument().size();
        if (Size > 0) {
            for (int i = 0; i < Size; i++) {
                AssetFile CurrentAssetFile = SelectedAsset.getWarrantyDocument().get(i);
                lnlyAssetWarrantyContainer.addView(getWarrantyListCell(i, CurrentAssetFile));
            }
        }
        else {
            lnlyAssetWarrantyContainer.addView(getNoWarrantyFile());
        }
    }

    private void startup() {
        getWarrantyList();
    }

    public AssetWarrantyInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_asset_warranty_info, container, false);

        //  GET REFERENCES
        lnlyAssetWarrantyContainer = rootView.findViewById(R.id.lnlyAssetWarrantyContainer_AssetWarrantyInfoFragment);

        //  START UP
        startup();
        
        return rootView;
    }

    private class AssetFileSelectionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int FileID = v.getId();
            linkToAssetFile(SelectedAsset.getWarrantyDocument().get(FileID).getURL());
        }
    }
}
