package com.mybos.bmapp.Activity.Broadcast;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

public class BroadcastSMSPurchaseActivity extends BaseActivity {

    Spinner spinnerPurchase;
    CheckBox cbPurch1, cbPurch2;
    Button btnPurchase;
    BroadcastController controller = new BroadcastController();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_smspurchase);

        cbPurch1 = findViewById(R.id.cbbr1);
        cbPurch2 = findViewById(R.id.cbbr2);
        btnPurchase = findViewById(R.id.btnPurchase);

        cbPurch1.setChecked(true);
        findViewById(R.id.btnBackTopUp).setOnClickListener(v -> this.finish());
        spinnerPurchase = findViewById(R.id.spSelectAmong);

        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

        ArrayAdapter arrayAdapter = new ArrayAdapter(BroadcastSMSPurchaseActivity.this,
                android.R.layout.simple_spinner_item, getArrayPurchaseSMS());
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPurchase.setAdapter(arrayAdapter);

        btnPurchase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.purchaseSMS(BroadcastSMSPurchaseActivity.this, selectedBuilding.getId(), buildDataToTopUp(),
                        purch -> {
                            if (Broadcast.getStat_send() == 200){
                                Toast.makeText(BroadcastSMSPurchaseActivity.this, "Purchase completed!",Toast.LENGTH_LONG).show();
                            }
                        },
                        error -> {
                            if (Offlinemode.checkNetworkConnection(BroadcastSMSPurchaseActivity.this)){
                                Log.e("purchaseSMS",error.toString());
                            }else {
                                Offlinemode.showWhenOfflineMode(BroadcastSMSPurchaseActivity.this);
                            }
                        });
            }
        });

        spinnerPurchase.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(BroadcastSMSPurchaseActivity.this, "You selected " + spinnerPurchase.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        cbPurch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cbPurch1.isChecked()) {
                    cbPurch2.setChecked(false);
                } else {
                    cbPurch2.setChecked(true);
                }
            }
        });

        cbPurch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (cbPurch2.isChecked()) {
                    cbPurch1.setChecked(false);
                } else {
                    cbPurch1.setChecked(true);
                }
            }
        });

    }

    private String buildDataToTopUp() {
        return "{\"amount\":" + Integer.valueOf(spinnerPurchase.getSelectedItem().toString()) +
                ",\"owner\":" + cbPurch1.isChecked() + "}";
    }

    private ArrayList<String> getArrayPurchaseSMS() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("100");
        arrayList.add("200");
        arrayList.add("300");
        arrayList.add("400");
        arrayList.add("500");
        arrayList.add("600");
        arrayList.add("700");
        arrayList.add("800");
        arrayList.add("900");
        arrayList.add("1000");
        return arrayList;
    }
}
