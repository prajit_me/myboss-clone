package com.mybos.bmapp.Activity.Broadcast;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Broadcast.Adapter.BroadcastSpinnerAdapter;
import com.mybos.bmapp.Activity.Broadcast.Adapter.BroadcastTabBarAdapter;
import com.mybos.bmapp.Activity.Broadcast.Adapter.BroadcastUserListAdapter;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Library.LibraryListAdapter;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Broadcast.Groups;
import com.mybos.bmapp.Data.Model.Broadcast.User;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;


public class BroadcastActivity extends a_base_toolbar {

    RecyclerView listView;
    Spinner spinner;
    TabLayout tabLayout;
    Button btnNext, btnSelect;
    static int check1touch;
    BroadcastController controller = new BroadcastController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    static final String SELECT = "Select all";
    static final String DESELECT = "Deselect all";
    private boolean start = false;
    private String selectedGroup = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast);
        updateToolbarTitle("Broadcast");

        btnNext = findViewById(R.id.btnNext);
        listView = findViewById(R.id.listViewUser);
        spinner = findViewById(R.id.spinnerGroupUser);
        btnSelect = findViewById(R.id.btnSelect);
        tabLayout = findViewById(R.id.topTabBarBroadcast);

        listView.setLayoutManager(new LinearLayoutManager(this));
        LibraryListAdapter adapter = new LibraryListAdapter();
        adapter.setup(listView);
        listView.setAdapter(adapter);

        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent backToDashboard = new Intent(BroadcastActivity.this, DashboardActivity.class);
                startActivity(backToDashboard);
                finish();
            }
        });

        ViewPager viewPager = findViewById(R.id.viewPagerBroadcast);
        viewPager.setAdapter(new BroadcastTabBarAdapter(this, getSupportFragmentManager()));

        tabLayout.setupWithViewPager(viewPager);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tabLayout.getSelectedTabPosition() == SearchTableQueryBuilder.SectionHint.BroadcastEmail) {
                    Intent intent = new Intent(BroadcastActivity.this, BroadcastMailActivity.class);
                    startActivity(intent);
                } else if (tabLayout.getSelectedTabPosition() == SearchTableQueryBuilder.SectionHint.BroadcastSMS) {
                    Intent intent = new Intent(BroadcastActivity.this, BroadcastSMSActivity.class);
                    startActivity(intent);
                }
            }
        });

        btnSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btnSelect.getText().toString().equals(SELECT)){
                    btnSelect.setText(DESELECT);
                    Broadcast.setChoose_users(new ArrayList<>(User.getUsers()));
                } else if (btnSelect.getText().toString().equals(DESELECT)) {
                    btnSelect.setText(SELECT);
                    Broadcast.setChoose_users(new ArrayList<User>());
                }
                listView.setAdapter(new BroadcastUserListAdapter());
                BroadcastUserListAdapter adapter = (BroadcastUserListAdapter) listView.getAdapter();
                adapter.setMaximum(User.getUsers().size());
                adapter.updateModel(User.getUsers());
                adapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        check1touch = 0;
        spinner.setAdapter(new BroadcastSpinnerAdapter(this, R.layout.a_broadcast_group));
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public synchronized boolean onTouch(View view, MotionEvent motionEvent) {
                if (check1touch < 1) {
                    Intent intent = new Intent(BroadcastActivity.this, BroadcastGroupUserActivity.class);
                    startActivity(intent);
                    check1touch = 1;
                    return true;
                }
                return true;
            }
        });
        if (!selectedGroup.isEmpty()){
            spinner.setSelection(Groups.getPositionFromName(selectedGroup));
        }

        if (getIntent().getStringExtra(BroadcastGroupUserActivity.GROUP_NAME) != null) {
            selectedGroup = getIntent().getStringExtra(BroadcastGroupUserActivity.GROUP_NAME);
            spinner.setSelection(Groups.getPositionFromName(getIntent().getStringExtra(BroadcastGroupUserActivity.GROUP_NAME)));
            getListUser(Groups.getIdFromName(getIntent().getStringExtra(BroadcastGroupUserActivity.GROUP_NAME)),true);
            getIntent().removeExtra(BroadcastGroupUserActivity.GROUP_NAME);
            start = true;
        } else {
            if(!start) {
                getListUser(0, true);
                start = true;
            }
        }
    }

    public void getListUser(int idGroup, boolean checkAll){
        Broadcast.setChoose_users(new ArrayList<User>());
        controller.getBroadcastListUser(this, selectedBuilding.getId(), idGroup,
                user -> {
                    if(checkAll){
                        Broadcast.setChoose_users(new ArrayList<>(User.getUsers()));
                        btnSelect.setText(DESELECT);
                    }
                    listView.setAdapter(new BroadcastUserListAdapter());
                    BroadcastUserListAdapter adapter = (BroadcastUserListAdapter) listView.getAdapter();
                    adapter.setMaximum(User.getUsers().size());
                    adapter.updateModel(User.getUsers());
                    adapter.notifyDataSetChanged();
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(BroadcastActivity.this)){
                        Log.e("getAvaliableSMS",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(BroadcastActivity.this);
                    }
                });
    }
}
