package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment;

import android.content.Context;

import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseController;
import com.mybos.bmapp.Data.Mapper.Apartment.ApartmentMapper;
import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingAssetMapper;
import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseMapper;
import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCategoryMapper;
import com.mybos.bmapp.Data.Model.Apartment.ApartmentList;
import com.mybos.bmapp.Data.Model.Asset.AssetsList;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseTypeList;
import com.mybos.bmapp.Data.Model.Cases.Category.BuildingCategory;
import com.mybos.bmapp.Data.Model.Filter.TypeList;

import java.util.List;

/**
 * Created by EmLaAi on 31/03/2018.
 *
 * @author EmLaAi
 */
public class CaseDetailFragmentController {
    public void getBuildingCaseType(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<BuildingCaseTypeList> onSuccess, NewBuildingCaseController.FailureCallback onError){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.getAllCaseType(context,buildingId,onSuccess::onSuccess,onError::onFailure);
    }
    public void getBuildingCaseStatus(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<BuildingCaseStatusList> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.getAllCaseStatus(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void getBuildingCaseStatusForFilter(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<TypeList> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.getAllCaseStatusForFilter(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void getAllBuildingApartment(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<ApartmentList> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        ApartmentMapper mapper = new ApartmentMapper();
        mapper.get(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }
    public  void getAllAsset(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<AssetsList> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        BuildingAssetMapper mapper = new BuildingAssetMapper();
        mapper.get(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getAsset(Context context,int buildingId,int categoryId,SuccessCallback<AssetsList> onSuccess,FailureCallback onFailure){
        BuildingAssetMapper mapper = new BuildingAssetMapper();
        mapper.get(context,buildingId,categoryId,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void getCategoryTree(Context context, int buildingId, NewBuildingCaseController.SuccessCallback<List<BuildingCategory>> onSuccess, NewBuildingCaseController.FailureCallback onFailure){
        BuildingCategoryMapper mapper = new BuildingCategoryMapper();
        mapper.getCategoryTree(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
