package com.mybos.bmapp.Activity.BuildingCase.NewCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 01/04/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseNotesComponent extends ConstraintLayout {
    EditText txtNotes;
    OnNoteChange noteChange;

    public BuildingCaseNotesComponent(Context context) {
        super(context);
        additionalInit();
    }

    public BuildingCaseNotesComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public BuildingCaseNotesComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_new_case_f_case_detail_c_notes,this);
        txtNotes = findViewById(R.id.txtNotes);
        txtNotes.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (null != noteChange){
                    noteChange.updateNotes(s.toString());
                }
            }
        });
    }

    public void setNote(String notes,OnNoteChange noteListener){
        txtNotes.setText(notes);
        noteChange = noteListener;
    }

    public interface OnNoteChange{
        void updateNotes(String notes);
    }
}
