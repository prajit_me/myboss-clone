package com.mybos.bmapp.Activity.Calendar.Activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseActivity;
import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarBookingListAdapter;
import com.mybos.bmapp.Activity.Calendar.CalendarController;
import com.mybos.bmapp.Activity.Calendar.Fragment.CalendarBookingUpdateComplete;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Calendar.Booking;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;

public class CalendarBookingInfoActivity extends BaseActivity {

    Spinner spStatusBooking;
    TextView tvBookingNumValue,tvBuildingValue,tvApartmentValue,tvAmenityValue,tvBookingDateValue,
            tvBookingTimeValue,tvFirstNameValue,tvLastNameValue,tvPhoneValue,tvMoblieValue,
            tvEmailValue,tvCommentValue,tvDateBookingInfo,tvNameBookingInfo,tvStatusBooking;
    EditText edtComment;
    Button btnSendComment,btnBackBookingInfo,btnSaveBookingInfo;
    Booking booking;
    ConstraintLayout layoutStatusBooking;
    private String stastus = "0";

    CalendarController controller = new CalendarController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_booking_info);

        spStatusBooking = findViewById(R.id.spStatusBooking);
        tvBookingNumValue = findViewById(R.id.tvBookingNumValue);
        tvBuildingValue = findViewById(R.id.tvBuildingValue);
        tvApartmentValue = findViewById(R.id.tvApartmentValue);
        tvAmenityValue = findViewById(R.id.tvAmenityValue);
        tvBookingDateValue = findViewById(R.id.tvBookingDateValue);
        tvBookingTimeValue = findViewById(R.id.tvBookingTimeValue);
        tvFirstNameValue = findViewById(R.id.tvFirstNameValue);
        tvLastNameValue = findViewById(R.id.tvLastNameValue);
        tvPhoneValue = findViewById(R.id.tvPhoneValue);
        tvMoblieValue = findViewById(R.id.tvMoblieValue);
        tvEmailValue = findViewById(R.id.tvEmailValue);
        tvCommentValue = findViewById(R.id.tvCommentValue);
        edtComment = findViewById(R.id.edtComment);
        btnSendComment = findViewById(R.id.btnSendComment);
        tvDateBookingInfo = findViewById(R.id.tvDateBookingInfo);
        tvNameBookingInfo = findViewById(R.id.tvNameBookingInfo);
        btnBackBookingInfo = findViewById(R.id.btnBackBookingInfo);
        btnSaveBookingInfo = findViewById(R.id.btnSaveBookingInfo);
        tvStatusBooking = findViewById(R.id.tvStatusBooking);
        layoutStatusBooking = findViewById(R.id.layoutStatusBooking);
        setListener();

        ArrayList<String> itemsSpinner = new ArrayList<>();
        itemsSpinner.add("Pending");
        itemsSpinner.add("Approved");
        itemsSpinner.add("Rejected");
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item,itemsSpinner);
        spStatusBooking.setAdapter(arrayAdapter);

    }

    @Override
    protected void onStart() {
        super.onStart();
        btnSendComment.setEnabled(false);
        btnSendComment.setVisibility(View.INVISIBLE);
        booking = Booking.getBookingFromId(getIntent().getStringExtra(CalendarBookingListAdapter.BOOKING_ID));
        checkSpinnerStatus();

        String dateTitle = SelectedDay.getDateOfweek()+", "+SelectedDay.getFullDate();
        tvDateBookingInfo.setText(dateTitle);
        tvNameBookingInfo.setText(booking.getCalendarName());

        tvBookingNumValue.setText(booking.getId());
        tvBuildingValue.setText(booking.getBuildingName());
        tvApartmentValue.setText(booking.getApartment());
        tvAmenityValue.setText(booking.getCalendarName());
        tvBookingDateValue.setText(DateUtils.editDateTime2(booking.getDate()));
        String timeString = DateUtils.changeTimeformat(booking.getSlot_from())+"-"+DateUtils.changeTimeformat(booking.getSlot_to());
        tvBookingTimeValue.setText(timeString);
        tvFirstNameValue.setText(booking.getBooker_fname());
        tvLastNameValue.setText(booking.getBooker_lname());
        tvPhoneValue.setText(booking.getBooker_phone());
        tvMoblieValue.setText(booking.getBooker_mobile());
        tvEmailValue.setText(booking.getBooker_email());
        tvCommentValue.setText(booking.getComment().equals("null")?"":booking.getComment());
    }

    private void checkSpinnerStatus(){
        if (booking.getStatusText().equals("Pending")){
            spStatusBooking.setSelection(0);
        } else if (booking.getStatusText().equals("Confirm")){
            spStatusBooking.setSelection(1);
            stastus = "1";
        } else {
            spStatusBooking.setSelection(2);
            stastus = "4";
        }
    }

    private String buildParam(String type){
        StringBuffer stringBuffer = new StringBuffer();
        if (type.equals("status")){
            stringBuffer.append("{\"status\":\""+stastus+"\"}");
        } else{
            stringBuffer.append("{\"comment\":\""+edtComment.getText().toString()+"\"}");
        }
        return stringBuffer.toString();
    }

    private void updateBooking(){
        displayLoadingScreen();
        if (!stastus.equals("0")) {
            controller.changeStatusBooking(CalendarBookingInfoActivity.this, selectedBuilding.getId(), booking.getId(), buildParam("status"),
                    a -> {
                        if (a.equals("200")) {
                            dismissLoadingScreen();
                            Booking.getBookingFromId(getIntent().getStringExtra(CalendarBookingListAdapter.BOOKING_ID)).setStatus(stastus);
                            Booking.getBookingFromId(getIntent().getStringExtra(CalendarBookingListAdapter.BOOKING_ID)).setStatusText((stastus.equals("1")?"Confirm":"Reject"));
                            if (edtComment.getText().toString().isEmpty()){
                                CalendarBookingUpdateComplete dialog = new CalendarBookingUpdateComplete();
                                dialog.show(getSupportFragmentManager(), "Completed");
                            }
                        } else {
                            dismissLoadingScreen();
                        }
                    },
                    error -> {
                        dismissLoadingScreen();
                        if (Offlinemode.checkNetworkConnection(CalendarBookingInfoActivity.this)){
                            Log.e("updateBooking",error.toString());
                        }else{
                            Offlinemode.showWhenOfflineMode(CalendarBookingInfoActivity.this);
                        }
                    });
        }
        if (!edtComment.getText().toString().isEmpty()) {
            controller.changeCommentBooking(CalendarBookingInfoActivity.this, selectedBuilding.getId(), booking.getId(), buildParam("comment"),
                    a -> {
                        dismissLoadingScreen();
                        if (a.equals("200")) {
                            CalendarBookingUpdateComplete dialog = new CalendarBookingUpdateComplete();
                            dialog.show(getSupportFragmentManager(), "Completed");
                            Booking.getBookingFromId(getIntent().getStringExtra(CalendarBookingListAdapter.BOOKING_ID)).setComment(edtComment.getText().toString());
                            tvCommentValue.setText(edtComment.getText().toString());
                        } else {
                            dismissLoadingScreen();
                        }
                    },
                    error -> {
                        dismissLoadingScreen();
                        if (Offlinemode.checkNetworkConnection(CalendarBookingInfoActivity.this)){
                            Log.e("updateBooking",error.toString());
                        }else{
                            Offlinemode.showWhenOfflineMode(CalendarBookingInfoActivity.this);
                        }
                    });
        }
        if (stastus.equals("0") && edtComment.getText().toString().isEmpty()){
            dismissLoadingScreen();
        }
    }

    private void setListener(){

        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateBooking();
            }
        });

        btnSaveBookingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateBooking();
            }
        });

        edtComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (edtComment.getText().toString().isEmpty()){
                    btnSendComment.setEnabled(false);
                    btnSendComment.setVisibility(View.INVISIBLE);
                } else {
                    btnSendComment.setEnabled(true);
                    btnSendComment.setVisibility(View.VISIBLE);
                }
            }
        });

        spStatusBooking.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (spStatusBooking.getSelectedItem().toString().equals("Approved")){
                    tvStatusBooking.setBackgroundColor(CalendarBookingInfoActivity.this.getResources().getColor(R.color.calendar_status_complete));
                    layoutStatusBooking.setBackgroundColor(CalendarBookingInfoActivity.this.getResources().getColor(R.color.calendar_status_complete));
                    stastus = "1";
                } else if (spStatusBooking.getSelectedItem().toString().equals("Rejected")){
                    tvStatusBooking.setBackgroundColor(CalendarBookingInfoActivity.this.getResources().getColor(R.color.calendar_status_failed));
                    layoutStatusBooking.setBackgroundColor(CalendarBookingInfoActivity.this.getResources().getColor(R.color.calendar_status_failed));
                    stastus = "4";
                }

                if (spStatusBooking.getSelectedItem().toString().equals("Pending") &&
                !booking.getStatusText().equals("Pending")) {
                    if (booking.getStatusText().equals("Confirm")){
                        spStatusBooking.setSelection(1);
                    }else {
                        spStatusBooking.setSelection(2);
                    }
                    Toast.makeText(CalendarBookingInfoActivity.this,"Can't select Pending",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnBackBookingInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarBookingInfoActivity.this, CalendarBookingsActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
