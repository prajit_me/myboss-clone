package com.mybos.bmapp.Activity.Offlinemode;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ChangNetworkStateReciver extends BroadcastReceiver {

    public static final String NETWORK_AVAILABLE_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    public static final String IS_NETWORK_AVAILABLE = "isNetworkAvailable";

    @Override
    public void onReceive( Context context, Intent intent )
    {

    }

    private Boolean isConnectedToInternet(Context context){
        if (Offlinemode.checkNetworkConnection(context)){
            return true;
        }else {
            return false;
        }
    }
}
