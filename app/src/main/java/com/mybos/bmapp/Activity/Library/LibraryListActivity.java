package com.mybos.bmapp.Activity.Library;

        import androidx.appcompat.app.AppCompatActivity;
        import android.os.Bundle;
        import androidx.recyclerview.widget.LinearLayoutManager;
        import androidx.recyclerview.widget.RecyclerView;
        import android.util.Log;

        import com.mybos.bmapp.Activity.Base.a_base_toolbar;
        import com.mybos.bmapp.Data.Model.Building.Building;
        import com.mybos.bmapp.Data.Model.Library.LibraryFileOrFolder;
        import com.mybos.bmapp.R;
        import com.mybos.bmapp.Services.SingletonObjectHolder;

public class LibraryListActivity extends a_base_toolbar {

    RecyclerView lvLibraryList;
    LibraryController controller = new LibraryController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    public static final String TITLE = "title";
    public static final String ID = "id";
    private String title = "Library";
    private String id = "0";
    private boolean reLoad = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_list);
        lvLibraryList = findViewById(R.id.lvLibraryList);
        if (null != getIntent().getStringExtra(TITLE)) {
            title = getIntent().getStringExtra(TITLE);
            id = getIntent().getStringExtra(ID);
            getIntent().removeExtra(ID);
            getIntent().removeExtra(TITLE);
        }
        updateToolbarTitle(title);

        lvLibraryList.setLayoutManager(new LinearLayoutManager(this));
        LibraryListAdapter adapter = new LibraryListAdapter();
        adapter.setup(lvLibraryList);
        lvLibraryList.setAdapter(adapter);
        getLibraryList();
    }

    public void getLibraryList() {
        controller.getLibraryList(LibraryListActivity.this, selectedBuilding.getId(), id,
                success -> {
                        LibraryListAdapter adapter = (LibraryListAdapter) lvLibraryList.getAdapter();
                        adapter.setMaximum(LibraryFileOrFolder.getListFileFolderCurrentPage().size());
                        adapter.updateModel(LibraryFileOrFolder.getListFileFolderCurrentPage());
                        adapter.notifyDataSetChanged();
                },
                error -> {
                    Log.e("Log_getLibraryList", "failed");
                });
    }
}
