package com.mybos.bmapp.Activity.BuildingCase.EditCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 24/04/2018.
 *
 * @author EmLaAi
 */
public class CaseNotesComponent extends ConstraintLayout {
    TextView txtNotes;
    public CaseNotesComponent(Context context) {
        super(context);
        additionalInit();
    }

    public CaseNotesComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public CaseNotesComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_edit_case_f_edit_case_detail_c_case_notes,this);
        txtNotes = findViewById(R.id.txtNotes);
    }

    public void setNotes(String value){
        txtNotes.setText(value);
    }
}
