package com.mybos.bmapp.Activity.Contractor.Activities;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Filter.FilterFragment;
import com.mybos.bmapp.Activity.Filter.FilterStoreRealm;
import com.mybos.bmapp.Activity.Filter.ListFilterStoreRealm;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.StringUtils;

import java.util.ArrayList;

public class ContractorListActivity extends a_base_toolbar {

    private EditText edtSearchContractor;
    private LinearLayout lnlyContractorListContainer;
    private ContractorMapper contractorMapper = new ContractorMapper();
    private ArrayList<Contractor> List;
    private final int ConstantTextSize = 15, ConstantMeasure = 120, ConstantHeight = 100;
    private final String NoDataMessage = "No contractor was found.";
    private Button btnTagFilter;
    private boolean checkInside = false;


    private static String selectedCategory = "all";

    private void goToContractorDetail(){
        Intent main = new Intent(this,ContractorListActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent ContractorDetail = new Intent(this, ContractorDetailActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(ContractorDetail);
        builder.startActivities();
        finish();
    }

    private void getReferences(){
        edtSearchContractor = findViewById(R.id.edtSearchContractor_ContractorListActivity);
        lnlyContractorListContainer = findViewById(R.id.lnlyContractorListContainer);
    }

    private void setListener() {
        SearchContractorListener SearchListener = new SearchContractorListener();
        edtSearchContractor.setOnKeyListener(SearchListener);
        edtSearchContractor.addTextChangedListener(SearchListener);
    }

    public void showContractorList(){
        if (checkInside){
            return;
        }else {
            checkInside = true;
        }
        //  CLEAR LIST
        ContractorInfoCollector.clearContractorList();
        lnlyContractorListContainer.removeAllViews();
        getContractorList(1);
    }

    private void getContractorList(int page){
        String SearchInfo = edtSearchContractor.getText().toString().trim();
        contractorMapper.get(this,
                BuildingInfoCollection.getBuildingID(),selectedCategory,SearchInfo,page,
                response -> {
                    for (Contractor contractor:response.getContractorList()
                    ) {
                        lnlyContractorListContainer.addView(getContractor(contractor));
                    }
                    if (response.getContractorList().size()>0){
                        getContractorList(page+1);
                    }else {
                        checkInside = false;
                    }
                },
                error -> {
                    checkInside = false;
                    if (Offlinemode.checkNetworkConnection(ContractorListActivity.this)){
                        Log.e("showContractorList",error.toString());
                    }else {
                        Intent intent = new Intent(ContractorListActivity.this, DashboardActivity.class);
                        startActivity(intent);
                    }
                });
    }



    private RelativeLayout getContractor(Contractor contractor){
        RelativeLayout.LayoutParams rllyContractorParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rllyContractorParams.setMargins(40, 20, 40, 30);
        RelativeLayout rllyContractor = new RelativeLayout(this);
        rllyContractor.setId(contractor.getId());
        rllyContractor.setLayoutParams(rllyContractorParams);
        rllyContractor.setBackground(getDrawable(R.drawable.round_rectangle_bg));
        rllyContractor.setOnClickListener(new ContractorSelectedListener());

        //  CONTRACTOR INFO: NAME + SERVICE
        RelativeLayout.LayoutParams rllyContractorInfoParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        rllyContractorInfoParams.setMargins(15, 15, 15, 15);
        rllyContractorInfoParams.addRule(RelativeLayout.ALIGN_PARENT_START);

        LinearLayout rllyContractorInfo = new LinearLayout(this);
        rllyContractorInfo.setOrientation(LinearLayout.VERTICAL);
        rllyContractorInfo.setLayoutParams(rllyContractorInfoParams);

        //  NAME
        TextView tvContractorName = new TextView(this);
        tvContractorName.setId(contractor.getId());
        tvContractorName.setMinHeight(100);
        tvContractorName.setTextColor(Color.BLACK);
        tvContractorName.setTextSize(ConstantTextSize);
        StringUtils.writeStringInBold(contractor.getCompany(), tvContractorName);
        rllyContractorInfo.addView(tvContractorName);

        //  SERVICE
        TextView tvContractorService = new TextView(this);
        tvContractorService.setId(contractor.getId());
        tvContractorService.setMinHeight(75);
        tvContractorService.setTextColor(Color.LTGRAY);
        tvContractorName.setTextSize(ConstantTextSize);
        tvContractorService.setText(StringUtils.firstCapitalized(contractor.getCategoryName()));
        rllyContractorInfo.addView(tvContractorService);
        
        //  CONTRACTOR CONTACT: PHONE + EMAIL
        RelativeLayout.LayoutParams rllyContractorContactParams = new RelativeLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        rllyContractorContactParams.setMargins(15, 15, 15, 15);
        rllyContractorContactParams.addRule(RelativeLayout.ALIGN_PARENT_END);

        LinearLayout rllyContractorContact = new LinearLayout(this);
        rllyContractorContact.setOrientation(LinearLayout.HORIZONTAL);
        rllyContractorContact.setLayoutParams(rllyContractorContactParams);

        //  EMAIL
        LinearLayout.LayoutParams ContactParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ContactParams.setMargins(25, 10, 25, 10);

        ImageView ivEmail = new ImageView(this);
        ivEmail.setId(contractor.getId());
        ivEmail.setLayoutParams(ContactParams);
        ivEmail.setMinimumHeight(ConstantMeasure);
        ivEmail.setMaxHeight(ConstantMeasure);
        ivEmail.setMinimumWidth(ConstantMeasure);
        ivEmail.setMaxWidth(ConstantMeasure);
        ivEmail.setBackground(getDrawable(R.drawable.ic_email));
        rllyContractorContact.addView(ivEmail);

        //  PHONE
        ImageView ivPhone = new ImageView(this);
        ivPhone.setId(contractor.getId());
        ivPhone.setLayoutParams(ContactParams);
        ivPhone.setMinimumHeight(ConstantMeasure);
        ivPhone.setMaxHeight(ConstantMeasure);
        ivPhone.setMinimumWidth(ConstantMeasure);
        ivPhone.setMaxWidth(ConstantMeasure);
        ivPhone.setBackground(getDrawable(R.drawable.ic_blue_phone));
        rllyContractorContact.addView(ivPhone);

        //  GROUP INFO + CONTACT
        rllyContractor.addView(rllyContractorInfo);
        rllyContractor.addView(rllyContractorContact);

        return rllyContractor;
    }

    private void startup(){
        //  GET REFERENCE
        getReferences();

        //  SET LISTENER
        setListener();

        //  SET TITLE
        super.updateToolbarTitle("Contractors");

        getRealmData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_list);

        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ContractorListActivity.this,DashboardActivity.class);
                startActivity(intent);
            }
        });

        btnTagFilter = findViewById(R.id.btnTagFilter);

        //  START UP
        setupBtnFilter();
        startup();
    }

    public void setupBtnFilter(){
        findViewById(R.id.btnFilter).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterFragment dialog = new FilterFragment(3);
                dialog.show(getSupportFragmentManager(),"Filter");
            }
        });
    }

    public void getRealmData(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        ListFilterStoreRealm listFilterRealm = ListFilterStoreRealm.get();
        if (null!=listFilterRealm && null!=listFilterRealm.getFilterByBuildingId(selectedBuilding.getId())){
            FilterStoreRealm filterRealm = listFilterRealm.getFilterByBuildingId(selectedBuilding.getId());
            if (null!=filterRealm.getSelectedFilterContractor())
            {
                selectedCategory = filterRealm.getSelectedFilterContractor();
                addTag(filterRealm.getSelectedFilterContractorName(),false);

            }

        }
        //  SHOW CONTRACTOR LIST
        showContractorList();
    }

    public void addTag(String name,boolean save){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

        btnTagFilter.setText(name);
        btnTagFilter.setVisibility(View.VISIBLE);
        btnTagFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnTagFilter.setVisibility(View.GONE);
                setSelectedCategory("all");
                showContractorList();
                ListFilterStoreRealm temp2 = ListFilterStoreRealm.get();
                if (null!=temp2){
                    if (temp2.getFilterByBuildingId(selectedBuilding.getId()) != null){
                        temp2.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterContractor(null);
                    }
                }
                ListFilterStoreRealm.save(temp2);
            }
        });
        //save Realm
        if (save){
            ListFilterStoreRealm temp = ListFilterStoreRealm.get();
            FilterStoreRealm newFilter = new FilterStoreRealm();
            newFilter.setBuildingId(selectedBuilding.getId());
            newFilter.setSelectedFilterContractor(selectedCategory);
            newFilter.setSelectedFilterContractorName(name);
            if (null!=temp){
                if (temp.getFilterByBuildingId(selectedBuilding.getId()) != null){
                    temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterContractor(selectedCategory);
                    temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterContractorName(name);
                }else {
                    temp.addNewFilter(newFilter);
                }

            }else{
                temp = new ListFilterStoreRealm();
                temp.addNewFilter(newFilter);

            }
            ListFilterStoreRealm.save(temp);
        }
    }

    private class ContractorSelectedListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            ContractorInfoCollector.setSelectedContractorIndex(v.getId());
            goToContractorDetail();
        }
    }

    private class SearchContractorListener implements View.OnKeyListener, TextWatcher {

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                showContractorList();
                return true;
            }
            return false;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (edtSearchContractor.getText().toString().trim().length() == 0) {
                showContractorList();
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }

    public static String getSelectedCategory() {
        return selectedCategory;
    }

    public static void setSelectedCategory(String selectedCategory) {
        ContractorListActivity.selectedCategory = selectedCategory;
    }

    public void removeTag(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        btnTagFilter.setVisibility(View.GONE);
        ListFilterStoreRealm temp2 = ListFilterStoreRealm.get();
        if (null!=temp2){
            if (temp2.getFilterByBuildingId(selectedBuilding.getId()) != null){
                temp2.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterContractor(null);
            }
        }
        ListFilterStoreRealm.save(temp2);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(ContractorListActivity.this,DashboardActivity.class);
        startActivity(intent);
    }
}
