package com.mybos.bmapp.Activity.Keys.Activities.KeyList;

import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.View;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyListAdapter;
import com.mybos.bmapp.R;

public class KeyListActivity extends a_base_toolbar {

    TabLayout tblKeyList;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_list);
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(KeyListActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });
        updateToolbarTitle("Keys");

        viewPager = findViewById(R.id.viewPagerKey);
        tblKeyList = findViewById(R.id.topTabBarKeyList);
        viewPager.setAdapter(new KeyListAdapter(this, getSupportFragmentManager()));
        tblKeyList.setupWithViewPager(viewPager);

    }
}
