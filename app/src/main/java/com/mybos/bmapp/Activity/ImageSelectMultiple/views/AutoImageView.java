package com.mybos.bmapp.Activity.ImageSelectMultiple.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.View.MeasureSpec;
import android.widget.ImageView;

public class AutoImageView extends ImageView {
    private boolean adjustViewBounds;

    public AutoImageView(Context context) {
        super(context);
    }

    public AutoImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setAdjustViewBounds(boolean adjustViewBounds) {
        this.adjustViewBounds = adjustViewBounds;
        super.setAdjustViewBounds(adjustViewBounds);
    }

    private boolean isInScrollingView() {
        for(ViewParent viewParent = this.getParent(); viewParent != null && viewParent instanceof ViewGroup; viewParent = viewParent.getParent()) {
            if (((ViewGroup)viewParent).shouldDelayChildPressedState()) {
                return true;
            }
        }

        return false;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        Drawable drawable = this.getDrawable();
        if (drawable == null) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        } else {
            if (this.adjustViewBounds) {
                int drawableWidth = drawable.getIntrinsicWidth();
                int drawableHeight = drawable.getIntrinsicHeight();
                int heightSize = MeasureSpec.getSize(heightMeasureSpec);
                int widthSize = MeasureSpec.getSize(widthMeasureSpec);
                int heightMode = MeasureSpec.getMode(heightMeasureSpec);
                int widthMode = MeasureSpec.getMode(widthMeasureSpec);
                int height;
                if (heightMode == 1073741824 && widthMode != 1073741824) {
                    height = heightSize * drawableWidth / drawableHeight;
                    if (this.isInScrollingView()) {
                        this.setMeasuredDimension(height, heightSize);
                    } else {
                        this.setMeasuredDimension(Math.min(height, widthSize), Math.min(heightSize, heightSize));
                    }
                } else if (widthMode == 1073741824 && heightMode != 1073741824) {
                    height = widthSize * drawableHeight / drawableWidth;
                    if (this.isInScrollingView()) {
                        this.setMeasuredDimension(widthSize, height);
                    } else {
                        this.setMeasuredDimension(Math.min(widthSize, widthSize), Math.min(height, heightSize));
                    }
                } else {
                    super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                }
            } else {
                super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            }

        }
    }
}
