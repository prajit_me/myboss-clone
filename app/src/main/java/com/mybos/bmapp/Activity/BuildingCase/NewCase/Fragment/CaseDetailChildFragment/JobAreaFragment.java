package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.CaseDetailChildFragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mybos.bmapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class JobAreaFragment extends Fragment {


    public JobAreaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.a_new_building_case_f_case_detail_f_job_area, container, false);
    }

}
