package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Calendar.Activities.CalendarReminderInfoActivity;
import com.mybos.bmapp.Data.Model.Calendar.DaysEvent;
import com.mybos.bmapp.Data.Model.Calendar.Reminder;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

public class ReminderListAdapter extends BaseAdapter {

    private String date;
    private Context context;
    public static final String REMINDER_ID = "reminder_id";

    public ReminderListAdapter(Context context, String date){
        this.date = date;
        this.context = context;
    }



    @Override
    public int getCount() {
        return DaysEvent.getDayEFromDateOfDayEs(date).getReminder().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.calendar_a_reminder, null);

        Reminder reminder =DaysEvent.getDayEFromDateOfDayEs(date).getReminder().get(i);

        TextView tvNameReminder = view.findViewById(R.id.tvNameReminder);
        TextView tvTimeReminder = view.findViewById(R.id.tvTimeReminder);
        tvNameReminder.setText(reminder.getTitle());
        tvTimeReminder.setText(DateUtils.changeTimeformat2(reminder.getTime()));

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, CalendarReminderInfoActivity.class);
                intent.putExtra(REMINDER_ID,reminder.getId());
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });

        return view;
    }
}
