package com.mybos.bmapp.Activity.Keys.Activities.SignIn;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;
import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Keys.Activities.KeyContractorListActivity;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Key.History;
import com.mybos.bmapp.Data.Model.Key.KeyInOut;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.io.ByteArrayOutputStream;

public class KeySignInActivity extends a_base_toolbar {

    TextView tvSubTitleSignIn;
    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    TextView tvCompanySignIn, tvNameSignIn, tvMobileSignIn,
            tvReasonSignIn, tvDateSignIn, tvDueSignIn;
    ImageView ivSignatureSignIn;
    Button btnSignIn;
    private static Bitmap signatureBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_sign_in);
        updateToolbarTitle("Sign in key");

        tvSubTitleSignIn = findViewById(R.id.tvSubTitleSignIn);
        tvSubTitleSignIn.setText(getIntent().getStringExtra("sub_title"));

        tvCompanySignIn = findViewById(R.id.tvCompanySignIn);
        tvNameSignIn = findViewById(R.id.tvNameSignIn);
        tvMobileSignIn = findViewById(R.id.tvMobileSignIn);
        tvReasonSignIn = findViewById(R.id.tvReasonSignIn);
        tvDateSignIn = findViewById(R.id.tvDateSignIn);
        tvDueSignIn = findViewById(R.id.tvDueSignIn);
        ivSignatureSignIn = findViewById(R.id.ivSignatureSignIn);
        btnSignIn = findViewById(R.id.btnSignIn);

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                controller.postSignIn(KeySignInActivity.this, selectedBuilding.getId(),
                        KeyInOut.getKeyIdSelected(), buildParam(),
                        state -> {

                            Log.e("postSignOut", state);
                            Log.e("postSignOut_key", KeyInOut.getKeyIdSelected());
                            finish();
                        },
                        error -> {
                            if (Offlinemode.checkNetworkConnection(KeySignInActivity.this)){
                                Log.e("postSignIn",error.toString());
                            }else {
                                Offlinemode.showWhenOfflineMode(KeySignInActivity.this);
                            }
                        }
                );
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        signatureBitmap = null;
        displayLoadingScreen();
        Log.e("url", History.getKeyIdSelected());
        controller.getKeyHistory(KeySignInActivity.this, selectedBuilding.getId(),
                history -> {
                    tvNameSignIn.setText(History.getHistories().get(0).getContact());
                    tvCompanySignIn.setText(History.getHistories().get(0).getCompany().equals("null") ?
                            "" : History.getHistories().get(0).getCompany());
                    tvMobileSignIn.setText(History.getHistories().get(0).getNumber().equals("null") ?
                            "" : History.getHistories().get(0).getNumber());
                    tvReasonSignIn.setText(History.getHistories().get(0).getReason().equals("null") ?
                            "" : History.getHistories().get(0).getReason());
                    tvDateSignIn.setText(History.getHistories().get(0).getExpiry().equals("null") ?
                            "" : DateUtils.editDateTime(History.getHistories().get(0).getExpiry()));
                    tvDueSignIn.setText(History.getHistories().get(0).getDue().equals("null") ?
                            "" : DateUtils.editDateTime(History.getHistories().get(0).getDue()));
                    setImageSignature(History.getHistories().get(0).getSignOut().getURL());
                },
                error -> {
                    dismissLoadingScreen();
                    if (Offlinemode.checkNetworkConnection(KeySignInActivity.this)){
                        Log.e("getKeyHistory",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(KeySignInActivity.this);
                    }
                });


    }

    private void setImageSignature(String url){
        ImageRequest imageRequest = new ImageRequest(url, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap response) {
                ivSignatureSignIn.setImageBitmap(response);
                signatureBitmap = response;
                dismissLoadingScreen();
            }
        }, 0, 0, null, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dismissLoadingScreen();
                //cant found Signature
                signatureBitmap = BitmapFactory.decodeResource(KeySignInActivity.this.getResources(),R.drawable.signature_empty);
                ivSignatureSignIn.setImageBitmap(signatureBitmap);
                Log.e("ImageSignature","not found");
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(imageRequest);
    }

    public String buildParam() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("{\"sign\":\"data:image/png;base64,");
        //build image base64
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        signatureBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        String image64 = Base64.encodeToString(byteArray, Base64.NO_WRAP);

        stringBuffer.append(image64);
        stringBuffer.append("\"}");
        return stringBuffer.toString();
    }

}
