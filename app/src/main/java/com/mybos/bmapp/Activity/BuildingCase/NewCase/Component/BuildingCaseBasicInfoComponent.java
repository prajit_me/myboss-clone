package com.mybos.bmapp.Activity.BuildingCase.NewCase.Component;


import android.app.DatePickerDialog;
import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleSpinnerAdapter;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseType;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseTypeList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by EmLaAi on 31/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseBasicInfoComponent extends ConstraintLayout{
    TextView tvCreateDate,tvDueDate;
    Spinner dropdownCaseType,dropdownCasePriority;

    public BuildingCaseBasicInfoComponent(Context context) {
        super(context);
        additionalInit();
    }

    public BuildingCaseBasicInfoComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public BuildingCaseBasicInfoComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_new_case_f_case_detail_c_case_basic,this);
        tvCreateDate = findViewById(R.id.tvDateCreate);
        tvDueDate = findViewById(R.id.tvDateDue);
        dropdownCaseType = findViewById(R.id.dropdownCaseType);
        dropdownCasePriority = findViewById(R.id.dropdownPriority);

    }

    public void setupTypeDropDown(BuildingCaseTypeList typeList, int selectedId,TypeListDropDownSeletedListener listener){
        SimpleTitleSpinnerAdapter adapter = new SimpleTitleSpinnerAdapter(getContext(),typeList);
        dropdownCaseType.setAdapter(adapter);
        dropdownCaseType.setSelection(typeList.getSelectedType(selectedId));
        dropdownCaseType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BuildingCaseType type = (BuildingCaseType) dropdownCaseType.getAdapter().getItem(position);
                listener.didSeletedItem(type);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setupPriorityDropDown(int selectedPriority,PriorityDropDownSeletedListener listener){
        List<String> priorityList = new ArrayList<>(Arrays.asList(
                getContext().getString(R.string.new_case_priority_low),
                getContext().getString(R.string.new_case_priority_medium),
                getContext().getString(R.string.new_case_priority_high)));
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(),R.layout.spinner_title_only,R.id.tvTitle,priorityList);
        dropdownCasePriority.setAdapter(adapter);
        dropdownCasePriority.setSelection(selectedPriority >= 0 && selectedPriority < 3 ? selectedPriority : 0);
        dropdownCasePriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listener.didSelectedPriority(position);
                if (view != null) {
                    ((TextView) view.findViewById(R.id.tvTitle)).setTextColor(getContext().getResources().getColor(R.color.text_gray_light));
                    ((TextView) view.findViewById(R.id.tvTitle)).setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setupCreateDateCalendar(Date date,CreateDateSeletedListener listener){
        tvCreateDate.setText(DateUtils.getDateString(date));
        tvCreateDate.setOnClickListener(view->{
            Calendar c = Calendar.getInstance();
            DatePickerDialog dialog = new DatePickerDialog(getContext(),R.style.DatePickerTheme,(view1, year, month, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,month,dayOfMonth);
                Date updateDate = calendar.getTime();
                tvCreateDate.setText(DateUtils.getDateString(updateDate));
                listener.didChooseCreateDate(updateDate);
            },c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
            dialog.show();
        });
    }

    public void setupDueDateCalendar(Date date,DueDateSeletedListener listener){
        tvDueDate.setText(DateUtils.getDateString(date));
        tvDueDate.setOnClickListener(view->{
            Calendar c = Calendar.getInstance();
            DatePickerDialog dialog = new DatePickerDialog(getContext(),R.style.DatePickerTheme,(view1, year, month, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,month,dayOfMonth);
                Date updateDate = calendar.getTime();
                tvDueDate.setText(DateUtils.getDateString(updateDate));
                listener.didChooseDueDate(updateDate);
            },c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
            dialog.show();
        });
    }

    public interface TypeListDropDownSeletedListener{
        void didSeletedItem(BuildingCaseType type);
    }
    public interface PriorityDropDownSeletedListener{
        void didSelectedPriority(int priority);
    }

    public interface CreateDateSeletedListener{
        void didChooseCreateDate(Date date);
    }

    public interface DueDateSeletedListener{
        void didChooseDueDate(Date date);
    }
}
