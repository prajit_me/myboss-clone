package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Activity.Keys.Fragment.ContractorKey.ContractorListFragment;
import com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.PrivateKeyListFragment;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.R;

import java.util.Arrays;
import java.util.List;

public class KeyListAdapter extends FragmentPagerAdapter {

    private @SearchTableQueryBuilder.SectionHint.KeySectionHint
    List<Integer> sections = Arrays.asList(SearchTableQueryBuilder.SectionHint.KeyPrivate,
            SearchTableQueryBuilder.SectionHint.KeyContractor);
    private Context context;
    private PrivateKeyListFragment fmPrivate;
    private ContractorListFragment fmContractor;

    public KeyListAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
        fmPrivate =  new PrivateKeyListFragment();
        fmContractor = new ContractorListFragment();
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return fmPrivate;
        }else if(position == 1){
            return fmContractor;
        }
        return null;
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        int section = sections.get(position);
        if (section == SearchTableQueryBuilder.SectionHint.KeyPrivate){
            return context.getString(R.string.key_list_section_private);
        }
        if (section == SearchTableQueryBuilder.SectionHint.KeyContractor){
            return context.getString(R.string.key_list_section_contractor);
        }
        return super.getPageTitle(position);
    }
}
