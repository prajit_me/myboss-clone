package com.mybos.bmapp.Activity.ImageSelectMultiple.utils;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class Image implements Parcelable {
    public long _id;
    public Uri uri;
    public String imagePath;
    public boolean isPortraitImage;
    public static final Creator<Image> CREATOR = new Creator<Image>() {
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }

        public Image[] newArray(int size) {
            return new Image[size];
        }
    };

    public Image(long _id, Uri uri, String imagePath, boolean isPortraitImage) {
        this._id = _id;
        this.uri = uri;
        this.imagePath = imagePath;
        this.isPortraitImage = isPortraitImage;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeParcelable(this.uri, flags);
        dest.writeString(this.imagePath);
        dest.writeByte((byte)(this.isPortraitImage ? 1 : 0));
    }

    protected Image(Parcel in) {
        this._id = in.readLong();
        this.uri = (Uri)in.readParcelable(Uri.class.getClassLoader());
        this.imagePath = in.readString();
        this.isPortraitImage = in.readByte() != 0;
    }

    public Uri getUri() {
        return uri;
    }
}
