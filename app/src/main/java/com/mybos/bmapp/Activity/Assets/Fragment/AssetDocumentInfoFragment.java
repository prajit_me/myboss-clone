package com.mybos.bmapp.Activity.Assets.Fragment;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.BuildingAssets.AssetFile;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.StringUtils;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssetDocumentInfoFragment extends Fragment {

    private LinearLayout lnlyAssetDocumentContainer;
    private BuildingAsset SelectedAsset = BuildingAssetInfoCollector.getSelectedBuildingAsset();
    private final int MIN_HEIGHT = 80, TEXT_SIZE = 15;
    private final String ORANGE_TEXT_COLOR = "#f19c0f";

    private ImageView getDocumentType(String DocumentType) {
        ImageView ivDocumentType = new ImageView(this.getActivity());
        LinearLayout.LayoutParams lpTypeParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpTypeParams.setMargins(40, 5, 20, 5);
        lpTypeParams.height = 80;
        lpTypeParams.width = 70;
        ivDocumentType.setLayoutParams(lpTypeParams);
        ivDocumentType.setMinimumHeight(MIN_HEIGHT);
        ivDocumentType.setMinimumWidth(MIN_HEIGHT);


        if (DocumentType.contains("pdf")) {
            ivDocumentType.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ic_pdf));
        }
        else {
            ivDocumentType.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ic_doc));
        }

        return ivDocumentType;
    }

    private TextView getDocumentName(String DocumentName) {
        TextView tvDocumentName = new TextView(this.getActivity());

        LinearLayout.LayoutParams lpNameParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpNameParams.setMargins(20, 10, 5, 0);
        tvDocumentName.setLayoutParams(lpNameParams);

        tvDocumentName.setGravity(Gravity.CENTER|Gravity.START);
        tvDocumentName.setMinHeight(MIN_HEIGHT);
        tvDocumentName.setText(DocumentName);
        tvDocumentName.setTextSize(TEXT_SIZE);
        tvDocumentName.setTextColor(Color.BLACK);
        tvDocumentName.setTypeface(tvDocumentName.getTypeface(), Typeface.BOLD);

        return tvDocumentName;
    }

    private LinearLayout getAddedDate(String AddedDate) {
        LinearLayout lnlyAddedDate = new LinearLayout(this.getActivity());
        lnlyAddedDate.setOrientation(LinearLayout.HORIZONTAL);

        //  ADDED LABEL
        TextView tvAddedLabel = new TextView(this.getActivity());
        LinearLayout.LayoutParams lpDateLabelParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpDateLabelParams.setMargins(20, 20, 5, 5);
        tvAddedLabel.setLayoutParams(lpDateLabelParams);
        tvAddedLabel.setMinHeight(MIN_HEIGHT);
        tvAddedLabel.setText("Updated: ");
        tvAddedLabel.setTextSize(TEXT_SIZE);
        tvAddedLabel.setTextColor(Color.LTGRAY);

        //  ADDED DATE
        TextView tvAddedDate = new TextView(this.getActivity());
        LinearLayout.LayoutParams lpAddedDateParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpAddedDateParams.setMargins(5, 20, 5, 5);
        tvAddedDate.setLayoutParams(lpAddedDateParams);
        tvAddedDate.setMinHeight(MIN_HEIGHT);
        tvAddedDate.setText(StringUtils.getDate(AddedDate.substring(0, 10)));
        tvAddedDate.setTextSize(TEXT_SIZE);
        tvAddedDate.setTextColor(Color.parseColor(ORANGE_TEXT_COLOR));
        tvAddedDate.setTypeface(tvAddedDate.getTypeface(), Typeface.BOLD);

        lnlyAddedDate.addView(tvAddedLabel);
        lnlyAddedDate.addView(tvAddedDate);

        return lnlyAddedDate;
    }

    private LinearLayout getDocumentNameAndDate(String DocumentName, String DocumentDate) {
        LinearLayout lnlyNameAndDate = new LinearLayout(this.getActivity());
        LinearLayout.LayoutParams lpDocumentNameAndDateParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lpDocumentNameAndDateParams.setMargins(5, 5, 5, 5);
        lnlyNameAndDate.setLayoutParams(lpDocumentNameAndDateParams);
        lnlyNameAndDate.setOrientation(LinearLayout.VERTICAL);

        lnlyNameAndDate.addView(getDocumentName(DocumentName));
        lnlyNameAndDate.addView(getAddedDate(DocumentDate));

        return lnlyNameAndDate;
    }

    private LinearLayout getDocumentInfo(AssetFile CurrentAssetFile) {
        LinearLayout lnlyDocumentInfo = new LinearLayout(this.getActivity());
        lnlyDocumentInfo.setOrientation(LinearLayout.HORIZONTAL);
        lnlyDocumentInfo.setGravity(Gravity.CENTER|Gravity.START);

        String FileName = CurrentAssetFile.getAssetFileName();
        lnlyDocumentInfo.addView(getDocumentType(FileName.substring(FileName.indexOf("."))));
        lnlyDocumentInfo.addView(getDocumentNameAndDate(FileName.substring(0, FileName.indexOf(".")), CurrentAssetFile.getAdded()));

        return lnlyDocumentInfo;
    }

    private ImageView getViewButton() {
        ImageView ivView = new ImageView(this.getActivity());
        LinearLayout.LayoutParams btnViewParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ivView.setLayoutParams(btnViewParams);
        ivView.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.ic_next));

        return ivView;
    }

    private void linkToAssetFile(String FileURL) {
        Intent InsuranceIntent = new Intent(Intent.ACTION_VIEW);
        InsuranceIntent.setData(Uri.parse(FileURL));
        startActivity(InsuranceIntent);
    }

    private RelativeLayout getDocumentListCell(int CellID, AssetFile CurrentAssetFile) {
        RelativeLayout DocumentListCell = new RelativeLayout(this.getActivity());
        RelativeLayout.LayoutParams DocumentListCellParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        DocumentListCellParams.setMargins(5, 10, 5, 10);
        DocumentListCell.setLayoutParams(DocumentListCellParams);
        DocumentListCell.setBackground(Objects.requireNonNull(getActivity()).getDrawable(R.drawable.bg_corner_radius_8));
        DocumentListCell.setGravity(Gravity.CENTER|Gravity.START);
        DocumentListCell.setId(CellID);
        DocumentListCell.setOnClickListener(new AssetFileSelectionListener());

        LinearLayout lnlyDocumentInfo = getDocumentInfo(CurrentAssetFile);
        RelativeLayout.LayoutParams lnlyDocumentInfoParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lnlyDocumentInfoParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        lnlyDocumentInfo.setLayoutParams(lnlyDocumentInfoParams);

        ImageView ivView = getViewButton();
        RelativeLayout.LayoutParams ivViewParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        ivViewParams.setMargins(10,10,20,10);
        ivViewParams.addRule(RelativeLayout.CENTER_VERTICAL);
        ivViewParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        ivView.setLayoutParams(ivViewParams);

        DocumentListCell.addView(lnlyDocumentInfo);
        DocumentListCell.addView(ivView);

        return DocumentListCell;
    }

    private TextView getNoDocumentFile() {
        TextView tvNoDocumentFile = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvNoDocumentFileParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvNoDocumentFileParams.setMargins(5, 5, 5, 5);
        tvNoDocumentFile.setLayoutParams(tvNoDocumentFileParams);

        tvNoDocumentFile.setGravity(Gravity.CENTER);
        tvNoDocumentFile.setMinHeight(MIN_HEIGHT * 2);
        tvNoDocumentFile.setText("No document was found.");
        tvNoDocumentFile.setTextSize(TEXT_SIZE);
        tvNoDocumentFile.setTextColor(Color.BLACK);
        tvNoDocumentFile.setTypeface(tvNoDocumentFile.getTypeface(), Typeface.ITALIC);

        return tvNoDocumentFile;
    }

    private void getDocumentList() {
        int Size = SelectedAsset.getDocuments().size();
        if (Size > 0) {
            for (int i = 0; i < Size; i++) {
                AssetFile CurrentAssetFile = SelectedAsset.getDocuments().get(i);
                lnlyAssetDocumentContainer.addView(getDocumentListCell(i, CurrentAssetFile));
            }
        }
        else {
            lnlyAssetDocumentContainer.addView(getNoDocumentFile());
        }
    }

    private void startup() {
        getDocumentList();
    }

    public AssetDocumentInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_asset_document_info, container, false);

        //  GET REFERENCES
        lnlyAssetDocumentContainer = rootView.findViewById(R.id.lnlyAssetDocumentContainer_AssetDocumentInfoFragment);

        //  START UP
        startup();

        return rootView;
    }

    private class AssetFileSelectionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            int FileID = v.getId();
            linkToAssetFile(SelectedAsset.getDocuments().get(FileID).getURL());
        }
    }

}
