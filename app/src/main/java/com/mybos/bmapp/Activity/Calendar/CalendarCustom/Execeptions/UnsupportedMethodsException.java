package com.mybos.bmapp.Activity.Calendar.CalendarCustom.Execeptions;


public class UnsupportedMethodsException extends RuntimeException {
    public UnsupportedMethodsException(String message) {
        super(message);
    }
}
