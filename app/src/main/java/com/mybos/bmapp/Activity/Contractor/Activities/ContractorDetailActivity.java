package com.mybos.bmapp.Activity.Contractor.Activities;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.*;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Activity.Contractor.Fragments.*;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.StringUtils;

public class ContractorDetailActivity extends a_base_toolbar {

    private Contractor SelectedContractor = ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex());
    private LinearLayout lnlyContractorInfoContainer;
    private TextView tvGeneral, tvInsurance, tvDocument, tvHistory;
    private TextView tvGeneralIndicator, tvInsuranceIndicator, tvDocumentIndicator, tvHistoryIndicator;
    
    private void getReferences(){
        lnlyContractorInfoContainer = findViewById(R.id.lnlyContractorInfoContainer);
        
        tvGeneral = findViewById(R.id.tvContractorGeneralInfo);
        tvGeneralIndicator = findViewById(R.id.tvGeneralIndicator);

        tvInsurance = findViewById(R.id.tvContractorInsuranceInfo);
        tvInsuranceIndicator = findViewById(R.id.tvInsuranceIndicator);

        tvDocument = findViewById(R.id.tvContractorDocumentInfo);
        tvDocumentIndicator = findViewById(R.id.tvDocumentIndicator);

        tvHistory = findViewById(R.id.tvContractorHistoryInfo);
        tvHistoryIndicator = findViewById(R.id.tvHistoryIndicator);
    }

    private void setupButtons(){
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 4;
        
        tvGeneral.setWidth(width);
        tvInsurance.setWidth(width);
        tvDocument.setWidth(width);
        tvHistory.setWidth(width);
    }

    private void hideIndicator(){
        tvGeneralIndicator.setVisibility(View.GONE);
        tvInsuranceIndicator.setVisibility(View.GONE);
        tvDocumentIndicator.setVisibility(View.GONE);
        tvHistoryIndicator.setVisibility(View.GONE);
    }

    private void showGeneralInfoFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyContractorInfoContainer, new ContractorGeneralInfoFragment())
                .commit();
    }

    private void showGeneralSelected(){
        hideIndicator();

        tvGeneralIndicator.setVisibility(View.VISIBLE);
        showGeneralInfoFragment();
    }

    private void showInsuranceInfoFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyContractorInfoContainer, new ContractorInsuranceInfoFragment())
                .commit();
    }

    private void showInsuranceSelected(){
        hideIndicator();

        tvInsuranceIndicator.setVisibility(View.VISIBLE);
        showInsuranceInfoFragment();
    }

    private void showDocumentInfoFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyContractorInfoContainer, new ContractorDocumentInfoFragment())
                .commit();
    }

    private void showDocumentSelected(){
        hideIndicator();

        tvDocumentIndicator.setVisibility(View.VISIBLE);
        showDocumentInfoFragment();
    }

    private void showHistoryInfoFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyContractorInfoContainer, new ContractorHistoryInfoFragment())
                .commit();
    }

    private void showHistorySelected(){
        hideIndicator();

        tvHistoryIndicator.setVisibility(View.VISIBLE);
        showHistoryInfoFragment();
    }

    private void setListener() {
        InfoSelectionListener SelectionListener = new InfoSelectionListener();
        tvGeneral.setOnClickListener(SelectionListener);
        tvInsurance.setOnClickListener(SelectionListener);
        tvDocument.setOnClickListener(SelectionListener);
        tvHistory.setOnClickListener(SelectionListener);
    }

    private void startup() {
        //  SET TITLE
        super.updateToolbarTitle(SelectedContractor.getCompany());

        //  GET REFERENCES
        getReferences();

        //  SET UP UI
        setupButtons();
        hideIndicator();

        //  SET LISTENER
        setListener();

        //  SHOW GENERAL INFO BY DEFAULT
        showGeneralSelected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_detail);

        //  START UP
        startup();
    }

    private class InfoSelectionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvContractorGeneralInfo:
                    showGeneralSelected();
                    break;
                case R.id.tvContractorInsuranceInfo:
                    showInsuranceSelected();
                    break;
                case R.id.tvContractorDocumentInfo:
                    showDocumentSelected();
                    break;
                case R.id.tvContractorHistoryInfo:
                    showHistorySelected();
                    break;
                default:
                    showGeneralSelected();
                    break;
            }
        }
    }
}
