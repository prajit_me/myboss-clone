package com.mybos.bmapp.Activity.Assets.Fragment;


import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mybos.bmapp.Data.Model.BuildingAssets.AssetFile;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssetPhotoInfoFragment extends Fragment {

    private LinearLayout lnlyPhotoContainer;
    private BuildingAsset SelectedAsset = BuildingAssetInfoCollector.getSelectedBuildingAsset();
    private final int TEXT_SIZE = 15, MIN_HEIGHT = 120;

    private ImageView getPhoto(String PhotoURL) {
        ImageView ivPhoto = new ImageView(this.getActivity());
        LinearLayout.LayoutParams ivPhotoParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        ivPhotoParams.setMargins(10, 10, 10, 10);
        ivPhoto.setLayoutParams(ivPhotoParams);

        Uri PhotoUri = Uri.parse(PhotoURL);
        Glide.with(this.getActivity()).load(PhotoUri).into(ivPhoto);

        return ivPhoto;
    }

    private TextView getNoPhoto() {
        TextView tvNoPhotoFile = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvNoPhotoFileParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvNoPhotoFileParams.setMargins(5, 5, 5, 5);
        tvNoPhotoFile.setLayoutParams(tvNoPhotoFileParams);

        tvNoPhotoFile.setGravity(Gravity.CENTER);
        tvNoPhotoFile.setMinHeight(MIN_HEIGHT * 2);
        tvNoPhotoFile.setText("No photo was found.");
        tvNoPhotoFile.setTextSize(TEXT_SIZE);
        tvNoPhotoFile.setTextColor(Color.BLACK);
        tvNoPhotoFile.setTypeface(tvNoPhotoFile.getTypeface(), Typeface.ITALIC);

        return tvNoPhotoFile;
    }

    private void getPhotoList() {
        int Size = SelectedAsset.getPhotos().size();
        if (Size > 0) {
            for (int i = 0; i < Size; i++) {
                AssetFile CurrentPhoto = SelectedAsset.getPhotos().get(i);
                lnlyPhotoContainer.addView(getPhoto(CurrentPhoto.getURL()));
            }
        }
        else {
            lnlyPhotoContainer.addView(getNoPhoto());
        }
    }

    private void startup() {
        getPhotoList();
    }

    public AssetPhotoInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_asset_photo_info, container, false);

        //  GET REFERENCES
        lnlyPhotoContainer = rootView.findViewById(R.id.lnlyAssetPhotoContainer_AssetPhotoInfoFragment);

        //  START UP
        startup();

        return rootView;
    }

}
