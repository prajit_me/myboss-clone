package com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Adapter;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Fragment.EmailDetailFragment;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;

/**
 * Created by EmLaAi on 14/04/2018.
 *
 * @author EmLaAi
 */
public class EmailContractorDetailPagerAdapter extends FragmentStatePagerAdapter {
    private ContractorList contractorList;
    private String caseNumber;
    private DeleteContractor listener;
    public EmailContractorDetailPagerAdapter(FragmentManager fragmentManager, ContractorList contractorList , String caseNumber){
        super(fragmentManager);
        this.contractorList = contractorList;
        this.caseNumber = caseNumber;
    }






    @Override
    public int getCount() {
        return contractorList.getSelectedId().size();
//        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        Contractor contractor = contractorList.getSelectedItemAt(position);
        EmailDetailFragment fragment = EmailDetailFragment.createInstance(caseNumber);
        fragment.setContractor(contractor);
        fragment.setRemoveListener(deleteContractor -> {
            contractorList.deselectId(deleteContractor.getId());
            if (null != listener){
                listener.makeDelete();
            }else {
                notifyDataSetChanged();
            }
        });
        return fragment;
    }


    public void setListener(DeleteContractor listener) {
        this.listener = listener;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }



    public void addSelectContractor(Contractor contractor){
        contractorList.selectId(contractor.getId());
        notifyDataSetChanged();
    }
    public interface DeleteContractor{
        void makeDelete();
    }
}
