package com.mybos.bmapp.Activity.Assets.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.*;

import com.mybos.bmapp.Activity.Assets.Fragment.AssetDocumentInfoFragment;
import com.mybos.bmapp.Activity.Assets.Fragment.AssetGeneralInfoFragment;
import com.mybos.bmapp.Activity.Assets.Fragment.AssetHistoryInfoFragment;
import com.mybos.bmapp.Activity.Assets.Fragment.AssetPhotoInfoFragment;
import com.mybos.bmapp.Activity.Assets.Fragment.AssetWarrantyInfoFragment;
import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Data.Mapper.Assets.AssetMapper;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;

public class BuildingAssetDetailActivity extends a_base_toolbar {

    private TextView tvGeneral, tvDocument, tvWarranty, tvHistory, tvPhotos;
    private TextView tvGeneralIndicator, tvDocumentIndicator, tvWarrantyIndicator, tvHistoryIndicator, tvPhotosIndicator;
    private LinearLayout lnlyAssetInfoContainer;
    private AssetMapper assetMapper = new AssetMapper();
    
    private void getReferences() {
        tvGeneral = findViewById(R.id.tvAssetGeneralInfo_BuildingAssetDetailActivity);
        tvGeneralIndicator = findViewById(R.id.tvGeneralIndicator_BuildingAssetDetailActivity);

        tvDocument = findViewById(R.id.tvAssetDocumentInfo_BuildingAssetDetailActivity);
        tvDocumentIndicator = findViewById(R.id.tvDocumentIndicator_BuildingAssetDetailActivity);

        tvWarranty = findViewById(R.id.tvAssetWarrantyInfo_BuildingAssetDetailActivity);
        tvWarrantyIndicator = findViewById(R.id.tvWarrantyIndicator_BuildingAssetDetailActivity);

        tvHistory = findViewById(R.id.tvAssetHistoryInfo_BuildingAssetDetailActivity);
        tvHistoryIndicator = findViewById(R.id.tvHistoryIndicator_BuildingAssetDetailActivity);

        tvPhotos = findViewById(R.id.tvAssetPhotosInfo_BuildingAssetDetailActivity);
        tvPhotosIndicator = findViewById(R.id.tvPhotosIndicator_BuildingAssetDetailActivity);

        lnlyAssetInfoContainer = findViewById(R.id.lnlyAssetInfoContainer_BuildingAssetDetailActivity);
    }

    private void setListener() {
        SectionSelectionListener SectionListener = new SectionSelectionListener();
        tvGeneral.setOnClickListener(SectionListener);
        tvDocument.setOnClickListener(SectionListener);
        tvWarranty.setOnClickListener(SectionListener);
        tvHistory.setOnClickListener(SectionListener);
        tvPhotos.setOnClickListener(SectionListener);
    }

    private void setupSection() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels / 5;

        tvGeneral.setWidth(width);
        tvDocument.setWidth(width);
        tvWarranty.setWidth(width);
        tvHistory.setWidth(width);
        tvPhotos.setWidth(width);
    }

    private void hideIndicator() {
        tvGeneralIndicator.setVisibility(View.GONE);
        tvDocumentIndicator.setVisibility(View.GONE);
        tvWarrantyIndicator.setVisibility(View.GONE);
        tvHistoryIndicator.setVisibility(View.GONE);
        tvPhotosIndicator.setVisibility(View.GONE);
    }
    
    private void showGeneralInfo() {
        hideIndicator();
        tvGeneralIndicator.setVisibility(View.VISIBLE);
        
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyAssetInfoContainer_BuildingAssetDetailActivity, new AssetGeneralInfoFragment())
                .commit();
    }

    private void showDocumentInfo() {
        hideIndicator();
        tvDocumentIndicator.setVisibility(View.VISIBLE);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyAssetInfoContainer_BuildingAssetDetailActivity, new AssetDocumentInfoFragment())
                .commit();
    }

    private void showWarrantyInfo() {
        hideIndicator();
        tvWarrantyIndicator.setVisibility(View.VISIBLE);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyAssetInfoContainer_BuildingAssetDetailActivity, new AssetWarrantyInfoFragment())
                .commit();
    }

    private void showHistoryInfo() {
        hideIndicator();
        tvHistoryIndicator.setVisibility(View.VISIBLE);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyAssetInfoContainer_BuildingAssetDetailActivity, new AssetHistoryInfoFragment())
                .commit();
    }

    private void showPhotosInfo() {
        hideIndicator();
        tvPhotosIndicator.setVisibility(View.VISIBLE);

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.lnlyAssetInfoContainer_BuildingAssetDetailActivity, new AssetPhotoInfoFragment())
                .commit();
    }

    private void getSelectedAssetInfo() {
        displayLoadingScreen();
        assetMapper.getSpecificBuildingAsset(this,
                BuildingAssetInfoCollector.getSelectedBuildingAsset(),
                response -> {
            BuildingAssetInfoCollector.setSelectedBuildingAsset(response);

                    assetMapper.getSpecificBuildingAssetHistory(this,
                            BuildingAssetInfoCollector.getSelectedBuildingAsset(),
                            HistoryResponse -> {
                                BuildingAssetInfoCollector.setAssetHistoryList(HistoryResponse);
                                showGeneralInfo();
                                dismissLoadingScreen();
                            },
                            error -> {dismissLoadingScreen();});
                },
                error -> {dismissLoadingScreen();});
    }

    private void startup() {
        //  SET TITLE
        this.updateToolbarTitle(BuildingAssetInfoCollector.getSelectedBuildingAsset().getName());

        //  GET REFERENCE
        getReferences();

        //  SET LISTENER
        setListener();

        //  SET UP SECTION
        setupSection();

        //  HIDE INDICATOR
        hideIndicator();

        //  GET SELECTED ASSET INFO
        getSelectedAssetInfo();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_asset_detail);

        setUpOptionBtn();

        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuildingAssetDetailActivity.this,BuildingAssetActivity.class);
                startActivity(intent);
            }
        });

        //  START UP
        startup();
    }

    private void setUpOptionBtn(){
        findViewById(R.id.btnOption).setVisibility(View.VISIBLE);
        findViewById(R.id.btnOption).setBackground(getDrawable(R.drawable.ic_create_black_24dp));
        findViewById(R.id.btnOption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuildingAssetDetailActivity.this, NewAssetActivity.class);
                intent.putExtra(NewAssetActivity.TYPE_EDIT,1);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(BuildingAssetDetailActivity.this,BuildingAssetActivity.class);
        startActivity(intent);

    }

    class SectionSelectionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.tvAssetGeneralInfo_BuildingAssetDetailActivity:
                    showGeneralInfo();
                    break;
                case R.id.tvAssetDocumentInfo_BuildingAssetDetailActivity:
                    showDocumentInfo();
                    break;
                case R.id.tvAssetWarrantyInfo_BuildingAssetDetailActivity:
                    showWarrantyInfo();
                    break;
                case R.id.tvAssetHistoryInfo_BuildingAssetDetailActivity:
                    showHistoryInfo();
                    break;
                case R.id.tvAssetPhotosInfo_BuildingAssetDetailActivity:
                    showPhotosInfo();
                    break;
            }
        }
    }
}
