package com.mybos.bmapp.Activity.Filter;

import android.app.Dialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mybos.bmapp.Activity.Assets.Activity.BuildingAssetActivity;
import com.mybos.bmapp.Activity.Contractor.Activities.ContractorListActivity;
import com.mybos.bmapp.Activity.Resident.ResidentList.ResidentListActivity;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Filter.Filter;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

public class FilterFragment extends AppCompatDialogFragment implements FilterRecycleAdapter.OnItemClickListener {
    //check type need filter
    private int type;// 1 = resident; 2 = assets; 3 = contractors; 4 = case
    private int subType = 0;
    private FilterController controller = new FilterController();
    private Building building = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    private TextView tvTitle;
    private ConstraintLayout containerBtnBack;
    private RecyclerView recycleView;
    private ImageView ivBack;

    public FilterFragment(int type){
        this.type = type;
    }

    public FilterFragment(int type, int subType){
        this.type = type;
        this.subType = subType;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.filter_fragment_dialog,null);
        builder.setView(view);
        setupBtnClear(view);
        setupRecycleView(view);
        return builder.create();
    }

    public void setupRecycleView(View view){
        recycleView = view.findViewById(R.id.recyclerFilter) ;
        ivBack = view.findViewById(R.id.ivBack);
        recycleView.setLayoutManager(new LinearLayoutManager(null));
        FilterRecycleAdapter adapter = new FilterRecycleAdapter(type,subType);
        adapter.setup(recycleView);
        recycleView.setAdapter(adapter);
        if(type == 1){
            //setup RecycleView for resident
            setupRecycleForResident(adapter);
        }
        if(type == 2){
            //setup RecycleView for assets
            tvTitle = view.findViewById(R.id.tvTitle);
            containerBtnBack = view.findViewById(R.id.containerBtnBack);
            containerBtnBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BackFilterPrevious();
                }
            });
            controller.getCategoryTree(getContext(),building.getId(),ok->{
                setupRecycleForAssets(adapter);
            },error -> {});

        }
        if (type == 3){
            controller.getContractorCategoryTree(getContext(),building.getId(),ok->{
                setupRecycleForContractor(adapter);
            },error -> {});
        }
    }

    public void setupBtnClear(View view){
        if (type == 2 && subType != 1){
            //setup filter for Assets
            view.findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
            view.findViewById(R.id.btnClear).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BuildingAssetActivity)getContext()).removeTag();
                    ((BuildingAssetActivity)getContext()).setSelectedCategory("all");
                    ((BuildingAssetActivity)getContext()).setCurrentPage(1);
                    ((BuildingAssetActivity)getContext()).showAssetList();
                    getDialog().dismiss();
                }
            });
        }
        if (type == 3){
            view.findViewById(R.id.btnClear).setVisibility(View.VISIBLE);
            view.findViewById(R.id.btnClear).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((ContractorListActivity)getContext()).removeTag();
                    ContractorListActivity.setSelectedCategory("all");
                    ((ContractorListActivity)getContext()).showContractorList();
                    getDialog().dismiss();
                }
            });
        }

        //for all
        view.findViewById(R.id.btnCancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
    }

    public void setupRecycleForResident(FilterRecycleAdapter adapter){
//        adapter.setOnNeedLoadMoreListener(this::createListFilterForResident);
        adapter.setMaximum(createListFilterForResident().size());
        adapter.updateModel(createListFilterForResident());
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new FilterRecycleAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int pos) {
                getDialog().dismiss();
            }

            @Override
            public void clickIntoItem() {

            }
        });
    }

    public void setupRecycleForAssets(FilterRecycleAdapter adapter){
        adapter.setMaximum(Filter.getAssetsFilterList().size());
        adapter.updateModel(Filter.getAssetsFilterList());
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new FilterRecycleAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int pos) {
                getDialog().dismiss();
            }

            @Override
            public void clickIntoItem() {
                if (FilterRecycleAdapter.currentLv>0){
                    ivBack.setVisibility(View.VISIBLE);
                }
                tvTitle.setText(FilterRecycleAdapter.progressTitle.get(FilterRecycleAdapter.currentLv));

            }
        });
    }

    public void setupRecycleForContractor(FilterRecycleAdapter adapter){
        adapter.setMaximum(Filter.getContractorsFilterList().size());
        adapter.updateModel(Filter.getContractorsFilterList());
        adapter.notifyDataSetChanged();
        adapter.setOnItemClickListener(new FilterRecycleAdapter.OnItemClickListener() {
            @Override
            public void onItemClicked(View view, int pos) {
                getDialog().dismiss();
            }

            @Override
            public void clickIntoItem() {

            }
        });
    }

    public void BackFilterPrevious(){
        //handle when click back
        if(FilterRecycleAdapter.currentLv!=0){
            FilterRecycleAdapter.progressfinder.remove(FilterRecycleAdapter.currentLv);
            FilterRecycleAdapter.progressTitle.remove(FilterRecycleAdapter.currentLv);
            Log.e("check_size",String.valueOf(FilterRecycleAdapter.progressfinder.size())+"=="+FilterRecycleAdapter.currentLv);
            FilterRecycleAdapter.currentLv--;
            tvTitle.setText(FilterRecycleAdapter.progressTitle.get(FilterRecycleAdapter.currentLv));
            FilterRecycleAdapter adapter = (FilterRecycleAdapter) recycleView.getAdapter();
            adapter.reset();
            adapter.setMaximum(FilterRecycleAdapter.progressfinder.get(FilterRecycleAdapter.currentLv).size());
            adapter.updateModel(FilterRecycleAdapter.progressfinder.get(FilterRecycleAdapter.currentLv));
            adapter.notifyDataSetChanged();
            if(FilterRecycleAdapter.currentLv==0){
                ivBack.setVisibility(View.INVISIBLE);
            }
        }
    }

    public ArrayList<Filter> createListFilterForResident(){
        ArrayList<Filter> sourceList = new ArrayList<>();
//        Filter filter1 = new Filter();
        Filter filter2 = new Filter();
        Filter filter3 = new Filter();
//        filter1.setName("All residents");
//        filter1.setId(ResidentListActivity.ALL);
//        sourceList.add(filter1);
        filter2.setName("Owner Occupier");
        filter2.setId(ResidentListActivity.OWNER);
        sourceList.add(filter2);
        filter3.setName("Tenant Occupier");
        filter3.setId(ResidentListActivity.TENANT);
        sourceList.add(filter3);
        return sourceList;
    }

    @Override
    public void onItemClicked(View view, int pos) {
        getDialog().dismiss();
    }

    @Override
    public void clickIntoItem() {

    }
}
