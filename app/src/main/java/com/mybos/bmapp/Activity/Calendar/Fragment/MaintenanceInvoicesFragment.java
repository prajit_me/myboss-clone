package com.mybos.bmapp.Activity.Calendar.Fragment;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mybos.bmapp.Activity.Calendar.Adapter.CalendarListDocAdapter;
import com.mybos.bmapp.R;

public class MaintenanceInvoicesFragment extends Fragment {

    private View view;
    ListView lvCalendarInvoices;

    public MaintenanceInvoicesFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_invoices, container, false);
        this.view = view;

        lvCalendarInvoices = view.findViewById(R.id.lvCalendarInvoices);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        lvCalendarInvoices.setAdapter(new CalendarListDocAdapter(getContext(),"invoices"));
    }
}
