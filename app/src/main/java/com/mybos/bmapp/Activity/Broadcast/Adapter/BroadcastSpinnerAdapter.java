package com.mybos.bmapp.Activity.Broadcast.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Broadcast.Group;
import com.mybos.bmapp.Data.Model.Broadcast.Groups;
import com.mybos.bmapp.R;

import java.util.ArrayList;

public class BroadcastSpinnerAdapter extends BaseAdapter {

    private Context context;
    private int layout;


    public BroadcastSpinnerAdapter(Context context, int layout) {
        this.context = context;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return Groups.getGroups().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflater.inflate(layout,null);
        TextView tvName = view.findViewById(R.id.tvNameGroup);
        tvName.setText(Groups.getGroups().get(i).getName());


        return view;
    }
}
