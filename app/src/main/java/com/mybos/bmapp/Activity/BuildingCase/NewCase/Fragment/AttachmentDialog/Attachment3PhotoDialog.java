package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentDialog;

import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringDef;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.mybos.bmapp.Activity.ImageSelectMultiple.activities.GalleryActivity;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Constants;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Image;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Params;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Error.ImageEncodeError;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.Logger;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class Attachment3PhotoDialog extends DialogFragment {
    private static final String CONTRACTOR_KEY = "AttachmentUploadDialog.CONTRACTOR_KEY";
    private static final String DISPLAY_MODE_KEY = "AttachmentUploadDialog.DISPLAY_MODE_KEY";
    private static final int PHOTO_LIBRARY_REQUEST_CODE = 1;
    private static final int PHOTO_TAKE_REQUEST_CODE = 2;
    public static final String PHOTO_MODE = "PHOTO_MODE";
    public static final String QUOTE_MODE = "QUOTE_MODE";
    public static final String INVOICE_MODE = "INVOICE_MODE";
    ImageView imageView1;
    ImageView imageView2;
    ImageView imageView3;
    boolean setImage1,setImage2,setImage3;
    @Mode String mode = PHOTO_MODE;
    Attachment3PhotoDialog.AttachmentUploadPhotosDialogListener listener;
    Button btnTakePhoto, btnAddPhoto, btnSave, btnCancel;
    PhotoModel selectPhoto1,selectPhoto2,selectPhoto3;

    Uri currentUri;
    String currentPhotoPath;
    static int maxImage;

    @StringDef({PHOTO_MODE,QUOTE_MODE,INVOICE_MODE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Mode{}
    public Attachment3PhotoDialog(){}

    public static Attachment3PhotoDialog createInstance(ContractorList contractorList, String mode){
        Attachment3PhotoDialog dialog = new Attachment3PhotoDialog();
        Bundle arg = new Bundle();
        arg.putParcelable(CONTRACTOR_KEY,contractorList);
        arg.putString(DISPLAY_MODE_KEY,mode);
        dialog.setArguments(arg);
        return dialog;
    }

    public static void setMaxImage(int cur){
        maxImage = 3-cur;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_attachment3_photo_dialog,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageView1 = view.findViewById(R.id.imageView1);
        imageView2 = view.findViewById(R.id.imageView2);
        imageView3 = view.findViewById(R.id.imageView3);
        btnSave = view.findViewById(R.id.btnSave);

        setupTakePhotoCameraAndLibraryAction(view);

        view.findViewById(R.id.btnCancel).setOnClickListener(v ->Attachment3PhotoDialog.this.dismiss());
        view.findViewById(R.id.btnSave).setOnClickListener(v -> {
            ArrayList<PhotoModel> uploadList = new ArrayList<>();
            if (selectPhoto1!=null){
                uploadList.add(selectPhoto1);
            }
            if (selectPhoto2!=null){
                uploadList.add(selectPhoto2);
            }
            if (selectPhoto3!=null){
                uploadList.add(selectPhoto3);
            }
            if (uploadList.size()>0){
                listener.uploadPhotos(uploadList);
            }else{
                Toast.makeText(getContext(), "please! add photos", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.shared().i("-------------GET BACK ----------------");
        if (requestCode == PHOTO_LIBRARY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                clearImageFPick();
                ArrayList<Image> imagesList = data.getParcelableArrayListExtra(Constants.KEY_BUNDLE_LIST);
                for (Image image:imagesList
                     ) {
                    Uri photoUri = image.getUri();
                    if (getActivity() != null && photoUri != null) {
                        String mime = getActivity().getContentResolver().getType(photoUri);
                        String name;
                        Cursor cursor = getActivity().getContentResolver().query(photoUri, null, null, null, null);
                        if (cursor != null) {
                            int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                            cursor.moveToFirst();
                            name = cursor.getString(nameIndex);
                            cursor.close();
                        } else {
                            name = "photo" + (System.currentTimeMillis() / 1000);
                        }
                        Logger.shared().i(mime);
                        try {
                            Bitmap selectedImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoUri);
                            if (!setImage1) {
                                imageView1.setImageBitmap(selectedImage);
                                selectPhoto1 = new PhotoModel(selectedImage, name, mime);
                                setImage1 = true;
                            } else if (!setImage2) {
                                imageView2.setImageBitmap(selectedImage);
                                selectPhoto2 = new PhotoModel(selectedImage, name, mime);
                                setImage2 = true;
                            } else if (!setImage3) {
                                imageView3.setImageBitmap(selectedImage);
                                selectPhoto3 = new PhotoModel(selectedImage, name, mime);
                                setImage3 = true;
                            }
                            Log.e("select_image", photoUri.toString());
                        } catch (IOException e) {
                            Toast.makeText(getContext(), new ImageEncodeError().toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }

        }
        if (requestCode == PHOTO_TAKE_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                try {
                    File file = new File(currentPhotoPath);
                    String name = file.getName();
                    String mime = "Content-Type: image/jpeg";
                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),Uri.fromFile(file));

                    ExifInterface ei = new ExifInterface(file.getPath());
                    int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                    switch(orientation) {

                        case ExifInterface.ORIENTATION_ROTATE_90:
                            selectedImage = rotateImage(selectedImage, 90);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_180:
                            selectedImage = rotateImage(selectedImage, 180);
                            break;

                        case ExifInterface.ORIENTATION_ROTATE_270:
                            selectedImage = rotateImage(selectedImage, 270);
                            break;

                        case ExifInterface.ORIENTATION_NORMAL:
                        default:
                            selectedImage = selectedImage;
                    }

                    if (!setImage1){
                        imageView1.setImageBitmap(selectedImage);
                        selectPhoto1 = new PhotoModel(selectedImage,name,mime);
                        setImage1=true;
                    }else if(!setImage2){
                        imageView2.setImageBitmap(selectedImage);
                        selectPhoto2 = new PhotoModel(selectedImage,name,mime);
                        setImage2=true;
                    }else if (!setImage3){
                        imageView3.setImageBitmap(selectedImage);
                        selectPhoto3 = new PhotoModel(selectedImage,name,mime);
                        setImage3=true;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void setListener(Attachment3PhotoDialog.AttachmentUploadPhotosDialogListener listener) {
        this.listener = listener;
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    public void setupTakePhotoCameraAndLibraryAction(View view){
        view.findViewById(R.id.btnAddPhoto).setOnClickListener(v -> {
            Intent intent = new Intent(getContext(), GalleryActivity.class);
            Params params = new Params();
            params.setPickerLimit(maxImage);
            intent.putExtra(Constants.KEY_PARAMS, params);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivityForResult(intent, PHOTO_LIBRARY_REQUEST_CODE);
            }
        });

        view.findViewById(R.id.btnTakePhoto).setOnClickListener(v -> {
            if (maxImage == 1){
                if (setImage1||setImage2||setImage3){
                    Toast.makeText(getContext(), "you cannot add more than 3 photos", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (maxImage == 2){
                if ((setImage1&&setImage2)||(setImage2&&setImage3)||(setImage3&&setImage1)){
                    Toast.makeText(getContext(), "you cannot add more than 3 photos", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (maxImage == 3){
                if (setImage1&&setImage2&&setImage3){
                    Toast.makeText(getContext(), "you cannot add more than 3 photos", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getActivity().getPackageManager()) != null){
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                            "com.mybos.bmapp.fileprovider",
                            photoFile);
                    currentUri = photoURI;
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(intent,PHOTO_TAKE_REQUEST_CODE);
                }

            }
        });
    }

    public interface AttachmentUploadPhotosDialogListener{
        void uploadPhotos(ArrayList<PhotoModel> photoModel);
    }

    private void clearImageFPick(){
        selectPhoto1 = null;
        selectPhoto2 = null;
        selectPhoto3 = null;
        imageView2.setImageBitmap(null);
        imageView3.setImageBitmap(null);
        imageView1.setImageBitmap(null);
        setImage1 = false;
        setImage2 = false;
        setImage3 = false;
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpeg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
