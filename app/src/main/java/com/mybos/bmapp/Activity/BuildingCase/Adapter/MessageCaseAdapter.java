package com.mybos.bmapp.Activity.BuildingCase.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.mybos.bmapp.Activity.BuildingCase.EditCase.EditCaseController;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Fragment.MessageCaseFragment;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.Message;
import com.mybos.bmapp.Data.Model.User;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.Date;

import io.realm.Realm;

public class MessageCaseAdapter extends RecyclerView.Adapter<MessageCaseAdapter.MessageViewHolder> {

    Context parentContext;
    Fragment parentFragment;
    EditCaseController controller = new EditCaseController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    public MessageCaseAdapter(Fragment fragment){
        parentFragment = fragment;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        parentContext = viewGroup.getContext();
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.a_message_case, viewGroup, false);
        return new MessageViewHolder(view);
}

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        Message message = Message.getMessages().get(position);
        holder.tvUserName.setText(message.getWriterName());
        holder.tvComment.setText(message.getComment());
        Date date = DateUtils.parseDate(message.getTime(), "yyyy-MM-dd'T'HH:mm:ssZZZZZ");
        holder.tvDate.setText(DateUtils.getDateString(date,"dd/MM/yyyy, h:mma"));

        Realm realm = Realm.getDefaultInstance();
        int userId=(realm.where(User.class).findFirst()).getId();

        if (userId != Integer.valueOf(message.getWriter())&&Integer.valueOf(message.getState())!=2) {
            holder.tvDate.setPaintFlags(0);
            holder.tvComment.setPaintFlags(0);
            holder.tvUserName.setPaintFlags(0);
            holder.tvDate.setTextColor(Color.rgb(128, 128, 128));
            holder.tvComment.setTextColor(Color.rgb(128, 128, 128));
            holder.tvUserName.setTextColor(Color.rgb(128, 128, 128));
            holder.containerMessage.setBackgroundColor(Color.rgb(225,225,225));

        }

        if(userId == Integer.valueOf(message.getWriter())&&Integer.valueOf(message.getState())!=2) {
            holder.tvDate.setPaintFlags(0);
            holder.tvComment.setPaintFlags(0);
            holder.tvUserName.setPaintFlags(0);
            holder.tvDate.setTextColor(Color.rgb(128, 128, 128));
            holder.tvComment.setTextColor(Color.rgb(128, 128, 128));
            holder.tvUserName.setTextColor(Color.rgb(128, 128, 128));
            holder.containerMessage.setBackgroundColor(Color.rgb(222,229,247));
            holder.cardViewMessage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Dialog dialog = new Dialog(parentContext);
                    dialog.setContentView(R.layout.a_message_detail);
                    EditText edtMessage = dialog.findViewById(R.id.edtMessage);
                    edtMessage.setMaxHeight(1);
                    edtMessage.setText(message.getComment());
                    Button btnUpdate = dialog.findViewById(R.id.btnUpdate);
                    Button btnDelete = dialog.findViewById(R.id.btnDelete);
                    Button btnClose = dialog.findViewById(R.id.btnClose);

                    btnUpdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!edtMessage.getText().toString().isEmpty()) {
                                controller.updateMessage(parentContext, selectedBuilding.getId(), message.getRequestId(), message.getId(), edtMessage.getText().toString(),
                                        success -> {
                                            dialog.dismiss();
                                            ((MessageCaseFragment) parentFragment).getMessages();
                                        },
                                        e -> {
                                        });
                            }


                        }
                    });

                    btnDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!edtMessage.getText().toString().isEmpty()) {
                                controller.deleteMessage(parentContext, selectedBuilding.getId(), message.getRequestId(), message.getId(),
                                        success -> {
                                            dialog.dismiss();
                                            ((MessageCaseFragment) parentFragment).getMessages();
                                            ((MessageCaseFragment) parentFragment).dismissKeyboard();
                                        },
                                        e -> {
                                        });
                            }


                        }
                    });
                    btnClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    InputMethodManager inputMethodManager = (InputMethodManager) parentContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                    dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

                        }
                    });
                    dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                            inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                        }
                    });

                    dialog.show();
                    if (dialog.isShowing()) {
                        edtMessage.requestFocus();
                        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                    }
                }
            });
        }
        if (Integer.valueOf(message.getState())==2){
            holder.containerMessage.setBackgroundColor(Color.rgb(250,250,250));
            holder.tvDate.setTextColor(Color.rgb(225,225,225));
            holder.tvComment.setTextColor(Color.rgb(225,225,225));
            holder.tvUserName.setTextColor(Color.rgb(225,225,225));
            holder.tvDate.setPaintFlags(holder.tvDate.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvComment.setPaintFlags(holder.tvComment.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.tvUserName.setPaintFlags(holder.tvUserName.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    @Override
    public int getItemCount() {
        return Message.getMessages().size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        TextView tvUserName, tvComment, tvDate;
        CardView cardViewMessage;
        ConstraintLayout containerMessage;

        public MessageViewHolder(View itemView) {
            super(itemView);
            tvUserName = itemView.findViewById(R.id.tvUserName);
            tvComment = itemView.findViewById(R.id.tvComment);
            tvDate = itemView.findViewById(R.id.tvDate);
            containerMessage = itemView.findViewById(R.id.containerMessage);
            cardViewMessage = itemView.findViewById(R.id.cardViewMessage);
        }

    }
}
