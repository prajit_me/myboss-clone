package com.mybos.bmapp.Activity.BuildingCase.NewCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Component.TagViewGroup.TagViewGroup;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.R;


import java.util.ArrayList;
import java.util.List;



/**
 * Created by EmLaAi on 31/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseJobAreaComponent extends ConstraintLayout {
    Spinner dropdownContractor,dropdownApartment,dropdownAsset,dropdownCategory,dropdownJobArea;
    TagViewGroup tagApartment,tagAsset;

    BuildingCaseAreaComponentListener listener;
    List<Contractor> selectedContractor = new ArrayList<>();
    public BuildingCaseJobAreaComponent(Context context) {
        super(context);
        additionalInit();
    }

    public BuildingCaseJobAreaComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public BuildingCaseJobAreaComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);

    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_new_case_f_case_detail_c_job_area,this);
        dropdownContractor = findViewById(R.id.dropdownContractor);
        dropdownApartment = findViewById(R.id.dropdownApartment);
        dropdownAsset = findViewById(R.id.dropdownAsset);
        dropdownCategory = findViewById(R.id.dropdownCategory);
        dropdownJobArea = findViewById(R.id.dropdownJobArea);

        tagApartment = findViewById(R.id.tagAppartmentCloud);
        tagAsset = findViewById(R.id.tagAssetCloud);


    }

    public void setListener(BuildingCaseAreaComponentListener listener) {
        this.listener = listener;
    }

//    public void setupContractor(ContractorList contractorList, List<Integer> seletedContractors){
//        SimpleTitleSpinnerAdapter adapter = new SimpleTitleSpinnerAdapter(getContext(),contractorList);
//        dropdownContractor.setAdapter(adapter);
//        dropdownContractor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                Contractor contractor = (Contractor) (dropdownContractor.getAdapter()).getItem(position);
//                BuildingCaseJobAreaComponent.this.selectedContractor.add(contractor);
//                tagContractor.setTags("ABC");
//
//                view.forceLayout();
//                BuildingCaseJobAreaComponent.this.forceLayout();
//                if (null != listener){
//                    listener.needUpdate();
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
//    }

    public void addContractorTag(String text){
        TextView textView = new TextView(getContext());
        textView.setText(text);
        textView.setTextSize(20);
        textView.setPadding(5,5,5,5);
        textView.setBackgroundColor(getContext().getResources().getColor(R.color.colorPrimary));
        textView.setTextColor(getContext().getResources().getColor(R.color.white));
//        textView.requestLayout();
//        tagContractor.addView(textView);
//        tagContractor.requestLayout();

    }

    public interface BuildingCaseAreaComponentListener{
        void needUpdate();
    }
}
