package com.mybos.bmapp.Activity.Broadcast;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseActivity;
import com.mybos.bmapp.Activity.Broadcast.Dialog.BroadcastSendCompleted;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class BroadcastMailActivity extends BaseActivity {

    BroadcastController controller = new BroadcastController();
    EditText edtSubject, edtCompose;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(Broadcast.getChoose_users().size() == 0){
            Toast.makeText(BroadcastMailActivity.this, "You didn't Select any user!", Toast.LENGTH_LONG).show();
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_mail);

        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

        edtCompose = findViewById(R.id.edtComposeMail);
        edtSubject = findViewById(R.id.edtSubjectMail);
        findViewById(R.id.btnBackBrMail).setOnClickListener(v -> this.finish());

        findViewById(R.id.btnSendEmail).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Broadcast.getChoose_users().size() == 0){
                    Toast.makeText(BroadcastMailActivity.this, "Select the user to send!", Toast.LENGTH_LONG).show();
                    return;
                }
                displayLoadingScreen();
                controller.sendContentEmailBroadcast(BroadcastMailActivity.this, selectedBuilding.getId(),
                        edtSubject.getText().toString(), edtCompose.getText().toString(),
                        broadcast->{
                        controller.sendEmail(BroadcastMailActivity.this, selectedBuilding.getId(),
                                send->{
                                    if (Broadcast.getStat_send() == 200){
                                        dismissLoadingScreen();
                                        BroadcastSendCompleted dialog = new BroadcastSendCompleted();
                                        dialog.show(getSupportFragmentManager(), "Completed");
                                }else {
                                        dismissLoadingScreen();
                                        Log.e("State_send",String.valueOf(Broadcast.getStat_send()));
                                    }
                                },
                                error -> {
                                    if (Offlinemode.checkNetworkConnection(BroadcastMailActivity.this)){
                                        Log.e("sendContentEmailBC",error.toString());
                                    }else {
                                        Offlinemode.showWhenOfflineMode(BroadcastMailActivity.this);
                                    }
                                }
                                );
                        },
                        error -> {
                            if (Offlinemode.checkNetworkConnection(BroadcastMailActivity.this)){
                                Log.e("sendContentEmailBC",error.toString());
                            }else {
                                Offlinemode.showWhenOfflineMode(BroadcastMailActivity.this);
                            }
                        });
            }
        });
    }
}
