package com.mybos.bmapp.Activity.Offlinemode;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.provider.MediaStore;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Inspection.CreateInspection.MakeInpsectionController;
import com.mybos.bmapp.Activity.Inspection.InspectionList.InspectionListController;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Mapper.Inspection.InspectionMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItem;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItemOffline;
import com.mybos.bmapp.Data.Model.Inspection.InspectionList;
import com.mybos.bmapp.Data.Model.Inspection.InspectionOfflinetoSave;
import com.mybos.bmapp.Data.Model.Inspection.InspectionOfflinetoSaveList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Offlinemode {

    private static MakeInpsectionController controller = new MakeInpsectionController();
    private static InspectionListController inspectionListController = new InspectionListController();
    private static InspectionMapper inspectionMapper = new InspectionMapper();
    static Boolean saveIsOK = true;
    public final int REQUEST_PERMISSION = 99;
    private static ProgressDialog progressDialog;

    public static String buildingName="buildingName";
    public static String temp="0";
    public static String tempStatus="--";
    public static String city="City";
    public static String monthDate="--";


    public static boolean checkNetworkConnection(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm.getActiveNetworkInfo() != null) {
            return cm.getActiveNetworkInfo().isConnected();
        }else {
            return false;
        }
    }

    public static void dialogAsyncInspection(Context context){
        progressDialog = new ProgressDialog(context,R.style.DialogTheme);
        progressDialog.setMax(InspectionOfflinetoSaveList.get().getListInspectionOfflinetoSave().size());
        progressDialog.setTitle("Syncing please wait...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
        for (InspectionOfflinetoSave item: InspectionOfflinetoSaveList.get().getListInspectionOfflinetoSave()
        ) {
            saveInspectionFromOffline(item,context);
        }
    }

    public static void closeDialogAsyncInspection(){
        progressDialog.dismiss();
    }

    public static void checkEnd(Context context){
        if(progressDialog.getProgress()==progressDialog.getMax()){
            if (saveIsOK){
                InspectionOfflinetoSaveList.clear();
            }else {
                Toast.makeText(context, "save inspections offline are failed!", Toast.LENGTH_SHORT).show();
            }
            closeDialogAsyncInspection();
        }
    }



    public static void downloadInspectionFOfflinemode(Context context){

        SearchTableQueryBuilder builder = new SearchTableQueryBuilder();
        builder.setCurrentPage(1);
        builder.setOrderBy("added");
        builder.setOrderType("desc");
        builder.setKeyword(null);
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        // get Inspection common and private
        for (int i=0;i<=1;i++){
            int j  = i;
            inspectionListController.getInspectionList(context,selectedBuilding.getId(),j,builder,list->{
                if (j==0){
                    if (list!=null){
                        InspectionList.setOfflineList(list);
                    }
                }else{
                    if (list!=null) {
                        InspectionList.setOfflineListPrivate(list);
                    }
                }
                    getItemsInspection(context);
            },error -> {
//                Log.e("Error","get Inspection list");
            });
        }
        //get apartmentList fot private Inspection
        controller.getApartmentList(context,selectedBuilding.getId(),apartmentList->{
            Inspection.setOfflineApartmentList(apartmentList);
        },error -> {
            Log.e("get_Inspection","failed");
        });
    }

    private static void getItemsInspection(Context context){
        ArrayList<Inspection> tempList = new ArrayList<>();
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        for (Inspection inspectionInList: InspectionList.getOfflineList().getAll()
        ) {
            controller.getInpsection(context,selectedBuilding.getId(),inspectionInList.getId(),
                    inspection->{
                        tempList.add(inspection);
                    },
                    error -> {
                        Log.e("error get inspection:","buildingId,inspectionId:"+selectedBuilding.getId()+"-"+inspectionInList.getId());
                    });
        }

        for (Inspection inspectionInList:InspectionList.getOfflineListPrivate().getAll()
        ) {
            controller.getInpsection(context,selectedBuilding.getId(),inspectionInList.getId(),
                    inspection->{
                        tempList.add(inspection);
                    },
                    error -> {
                        Log.e("error get inspection:","buildingId,inspectionId:"+selectedBuilding.getId()+"-"+inspectionInList.getId());
                    });
        }
        Inspection.setOfflineInspection(tempList);
        Log.e("get_Inspection","OK");
    }

    public static void saveInspectionFromOffline(InspectionOfflinetoSave item, Context context){
        final int[] ShowTime = {1};
//        displayLoadingScreen();
        inspectionMapper.getNewHistoryID(context, item.getBuildingId(), item.getInspectionId(), item.getApartmentId()==null?"":item.getApartmentId(),
                response -> {
                    int NewHistoryID = response;
                    ArrayList<InspectionItem> listInspectionItem = new ArrayList<>();
                    for (InspectionItemOffline i : item.getUpdatedInspectionItemList()
                    ) {
                        listInspectionItem.add(convertToInspectionItem(i, context));
                    }
                    inspectionMapper.saveInspectionItemFromOffline(context, item.getBuildingId(), item.getInspectionId(), NewHistoryID, listInspectionItem,
                            InnerResponse -> {
                                saveNewHistoryId(item.isConvert(), ShowTime, NewHistoryID, String.valueOf(item.getInspectionId()), item, context);
                            },
                            InnerError -> {
//                                    dismissLoadingScreen();
                                saveIsOK = false;
                                return;
                            }
                    );

                },
                error -> {
//                    dismissLoadingScreen();
                    saveIsOK = false;
                    return;
                }
        );
    }

    public static void saveNewHistoryId(boolean convert,int[] ShowTime,int NewHistoryID,String idItem,InspectionOfflinetoSave item,Context context){
        inspectionMapper.saveNewHistoryID(context, item.getBuildingId(), item.getInspectionId(), NewHistoryID,
                SecondInnerResponse -> {
                    if (ShowTime[0] <= 1) {
                        if (!convert) {
                                progressDialog.incrementProgressBy(1);
                                checkEnd(context);
                            ShowTime[0]++;
                        } else {
                            inspectionMapper.convertToCase(context, item.getBuildingId(), item.getInspectionId(), NewHistoryID,
                                    idItem,
                                    convertSuccess -> {
                                        if (!convertSuccess.equals("200")) {
                                            Log.e("check_save_item","error");
                                        } else {
                                                progressDialog.incrementProgressBy(1);
                                                checkEnd(context);
                                        }
                                        ShowTime[0]++;
                                    },
                                    error -> {
                                        saveIsOK = false;
                                        ShowTime[0]++;
                                        return;
                                    });
                        }
                    }
                },
                InnerError -> {
                    if (ShowTime[0] <= 1) {
                        saveIsOK = false;
                        ShowTime[0]++;
                        return;
                    }
                }
        );
    }

    public static void showWhenOfflineMode(Context context) {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(context, R.style.DialogTheme);
            String message = "Can not connect to server";
            builder1.setTitle("Disconnect");
            builder1.setMessage(message);
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "Retry",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            builder1.setNegativeButton(
                    "Back to Home",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(context, DashboardActivity.class);
                            context.startActivity(intent);
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            if (null!=context) {
                alert11.show();
            }
    }

    public static InspectionItem convertToInspectionItem(InspectionItemOffline item,Context context){
        InspectionItem result = new InspectionItem();

        result.setId(item.getId());
        result.setArea(item.getArea());
        result.setIsChecked(item.getIsChecked());
        result.setName(item.getName());
        result.setInspection(item.getInspection());
        result.setComment(item.getComment());
        //get image
        if (item.getPhotoPath() != null){
            File file = new File(item.getPhotoPath());
            String name = file.getName();
            String mime = "Content-Type: image/jpeg";
            try {
                Bitmap selectedImage = MediaStore.Images.Media.getBitmap(context.getContentResolver(), Uri.fromFile(file));
                result.setImage(new PhotoModel(selectedImage,name,mime));
            } catch (IOException e) {
                Log.e("check_image",e.toString());
                e.printStackTrace();
            }
        }

        return result;
    }

    public static String getBuildingName() {
        return buildingName;
    }

    public static void setBuildingName(String buildingName) {
        Offlinemode.buildingName = buildingName;
    }

    public static String getTemp() {
        return temp;
    }

    public static void setTemp(String temp) {
        Offlinemode.temp = temp;
    }

    public static String getTempStatus() {
        return tempStatus;
    }

    public static void setTempStatus(String tempStatus) {
        Offlinemode.tempStatus = tempStatus;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        Offlinemode.city = city;
    }

    public static String getMonthDate() {
        return monthDate;
    }

    public static void setMonthDate(String monthDate) {
        Offlinemode.monthDate = monthDate;
    }
}
