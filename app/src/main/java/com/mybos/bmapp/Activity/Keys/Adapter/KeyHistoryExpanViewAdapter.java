package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Key.History;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class KeyHistoryExpanViewAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<String> headers;
    private HashMap<String,History> content;

    public KeyHistoryExpanViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getGroupCount() {
        return History.getHistories().size();
    }

    @Override
    public int getChildrenCount(int i) {
        return 1;
    }

    @Override
    public Object getGroup(int i) {
        return headers.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return content.get(headers.get(i));
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.key_history_a_line_header,null);

        TextView tvValueKeyHistoryHeader = view.findViewById(R.id.tvValueKeyHistoryHeader);

        String nameCopany = History.getHistories().get(i).getCompany();
        tvValueKeyHistoryHeader.setText(nameCopany.equals("null")?"":nameCopany);
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.key_history_a_line,null);

        TextView tvContactKeyHistory = view.findViewById(R.id.tvContactKeyHistory);
        TextView tvHistoryMobile = view.findViewById(R.id.tvHistoryMobile);
        TextView tvHistoryReason = view.findViewById(R.id.tvHistoryReason);
        TextView tvHistoryDateTime = view.findViewById(R.id.tvHistoryDateTime);
        TextView tvHistoryDueBack = view.findViewById(R.id.tvHistoryDueBack);

        History history = History.getHistories().get(i);

        tvContactKeyHistory.setText(history.getContact().equals("null")?"":history.getContact());
        tvHistoryMobile.setText(history.getNumber().equals("null")?"":history.getNumber());
        tvHistoryReason.setText(history.getReason().equals("null")?"":history.getReason());
        tvHistoryDateTime.setText(history.getAdded().equals("null")?"":
                DateUtils.editDateTime(history.getAdded()));
        tvHistoryDueBack.setText(history.getFinish().equals("null")?"":
                DateUtils.editDateTime(history.getFinish()));

        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return false;
    }
}
