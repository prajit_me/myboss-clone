package com.mybos.bmapp.Activity.WebView;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

import com.mybos.bmapp.R;

public class WebViewActivity extends AppCompatActivity {
    public static final String ATTACHMENT_URL_KEY = "WebViewActivity.ATTACHMENT_URL_KEY";
    public static final String ATTACHMENT_TYPE_KEY = "WebViewActivity.ATTACHMENT_TYPE_KEY";
    public static final String ATTACHMENT_TYPE_FILE_KEY = "WebViewActivity.ATTACHMENT_TYPE_FILE_KEY";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_web_view);
        WebView webView = findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        String url = getIntent().getStringExtra(ATTACHMENT_URL_KEY);
        String type = getIntent().getStringExtra(ATTACHMENT_TYPE_KEY);
        String typefile = getIntent().getStringExtra(ATTACHMENT_TYPE_FILE_KEY);
        Log.e("check_type",type);
        if ("13".equals(type)||isPictureType(typefile)){
            webView.loadUrl(url);
        }else {
            webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + url);
        }

    }

    public boolean isPictureType(String type){
        if (type == null){return  true;}
        if (type.equals("image/png")){return true;}
        if (type.equals("image/jpg")){return true;}
        if (type.equals("image/jpeg")){return true;}
        if (type.contains("png")){return true;}
        if (type.contains("jpg")){return true;}
        if (type.contains("jpeg")){return true;}
        return false;
    }
}
