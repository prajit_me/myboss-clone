package com.mybos.bmapp.Activity.Assets.Fragment;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetHistory;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AssetHistoryInfoFragment extends Fragment {

    private LinearLayout lnlyAssetHistoryContainer;
    private ArrayList<BuildingAssetHistory> SelectedAssetHistory = BuildingAssetInfoCollector.getAssetHistoryList();
    private final int TEXT_SIZE = 15, MIN_HEIGHT = 120;
    private final String ORANGE_TEXT_COLOR = "#f19c0f";

    private TextView getHistoryName(String HistoryName) {
        TextView tvHistoryName = new TextView(this.getActivity());

        LinearLayout.LayoutParams tvHistoryNameParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvHistoryNameParams.setMargins(20, 5,5,5);
        tvHistoryName.setLayoutParams(tvHistoryNameParams);
        tvHistoryName.setGravity(Gravity.CENTER|Gravity.START);

        tvHistoryName.setMinHeight(MIN_HEIGHT);
        tvHistoryName.setText(HistoryName);
        tvHistoryName.setTextSize(TEXT_SIZE);
        tvHistoryName.setTextColor(Color.BLACK);
        tvHistoryName.setTypeface(tvHistoryName.getTypeface(), Typeface.BOLD);

        return tvHistoryName;
    }

    private LinearLayout getCaseNumber(String CaseNumber) {
        LinearLayout lnlyCaseNumber = new LinearLayout(this.getActivity());
        LinearLayout.LayoutParams lnlyCaseNumberParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lnlyCaseNumberParams.setMargins(20, 5,5,5);
        lnlyCaseNumber.setLayoutParams(lnlyCaseNumberParams);
        lnlyCaseNumber.setOrientation(LinearLayout.HORIZONTAL);

        //  CASE NUMBER LABEL
        TextView tvCaseNumberLabel = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvCaseNumberLabelParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvCaseNumberLabelParams.setMargins(5, 0, 20, 0);
        tvCaseNumberLabel.setLayoutParams(tvCaseNumberLabelParams);
        tvCaseNumberLabel.setGravity(Gravity.CENTER|Gravity.START);
        tvCaseNumberLabel.setText("Case");
        tvCaseNumberLabel.setMinHeight(MIN_HEIGHT);
        tvCaseNumberLabel.setMinWidth(MIN_HEIGHT);
        tvCaseNumberLabel.setTextSize(TEXT_SIZE);
        tvCaseNumberLabel.setTextColor(Color.LTGRAY);

        //  CASE NUMBER
        TextView tvCaseNumber = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvCaseNumberParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvCaseNumberParams.setMargins(5, 0, 20, 0);
        tvCaseNumber.setLayoutParams(tvCaseNumberParams);
        tvCaseNumber.setGravity(Gravity.CENTER|Gravity.START);
        String StCaseNumber = "#" + CaseNumber;
        tvCaseNumber.setText(StCaseNumber);
        tvCaseNumber.setMinHeight(MIN_HEIGHT);
        tvCaseNumber.setMinWidth(MIN_HEIGHT);
        tvCaseNumber.setTextSize(TEXT_SIZE);
        tvCaseNumber.setTextColor(Color.parseColor(ORANGE_TEXT_COLOR));
        tvCaseNumber.setTypeface(tvCaseNumber.getTypeface(), Typeface.BOLD);

        lnlyCaseNumber.addView(tvCaseNumberLabel);
        lnlyCaseNumber.addView(tvCaseNumber);

        return lnlyCaseNumber;
    }

    private LinearLayout getAddedOn(String AddedOnDate) {
        LinearLayout lnlyAddedOn = new LinearLayout(this.getActivity());
        LinearLayout.LayoutParams lnlyAddedOnParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lnlyAddedOnParams.setMargins(10, 5,5,5);
        lnlyAddedOn.setLayoutParams(lnlyAddedOnParams);
        lnlyAddedOn.setOrientation(LinearLayout.HORIZONTAL);

        //  ADDED ON LABEL
        TextView tvAddedOnLabel = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvAddedOnLabelParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvAddedOnLabelParams.setMargins(5, 0, 20, 0);
        tvAddedOnLabel.setLayoutParams(tvAddedOnLabelParams);
        tvAddedOnLabel.setGravity(Gravity.CENTER|Gravity.START);
        tvAddedOnLabel.setText("Added on");
        tvAddedOnLabel.setMinHeight(MIN_HEIGHT);
        tvAddedOnLabel.setMinWidth(MIN_HEIGHT);
        tvAddedOnLabel.setTextSize(TEXT_SIZE);
        tvAddedOnLabel.setTextColor(Color.LTGRAY);

        //  ADDED ON
        TextView tvAddedOn = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvAddedOnParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvAddedOnParams.setMargins(5, 0, 20, 0);
        tvAddedOn.setLayoutParams(tvAddedOnParams);
        tvAddedOn.setGravity(Gravity.CENTER|Gravity.START);
        tvAddedOn.setText(AddedOnDate);
        tvAddedOn.setMinHeight(MIN_HEIGHT);
        tvAddedOn.setMinWidth(MIN_HEIGHT);
        tvAddedOn.setTextSize(TEXT_SIZE);
        tvAddedOn.setTextColor(Color.parseColor(ORANGE_TEXT_COLOR));
        tvAddedOn.setTypeface(tvAddedOn.getTypeface(), Typeface.BOLD);

        lnlyAddedOn.addView(tvAddedOnLabel);
        lnlyAddedOn.addView(tvAddedOn);

        return lnlyAddedOn;
    }

    private LinearLayout getAssetHistoryCell(BuildingAssetHistory SelectedAssetHistory) {
        LinearLayout lnlyHistoryCell = new LinearLayout(this.getActivity());
        LinearLayout.LayoutParams lnlyHistoryCellParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lnlyHistoryCellParams.setMargins(5, 5, 5, 10);
        lnlyHistoryCell.setLayoutParams(lnlyHistoryCellParams);
        lnlyHistoryCell.setOrientation(LinearLayout.VERTICAL);
        lnlyHistoryCell.setBackground(getActivity().getDrawable(R.drawable.bg_corner_radius_8));

        //  HISTORY NAME
        lnlyHistoryCell.addView(getHistoryName(SelectedAssetHistory.getSubject()));

        RelativeLayout rllyCaseAdded = new RelativeLayout(this.getActivity());

        //  CASE NUMBER
        LinearLayout lnlyCaseNumber = getCaseNumber(String.valueOf(SelectedAssetHistory.getHistoryNumber()));
        RelativeLayout.LayoutParams rllyCaseParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rllyCaseParams.setMargins(20, 0, 20 ,0);
        rllyCaseParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        lnlyCaseNumber.setLayoutParams(rllyCaseParams);
        rllyCaseAdded.addView(lnlyCaseNumber);

        //  ADDED ON
        LinearLayout lnlyAddedOnDate = getAddedOn(String.valueOf(SelectedAssetHistory.getAdded()));
        RelativeLayout.LayoutParams rllyAddedOnParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rllyAddedOnParams.setMargins(20, 0, 20 ,0);
        rllyAddedOnParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        lnlyAddedOnDate.setLayoutParams(rllyAddedOnParams);
        rllyCaseAdded.addView(lnlyAddedOnDate);

        lnlyHistoryCell.addView(rllyCaseAdded);


        return lnlyHistoryCell;
    }

    private TextView getNoHistory() {
        TextView tvNoHistoryFile = new TextView(this.getActivity());
        LinearLayout.LayoutParams tvNoHistoryFileParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        tvNoHistoryFileParams.setMargins(5, 5, 5, 5);
        tvNoHistoryFile.setLayoutParams(tvNoHistoryFileParams);

        tvNoHistoryFile.setGravity(Gravity.CENTER);
        tvNoHistoryFile.setMinHeight(MIN_HEIGHT * 2);
        tvNoHistoryFile.setText("No history was found.");
        tvNoHistoryFile.setTextSize(TEXT_SIZE);
        tvNoHistoryFile.setTextColor(Color.BLACK);
        tvNoHistoryFile.setTypeface(tvNoHistoryFile.getTypeface(), Typeface.ITALIC);

        return tvNoHistoryFile;
    }

    private void getHistoryCellList() {
        int Size = SelectedAssetHistory.size();
        if (Size > 0) {
            for (int i = 0; i < Size; i++) {
                BuildingAssetHistory CurrentHistory = SelectedAssetHistory.get(i);
                lnlyAssetHistoryContainer.addView(getAssetHistoryCell(CurrentHistory));
            }
        }
        else {
            lnlyAssetHistoryContainer.addView(getNoHistory());
        }


    }

    private void startup() {
        getHistoryCellList();
    }

    public AssetHistoryInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_asset_history_info, container, false);

        //  GET REFERENCES
        lnlyAssetHistoryContainer = rootView.findViewById(R.id.lnlyAssetHistoryContainer_AssetHistoryInfoFragment);

        //  START UP
        startup();

        return rootView;
    }

}
