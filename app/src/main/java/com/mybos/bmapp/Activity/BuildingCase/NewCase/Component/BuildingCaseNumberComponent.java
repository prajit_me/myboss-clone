package com.mybos.bmapp.Activity.BuildingCase.NewCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleSpinnerAdapter;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatus;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 29/03/2018.
 *
 * @author EmLaAi
 */
public class BuildingCaseNumberComponent extends ConstraintLayout {
    private TextView tvCaseNumber;
    private Spinner statusDropDown;

    public BuildingCaseNumberComponent(Context context) {
        super(context);
        additionalInit();
    }

    public BuildingCaseNumberComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public BuildingCaseNumberComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_new_case_f_case_detail_c_case_number,this);

        tvCaseNumber = findViewById(R.id.tvCaseNumber);
        statusDropDown = findViewById(R.id.statusDropdown);
    }

    public void setCaseNumber(String caseNumber) {
        String StCaseNumber = " #" + caseNumber;
        tvCaseNumber.setText(StCaseNumber);
    }

    public void setupStatusDropDown(BuildingCaseStatusList statusList,int selectedId,StatusListDropDownSelectListener listener){
        SimpleTitleSpinnerAdapter adapter = new SimpleTitleSpinnerAdapter(getContext(), statusList);
        adapter.setDisplayTextSize(14);
        adapter.setDisplayTextColor(getResources().getColor(R.color.text_title_dark));
        statusDropDown.setAdapter(adapter);
        statusDropDown.setSelection(statusList.getSelectedIndex(selectedId));
        statusDropDown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BuildingCaseStatus model = (BuildingCaseStatus) statusDropDown.getAdapter().getItem(position);
                listener.didSeletedItem(model);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public interface StatusListDropDownSelectListener{
        void didSeletedItem(BuildingCaseStatus status);
    }
}
