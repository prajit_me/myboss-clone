package com.mybos.bmapp.Activity.Calendar.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Calendar.CalendarController;
import com.mybos.bmapp.Activity.Calendar.CalendarCustom.CalendarView;
import com.mybos.bmapp.Activity.Calendar.CalendarCustom.EventDay;
import com.mybos.bmapp.Activity.Calendar.CalendarCustom.Listener.OnCalendarPageChangeListener;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Calendar.Booking;
import com.mybos.bmapp.Data.Model.Calendar.DaysEvent;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.Data.Model.Calendar.Reminder;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarActivity extends a_base_toolbar {

    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    CalendarController controller = new CalendarController();
    CalendarView calendarView;
    ProgressBar progressBarCalendar;
    private static boolean onPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        updateToolbarTitle("Calendar");

        calendarView = (CalendarView) findViewById(R.id.calendarViewAc);
        progressBarCalendar = findViewById(R.id.progressBarCalendar);
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarActivity.this, DashboardActivity.class);
                startActivity(intent);
                finish();
            }
        });
        onPause = false;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CalendarActivity.this, DashboardActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        onPause = true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!onPause){
            main(false);
        }
        calendarView.setOnPreviousPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                displayLoadingScreen();
                main(true);
            }
        });

        calendarView.setOnForwardPageChangeListener(new OnCalendarPageChangeListener() {
            @Override
            public void onChange() {
                displayLoadingScreen();
                main(true);
            }
        });

        calendarView.setOnDayClickListener(eventDay ->
                clickDay(eventDay));
    }

    private void main(boolean changeMonth) {
        List<EventDay> events = new ArrayList<>();
        Reminder.setCurMonth(DateUtils.getDateString(calendarView.getCurrentPageDate().getTime(), "M"));
        Reminder.setCurYear(DateUtils.getDateString(calendarView.getCurrentPageDate().getTime(), "yyyy"));
        SelectedDay.setAllDayOfCurrentMonth(new ArrayList<>(getAllDayOfMont()));
        DaysEvent.setDaysEvents(new ArrayList<>());
        MaintenancePerDay.setMaintenancePerDays(new ArrayList<>());

        controller.getDateMaintenance(CalendarActivity.this, selectedBuilding.getId(),
                m -> {
                    controller.getBooking(CalendarActivity.this, selectedBuilding.getId(),
                            b -> {
                                //get reminder and set evevnt
                                controller.getReminder(CalendarActivity.this, selectedBuilding.getId(),
                                        r -> {
                                            for (int i = 0; i < calendarView.getCurrentPageDate().getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
                                                Calendar calendar = calendarView.getCurrentPageDate();
                                                calendar.add(Calendar.DAY_OF_MONTH, i);
                                                Date date = calendar.getTime();
                                                String stringDate = DateUtils.getDateString(date, "yyyy-MM-dd");
                                                DaysEvent daysEvent = new DaysEvent();
                                                daysEvent.setDate(stringDate);

                                                //checking booking
                                                for (Booking booking : Booking.getBookings()
                                                ) {
                                                    if (stringDate.equals(booking.getDate())) {
                                                        events.add(new EventDay(calendar, R.drawable.calendar_event_notice));
                                                        daysEvent.getBooking().add(booking);
                                                    }
                                                }
                                                //checking reminder
                                                for (Reminder reminder : Reminder.getReminders()
                                                ) {
                                                    if (stringDate.equals(reminder.getDate())) {
                                                        events.add(new EventDay(calendar, R.drawable.calendar_event_notice));
                                                        daysEvent.getReminder().add(reminder);
                                                    }
                                                }
                                                //checking maintenance
                                                for (MaintenancePerDay maintenancePerDay : MaintenancePerDay.getMaintenancePerDays()
                                                     ) {
                                                    if (stringDate.equals(maintenancePerDay.getDate())){
                                                        events.add(new EventDay(calendar, R.drawable.calendar_event_notice));
                                                    }
                                                }

                                                //add to daysEvent
                                                DaysEvent.getDaysEvents().add(daysEvent);

                                            }
                                            calendarView.setEnabled(true);
                                            calendarView.setEvents(events);
                                            progressBarCalendar.setVisibility(View.INVISIBLE);
                                            calendarView.setVisibility(View.VISIBLE);
                                            if (changeMonth){
                                                dismissLoadingScreen();
                                            }
                                        },
                                        error -> {
                                            if (Offlinemode.checkNetworkConnection(CalendarActivity.this)){
                                                Log.e("getDateMaintenance",error.toString());
                                            }else{
                                                Intent intent = new Intent(CalendarActivity.this,DashboardActivity.class);
                                                startActivity(intent);
                                            }
                                        });
                            },
                            error -> {
                                if (Offlinemode.checkNetworkConnection(CalendarActivity.this)){
                                    Log.e("getDateMaintenance",error.toString());
                                }else{
                                    Intent intent = new Intent(CalendarActivity.this,DashboardActivity.class);
                                    startActivity(intent);
                                }
                            });
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(CalendarActivity.this)){
                        Log.e("getDateMaintenance",error.toString());
                    }else{
                        Intent intent = new Intent(CalendarActivity.this,DashboardActivity.class);
                        startActivity(intent);
                    }
                });


    }

    private void clickDay(EventDay eventDay) {
        String dateEvent = DateUtils.getDateString(eventDay.getCalendar().getTime(), "yyyy-MM-dd");
        if (getAllDayOfMont().contains(dateEvent)) {
            Intent intent = new Intent(CalendarActivity.this, CalendarDayEventActivity.class);
            SelectedDay.setFullDate(DateUtils.getDateString(eventDay.getCalendar().getTime(), "dd MMMM yyyy"));
            SelectedDay.setDate(DateUtils.getDateString(eventDay.getCalendar().getTime(), "yyyy-MM-dd"));
            SelectedDay.setDateOfweek(DateUtils.getDateString(eventDay.getCalendar().getTime(), "EEEE"));
            startActivity(intent);
        }
    }

    private ArrayList<String> getAllDayOfMont(){
        ArrayList<String> dates =  new ArrayList<>();
        for (int i = 0; i < calendarView.getCurrentPageDate().getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            Calendar calendar = calendarView.getCurrentPageDate();
            calendar.add(Calendar.DAY_OF_MONTH, i);
            Date date = calendar.getTime();
            String stringDate = DateUtils.getDateString(date, "yyyy-MM-dd");
            dates.add(stringDate);
        }
        return dates;
    }
}
