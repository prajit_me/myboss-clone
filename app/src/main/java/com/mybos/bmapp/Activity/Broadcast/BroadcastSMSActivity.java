package com.mybos.bmapp.Activity.Broadcast;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseActivity;
import com.mybos.bmapp.Activity.Broadcast.Dialog.BroadcastSendCompleted;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class BroadcastSMSActivity extends BaseActivity {

    BroadcastController controller = new BroadcastController();
    TextView tvCredit,tvSMSInfo;
    EditText edtComposeSMS;
    Button btnTopUp, btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_broadcast_sms);

        tvCredit = findViewById(R.id.tvCredit);
        btnTopUp = findViewById(R.id.btnTopUpSMS);
        btnSend = findViewById(R.id.btnSendSMS);
        edtComposeSMS = findViewById(R.id.edtComposeSMS);
        tvSMSInfo = findViewById(R.id.smsinfo);

        findViewById(R.id.btnBackBrSMS).setOnClickListener(v -> this.finish());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (Broadcast.getChoose_users().size() == 0){
            Toast.makeText(BroadcastSMSActivity.this, "You didn't Select any user!", Toast.LENGTH_LONG).show();
        }
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        btnTopUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(BroadcastSMSActivity.this, BroadcastSMSPurchaseActivity.class);
                startActivity(intent);
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Broadcast.getChoose_users().size() == 0){
                    Toast.makeText(BroadcastSMSActivity.this, "Select the user to send!", Toast.LENGTH_LONG).show();
                    return;
                }
                displayLoadingScreen();
                controller.sendSMS(BroadcastSMSActivity.this,
                        selectedBuilding.getId(), edtComposeSMS.getText().toString(),
                        send -> {
                            if(Broadcast.getStat_send() == 200){
                                dismissLoadingScreen();
                                BroadcastSendCompleted dialog = new BroadcastSendCompleted();
                                dialog.show(getSupportFragmentManager(), "Completed");
                                getAvaliableSMS();
                            }else {
                                dismissLoadingScreen();
                                Toast.makeText(BroadcastSMSActivity.this, "state_send: "+String.valueOf(Broadcast.getStat_send()), Toast.LENGTH_SHORT).show();
                            }
                        }, error -> {
                            if (Offlinemode.checkNetworkConnection(BroadcastSMSActivity.this)){
                                Log.e("sendSMS",error.toString());
                            }else {
                                Offlinemode.showWhenOfflineMode(BroadcastSMSActivity.this);
                            }
                        });
            }
        });

        tvSMSInfo.setText(tvSMSInfo.getText().toString().replace("?",String.valueOf(Broadcast.getChoose_users().size())));
        getAvaliableSMS();
    }

    public void getAvaliableSMS(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getBroadcastCredit(BroadcastSMSActivity.this, selectedBuilding.getId(),
                broadcast -> {
                    tvCredit.setText(String.valueOf(Broadcast.getCredit()));
                }, error -> {
                    if (Offlinemode.checkNetworkConnection(BroadcastSMSActivity.this)){
                        Log.e("getAvaliableSMS",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(BroadcastSMSActivity.this);
                    }
                }
        );
    }
}
