package com.mybos.bmapp.Activity.Adapter;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mybos.bmapp.Activity.Assets.Activity.BuildingAssetActivity;
import com.mybos.bmapp.Activity.Assets.Activity.BuildingAssetDetailActivity;
import com.mybos.bmapp.Activity.Resident.Adapter.ResidentRecyclerAdapter;
import com.mybos.bmapp.Activity.Resident.ResidentDetailActivity_2nd;
import com.mybos.bmapp.Component.RecycleAdapter.AbstractScrollToLoadMoreRecyclerAdapter;
import com.mybos.bmapp.Data.Model.Asset.Asset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.Data.Model.Resident.Resident;
import com.mybos.bmapp.R;

public class AssetsRecyclerAdapter extends AbstractScrollToLoadMoreRecyclerAdapter<BuildingAsset> {


    @Override
    public int getItemLayout() {
        return R.layout.a_assets_adapter;
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new AssetsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AssetsViewHolder){
            ((AssetsViewHolder)holder).titleAssetsItem.setText(sourceList.get(position).getName());
            ((AssetsViewHolder)holder).tvAssetLocation.setText(sourceList.get(position).getLocation().equals("null")?"":sourceList.get(position).getLocation());
            ((AssetsViewHolder)holder).tvAssetTag.setText(sourceList.get(position).getCategoryName());

            ((AssetsViewHolder)holder).cvAssetItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    BuildingAssetInfoCollector.setSelectedBuildingAsset(sourceList.get(position));

                    //  SHOW ASSET DETAIL
                    Intent intent = new Intent(((AssetsViewHolder)holder).tvAssetTag.getContext(),BuildingAssetDetailActivity.class);
                    ((AssetsViewHolder)holder).tvAssetTag.getContext().startActivity(intent);
                }
            });
        }
    }

    class AssetsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView titleAssetsItem,tvAssetTag,tvAssetLocation;
        CardView cvAssetItem;
        ResidentRecyclerAdapter.OnItemClickListener listener;
        public AssetsViewHolder(View itemView) {
            super(itemView);
            titleAssetsItem = itemView.findViewById(R.id.titleAssetsItem);
            tvAssetTag = itemView.findViewById(R.id.tvAssetTag);
            tvAssetLocation = itemView.findViewById(R.id.tvAssetLocation);
            cvAssetItem = itemView.findViewById(R.id.cvAssetItem);
        }

        @Override
        public void onClick(View v) {
            if (null != listener){
                listener.onClick();
            }
        }

        public void setListener(ResidentRecyclerAdapter.OnItemClickListener listener) {
            this.listener = listener;
        }
    }

    public static interface OnItemClickListener{
        void onClick();
    }
}
