package com.mybos.bmapp.Activity.Inspection.Adapter;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.RecycleAdapter.MultiSectionRecyclerAdapter;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItem;
import com.mybos.bmapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by EmLaAi on 19/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionDetailRecyclerAdapter extends MultiSectionRecyclerAdapter {
    private ItemViewHolderEventListener listener;
    private JSONObject dataDraf;
    private ArrayList<Integer> listInspectionCopied = new ArrayList<>();

    @Override
    protected RecyclerView.ViewHolder creatHeaderViewHolder(View view) {
        return new HeaderViewHolder(view);
    }

    @Override
    protected RecyclerView.ViewHolder creatNormalCellHolder(View view) {
        return new ItemViewHolder(view);
    }

    @Override
    public int getHeaderLayout() {
        return R.layout.r_a_make_inspection_n_area_header;
    }

    @Override
    protected int getItemLayout() {
        return R.layout.r_make_inspectino_n_item_cell;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder){
            SourceHolder sourceHolder = displaySource.get(position);
            if (sourceHolder.viewType == VIEW_HEADER_TYPE){
                ((HeaderViewHolder) holder).tvTitle.setText(sourceHolder.name);
            }
        }
        if (holder instanceof ItemViewHolder){
            SourceHolder sourceHolder = displaySource.get(position);
            if (sourceHolder.viewType == VIEW_NORMAL_TYPE){
                if (null != sourceHolder.source){
                    if (sourceHolder.source instanceof InspectionItem){
                        InspectionItem inspection = (InspectionItem) sourceHolder.source;
                        if (dataDraf!=null){
                            try {
                                inspection = copyDataDraft(inspection);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        ImageView imgPhoto = ((ItemViewHolder) holder).imgPhoto;
                        if (inspection.getImage() != null||!inspection.getPhotoPath().equals("")){
                            imgPhoto.setImageResource(R.drawable.ic_add_photo);
                            Log.e("check_photo",inspection.getPhotoPath());
                        }else {
                            imgPhoto.setImageResource(R.drawable.ic_add_photo_gray);
                        }
                        ImageView imgMessage = ((ItemViewHolder) holder).imgMessage;
                        if (inspection.getComment() != null && inspection.getComment().trim().length() > 0){
                            imgMessage.setImageResource(R.drawable.ic_comment);
                        }else {
                            imgMessage.setImageResource(R.drawable.ic_comment_gray);
                        }
                        ((ItemViewHolder) holder).inspection = inspection;
                        ((ItemViewHolder) holder).check.setChecked(inspection.isFinish());

                        ((ItemViewHolder) holder).tvTitle.setText(((InspectionItem) sourceHolder.source).getName());
                        InspectionItem finalInspection = inspection;
                        ((ItemViewHolder) holder).setListener(new ItemViewHolder.OnEventListener() {
                            @Override
                            public void photoClick() {
                                if (null != listener){
                                    listener.onPhotoClick(finalInspection);
                                }
                            }

                            @Override
                            public void messageClick() {
                                if (null != listener){
                                    listener.onMessageClick(finalInspection);
                                }
                            }

                            @Override
                            public void convertClick() {
                                if (null != listener){
                                    listener.onConvertClick(finalInspection);
                                }
                            }

                            @Override
                            public void checkChanged(boolean changed) {
                                if(finalInspection.getIsChecked() == 0) {
                                    finalInspection.setFinish(false);
                                }
                                else {
                                    finalInspection.setFinish(true);
                                }
                                if (null != listener){
                                    listener.onCheckChanged(finalInspection);
                                }
                            }
                        });
                    }
                }
            }
        }
//        super.onBindViewHolder(holder, position);
    }

    public void setDataDraf(JSONObject dataDraf) {
        Log.e("check","setDataDraft");
        this.dataDraf = dataDraf;
    }

    public InspectionItem copyDataDraft(InspectionItem item) throws JSONException {
        Log.e("check_title","I"+String.valueOf(item.getId()));

        if (dataDraf.optString("I"+String.valueOf(item.getId()),"-1")!="-1"&&!listInspectionCopied.contains(item.getId())){
            JSONObject jsonItemDrafr = dataDraf.getJSONObject("I"+String.valueOf(item.getId()));
            item.setComment(jsonItemDrafr.optString("comment")=="null"?"":jsonItemDrafr.optString("comment"));
            item.setIsChecked(jsonItemDrafr.optInt("status"));
            if (jsonItemDrafr.optString("photo")!="null"){
                item.setPhotoPath("have photo");
            }
            listInspectionCopied.add(item.getId());
        }
        return item;
    }

    public void setListener(ItemViewHolderEventListener listener) {
        this.listener = listener;
    }

    public static class HeaderViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        public HeaderViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,CompoundButton.OnCheckedChangeListener{
        TextView tvTitle;
        ImageView imgMessage,imgPhoto,imgConvert;
        Switch check;
        InspectionItem inspection;
        OnEventListener listener;
        public ItemViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            imgMessage = itemView.findViewById(R.id.imgMess);
            imgPhoto = itemView.findViewById(R.id.imgPhoto);
            imgConvert = itemView.findViewById(R.id.imgConvert);
            check = itemView.findViewById(R.id.check);

            if (Offlinemode.checkNetworkConnection(imgConvert.getContext())){
                imgConvert.setOnClickListener(this);
            }else {
                imgConvert.setVisibility(View.INVISIBLE);
            }

            imgMessage.setOnClickListener(this);
            imgPhoto.setOnClickListener(this);
//            imgConvert.setOnClickListener(this);
            check.setOnClickListener(this);
            check.setOnCheckedChangeListener(this);
        }

        public void setListener(OnEventListener listener) {
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            if (null == listener){return;}
            if (v == imgPhoto){
                listener.photoClick();
            }else if (v == imgMessage){
                listener.messageClick();
            }else if (v == imgConvert){
                listener.convertClick();
            }
            else {
                if (v == check) {
                    inspection.onCheckClick();
                }
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (listener != null){
                listener.checkChanged(isChecked);
            }
        }

        public interface OnEventListener{
            void photoClick();
            void messageClick();
            void convertClick();
            void checkChanged(boolean changed);
        }
    }
    public interface ItemViewHolderEventListener{
        void onPhotoClick(InspectionItem inspectionItem);
        void onMessageClick(InspectionItem inspectionItem);
        void onConvertClick(InspectionItem inspectionItem);
        void onCheckChanged(InspectionItem inspectionItem);
    }
}
