package com.mybos.bmapp.Activity.Inspection.CreateInspection;

import android.content.Context;

import com.mybos.bmapp.Activity.Inspection.InspectionList.InspectionListController;
import com.mybos.bmapp.Data.Mapper.Apartment.ApartmentMapper;
import com.mybos.bmapp.Data.Mapper.Inspection.InspectionMapper;
import com.mybos.bmapp.Data.Model.Apartment.ApartmentList;
import com.mybos.bmapp.Data.Model.Inspection.Inspection;

import org.json.JSONObject;

/**
 * Created by EmLaAi on 18/04/2018.
 *
 * @author EmLaAi
 */
public class MakeInpsectionController {
    public void getInpsection(Context context, int buildingId, int inspectionId, SuccessCallback<Inspection> onSuccess, FailureCallback onFailure){
        InspectionMapper mapper = new InspectionMapper();
        mapper.get(context,buildingId,inspectionId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getApartmentList(Context context, int building, SuccessCallback<ApartmentList> onSuccess,FailureCallback onFailure){
        ApartmentMapper mapper = new ApartmentMapper();
        mapper.get(context,building,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void getInspectionDradft(Context context, int buildingId, String historyId, String inspectionId, InspectionListController.SuccessCallback<JSONObject> onSuccess, InspectionListController.FailureCallback onFailure){
        InspectionMapper mapper = new InspectionMapper();
        mapper.getInspectionDraft(context,buildingId,historyId,inspectionId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public void saveInspectionHistory(Context context,int building,int inspectionId,
                                      Inspection inspection,int historyId,String apartmentId,
                                      SuccessCallback<Integer> onSuccess,FailureCallback onFailure){
        InspectionMapper mapper = new InspectionMapper();
        mapper.saveHistory(context,building,inspectionId,inspection,historyId,apartmentId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
