package com.mybos.bmapp.Activity.Dashboard;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.google.firebase.FirebaseApp;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.mybos.bmapp.Activity.Base.a_base_drawer_with_no_action_bar;
import com.mybos.bmapp.Activity.Dashboard.Controller.DashboardController;
import com.mybos.bmapp.Activity.Offlinemode.ChangNetworkStateReciver;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Menu.DashboardFunctionalList;
import com.mybos.bmapp.Data.Model.Auth.Features;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.Data.Model.Building.BuildingList;
import com.mybos.bmapp.Data.Model.Inspection.InspectionOfflinetoSaveList;
import com.mybos.bmapp.Data.Model.Version;
import com.mybos.bmapp.Data.Response.WeatherResponse;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.ButtonUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;

public class DashboardActivity extends a_base_drawer_with_no_action_bar {

    // hold flipper info
    private ViewFlipper flipper;
    private GestureDetector viewGestureDetector;
    private DashboardController controller = new DashboardController();
    private BuildingList buildingList;
    private boolean needRequestBuildingList = false;
    public static boolean loading = true;
    private static String currentAppVersion = "0";
    private static String versionOnStore = "--";
    private static final int REQUEST_PERMISSION=99;
    BroadcastReceiver myBroadcast;
    public static String firebaseToken="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_dashboard);
        // request permissionsNeeded
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_NETWORK_STATE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_PERMISSION);
        }

        if (Offlinemode.checkNetworkConnection(this)){
            //FCM active device
            FirebaseMessaging.getInstance().getToken().addOnSuccessListener(this, instanceIdResult -> {
                String FirebaseToken = instanceIdResult;
                firebaseToken = FirebaseToken;
                Log.e("Firebase_token",FirebaseToken);
                controller.activeDevice(DashboardActivity.this,FirebaseToken,success->{
                    Log.e("Firebase_connect","ok");
                },error -> {
                    Log.e("Connect Firebase Failed",error.toString());
                });
            });
            //checking version
            checkingShowDialogUpdate();
        }else {
            updateBuildingUIOffline();
        }

        IntentFilter intentFilter = new IntentFilter(ChangNetworkStateReciver.NETWORK_AVAILABLE_ACTION);
        myBroadcast = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (Offlinemode.checkNetworkConnection(DashboardActivity.this)){
                    checkShowBtnSync();
                    ((Button) findViewById(R.id.textButtonOffline)).setVisibility(View.INVISIBLE);
                    setupDashboardFunctionalBox();
                    if (loading) {
                        displayLoadingScreen();
                    } else {
                        getWeatherForecast();
                    }
                    getBuildingList();
                }else{
                    ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.INVISIBLE);
                    ((Button) findViewById(R.id.textButtonOffline)).setVisibility(View.VISIBLE);
                    setupDashboardFunctionalBox();
                }
            }
        };
        registerReceiver(myBroadcast,intentFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(myBroadcast);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if (Offlinemode.checkNetworkConnection(this)) {
            if (needRequestBuildingList) {
                getBuildingList();
                needRequestBuildingList = false;
            }
            if (null != SingletonObjectHolder.getInstance().getBuildingList()) {
                if (null != SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding()) {
                    getWeatherForecast();
                }
            }
        }
    }

    private void checkShowBtnSync(){
        if (InspectionOfflinetoSaveList.get()!=null){
            if (InspectionOfflinetoSaveList.get().getListInspectionOfflinetoSave().size() > 0) {
                String sizeList = String.valueOf(InspectionOfflinetoSaveList.get().getListInspectionOfflinetoSave().size());
                ((Button) findViewById(R.id.btnSyncingInspection)).setText("Tap to sync " +sizeList+" inspection");
                ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.VISIBLE);
                setupBtnSyncingInspection();
            }
        }else {
            ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.INVISIBLE);
        }
    }

    private void setupBtnSyncingInspection(){
        ((Button) findViewById(R.id.btnSyncingInspection)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Offlinemode.dialogAsyncInspection(DashboardActivity.this);
                ((Button) findViewById(R.id.btnSyncingInspection)).setVisibility(View.INVISIBLE);
            }
        });
    }

    private void checkingShowDialogUpdate(){
        Realm realm = Realm.getDefaultInstance();
        Version version = realm.where(Version.class).findFirst();
        if (null==version){
            checkVersion();
            do {
                setUpDialogUpdate();
            }while ("--".equals(versionOnStore));
            return;
        }
        if (!(version.getDayShow().compareTo(Calendar.getInstance().getTime())<0)){
            checkVersion();
            do {
                setUpDialogUpdate();
            }while (versionOnStore.equals("--"));
        }
    }

    private void setupDashboardFunctionalBox() {
        RecyclerView recyclerView = findViewById(R.id.functionalGridView);
        ConnectivityManager cm = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        recyclerView.setAdapter(new DashboardFunctionalAdapter(this, id -> {
            switch (id) {
                case DashboardFunctionalList.ItemId.CASE_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.CASES)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToCaseList();
                            return;
                        }
                    }
                    break;
                case DashboardFunctionalList.ItemId.INSPECTION_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.BUILDING_INSPECTION)) {
                        navigateToInspectionList();
                        return;
                    }
                    break;
                case DashboardFunctionalList.ItemId.RESIDENT_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.RESIDENT)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToResidentList();
                            return;
                        }
                    }
                    break;
                case DashboardFunctionalList.ItemId.CONTRACTOR_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.CONTRACTOR)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToContractorList();
                            return;
                        }

                    }
                    break;
                case DashboardFunctionalList.ItemId.LIBRARY_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.BUILDING_LIBRARY)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToLibraryList();
                            return;
                        }

                    }
                    break;
                case DashboardFunctionalList.ItemId.ASSET_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.BUILDING_ASSET)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToAssetList();
                            return;
                        }
                    }
                    break;
                case DashboardFunctionalList.ItemId.BROADCAST_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.BROADCAST)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToBroadcast();
                            return;
                        }

                    }
                    break;
                case DashboardFunctionalList.ItemId.KEY_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.KEY)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToKey();
                            return;
                        }
                    }
                    break;
                case DashboardFunctionalList.ItemId.CALENDAR_ID:
                    if (Features.shared(this).hasFeature(Features.TYPES.CALENDAR)) {
                        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                            navigateToCalendar();
                            return;
                        }
                    }
                    break;
            }
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()){
                Toast.makeText(this, getString(R.string.AlertMessageUnathorizeException), Toast.LENGTH_LONG).show();
            }

        }));

        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    private void getBuildingPermissionAndFeatures(Building building) {
        Map<String, Boolean> checkMap = new HashMap<>();
        checkMap.put("error", false);
        checkMap.put("permission", false);
        checkMap.put("feature", false);
        controller.getPermission(building.getId(), this, (Void) -> {
            checkMap.put("permission", true);
            if (checkMap.get("permission") && checkMap.get("feature")) {
                this.dismissLoadingScreen();
            }
        }, (error) -> {
            if (!checkMap.get("error")) {
                checkMap.put("error", true);
                if (Offlinemode.checkNetworkConnection(this)){
                    displayAlert(getString(R.string.Error), "Permission " + error, (alert) -> {
                        List<Button> buttons = new ArrayList<>();

                        Button retry = ButtonUtils.createButton(this, R.string.Retry, () -> {
                            alert.dismiss();
                            this.getBuildingPermissionAndFeatures(building);
                        });
                        Button send = ButtonUtils.createButton(this, R.string.Send, () -> {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.recordException(new Exception(error));
                            alert.dismiss();
                            this.getBuildingPermissionAndFeatures(building);
                        });
                        Button logout = ButtonUtils.createButton(this, R.string.LogOut, this::doLogout);
                        buttons.addAll(Arrays.asList(retry, send, logout));
                        return buttons;
                    });
                }else {
                    Toast.makeText(this, "Offline_cannot getBuildingPermission", Toast.LENGTH_LONG).show();
                }

            }
        });
        controller.getFeatures(building.getId(), this, (Void) -> {
            checkMap.put("feature", true);
            if (checkMap.get("permission") && checkMap.get("feature")) {
                this.dismissLoadingScreen();
            }
        }, (error) -> {
            if (!checkMap.get("error")) {
                checkMap.put("error", true);
                if (Offlinemode.checkNetworkConnection(this)) {
                    displayAlert(getString(R.string.Error), "Feature: " + error, (alert) -> {
                        List<Button> buttons = new ArrayList<>();

                        Button retry = ButtonUtils.createButton(this, R.string.Retry, () -> {
                            alert.dismiss();
                            this.getBuildingPermissionAndFeatures(building);
                        });
                        Button send = ButtonUtils.createButton(this, R.string.Send, () -> {
                            FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
                            crashlytics.recordException(new Exception(error));
                            alert.dismiss();
                            this.getBuildingPermissionAndFeatures(building);
                        });
                        Button logout = ButtonUtils.createButton(this, R.string.LogOut, this::doLogout);
                        buttons.addAll(Arrays.asList(retry, send, logout));
                        return buttons;
                    });
                }else {
                    Toast.makeText(this, "Offline_cannot getBuildingPermission", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getBuildingList() {
        controller.getBuildingList(DashboardActivity.this, (BuildingList) -> {
            SingletonObjectHolder.getInstance().saveBuildingList(BuildingList);
            this.buildingList = SingletonObjectHolder.getInstance().getBuildingList();

            setupBuildingDropdownList();

            if (loading) {
                getWeatherForecast();
                //download inspection for offine mode
                Offlinemode.downloadInspectionFOfflinemode(this);
                getBuildingPermissionAndFeatures(SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding());
                loading = false;
            }
        }, (error) -> {
            if (isVisible) {
                Toast.makeText(this, "getBuildingList_error", Toast.LENGTH_SHORT).show();
                Log.e("getBuildingList","failed");
            } else {
                needRequestBuildingList = true;
            }
            this.dismissLoadingScreen();
            doLogout();
        });
    }

    public void setUpDialogUpdate() {
        Log.e("on setup",currentAppVersion+"-"+versionOnStore);
        if (!currentAppVersion.equals(versionOnStore)&&!"--".equals(versionOnStore)){
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this, R.style.DialogTheme);
            String message = "Update "+versionOnStore+" is available to download. Downloading the latest update you will get the latest feature, improvements" +
                    " and bug fixes of BM.";
            builder1.setTitle("New update available!");
            builder1.setMessage(message);
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "UPDATE",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.mybos.bmapp")));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.mybos.bmapp")));
                            }
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Version versionTmp = new Version();
                            versionTmp.setDayShow(Calendar.getInstance().getTime());
                            Version.save(versionTmp);
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    public void checkVersion() {
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            currentAppVersion = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        controller.getVersionFromStore(this,
                version -> {
                    if (null!=version){
                        versionOnStore = version;
                    }

                    Log.e("check_version",currentAppVersion+"-"+version);
                },
                (error) -> {
                    Log.e("get version failed", "");
                }
        );

    }

    public void getWeatherForecast() {
        Building building = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getWeatherInfo(this, building, this::updateBuildingUI, error -> {
            updateBuildingUI(new WeatherResponse());
        });
    }

    public void updateBuildingUI(WeatherResponse response) {
        if (!this.isVisible) {
            return;
        }
        Calendar c = Calendar.getInstance();
        Building building = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

        String StDate = String.valueOf(Integer.toString(c.get(Calendar.DATE))) + " " + DateFormat.format("MMMM", c);
        ((TextView) findViewById(R.id.tvMonth)).setText(StDate);
        ((TextView) findViewById(R.id.tvPlace)).setText(building.getSuburd().toUpperCase());
        Offlinemode.setCity(building.getSuburd().toUpperCase());
        Offlinemode.setMonthDate(StDate);

        ((TextView) findViewById(R.id.tvWelcome)).setText(R.string.dashboard_welcome_title);

        String StBuilding = building.getName();
        ((TextView) findViewById(R.id.tvBuilding)).setText(StBuilding);
        Offlinemode.setBuildingName(StBuilding);

        BuildingInfoCollection.setBuildingName(building.getName());

        ((TextView) findViewById(R.id.tvStatus)).setText(response.status);
        Offlinemode.setTempStatus(response.status);

        String StTemp = response.temp + "\u2103";
        ((TextView) findViewById(R.id.tvTemp)).setText(StTemp);
        Offlinemode.setTemp(StTemp);

        findViewById(R.id.icHamburger).setOnClickListener((v) -> {
            if (Offlinemode.checkNetworkConnection(DashboardActivity.this)){
                if (drawer.isDrawerVisible(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
            }else{
                Log.e("check_click","click");
            }

        });
    }

    private void updateBuildingUIOffline(){
        ((TextView) findViewById(R.id.tvWelcome)).setText(R.string.dashboard_welcome_title);
        ((TextView) findViewById(R.id.tvMonth)).setText(Offlinemode.getMonthDate());
        ((TextView) findViewById(R.id.tvPlace)).setText(Offlinemode.getCity());
        ((TextView) findViewById(R.id.tvBuilding)).setText(Offlinemode.getBuildingName());
        ((TextView) findViewById(R.id.tvStatus)).setText(Offlinemode.getTempStatus());
        ((TextView) findViewById(R.id.tvTemp)).setText(Offlinemode.getTemp());

        findViewById(R.id.icHamburger).setOnClickListener((v) -> {
            if (Offlinemode.checkNetworkConnection(this)){
                if (drawer.isDrawerVisible(Gravity.LEFT)) {
                    drawer.closeDrawer(Gravity.LEFT);
                } else {
                    drawer.openDrawer(Gravity.LEFT);
                }
            }else{
                Log.e("check_click","click");
            }

        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean onTouchEventResult = false;
        try {
            viewGestureDetector.onTouchEvent(event);
            onTouchEventResult = super.onTouchEvent(event);
        } catch (NullPointerException e) {
        }

        return onTouchEventResult;
    }

    class FlipperGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (e1.getX() > e2.getX()) {
                flipper.showNext();
            }
            if (e1.getX() < e2.getX()) {
                flipper.showPrevious();
            }
            return super.onFling(e1, e2, velocityX, velocityY);
        }
    }
}
