package com.mybos.bmapp.Activity.Keys.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Key.BuildingKey;
import com.mybos.bmapp.Data.Model.Key.History;
import com.mybos.bmapp.Data.Model.Key.KeyInOut;
import com.mybos.bmapp.R;

import java.util.ArrayList;


public class KeyContractorListRecyclerAdapter extends RecyclerView.Adapter<KeyContractorListRecyclerAdapter.KeyContractorItemViewHolder> {

    ArrayList<BuildingKey> showKey;
    private OnItemClick listener;
    private OnItemClickSign listenerSign;

    public KeyContractorListRecyclerAdapter(){
        showKey = new ArrayList<>(BuildingKey.getBuildingKeys());
    }

    @NonNull
    @Override
    public KeyContractorItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.key_contractor_list_adaptor, parent, false);
        return new KeyContractorItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull KeyContractorItemViewHolder holder, int position) {

        BuildingKey buildingKey = showKey.get(position);
        holder.tvApartmentIdContractor.setText(buildingKey.getKeyName());
        if (buildingKey.getKeyStatus().equals("1")) {
            holder.ivStateKeyContractor.setImageResource(R.drawable.ic_group_7);
            holder.btnSignOutInContractor.setText(R.string.key_sign_out_key);
        } else {
            holder.ivStateKeyContractor.setImageResource(R.drawable.ic_group_7_1);
            holder.btnSignOutInContractor.setText(R.string.key_sign_in_key);
        }
        holder.btnViewHistoryContractor.setOnClickListener(v -> {
            History.setKeyIdSelected(buildingKey.getKeyID());
            listener.didSelectHistory(buildingKey);
        });
        holder.btnSignOutInContractor.setOnClickListener(v->{
            History.setKeyIdSelected(buildingKey.getKeyID());
            KeyInOut.setKeyIdSelected(buildingKey.getKeyID());
            listenerSign.didSelectSign(buildingKey);
        });
    }

    public void performSearch(String keyword) {
        showKey = new ArrayList<>();
        if (keyword.isEmpty()) {
            showKey = new ArrayList<>(BuildingKey.getBuildingKeys());
        } else {
            for (BuildingKey key : BuildingKey.getBuildingKeys()) {
                if (key.getKeyName().toLowerCase().contains(keyword.toLowerCase())) {
                    showKey.add(key);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return showKey.size();
    }

    public class KeyContractorItemViewHolder extends RecyclerView.ViewHolder {

        TextView tvApartmentIdContractor;
        ImageView ivStateKeyContractor;
        Button btnSignOutInContractor,btnViewHistoryContractor;

        public KeyContractorItemViewHolder(View itemView) {
            super(itemView);
            tvApartmentIdContractor = itemView.findViewById(R.id.tvApartmentIdContractor);
            ivStateKeyContractor = itemView.findViewById(R.id.imageViewStateKeyContractor);
            btnSignOutInContractor = itemView.findViewById(R.id.btnSignOutInContractor);
            btnViewHistoryContractor = itemView.findViewById(R.id.btnViewHistoryContractor);
        }
    }

    public interface OnItemClick {
        void didSelectHistory(BuildingKey a);
    }

    public interface OnItemClickSign {
        void didSelectSign(BuildingKey a);
    }

    public void setListener(OnItemClick listener) {
        this.listener = listener;
    }
    public void setListenerSign(OnItemClickSign listener) {
        this.listenerSign = listener;
    }

}
