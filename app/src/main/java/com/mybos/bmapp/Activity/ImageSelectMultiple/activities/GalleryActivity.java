package com.mybos.bmapp.Activity.ImageSelectMultiple.activities;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AlertDialog.Builder;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.mybos.bmapp.Activity.ImageSelectMultiple.adapters.GalleryImagesAdapter;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Image;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Params;
import com.mybos.bmapp.Activity.ImageSelectMultiple.utils.Utils;
import com.mybos.bmapp.R;
import com.mybos.bmapp.R.dimen;
import com.mybos.bmapp.R.layout;
import com.mybos.bmapp.R.id;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.concurrent.Executors;

public class GalleryActivity extends BaseActivity {
    RelativeLayout parentLayout;
    Toolbar toolbar;
    TextView toolbar_title;
    RecyclerView recycler_view;
    AlertDialog alertDialog;
    GalleryImagesAdapter imageAdapter;
    ArrayList<Image> imagesList = new ArrayList();
    private Params params;

    public GalleryActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(layout.activity_gallery);
        this.parentLayout = (RelativeLayout)this.findViewById(id.parentLayout);
        this.toolbar = (Toolbar)this.findViewById(id.toolbar);
        this.toolbar_title = (TextView)this.findViewById(id.toolbar_title);
        this.recycler_view = (RecyclerView)this.findViewById(id.recycler_view);
        this.init();
        this.checkForPermissions();
    }

    private void init() {
        Utils.initToolBar(this, this.toolbar, true);
        this.toolbar_title.setText("Select Images");
        if (this.getIntent() != null && this.getIntent().hasExtra("PARAMS")) {
            Object object = this.getIntent().getSerializableExtra("PARAMS");
            if (object instanceof Params) {
                this.params = (Params)object;
            } else {
                Utils.showLongSnack(this.parentLayout, "Provided serializable data is not an instance of Params object.");
                this.setEmptyResult();
            }
        }

        this.handleInputParams();
        this.recycler_view.setLayoutManager(new StaggeredGridLayoutManager(this.getColumnCount(), 1));
    }

    private void handleInputParams() {
        if (this.params.getPickerLimit() == 0) {
            Utils.showLongSnack(this.parentLayout, "Please mention the picker limit as a parameter.");
            this.setEmptyResult();
        }

        Utils.setViewBackgroundColor(this, this.toolbar, this.params.getToolbarColor());
        if (this.params.getCaptureLimit() == 0) {
            this.params.setCaptureLimit(this.params.getPickerLimit());
        }

    }

    private void checkForPermissions() {
        if (this.hasStoragePermission(this)) {
            this.getImagesFromStorage();
        } else {
            this.requestStoragePermissions(this, 1);
        }

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode) {
            case 1:
                if (this.validateGrantedPermissions(grantResults)) {
                    this.getImagesFromStorage();
                } else {
                    Toast.makeText(this, "Permissions not granted.", Toast.LENGTH_LONG).show();
                    this.setEmptyResult();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    protected void onPause() {
        super.onPause();
        if (this.alertDialog != null && this.alertDialog.isShowing()) {
            this.alertDialog.dismiss();
        }

    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (this.imageAdapter != null) {
            this.recycler_view.setHasFixedSize(true);
            StaggeredGridLayoutManager manager = (StaggeredGridLayoutManager)this.recycler_view.getLayoutManager();
            manager.setSpanCount(this.getColumnCount());
            this.recycler_view.setLayoutManager(manager);
            this.recycler_view.requestLayout();
        }

    }

    public void onBackPressed() {
        this.handleBackPress();
    }

    private void handleBackPress() {
        if (this.imageAdapter != null) {
            if (this.imageAdapter.getSelectedIDs().size() > 0) {
                this.imageAdapter.disableSelection();
                this.setCountOnToolbar();
            } else {
                this.setEmptyResult();
            }
        } else {
            this.setEmptyResult();
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == 16908332) {
            this.handleBackPress();
            return true;
        } else if (item.getItemId() == id.action_done) {
            if (this.imageAdapter != null) {
                this.prepareResult();
            } else {
                this.setEmptyResult();
            }

            return true;
       } else {
            return super.onOptionsItemSelected(item);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == -1) {
            switch(requestCode) {
                case 4:
                    this.handleResponseIntent(intent);
                default:
            }
        }
    }

    private void handleResponseIntent(Intent intent) {
        ArrayList<Image> imagesList = intent.getParcelableArrayListExtra("BUNDLE_LIST");
        this.refreshView(imagesList);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.gallery_menu, menu);
        return true;
    }

    private void prepareResult() {
        ArrayList<Long> selectedIDs = this.imageAdapter.getSelectedIDs();
        ArrayList<Image> selectedImages = new ArrayList(selectedIDs.size());
        Iterator var3 = this.imagesList.iterator();

        while(var3.hasNext()) {
            Image image = (Image)var3.next();
            if (selectedIDs.contains(image._id)) {
                selectedImages.add(image);
            }
        }

        Intent intent = new Intent();
        intent.putParcelableArrayListExtra("BUNDLE_LIST", selectedImages);
        this.setIntentResult(intent);
    }

    private void getImagesFromStorage() {
        (new GalleryActivity.ApiSimulator(this)).executeOnExecutor(Executors.newSingleThreadExecutor(), new Void[0]);
    }

    private void populateView(ArrayList<Image> images) {
        if (this.imagesList == null) {
            this.imagesList = new ArrayList();
        }

        this.imagesList.addAll(images);
        ArrayList<Image> dupImageSet = new ArrayList();
        dupImageSet.addAll(this.imagesList);
        StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(this.getColumnCount(), 1);
        mLayoutManager.setGapStrategy(2);
        this.recycler_view.setLayoutManager(mLayoutManager);
        this.imageAdapter = new GalleryImagesAdapter(this, dupImageSet, this.getColumnCount(), this.params);
        this.recycler_view.setAdapter(this.imageAdapter);
        this.imageAdapter.setOnHolderClickListener(new OnClickListener() {
            public void onClick(View view) {
                Long imageId = (Long)view.getTag(id.image_id);
                GalleryActivity.this.imageAdapter.setSelectedItem(view, imageId);
                GalleryActivity.this.setCountOnToolbar();
            }
        });
    }

    private void refreshView(ArrayList<Image> images) {
        Collections.reverse(images);
        Iterator var2 = images.iterator();

        while(var2.hasNext()) {
            Image image = (Image)var2.next();
            this.imagesList.add(0, image);
        }

        ArrayList<Image> dupImageSet = new ArrayList();
        dupImageSet.addAll(this.imagesList);
        if (this.imageAdapter == null) {
            this.imageAdapter = new GalleryImagesAdapter(this, dupImageSet, this.getColumnCount(), this.params);
        } else {
            this.imageAdapter.setItems(dupImageSet);
        }

        this.imageAdapter.notifyDataSetChanged();
    }

    private int getColumnCount() {
        if (this.params.getColumnCount() != 0) {
            return this.params.getColumnCount();
        } else {
            DisplayMetrics displayMetrics;
            float screenWidthInDp;
            if (this.params.getThumbnailWidthInDp() != 0) {
                displayMetrics = this.getResources().getDisplayMetrics();
                screenWidthInDp = (float)displayMetrics.widthPixels / displayMetrics.density;
                return (int)(screenWidthInDp / (float)this.params.getThumbnailWidthInDp());
            } else {
                displayMetrics = this.getResources().getDisplayMetrics();
                screenWidthInDp = (float)displayMetrics.widthPixels / displayMetrics.density;
                float thumbnailDpWidth = this.getResources().getDimension(dimen.thumbnail_width) / displayMetrics.density;
                return (int)(screenWidthInDp / thumbnailDpWidth);
            }
        }
    }

    private void setCountOnToolbar() {
        if (this.imageAdapter.getSelectedIDs().size() > 0) {
            this.toolbar_title.setText("" + this.imageAdapter.getSelectedIDs().size() + " selected");
        } else {
            this.toolbar_title.setText("Select Images");
        }

    }

    private void setEmptyResult() {
        this.setResult(0);
        this.finish();
    }

    private void setIntentResult(Intent intent) {
        this.setResult(-1, intent);
        this.finish();
    }

    public void showLimitAlert(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

    }

    private class ApiSimulator extends AsyncTask<Void, Void, ArrayList<Image>> {
        Activity context;
        String error = "";

        public ApiSimulator(Activity context) {
            this.context = context;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            GalleryActivity.this.showProgressDialog("Loading..");
        }

        protected ArrayList<Image> doInBackground(@NonNull Void... voids) {
            ArrayList<Image> images = new ArrayList();
            Cursor imageCursor = null;

            try {
                String[] columns = new String[]{"_id", "_data", "date_added", "height", "width"};
                String orderBy = "date_added DESC";
                imageCursor = GalleryActivity.this.getContentResolver().query(Media.EXTERNAL_CONTENT_URI, columns, (String)null, (String[])null, "date_added DESC");

                while(imageCursor.moveToNext()) {
                    long _id = imageCursor.getLong(imageCursor.getColumnIndex("_id"));
                    int height = imageCursor.getInt(imageCursor.getColumnIndex("height"));
                    int width = imageCursor.getInt(imageCursor.getColumnIndex("width"));
                    String imagePath = imageCursor.getString(imageCursor.getColumnIndex("_data"));
                    Uri uri = Uri.withAppendedPath(Media.EXTERNAL_CONTENT_URI, String.valueOf(_id));
                    Image image = new Image(_id, uri, imagePath, height > width);
                    images.add(image);
                }
            } catch (Exception var16) {
                var16.printStackTrace();
                this.error = var16.toString();
            } finally {
                if (imageCursor != null && !imageCursor.isClosed()) {
                    imageCursor.close();
                }

            }

            return images;
        }

        protected void onPostExecute(ArrayList<Image> images) {
            super.onPostExecute(images);
            GalleryActivity.this.dismissProgressDialog();
            if (!GalleryActivity.this.isFinishing()) {
                if (this.error.length() == 0) {
                    GalleryActivity.this.populateView(images);
                } else {
                    Utils.showLongSnack(GalleryActivity.this.parentLayout, this.error);
                }

            }
        }
    }
}
