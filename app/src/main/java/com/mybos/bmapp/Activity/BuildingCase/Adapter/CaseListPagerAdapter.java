package com.mybos.bmapp.Activity.BuildingCase.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.mybos.bmapp.Activity.BuildingCase.CaseList.Fragment.CaseListFragment;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by EmLaAi on 24/03/2018.
 *
 * @author EmLaAi
 */

public class CaseListPagerAdapter extends FragmentStatePagerAdapter {
    private @SearchTableQueryBuilder.SectionHint.CaseSectionHint List<String> sections
            = Arrays.asList(SearchTableQueryBuilder.SectionHint.CaseCurrent,
                            SearchTableQueryBuilder.SectionHint.CaseCompleted,
                            SearchTableQueryBuilder.SectionHint.CaseTrash);
    private Context context;
    public CaseListPagerAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return CaseListFragment.newInstance(sections.get(position));
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String section = sections.get(position);
        if (section.equals(SearchTableQueryBuilder.SectionHint.CaseCurrent)){
            return context.getString(R.string.case_list_top_tab_bar_current);
        }else if (section.equals(SearchTableQueryBuilder.SectionHint.CaseCompleted)){
            return context.getString(R.string.case_list_top_tab_bar_completed);
        }else if (section.equals(SearchTableQueryBuilder.SectionHint.CaseTrash)){
            return context.getString(R.string.case_list_top_tab_bar_trash);
        }
        return super.getPageTitle(position);
    }
}
