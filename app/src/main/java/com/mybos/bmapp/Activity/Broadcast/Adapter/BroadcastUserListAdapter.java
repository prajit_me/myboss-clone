package com.mybos.bmapp.Activity.Broadcast.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mybos.bmapp.Component.RecycleAdapter.AbstractScrollToLoadMoreRecyclerAdapter;
import com.mybos.bmapp.Data.Model.Broadcast.Broadcast;
import com.mybos.bmapp.Data.Model.Broadcast.User;
import com.mybos.bmapp.R;


public class BroadcastUserListAdapter extends AbstractScrollToLoadMoreRecyclerAdapter<User> {

    @Override
    public int getItemLayout() {
        return R.layout.a_broadcast_user;
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new BroadcastViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof BroadcastViewHolder){
            User user = User.getUsers().get(i);
            if ((user.getName().equals("null"))) {
                ((BroadcastViewHolder) holder).tvName.setText("");
            } else {
                ((BroadcastViewHolder) holder).tvName.setText(user.getName());
            }
            ((BroadcastViewHolder) holder).tvUnit.setText(String.valueOf(user.getUnit()));

            if (Broadcast.getChoose_users().contains(user)) {
                ((BroadcastViewHolder) holder).checkBox.setChecked(true);
            }else {
                ((BroadcastViewHolder) holder).checkBox.setChecked(false);
            }

            // set check for click on cardView object
            ((BroadcastViewHolder) holder).setListener(()->{
                if (((BroadcastViewHolder) holder).checkBox.isChecked()) {
                    ((BroadcastViewHolder) holder).checkBox.setChecked(false);
                    Broadcast.getChoose_users().remove(user);
                } else if (!((BroadcastViewHolder) holder).checkBox.isChecked()) {
                    ((BroadcastViewHolder) holder).checkBox.setChecked(true);
                    Broadcast.getChoose_users().add(user);
                }
            });

            ((BroadcastViewHolder) holder).checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((BroadcastViewHolder) holder).checkBox.isChecked()) {
                        Broadcast.getChoose_users().add(user);
                    } else if (!((BroadcastViewHolder) holder).checkBox.isChecked()) {
                        Broadcast.getChoose_users().remove(user);
                    }
                }
            });
        }
    }

    class BroadcastViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvName,tvUnit;
        CheckBox checkBox;
        OnItemClickListener listener;
        public BroadcastViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvUnit = itemView.findViewById(R.id.tvUnit);
            checkBox = itemView.findViewById(R.id.cbUser);
            itemView.findViewById(R.id.containUser).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (null != listener){
                listener.onClick();
            }
        }
        public void setListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }
    public static interface OnItemClickListener{
        void onClick();
    }
}
