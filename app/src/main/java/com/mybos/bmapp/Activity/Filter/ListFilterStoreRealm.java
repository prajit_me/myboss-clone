package com.mybos.bmapp.Activity.Filter;

import android.os.Parcel;
import android.os.Parcelable;

import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.RealmClass;

@RealmClass
public class ListFilterStoreRealm implements Parcelable, RealmModel {

    RealmList<FilterStoreRealm> filterStoreRealms;

    protected ListFilterStoreRealm(Parcel in) {
    }

     public ListFilterStoreRealm (){
         filterStoreRealms = new RealmList<>();
    }

    public void addNewFilter(FilterStoreRealm filter){
        boolean contain = false;
        for (FilterStoreRealm filterStore:filterStoreRealms
             ) {
            if (filter.getBuildingId() == filterStore.getBuildingId()){
                filterStore.setSelectedFilterResident(filter.getSelectedFilterResident());
                filterStore.setSelectedFilterAsset(filter.getSelectedFilterAsset());
                filterStore.setSelectedFilterContractor(filter.getSelectedFilterContractor());

                filterStore.setSelectedFCContractor(filter.getSelectedFCContractor());
                filterStore.setSelectedFCPriority(filter.getSelectedFCPriority());
                filterStore.setSelectedFCType(filter.getSelectedFCType());
                filterStore.setSelectedFCStatus(filter.getSelectedFCStatus());
                contain = true;
            }
        }

        if (!contain){
            filterStoreRealms.add(filter);
        }

    }

    public static final Creator<ListFilterStoreRealm> CREATOR = new Creator<ListFilterStoreRealm>() {
        @Override
        public ListFilterStoreRealm createFromParcel(Parcel in) {
            return new ListFilterStoreRealm(in);
        }

        @Override
        public ListFilterStoreRealm[] newArray(int size) {
            return new ListFilterStoreRealm[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        filterStoreRealms = new RealmList<>();
    }

    public RealmList<FilterStoreRealm> getFilterStoreRealms() {
        return filterStoreRealms;
    }

    public void setFilterStoreRealms(RealmList<FilterStoreRealm> filterStoreRealms) {
        this.filterStoreRealms = filterStoreRealms;
    }

    public FilterStoreRealm getFilterByBuildingId(int id){
        for (FilterStoreRealm filter:filterStoreRealms
             ) {
            if (filter.getBuildingId()==id){
                return filter;
            }
        }
        return null;
    }

    public static void save(ListFilterStoreRealm listFilterStoreRealm){
        try(Realm realm = Realm.getDefaultInstance()){
            realm.executeTransaction(mRealm->{
                mRealm.delete(FilterStoreRealm.class);
                mRealm.delete(ListFilterStoreRealm.class);
                mRealm.insertOrUpdate(listFilterStoreRealm);
            });
        }
    }

    public static ListFilterStoreRealm get(){
        try (Realm realm = Realm.getDefaultInstance()){
            ListFilterStoreRealm listFilterStoreRealm = realm.where(ListFilterStoreRealm.class).findFirst();
            return null != listFilterStoreRealm ? realm.copyFromRealm(listFilterStoreRealm) : null;
        }
    }
}
