package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.R;

public class KeyApartmentOtherAdapter extends BaseAdapter {

    private Context context;
    private int layout;

    public KeyApartmentOtherAdapter(Context context, int layout){
        this.context = context;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return Apartment.getApartment_keys_O().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout, null);

        TextView tvKeyIdKAO = view.findViewById(R.id.tvKeyIdKAO);
        TextView tvKeyTypeKAO = view.findViewById(R.id.tvKeyTypeKAO);
        TextView tvLocationKAO = view.findViewById(R.id.tvLocationKAO);
        TextView tvOwnerKAO = view.findViewById(R.id.tvOwnerKAO);
        TextView tvContactKAO = view.findViewById(R.id.tvContactKAO);

        Apartment apartment = Apartment.getApartment_keys_O().get(i);
        tvKeyIdKAO.setText(apartment.getKey());
        tvKeyTypeKAO.setText(apartment.getTypeText());
        tvLocationKAO.setText(apartment.getLocationText());
        tvOwnerKAO.setText((apartment.getOwner().equals("null"))?"-"
                :apartment.getOwner());
        return view;
    }
}
