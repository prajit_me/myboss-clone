package com.mybos.bmapp.Activity.BuildingCase.EditCase.Fragment;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.BuildingCase.BuildingCaseMapper;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;


/**
 * Created by EmLaAi on 23/04/2018.
 *
 * @author EmLaAi
 */
public class EditCaseDetailController {
    public void getBuildingCaseStatus(Context context, int buildingId, SuccessCallback<BuildingCaseStatusList> onSuccess, FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.getAllCaseStatus(context,buildingId,onSuccess::onSuccess,onFailure::onFailure);
    }
    public void updateBuildingCaseStatus(Context context,int buildingId,int caseId,int statusId,SuccessCallback<Void> onSuccess,FailureCallback onFailure){
        BuildingCaseMapper mapper = new BuildingCaseMapper();
        mapper.update(context,buildingId,caseId,statusId,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
