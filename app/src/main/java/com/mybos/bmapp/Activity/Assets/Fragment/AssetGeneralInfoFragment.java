package com.mybos.bmapp.Activity.Assets.Fragment;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class AssetGeneralInfoFragment extends Fragment {

    private TextView tvCategory, tvLocation, tvSerial, tvAssetValue, tvDescription, tvMake, tvModel,
                    tvServiceContractor, tvPhone, tvEmail, tvContactPerson;
    private ImageView ivPhone, ivEmail;
    private final int CALL_PHONE_PERMISSION = 1000;
    private String savedNumber;
    private BuildingAsset SelectedAsset = BuildingAssetInfoCollector.getSelectedBuildingAsset();

    public void callNumber(String number){
        if (number == null){
            return;
        }
        String uriNumber = "tel:" + number.replace("\\s+","");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uriNumber));
        if(ContextCompat.checkSelfPermission(this.getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
            savedNumber = number;
            ActivityCompat.requestPermissions(this.getActivity(), new String[]{Manifest.permission.CALL_PHONE},CALL_PHONE_PERMISSION);
        }else {
            startActivity(intent);
        }
    }

    public void sendEmail(String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL,new String[]{email});
        if (intent.resolveActivity(this.getActivity().getPackageManager()) != null){
            startActivity(intent);
        }else {
            Toast.makeText(this.getActivity(), getString(R.string.ALertNoEmailApp), Toast.LENGTH_LONG).show();
        }
    }

    private void showInfo() {
        try {
            tvCategory.setText(SelectedAsset.getCategoryName());
            tvLocation.setText(SelectedAsset.getLocation().equals("null")?"":SelectedAsset.getLocation());
            tvSerial.setText(String.valueOf(SelectedAsset.getSerial()));
            tvAssetValue.setText(String.valueOf(SelectedAsset.getAssetValue()));
            tvDescription.setText(String.valueOf(SelectedAsset.getDescription().equals("null")?"":SelectedAsset.getDescription()));
            tvMake.setText(String.valueOf(SelectedAsset.getMake().equals("null")?"":SelectedAsset.getMake()));
            tvModel.setText(String.valueOf(SelectedAsset.getModel().equals("null")?"":SelectedAsset.getModel()));

            tvServiceContractor.setText(String.valueOf(SelectedAsset.getContractors().get(0).getCompany()));
            tvPhone.setText(String.valueOf(SelectedAsset.getContractors().get(0).getPhone()));
            tvEmail.setText(String.valueOf(SelectedAsset.getContractors().get(0).getMainContact().getEmail()));
            tvContactPerson.setText(String.valueOf(SelectedAsset.getContractors().get(0).getMainContact().getName()));
        }
        catch (Exception e) { }
    }

    private void startup() {
        //  SHOW INFO
        showInfo();
    }

    public AssetGeneralInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_asset_general_info, container, false);

        //  GET REFERENCES
        tvCategory = rootView.findViewById(R.id.tvCategoryName_AssetGeneralInfoFragment);
        tvLocation = rootView.findViewById(R.id.tvLocation_AssetGeneralInfoFragment);
        tvSerial = rootView.findViewById(R.id.tvSerialNumber_AssetGeneralInfoFragment);
        tvAssetValue = rootView.findViewById(R.id.tvValue_AssetGeneralInfoFragment);
        tvDescription = rootView.findViewById(R.id.tvDescription_AssetGeneralInfoFragment);
        tvMake = rootView.findViewById(R.id.tvMake_AssetGeneralInfoFragment);
        tvModel = rootView.findViewById(R.id.tvModel_AssetGeneralInfoFragment);
        tvServiceContractor = rootView.findViewById(R.id.tvServiceContractor_AssetGeneralInfoFragment);
        tvPhone = rootView.findViewById(R.id.tvPhone_AssetGeneralInfoFragment);
        tvEmail = rootView.findViewById(R.id.tvEmail_AssetGeneralInfoFragment);
        tvContactPerson = rootView.findViewById(R.id.tvContactPerson_AssetGeneralInfoFragment);

        ivPhone = rootView.findViewById(R.id.ivPhone_AssetGeneralInfoFragment);
        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SelectedAsset.getContractors().size() > 0) {
                    callNumber(SelectedAsset.getContractors().get(0).getPhone());
                }
            }
        });

        ivEmail = rootView.findViewById(R.id.ivEmail_AssetGeneralInfoFragment);
        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SelectedAsset.getContractors().size() > 0) {
                    sendEmail(SelectedAsset.getContractors().get(0).getMainContact().getEmail());
                }
            }
        });

        //  START UP
        startup();

        return rootView;
    }

}
