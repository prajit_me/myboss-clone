package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Calendar.File;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;

public class CalendarListDocAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<File> showList = new ArrayList<>();

    public CalendarListDocAdapter(Context context,String view){
        this.context = context;
        if (view.equals("doc")){
            showList = MaintenancePerDay.getMaintenancePerDaySelected().getDocuments();
        }else {
            showList = MaintenancePerDay.getMaintenancePerDaySelected().getInvoices();
        }

    }

    @Override
    public int getCount() {
        return showList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.calendar_a_file, null);

        File file = showList.get(i);

        TextView tvTitle = view.findViewById(R.id.tvTitle);
        ImageView ivFileType = view.findViewById(R.id.ivFileType);
        TextView tvDateUpdated = view.findViewById(R.id.tvDateUpdated);

        tvTitle.setText(file.getName());
        String dateUpdated = DateUtils.editDateTime3(file.getAdded());
        tvDateUpdated.setText(dateUpdated);
        if (file.getUrl().contains(".doc")){
            ivFileType.setImageResource(R.drawable.ic_doc);
        }else {
            ivFileType.setImageResource(R.drawable.ic_pdf);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(file.getUrl()));
                context.startActivity(browserIntent);
            }
        });
        return view;
    }
}
