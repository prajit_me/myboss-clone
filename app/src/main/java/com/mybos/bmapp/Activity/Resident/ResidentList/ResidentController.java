package com.mybos.bmapp.Activity.Resident.ResidentList;

import android.content.Context;

import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Mapper.Resident.ResidentMapper;
import com.mybos.bmapp.Data.Response.ResidentListResponse;

/**
 * Created by EmLaAi on 21/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentController {
    public void get(Context context, int buildingId, SearchTableQueryBuilder searchTableQueryBuilder,
                    SuccessCallback<ResidentListResponse> onSuccess,FailureCallback onFailure){
        ResidentMapper mapper = new ResidentMapper();
        mapper.get(context,buildingId,searchTableQueryBuilder,onSuccess::onSuccess,onFailure::onFailure);
    }

    public interface SuccessCallback<T>{
        void onSuccess(T value);
    }
    public interface FailureCallback{
        void onFailure(Exception error);
    }
}
