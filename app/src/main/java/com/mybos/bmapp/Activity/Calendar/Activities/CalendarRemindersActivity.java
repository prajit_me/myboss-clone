package com.mybos.bmapp.Activity.Calendar.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Calendar.Adapter.ReminderListAdapter;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;

public class CalendarRemindersActivity extends a_base_toolbar {

    TextView tvDateReminders;
    ListView lvReminders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_reminders);
        updateToolbarTitle("Reminders");
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarRemindersActivity.this, CalendarDayEventActivity.class);
                startActivity(intent);
                finish();
            }
        });


        tvDateReminders = findViewById(R.id.tvDateReminders);
        lvReminders = findViewById(R.id.lvReminders);
        String dateTile = SelectedDay.getDateOfweek()+", "+SelectedDay.getFullDate();
        tvDateReminders.setText(dateTile);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CalendarRemindersActivity.this, CalendarDayEventActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        lvReminders.setAdapter(new ReminderListAdapter(CalendarRemindersActivity.this,SelectedDay.getDate()));
    }
}
