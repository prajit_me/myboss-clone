package com.mybos.bmapp.Activity.Keys.Activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Key.ContractorKey;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

public class KeyContractorListActivity extends a_base_toolbar {

    ListView lvKeyContractorList;
    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contractor_list2);
        updateToolbarTitle("Contractor list");

        controller.getContractorSignIn(KeyContractorListActivity.this, selectedBuilding.getId(),
                c -> {
                    lvKeyContractorList = findViewById(R.id.lvKeyContractorList);

                    ArrayList<String> contractorList = new ArrayList();
                    for (ContractorKey contractor : ContractorKey.getContractors()
                    ) {
                        contractorList.add(contractor.getCompanyName());
                    }
                    ArrayAdapter adapter1 = new ArrayAdapter(KeyContractorListActivity.this,
                            android.R.layout.simple_expandable_list_item_1, contractorList);
                    lvKeyContractorList.setAdapter(adapter1);

                    lvKeyContractorList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            ContractorKey.setSelectedContractor(contractorList.get(i));
                            KeyContractorListActivity.this.finish();
                        }
                    });
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(KeyContractorListActivity.this)){
                        Log.e("getContractorSignIn",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(KeyContractorListActivity.this);
                    }
                });


    }
}
