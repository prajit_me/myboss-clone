package com.mybos.bmapp.Activity.Base;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;


import com.mybos.bmapp.Activity.BuildingCase.CaseList.CaseListActivity;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Inspection.InspectionList.InspectionListActivity;
import com.mybos.bmapp.Activity.Resident.ResidentList.ResidentListActivity;
import com.mybos.bmapp.Data.Mapper.Building.BuildingMapper;
import com.mybos.bmapp.Data.Model.Auth.Features;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.R;
import com.bumptech.glide.Glide;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.List;

public class BaseDrawerActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    protected boolean check = false;
    protected DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        Toolbar toolbar = findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setTitle(R.string.dashboard_title);
//        DrawerLayout drawer = findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle action = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.drawer_open,R.string.drawer_close);
//        drawer.addDrawerListener(action);
//        action.syncState();
    }

    @Override
    public void setContentView(int layoutResID) {
        // overide default set contain to inject child contain into base layout
        super.setContentView(R.layout.a_base_drawer);
        getLayoutInflater().inflate(layoutResID,findViewById(R.id.content));
        // setup drawer
        Toolbar toolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);
        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle action = new ActionBarDrawerToggle(this,drawer,toolbar,R.string.drawer_open,R.string.drawer_close);
        drawer.addDrawerListener(action);
        action.syncState();
        action.setHomeAsUpIndicator(getDrawable(R.drawable.ic_menu_white));

        NavigationView navigationView = findViewById(R.id.navigation);
        View navHeader = navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.left_menu_home:
                navigateToRoot();
                break;
            case R.id.left_menu_cases:
                if (Features.shared(this).hasFeature(Features.TYPES.CASES)){
                    navigateToCaseList();
                }
                break;
            case R.id.left_menu_inspection:
                if (Features.shared(this).hasFeature(Features.TYPES.BUILDING_INSPECTION)){
                    navigateToInspectionList();
                }
                break;
            case R.id.left_menu_resident:
                if (Features.shared(this).hasFeature(Features.TYPES.RESIDENT)) {
                    navigateToResidentList();
                }
                break;
            case R.id.left_menu_logout:
                doLogout();
                break;
        }
        return false;
    }

    @Override
    public void doLogout(){
        super.doLogout();

    }

    public void advanceSetupDrawerMenu(){
        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        drawerLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                drawerLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setupBuildingDropdownList();
            }
        });
    }

    public void setupBuildingDropdownList(){

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        Spinner spinner = drawer.findViewById(R.id.buildingSpinner);

        List<String> buildingNames = SingletonObjectHolder.getInstance().getBuildingList().getBuildingListName();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,R.layout.spinner_title_only,R.id.tvTitle,buildingNames);
        if (null == spinner){
            return;
        }
        spinner.setAdapter(adapter);
        spinner.setDropDownVerticalOffset(spinner.getHeight());
        spinner.setSelection(SingletonObjectHolder.getInstance().getBuildingList().getSelectedIndex());

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                SingletonObjectHolder.getInstance().getBuildingList().setSelectedIndex(position);
                BuildingInfoCollection.setSelectedBuilding(position);
                if (null != view) {
                    TextView txtTitle = view.findViewById(R.id.tvTitle);
                    txtTitle.setText(null);
                }
                applyBuildingInfo(SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding());
                BuildingMapper mapper = new BuildingMapper();
                mapper.changeBuilding(BaseDrawerActivity.this,
                        SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding().getId(),
                        Void->{},e -> {});
                if (check){
                    navigateToRoot();
                }
                check = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void applyBuildingInfo(Building building){
        DrawerLayout drawer = findViewById(R.id.drawer_layout);

        ((TextView) drawer.findViewById(R.id.txtAddress)).setText(building.getAddress());

        ((TextView) drawer.findViewById(R.id.txtBuildingName)).setText(building.getName());
        BuildingInfoCollection.setBuildingName(building.getName());

        ImageView imageView = drawer.findViewById(R.id.imgNavBuilding);

        Glide.with(this).load(building.getPhoto()).into(imageView);
    }

    public void navigateToCaseList() {

        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent caseList = new Intent(this,CaseListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(caseList);
        builder.startActivities();
        finish();
    }

    public void navigateToResidentList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent resident = new Intent(this, ResidentListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(resident);
        builder.startActivities();
        finish();
    }

    public void navigateToInspectionList(){
        Intent main = new Intent(this,DashboardActivity.class);
        main.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);

        Intent inspectionList = new Intent(this, InspectionListActivity.class);
        TaskStackBuilder builder = TaskStackBuilder.create(this);
        builder.addNextIntent(main);
        builder.addNextIntent(inspectionList);
        builder.startActivities();
        finish();
    }
}
