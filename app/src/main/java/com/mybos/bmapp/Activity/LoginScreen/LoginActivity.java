package com.mybos.bmapp.Activity.LoginScreen;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseActivity;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Data.Model.User;
import com.mybos.bmapp.Error.InvalidResponse;
import com.mybos.bmapp.Error.LocalizeException;
import com.mybos.bmapp.Error.UnauthorizeException;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.PreferenceManager;
import com.mybos.bmapp.Services.VolleyRequest.VolleyBodyBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyHeaderBuilder;
import com.mybos.bmapp.Services.VolleyRequest.VolleyRequest;
import com.mybos.bmapp.Services.VolleyRequest.VolleyUrlBuilder;
import com.mybos.bmapp.Utils.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class LoginActivity extends BaseActivity {

    private final String PRIVACY_POLICY_LINK = "https://www.mybos.com/privacy-policy/";
    EditText txtUsername,txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_login);

        txtUsername = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        findViewById(R.id.btnLogin).setOnClickListener(this::doLogin);

        // check api and current version app
        PackageInfo pInfo = null;
        try {
            pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        ((TextView)findViewById(R.id.tvCheckApi)).setText(VolleyUrlBuilder.API_END_POINT+(pInfo==null?"":"."+pInfo.versionName));
        findViewById(R.id.tvPrivacyPolicy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPrivacyPolicy();
            }
        });
    }


    private void showPrivacyPolicy(){
        Intent PrivacyPolicyIntent = new Intent(Intent.ACTION_VIEW);
        PrivacyPolicyIntent.setData(Uri.parse(PRIVACY_POLICY_LINK));
        startActivity(PrivacyPolicyIntent);
    }

    public boolean valadiate(){
        return !("".equals(txtUsername.getText().toString()) || "".equals(txtPassword.getText().toString()));
    }

    public void doLogin(View v){
        turnOnLoadingLoginBtn(true);
        if (valadiate()){
            String body = new VolleyBodyBuilder()
                    .setParameter("username",txtUsername.getText().toString())
                    .setParameter("password",txtPassword.getText().toString())
                    .setParameter("remember",1)
                    .build();
            String url = new VolleyUrlBuilder(2).parseUrl(VolleyUrlBuilder.URLPATH.getPath(VolleyUrlBuilder.URLPATH.LOGIN)).build();
            VolleyRequest.shared(this).post(url,(new  VolleyHeaderBuilder()).buildDefaultJson(),body,
                    (response)->{
                        turnOnLoadingLoginBtn(false);
                        try {
                            Object obj = new JSONTokener(response).nextValue();
                            if (obj instanceof JSONObject){
                                if (((JSONObject) obj).getInt("state") == 200){
                                    if(((JSONObject) obj).getJSONObject("user").getString("role").equals("manager")){
                                        User user = new User();
                                        user.setId(((JSONObject) obj).getJSONObject("user").getInt("id"));
                                        user.setName(((JSONObject) obj).getJSONObject("user").getString("name"));
                                        user.setRole(((JSONObject) obj).getJSONObject("user").getString("role"));
                                        User.save(user);

                                        PreferenceManager.setAccessToken(this,((JSONObject) obj).getString("accessToken"));

                                        Intent intent = new Intent(this,DashboardActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }else{
                                        throw new UnauthorizeException();
                                    }
                                }else{
                                    String message = getString(R.string.AlertMessageInvalidUserNameOrPassword);
                                    throw new Exception(message);
                                }
                            }else{
                                throw new InvalidResponse();
                            }
                        } catch (JSONException e) {
                            Logger.shared().printExceptionStack(e);
                            Toast.makeText(this, getString(R.string.AlertMessageInvalidUserNameOrPassword), Toast.LENGTH_LONG).show();
                        }catch (LocalizeException e){
                            Toast.makeText(this, getString(R.string.AlertMessageInvalidUserNameOrPassword), Toast.LENGTH_LONG).show();
                        }catch (Exception e){
                            Toast.makeText(this, getString(R.string.AlertMessageInvalidUserNameOrPassword), Toast.LENGTH_LONG).show();
                        }
                    },(error)->{
                        Logger.shared().printExceptionStack(error);
                        String message = "Your request was unsuccessful. Please check your network and try again.";
                        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
                        turnOnLoadingLoginBtn(false);
                    });
        }else{
            Toast.makeText(this, getString(R.string.AlertMessageInvalidUserNameOrPassword), Toast.LENGTH_LONG).show();
            turnOnLoadingLoginBtn(false);
        }
    }

    public void turnOnLoadingLoginBtn(boolean turnOn){
        if (turnOn){
            findViewById(R.id.tvTittlebtnLogin).setVisibility(View.INVISIBLE);
            findViewById(R.id.progressBarbtnLogin).setVisibility(View.VISIBLE);
            findViewById(R.id.btnLogin).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //do nothing
                }
            });
        }else {
            findViewById(R.id.tvTittlebtnLogin).setVisibility(View.VISIBLE);
            findViewById(R.id.progressBarbtnLogin).setVisibility(View.INVISIBLE);
            findViewById(R.id.btnLogin).setOnClickListener(this::doLogin);
        }
    }
}
