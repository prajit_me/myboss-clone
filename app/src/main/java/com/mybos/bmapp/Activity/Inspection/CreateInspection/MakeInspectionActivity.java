package com.mybos.bmapp.Activity.Inspection.CreateInspection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.*;
import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Activity.Inspection.Adapter.InspectionDetailRecyclerAdapter;
import com.mybos.bmapp.Activity.Inspection.CreateInspection.Fragment.ChoosePhotoSourceDialog;
import com.mybos.bmapp.Activity.Inspection.CreateInspection.Fragment.MakeCommentDialog;
import com.mybos.bmapp.Activity.Inspection.InspectionInfoCollector;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.RecycleAdapter.MultiSectionRecyclerAdapter;
import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleNullSpinnerAdapter;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Mapper.Inspection.InspectionMapper;
import com.mybos.bmapp.Data.Model.Apartment.Apartment;
import com.mybos.bmapp.Data.Model.Apartment.ApartmentList;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.Data.Model.Inspection.Inspection;
import com.mybos.bmapp.Data.Model.Inspection.InspectionArea;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItem;
import com.mybos.bmapp.Data.Model.Inspection.InspectionItemOffline;
import com.mybos.bmapp.Data.Model.Inspection.InspectionOfflinetoSave;
import com.mybos.bmapp.Data.Model.Inspection.InspectionOfflinetoSaveList;
import com.mybos.bmapp.Error.ImageEncodeError;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.ImageUtils;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.RealmList;

public class MakeInspectionActivity extends a_base_toolbar implements InspectionDetailRecyclerAdapter.ItemViewHolderEventListener {
    public static final String INSPECTION_KEY = "MakeInspectionActivity.INSPECTION_KEY";
    public static final String INSPECTION_TYPE_KEY = "MakeInspectionActivity.INSPECTION_TYPE_KEY";
    public static final String INSPECTION_DRAFT = "MakeInspectionActivity.INSPECTION_DRAFT";
    public static final String INSPECTION_HIS_ID = "MakeInspectionActivity.INSPECTION_HIS_ID";
    private static final int PHOTO_LIBRARY_REQUEST_CODE = 1;
    private static final int PHOTO_TAKE_REQUEST_CODE = 2;
    private int inspectionId;
    private int inspectionType;
    private InspectionMapper inspectionMapper = new InspectionMapper();
    private Inspection inspection;
    private InspectionItem trackingItem;
    private ApartmentList apartmentList = new ApartmentList();
    private Apartment trackingApartment;
    private MakeInpsectionController controller = new MakeInpsectionController();
    private int historyId = 0;
    private int isDraft = 0;
    private boolean syncing = false;
    private boolean needSaving = false;
    private ArrayList<InspectionItem> waitingSyncList = new ArrayList<>();
    public JSONObject dataOfDarf;
    private String apartment="-1";
    RecyclerView rclInspectionItemList;
    Spinner apartmentSpinner;
    String currentPhotoPath;
    Uri currentUri;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_make_inspection);
        //IMPORTANT: setup drawer with drop down building list
        updateToolbarTitle(getString(R.string.make_inspection_title));

        inspectionId = getIntent().getIntExtra(INSPECTION_KEY,-1);
        inspectionType = getIntent().getIntExtra(INSPECTION_TYPE_KEY,0);
        isDraft = getIntent().getIntExtra(INSPECTION_DRAFT,0);
        historyId = getIntent().getIntExtra(INSPECTION_HIS_ID,-1);
        Log.e("check_type",String.valueOf(inspectionType));
        getInspectionDetail();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.do_inspection_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.barSave){
            needSaving = true;
            //checking save enable
            if (InspectionInfoCollector.getUpdatedInspectionItemList().isEmpty()&&historyId==-1){
                Toast.makeText(this, "You must fill in the Inspection before saving", Toast.LENGTH_LONG).show();
                needSaving = false;
            }else {
                displayLoadingScreen();
                needingSync(null);
            }
        }
        return false;
    }

    public void saveAction(boolean convert,InspectionItem inspectionItem){
            // checking network first
            if (Offlinemode.checkNetworkConnection(this)) {
                if (null == inspectionItem) {//from button save
                    if (needSaving) {
                        needSaving = false;
                        saveNewHistoryId(false, historyId, "from save");
                        return;
                    } else {
                        Log.e("check_saveAction", "inspection null without needSaving");
                        return;
                    }

                }
                if (historyId == -1) {
                    //don't have history id and need to get a new id
                    getNewHistoryId(convert, inspectionItem);
                } else {
                    syncInspection(convert, inspectionItem);
                }
            } else {
                if (!needSaving) {
                    dismissLoadingScreen();
                    syncing = false;
                    return;// checking click button save
                }
                Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
                InspectionOfflinetoSave inspection = new InspectionOfflinetoSave();
                inspection.setBuildingId(selectedBuilding.getId());
                inspection.setInspectionId(this.inspection.getId());
                inspection.setApartmentId(trackingApartment == null ? "" : String.valueOf(trackingApartment.getId()));
                inspection.setConvert(convert);
                // remove image file to save into realm
                RealmList<InspectionItemOffline> tempList = new RealmList<>();
                for (InspectionItem item : InspectionInfoCollector.getUpdatedInspectionItemList()
                ) {
                    InspectionItemOffline tempItem = new InspectionItemOffline();
                    tempItem.copyFromInspectionItem(item);
                    tempList.add(tempItem);
                }
                inspection.setUpdatedInspectionItemList(tempList);
                InspectionOfflinetoSaveList temp = InspectionOfflinetoSaveList.get();
                if (null != temp) {
                    temp.addToList(inspection);
                } else {
                    temp = new InspectionOfflinetoSaveList();
                    temp.addToList(inspection);
                }
                InspectionOfflinetoSaveList.save(temp);
                dismissLoadingScreen();
                needSaving = false;
                syncing = false;
                finish();
                Toast.makeText(this, "Your inspection is saved successfully", Toast.LENGTH_SHORT).show();
//                this.displayAlert("Saved", "Your inspection is saved successfully", null);
            }
    }

    private void getNewHistoryId(boolean convert,InspectionItem inspectionItem){
        Log.e("on","getNewHistoryId");
        inspectionMapper.getNewHistoryID(this, BuildingInfoCollection.getBuildingID(), inspection.getId(), trackingApartment==null?"":String.valueOf(trackingApartment.getId()),
                response -> {
                    Log.e("on","historyId"+String.valueOf(response));
                    historyId = response;
                    Log.e("check_history",String.valueOf(historyId));
                    syncInspection(convert,inspectionItem);
                },
                error -> {
                    Toast.makeText(this, "Can't save inspection without hisID", Toast.LENGTH_SHORT).show();
                }
        );
    }

    private void syncInspection(boolean convert,InspectionItem inspectionItem){
        Log.e("on","syncInspection");
        inspectionItem = InspectionInfoCollector.getInspection(inspectionItem);
        InspectionItem finalInspectionItem = inspectionItem;
        this.inspectionMapper.saveInspectionAItem(this, BuildingInfoCollection.getBuildingID(), inspection.getId(), historyId,inspectionItem,
                InnerResponseAItem -> {
                        if (convert){
                            saveNewHistoryId(convert,historyId,String.valueOf(finalInspectionItem.getId()));
                        }else {
                            if (!waitingSyncList.isEmpty()){
                                saveAction(false,waitingSyncList.get(0));
                                waitingSyncList.remove(0);
                            }else {
                                syncing = false;
                            }
                        }
                },
                InnerError -> {
                    Log.e("check_syncInspection",InnerError.toString());
                }
        );
    }

    public void saveNewHistoryId(boolean convert,int NewHistoryID,String idItem){
        Log.e("on","saveNewHistoryId");
        if (!convert) {
            this.inspectionMapper.saveNewHistoryID(this, BuildingInfoCollection.getBuildingID(), inspection.getId(), NewHistoryID,
                    SecondInnerResponse -> {
                        Log.e("save","ok-his:"+NewHistoryID+",idItem:"+idItem);
                        Toast.makeText(this, "Your inspection is saved successfully", Toast.LENGTH_SHORT).show();
//                        this.displayAlert("Saved", "Your inspection has been successfully saved", null);
                        syncing = false;
                        finish();
                        dismissLoadingScreen();

                    },
                    InnerError -> {
                        Log.e("save","error-his:"+NewHistoryID+",idItem:"+idItem);
                        dismissLoadingScreen();
                    }
            );
        } else {
            this.inspectionMapper.convertToCase(this, BuildingInfoCollection.getBuildingID(), inspection.getId(), NewHistoryID,
                    idItem,
                    convertSuccess -> {
                        if (!convertSuccess.equals("200")) {
                            this.displayAlert("Convert Failed", convertSuccess, null);
                        } else {
                            this.displayAlert("Saved", "Your item is converted successfully", null);
                        }
                        dismissLoadingScreen();
                    },
                    error -> {
                        this.displayAlert("Error", "Convert Failed", null);
                        dismissLoadingScreen();
                    });
        }
    }

    public void additionalSetup(){
        apartmentSpinner = findViewById(R.id.apartmentDropdown);

        Log.e("check_id",String.valueOf(historyId));
        //show alert need apartment
        if (inspectionType==1&&historyId ==-1){
            findViewById(R.id.needApartment).setVisibility(View.VISIBLE);
        }

        if (inspectionType == 1){
            findViewById(R.id.viewApartment).setVisibility(View.VISIBLE);
            SimpleTitleNullSpinnerAdapter adapter = new SimpleTitleNullSpinnerAdapter(this,this.apartmentList);
            apartmentSpinner.setAdapter(adapter);
            getApartmentList();
            apartmentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SimpleTitleNullSpinnerAdapter mAdapter = (SimpleTitleNullSpinnerAdapter) apartmentSpinner.getAdapter();
                    trackingApartment = (Apartment) mAdapter.getItem(position);
                    if (trackingApartment!=null){
                        findViewById(R.id.needApartment).setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    public void getInspectionDetail(){
        if (Offlinemode.checkNetworkConnection(this)){
            Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
            if (isDraft==1){
                controller.getInspectionDradft(this,selectedBuilding.getId(),String.valueOf(historyId),String.valueOf(inspectionId),
                        data->{
                            dataOfDarf = data;
                            additionalSetup();
                            getInspection(selectedBuilding);
                        },error -> {
                            Log.e("getInspectionDradft",error.toString());
                        });
            }else {
                additionalSetup();
                getInspection(selectedBuilding);
            }

        }else {
            this.inspection = Inspection.getInspectionFromInspectionId(inspectionId);
            this.inspection.setDefaultInspectionItem();
            setupRecycleView();
            additionalSetup();
        }

    }

    private void getInspection(Building selectedBuilding){
        controller.getInpsection(this,selectedBuilding.getId(),inspectionId,inspection->{
            this.inspection = inspection;
            setupRecycleView();
        },error -> {
            Log.e("getInspectionDetail",error.toString());
        });
    }

    public void getApartmentList(){
        if (Offlinemode.checkNetworkConnection(this)){
            Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
            controller.getApartmentList(this,selectedBuilding.getId(),apartmentList->{
                this.apartmentList = apartmentList;
                SimpleTitleNullSpinnerAdapter adapter = (SimpleTitleNullSpinnerAdapter) apartmentSpinner.getAdapter();
                adapter.setSource(this.apartmentList);
                if (historyId!=-1){
                    trackingApartment = new Apartment();
                    trackingApartment.setId(apartment);
                    for (int i = 0; i < apartmentList.getApartments().size(); i++) {
                        Log.e("check_apartment",apartment+"-"+apartmentList.getApartments().get(i).getId());
                            if (apartmentList.getApartments().get(i).getId().equals(apartment)){
                                apartmentSpinner.setSelection(i+1);
                                return;
                            }
                    }
                }
            },error -> {
                Log.e("getApartmentList",error.toString());
            });
        }else{
            this.apartmentList = Inspection.getOfflineApartmentList();
            SimpleTitleNullSpinnerAdapter adapter = (SimpleTitleNullSpinnerAdapter) apartmentSpinner.getAdapter();
            adapter.setSource(this.apartmentList);
        }

    }

    public void setupRecycleView(){
        rclInspectionItemList = findViewById(R.id.rclInspectionArea);
        rclInspectionItemList.setLayoutManager(new LinearLayoutManager(this));
        InspectionDetailRecyclerAdapter adapter = new InspectionDetailRecyclerAdapter();

        adapter.setListener(this);
        List<InspectionArea> areas = inspection.getAreas();
        adapter.clearSource();
        for (InspectionArea area:areas){
            List<BaseModel> parseList = new ArrayList<>(area.getItems());
            MultiSectionRecyclerAdapter.SectionList areaList = new MultiSectionRecyclerAdapter.SectionList(area.getName(),parseList);
            adapter.addSection(areaList);
        }
        rclInspectionItemList.setAdapter(adapter);
        ((InspectionDetailRecyclerAdapter)rclInspectionItemList.getAdapter()).setDataDraf(dataOfDarf);
        adapter.parseSource();
    }

    @Override
    public void onPhotoClick(InspectionItem inspectionItem) {
        ChoosePhotoSourceDialog dialog = ChoosePhotoSourceDialog.createInstance();
        dialog.setCameraClick(()->{
            trackingItem = inspectionItem;
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getPackageManager()) != null){
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(MakeInspectionActivity.this,
                            "com.mybos.bmapp.fileprovider",
                            photoFile);
                    currentUri = photoURI;
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(intent,PHOTO_TAKE_REQUEST_CODE);
                }
            }
        });
        dialog.setLibraryClick(()->{
            trackingItem = inspectionItem;
            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            intent.setType("image/jpeg,jpg,png");
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, PHOTO_LIBRARY_REQUEST_CODE);
            }
        });
        InspectionInfoCollector.updateInspectionItemList(inspectionItem);
        dialog.show(getSupportFragmentManager(),"TAG");
    }

    @Override
    public void onMessageClick(InspectionItem inspectionItem) {
        MakeCommentDialog dialog = MakeCommentDialog.createInstance(inspectionItem.getComment());
        dialog.setSaveClick(comment -> {
            inspectionItem.setComment(comment);
            InspectionInfoCollector.updateInspectionItemList(inspectionItem);
            needingSync(inspectionItem);
            rclInspectionItemList.getAdapter().notifyDataSetChanged();
        });
        dialog.show(getSupportFragmentManager(),"TAG");
    }

    @Override
    public void onConvertClick(InspectionItem inspectionItem) {
        if (Offlinemode.checkNetworkConnection(MakeInspectionActivity.this)) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MakeInspectionActivity.this, R.style.DialogTheme);
            builder.setMessage("Are you sure you want to convert this item to a Case?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            displayLoadingScreen();
                            saveAction(true, inspectionItem);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
        }else{
            this.displayAlert("Notice", "Convert to case unavailable on offline mode", null);
        }
    }

    public void needingSync(InspectionItem inspectionItem){
        if (!syncing){
            syncing = true;
            saveAction(false,inspectionItem);
        }else {
            waitingSyncList.add(inspectionItem);
        }

    }

    @Override
    public void onCheckChanged(InspectionItem inspectionItem) {
        needingSync(inspectionItem);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PHOTO_LIBRARY_REQUEST_CODE){
            if (data != null){
                Uri photoUri = data.getData();
                if (photoUri != null) {
                    try {
                        File file = new File(convertMediaUriToPath(photoUri));
                        String name = file.getName();
                        String mime = "Content-Type: image/jpeg";
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(getContentResolver(), photoUri);
                        selectedImage = ImageUtils.compressImage(selectedImage);
                        trackingItem.setImage(new PhotoModel(selectedImage,name,mime));
                        trackingItem.setPhotoPath(convertMediaUriToPath(photoUri));
                        rclInspectionItemList.getAdapter().notifyDataSetChanged();
                        needingSync(trackingItem);
                    } catch (IOException e) {
                        Log.e("Photo_library",new ImageEncodeError().toString());
                    }
                }
            }
        }
        if (requestCode == PHOTO_TAKE_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                    try {
                        File file = new File(currentPhotoPath);
                        String name = file.getName();
                        String mime = "Content-Type: image/jpeg";
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(getContentResolver(),Uri.fromFile(file));
                        trackingItem.setImage(new PhotoModel(selectedImage,name,mime));
                        trackingItem.setPhotoPath(currentPhotoPath);
                        rclInspectionItemList.getAdapter().notifyDataSetChanged();
                        needingSync(trackingItem);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpeg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.e("check2",currentPhotoPath);
        return image;
    }

    public String convertMediaUriToPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        Log.e("check_convert",s);
        return s;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }
}
