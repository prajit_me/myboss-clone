package com.mybos.bmapp.Activity.Resident.ResidentDetail.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentEmergencyComponent extends ConstraintLayout {
    TextView txtEmergencyContact,txtEmergencyPhone;
    OnEmergencyPhoneClickListener listener;
    public ResidentEmergencyComponent(Context context) {
        super(context);
        additionalInit();
    }

    public ResidentEmergencyComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public ResidentEmergencyComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_resident_detail_c_resident_emergency,this);
        txtEmergencyContact = findViewById(R.id.txtEmergencyContact);
        txtEmergencyPhone = findViewById(R.id.txtEmergencyPhone);
        findViewById(R.id.imgEmergencyPhone).setOnClickListener(v -> {
            if (null != listener){
                listener.phoneClick();
            }
        });
    }

    public void setListener(OnEmergencyPhoneClickListener listener) {
        this.listener = listener;
    }
    public void applyInfo(String contact,String phone){
        txtEmergencyPhone.setText(phone);
        txtEmergencyContact.setText(contact);
    }

    public interface OnEmergencyPhoneClickListener{
        void phoneClick();
    }
}
