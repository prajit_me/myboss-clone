package com.mybos.bmapp.Activity.Filter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.mybos.bmapp.Activity.Assets.Activity.BuildingAssetActivity;
import com.mybos.bmapp.Activity.Assets.Activity.NewAssetActivity;
import com.mybos.bmapp.Activity.Contractor.Activities.ContractorListActivity;
import com.mybos.bmapp.Activity.Resident.Adapter.ResidentRecyclerAdapter;
import com.mybos.bmapp.Activity.Resident.ResidentList.ResidentListActivity;
import com.mybos.bmapp.Component.RecycleAdapter.AbstractScrollToLoadMoreRecyclerAdapter;
import com.mybos.bmapp.Data.Model.Filter.Filter;
import com.mybos.bmapp.R;

import java.util.ArrayList;

public class FilterRecycleAdapter extends AbstractScrollToLoadMoreRecyclerAdapter<Filter> {

    private static OnItemClickListener mOnItemClickLister;
    private int type;// 1 = resident; 2 = assets; 3 = contractor; 4 = case
    private int subType;
    public static ArrayList<ArrayList<Filter>> progressfinder = new ArrayList<>();
    public static ArrayList<String> progressTitle = new ArrayList<>();
    public static int currentLv = 0;

    public FilterRecycleAdapter(int type,int subType){
        //int filter with reset all
        this.subType = subType;
        this.type = type;
        currentLv = 0;
        progressfinder.clear();
        progressTitle.clear();
        progressTitle.add("All category");
    }

    @Override
    public int getItemLayout() {
        return R.layout.r_a_filter;
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new FilterViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FilterViewHolder){
            ((FilterViewHolder)holder).name.setText(sourceList.get(position).getName());
            if (sourceList.get(position).getChildren().size()>0){
                ArrayList<Filter> filters = sourceList.get(position).getChildren();
                String title =  sourceList.get(position).getName();
                ((FilterViewHolder)holder).imageGo.setVisibility(View.VISIBLE);
                ((FilterViewHolder)holder).CbtnImageGo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(currentLv==0&&progressfinder.size()==0){//add root list when init
                            if (type == 2){
                                progressfinder.add(Filter.getAssetsFilterList());
                            }
                        }
                        reset();
                        setMaximum(filters.size());
                        updateModel(filters);
                        currentLv++;
                        progressTitle.add(title);
                        progressfinder.add(filters);
                        Log.e("check_size",String.valueOf(progressfinder.size())+"---"+String.valueOf(currentLv));
                        notifyDataSetChanged();
                        mOnItemClickLister.clickIntoItem();
                    }
                });
            }else {
                ((FilterViewHolder)holder).imageGo.setVisibility(View.INVISIBLE);
            }

            ((FilterViewHolder)holder).setListener(()->{
                Context context = ((FilterViewHolder)holder).imageGo.getContext();
                if (type == 1){
                    setupClickItemForResident(context,position);
                }
                if(type == 2){
                    setupClickItemForAssets(context,position);
                }
                if (type == 3){
                    setupClickItemForContractor(context,position);
                }
            });
        }
    }

    private void setupClickItemForResident(Context context, int position){
        ((ResidentListActivity)context).setCurrentPage(1);
        ((ResidentListActivity)context).setSection(sourceList.get(position).getId());
        ((ResidentListActivity)context).addTag(sourceList.get(position).getName(),true);
    }

    private void setupClickItemForAssets(Context context, int position){
        if (subType == 1){
            ((NewAssetActivity)context).setSelectedCategoryNameNId(sourceList.get(position).getName(),sourceList.get(position).getId());
            return;
        }
        ((BuildingAssetActivity)context).setSelectedCategory(sourceList.get(position).getId());
        ((BuildingAssetActivity)context).addTag(sourceList.get(position).getName(),true);
        ((BuildingAssetActivity)context).setSelectedCategory(sourceList.get(position).getId());
        ((BuildingAssetActivity)context).setCurrentPage(1);
        ((BuildingAssetActivity)context).showAssetList();
    }

    private void setupClickItemForContractor(Context context, int position){
        ContractorListActivity.setSelectedCategory(sourceList.get(position).getId());
        ((ContractorListActivity)context).addTag(sourceList.get(position).getName(),true);
        ((ContractorListActivity)context).showContractorList();
    }

    class FilterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name;
        ImageView imageGo;
        CardView cvFilter;
        ConstraintLayout CbtnImageGo;
        ResidentRecyclerAdapter.OnItemClickListener listener;
        public FilterViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            imageGo = itemView.findViewById(R.id.imageGo);
            cvFilter = itemView.findViewById(R.id.cvFilter);
            CbtnImageGo = itemView.findViewById(R.id.CbtnImageGo);
            itemView.findViewById(R.id.cvFilter).setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            if (null != listener){
                listener.onClick();
            }
            int position = v.getLayoutDirection();
            mOnItemClickLister.onItemClicked(v, position);
        }

        public void setListener(ResidentRecyclerAdapter.OnItemClickListener listener) {
            this.listener = listener;
        }
    }
    public interface OnItemClickListener {
        void onItemClicked(View view, int pos);
        void clickIntoItem();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickLister = listener;
    }
}
