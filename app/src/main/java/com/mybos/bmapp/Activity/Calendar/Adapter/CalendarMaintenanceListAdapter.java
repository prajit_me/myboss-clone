package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Calendar.Activities.CalendarMaintenanceInfoActivity;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;

import java.util.ArrayList;

public class CalendarMaintenanceListAdapter extends BaseAdapter {

    private Context context;
    ArrayList<MaintenancePerDay> listShow = new ArrayList<>();


    public CalendarMaintenanceListAdapter(Context context, String nameList) {
        this.context = context;
        ArrayList<MaintenancePerDay> listofDay = MaintenancePerDay.getListByDate(SelectedDay.getDate());
        if (nameList.equals("all")) {
            listShow = new ArrayList<>(listofDay);
        } else {
            for (MaintenancePerDay maintenancePerDay : listofDay
            ) {
                if (maintenancePerDay.getStatusText().equals(nameList)){
                    listShow.add(maintenancePerDay);
                }
            }
        }

    }

    @Override
    public int getCount() {
        return listShow.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.calendar_a_maintanence, null);

        MaintenancePerDay maintenancePerDay = listShow.get(i);

        ImageView ivMaintenance = view.findViewById(R.id.ivMaintenance);
        TextView tvNameMaintenance = view.findViewById(R.id.tvNameMaintenance);
        TextView tvStatusMaintenance = view.findViewById(R.id.tvStatusMaintenance);

        String status = maintenancePerDay.getStatusText().isEmpty()?"Pending":maintenancePerDay.getStatusText();
        tvStatusMaintenance.setText(status);
        tvNameMaintenance.setText(maintenancePerDay.getTitle());
        if (status.equals("Pending")){
            ivMaintenance.setImageResource(R.drawable.calendar_event_pending);
        }else if (status.equals("Completed")){
            ivMaintenance.setImageResource(R.drawable.calendar_event_complete);
        }else {
            ivMaintenance.setImageResource(R.drawable.calendar_event_failed);
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MaintenancePerDay.setMaintenancePerDaySelected(maintenancePerDay);
                Intent intent = new Intent(context, CalendarMaintenanceInfoActivity.class);
                context.startActivity(intent);
                ((Activity)context).finish();
            }
        });

        return view;
    }
}
