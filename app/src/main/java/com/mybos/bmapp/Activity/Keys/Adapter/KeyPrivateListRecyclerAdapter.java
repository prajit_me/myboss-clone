package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Keys.Activities.KeyApartmentActivity;
import com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.PrivateKeyListFragment;
import com.mybos.bmapp.Component.RecycleAdapter.AbstractScrollToLoadMoreRecyclerAdapter;
import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.R;

import java.util.ArrayList;


public class KeyPrivateListRecyclerAdapter extends AbstractScrollToLoadMoreRecyclerAdapter<Apartment> {

    public ArrayList<Apartment> getShowKey() {
        return showKey;
    }

    private ArrayList<Apartment> showKey = new ArrayList<>(Apartment.getApartments());

    @Override
    public int getItemLayout() {
        return R.layout.key_private_list_adapter;
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new KeyPrivateItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof KeyPrivateItemViewHolder){
            Apartment apartment = sourceList.get(i);
            Context context = ((KeyPrivateItemViewHolder) holder).ivApartmentState.getContext();
            ((KeyPrivateItemViewHolder) holder).tvApartmentId.setText(apartment.getApartment_Id());
            if (apartment.getApartment_state() == 0) {
                ((KeyPrivateItemViewHolder) holder).ivApartmentState.setImageResource(R.drawable.ic_i_lock_0);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setText(R.string.key_state_no_key_available);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setTextColor(context.getResources().getColor(R.color.no_key_available));
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setBackgroundColor(context.getResources().getColor(R.color.key_no_available_background));
            } else if (apartment.getApartment_state() == 1) {
                ((KeyPrivateItemViewHolder) holder).ivApartmentState.setImageResource(R.drawable.ic_i_lock_1);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setText(R.string.key_state_all_keys_in);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setTextColor(context.getResources().getColor(R.color.key_in_color));
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setBackgroundColor(context.getResources().getColor(R.color.key_in_background));
            } else if (apartment.getApartment_state() == 2) {
                ((KeyPrivateItemViewHolder) holder).ivApartmentState.setImageResource(R.drawable.ic_i_lock_2);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setText(R.string.key_state_all_key_singout);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setTextColor(context.getResources().getColor(R.color.all_key_out_color));
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setBackgroundColor(context.getResources().getColor(R.color.key_all_out_background));
            } else if (apartment.getApartment_state() == 3) {
                ((KeyPrivateItemViewHolder) holder).ivApartmentState.setImageResource(R.drawable.ic_i_lock_3);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setText(R.string.key_state_one_key_signout);
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setTextColor(context.getResources().getColor(R.color.key_out_partial_color));
                ((KeyPrivateItemViewHolder) holder).tvApartmentState.setBackgroundColor(context.getResources().getColor(R.color.key_out_partial_background));
            }
            ((KeyPrivateItemViewHolder) holder).cardViewKeyPrivate.setOnClickListener(v -> {
                Intent intent = new Intent(context, KeyApartmentActivity.class);
                intent.putExtra(PrivateKeyListFragment.KEY_SELECTED,apartment.getApartment_Id());
                context.startActivity(intent);
            });
        }
    }

    public void performSearch(String keyword) {
        showKey = new ArrayList<>();
        if (keyword.isEmpty()) {
            showKey = new ArrayList<>(Apartment.getApartments());
        } else {
            for (Apartment key : Apartment.getApartments()) {
                if (key.getApartment_Id().toLowerCase().contains(keyword.toLowerCase())) {
                    showKey.add(key);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class KeyPrivateItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvApartmentId, tvApartmentState;
        ImageView ivApartmentState;
        CardView cardViewKeyPrivate;
        OnItemClickListener listener;
        public KeyPrivateItemViewHolder(View itemView) {
            super(itemView);
            tvApartmentId = itemView.findViewById(R.id.tvApartmentId);
            tvApartmentState = itemView.findViewById(R.id.tvPrivateApartmentState);
            ivApartmentState = itemView.findViewById(R.id.imageViewStateKeyPrivate);
            cardViewKeyPrivate = itemView.findViewById(R.id.cardViewKeyPrivate);
        }

        @Override
        public void onClick(View v) {
            if (null != listener){
                listener.onClick();
            }
        }
        public void setListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }
    public static interface OnItemClickListener{
        void onClick();
    }
}
