package com.mybos.bmapp.Activity.Contractor.Fragments;


import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContractorHistoryInfoFragment extends Fragment {

    private LinearLayout lnlyContractorHistoryContainer;
    private ContractorMapper HistoryMapper = new ContractorMapper();
    private Contractor SelectedContractor = ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex());
    private ArrayList<Contractor.ContractorHistory> HistoryList;

    private final int ConstantTextSize = 15, ConstantHeight = 100;
    private final String NoDataMessage = "No history was found.";

    private void loadInfo() {
        HistoryMapper.getHistories(this.getContext(),
                BuildingInfoCollection.getBuildingID(),
                SelectedContractor.getId(),
                response -> {
                    HistoryList = SelectedContractor.getHistories();
                    int Size = HistoryList.size();
                    if (Size > 0) {
                        for (int i = 0; i < Size; i++) {
                            Contractor.ContractorHistory CurrentHistory = HistoryList.get(i);
                            lnlyContractorHistoryContainer.addView(showHistory(CurrentHistory.getSubject(), String.valueOf(CurrentHistory.getNumber()), CurrentHistory.getAdded()));
                        }
                    }
                    else {
                        LinearLayout.LayoutParams NoDataParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        NoDataParams.setMargins(10, 30, 10, 10);
                        TextView tvNoData = new TextView(this.getActivity());
                        tvNoData.setLayoutParams(NoDataParams);
                        tvNoData.setMinHeight(ConstantHeight);
                        tvNoData.setGravity(Gravity.CENTER);
                        tvNoData.setText(NoDataMessage);
                        tvNoData.setTextColor(Color.BLACK);
                        tvNoData.setTypeface(tvNoData.getTypeface(), Typeface.ITALIC);

                        lnlyContractorHistoryContainer.addView(tvNoData);
                    }
                },
                error -> {});
    }

    private LinearLayout showHistory(String Subject, String CaseNumber, String AddedDate) {
        LinearLayout.LayoutParams HistoryLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        HistoryLayoutParams.setMargins(20, 20, 20, 30);

        //  ROW
        LinearLayout HistoryLayout = new LinearLayout(this.getActivity());
        HistoryLayout.setOrientation(LinearLayout.VERTICAL);
        HistoryLayout.setLayoutParams(HistoryLayoutParams);
        HistoryLayout.setBackground(Objects.requireNonNull(this.getActivity()).getDrawable(R.drawable.round_rectangle_bg));

        //  ITEM MARGIN
        LinearLayout.LayoutParams itemLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        itemLayoutParam.setMargins(20, 10, 20, 10);

        //  SUBJECT
        TextView tvSubject = new TextView(this.getActivity());
        tvSubject.setLayoutParams(itemLayoutParam);
        tvSubject.setText(Subject);
        tvSubject.setTextSize(ConstantTextSize);
        tvSubject.setTextColor(Color.BLACK);
        tvSubject.setTypeface(tvSubject.getTypeface(), Typeface.BOLD);
        tvSubject.setMinHeight(ConstantHeight);

        //  CASE TEXT
        TextView tvCase = new TextView(this.getActivity());
        tvCase.setLayoutParams(itemLayoutParam);
        tvCase.setText("Case:");
        tvCase.setTextSize(ConstantTextSize);
        tvCase.setTextColor(Color.LTGRAY);
        tvCase.setMinHeight(ConstantHeight);

        //  CASE NUMBER
        TextView tvCaseNumber = new TextView(this.getActivity());
        tvCaseNumber.setLayoutParams(itemLayoutParam);
        String StCaseNumber = "#" + CaseNumber;
        tvCaseNumber.setText(StCaseNumber);
        tvCaseNumber.setTextSize(ConstantTextSize);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvCaseNumber.setTextColor(getActivity().getColor(R.color.orangeTextColor));
        }
        else {
            tvCaseNumber.setTextColor(Color.LTGRAY);
        }
        tvCaseNumber.setTypeface(tvSubject.getTypeface(), Typeface.BOLD);
        tvCaseNumber.setMinHeight(ConstantHeight);

        //  ADDED DATE
        TextView tvAddedDate = new TextView(this.getActivity());
        tvAddedDate.setLayoutParams(itemLayoutParam);
        String StAddedDate = "Added: " + AddedDate;
        tvAddedDate.setText(StAddedDate);
        tvAddedDate.setTextSize(ConstantTextSize);
        tvAddedDate.setTextColor(Color.LTGRAY);
        tvAddedDate.setMinHeight(ConstantHeight);

        //  GROUP CASE TEXT + CASE NUMBER
        LinearLayout.LayoutParams lnlyCaseParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout lnlyCase = new LinearLayout(this.getActivity());
        lnlyCase.setLayoutParams(lnlyCaseParams);
        lnlyCase.addView(tvCase);
        lnlyCase.addView(tvCaseNumber);

        //  GROUP CASE + ADDED
        RelativeLayout.LayoutParams rllyCaseAddedParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout rllyCaseAdded = new RelativeLayout(this.getActivity());
        rllyCaseAdded.setLayoutParams(rllyCaseAddedParams);
        
        RelativeLayout.LayoutParams rllyCaseParentParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rllyCaseParentParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        RelativeLayout rllyCaseParent = new RelativeLayout(this.getActivity());
        rllyCaseParent.setLayoutParams(rllyCaseParentParams);
        rllyCaseParent.addView(lnlyCase);

        RelativeLayout.LayoutParams rllyAddedParentParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        rllyAddedParentParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        RelativeLayout rllyAddedParent = new RelativeLayout(this.getActivity());
        rllyAddedParent.setLayoutParams(rllyAddedParentParams);
        rllyAddedParent.addView(tvAddedDate);

        rllyCaseAdded.addView(rllyCaseParent);
        rllyCaseAdded.addView(rllyAddedParent);

        //  GROUP SUBJECT + CASE + ADDED
        HistoryLayout.addView(tvSubject);
        HistoryLayout.addView(rllyCaseAdded);


        return HistoryLayout;
    }

    private void startup() {
        loadInfo();
    }

    public ContractorHistoryInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contractor_history_info, container, false);

        //  GET REFERENCES
        lnlyContractorHistoryContainer = rootView.findViewById(R.id.lnlyContractorHistoryContainer_ContractorHistoryInfoFragment);

        //  START UP
        startup();

        return rootView;
    }

}
