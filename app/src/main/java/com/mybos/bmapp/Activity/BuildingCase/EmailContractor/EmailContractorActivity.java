package com.mybos.bmapp.Activity.BuildingCase.EmailContractor;

import android.app.DatePickerDialog;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Activity.Base.BaseDrawerActivity;
import com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Adapter.EmailContractorDetailPagerAdapter;
import com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Fragment.EmailAddContractorDialogFragment;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleSpinnerAdapter;
import com.mybos.bmapp.Data.Base.BaseModel;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Cases.BuildingCase;
import com.mybos.bmapp.Data.Model.Cases.EmailContractor.EmailContractor;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class EmailContractorActivity extends BaseDrawerActivity {
    public static final String CASE_ID_KEY = "CASE_ID_KEY";
    private String caseId = "new";
    private EmailContractorController controller = new EmailContractorController();
    private BuildingCase buildingCase = new BuildingCase();
    private ContractorList contractorList = new ContractorList();
    private EmailContractor email = new EmailContractor();
    private ViewPager vpContractor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_email_contractor);

        //IMPORTANT: setup drawer with drop down building list
        advanceSetupDrawerMenu();
        String caseId = getIntent().getStringExtra(CASE_ID_KEY);
        if (null != caseId){
            this.caseId = caseId;
        }

        if (null != getSupportActionBar()) {
            getSupportActionBar().setTitle(R.string.email_contractor_activity_title);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        setupTapOutsideToDismiss(findViewById(R.id.contentView));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setupSwitch();
        getInitData();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.email_contractor_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.barSave){
            if (contractorList.getSelectedItem().size() > 0){
                displayLoadingScreen();
                Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
                List<BaseModel> selectedContractor = contractorList.getSelectedItem();
                List<Contractor> convertList = new ArrayList<>();
                for (BaseModel selectedModel:selectedContractor){
                    if (selectedModel instanceof Contractor){
                        convertList.add((Contractor) selectedModel);
                    }
                }
                controller.sendEmailToContractor(this,selectedBuilding.getId(),caseId,email,convertList,Void->{
                    dismissLoadingScreen();
                    navigateToCaseList();
                },error -> {
                    dismissLoadingScreen();
                    if (Offlinemode.checkNetworkConnection(this)){
                        Log.e("onOptionsItemSelected",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(this);
                    }
                });
            }else {
                Toast.makeText(this, getString(R.string.AlertEmailContractorMustSelectAContractor), Toast.LENGTH_SHORT).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void getInitData(){
        this.displayLoadingScreen();
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        controller.getBuildingCaseDetail(this,selectedBuilding.getId(),caseId,value -> {
            this.buildingCase = value;
            getContractorList(selectedBuilding.getId(),1);
        },error -> {
            this.dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(this)){
                Log.e("getInitData",error.toString());
            }else {
                Offlinemode.showWhenOfflineMode(this);
            }
        });
    }

    private void showContractorList(){
        List<Integer> selectedContractorIds = buildingCase.getContractorsSelectedList();
        for (Integer selectedId:selectedContractorIds){
            contractorList.selectId(selectedId);
        }
        List<BaseModel> selectedContractors = this.contractorList.getSelectedItem();
        for (BaseModel selected:selectedContractors){
            if (selected instanceof Contractor){
                ((Contractor) selected).setSubject(buildingCase.getSubject());
            }
        }
        setupDueDateCalendar();
        setupEmailDropDown();
        setupViewPager();
        setupFABAction();
    }

    private void getContractorList(int buildingId,int page){
        controller.getContractorList(this, buildingId, "all",page, contractorList -> {
            if (contractorList.getContractorList().size() == 0) {
                this.dismissLoadingScreen();
                showContractorList();
            } else {
                for (Contractor c : contractorList.getContractorList()
                ) {
                    this.contractorList.addContrator(c);
                }
                getContractorList(buildingId, page + 1);
            }
        }, error -> {
            this.dismissLoadingScreen();
            if (Offlinemode.checkNetworkConnection(this)) {
                Log.e("getInitData", error.toString());
            } else {
                Offlinemode.showWhenOfflineMode(this);
            }
        });
    }

    public void setupEmailDropDown(){
        Spinner emailDropdown = findViewById(R.id.emailDropdown);
        SimpleTitleSpinnerAdapter adapter = new SimpleTitleSpinnerAdapter(this,new EmailContractor.TypeList(this));
        adapter.setDisplayTextSize(20);
        adapter.setDisplayTextColor(getResources().getColor(R.color.text_title_dark));
        emailDropdown.setAdapter(adapter);
        emailDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EmailContractor.TypeListItem item = (EmailContractor.TypeListItem) emailDropdown.getAdapter().getItem(position);
                email.setType(item);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    public void setupDueDateCalendar(){
        TextView tvDueDate = findViewById(R.id.txtDueDate);
        email.setDueBy(buildingCase.getDueDate());
        tvDueDate.setText(DateUtils.getDateString(buildingCase.getDueDate()));
        tvDueDate.setOnClickListener(v -> {
            Calendar c =  Calendar.getInstance();
            DatePickerDialog dialog = new DatePickerDialog(this,R.style.DatePickerTheme,(view, year, month, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,month,dayOfMonth);
                Date updateDate = calendar.getTime();
                tvDueDate.setText(DateUtils.getDateString(updateDate));
                email.setDueBy(updateDate);
            },c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DATE));
            dialog.show();
        });
    }
    public void setupSwitch(){
        Switch s = findViewById(R.id.switchAddDescription);
        s.setOnCheckedChangeListener((buttonView, isChecked) -> {
            email.setEnableDescription(isChecked);
        });

        ((Switch) findViewById(R.id.switchAddDescription)).setChecked(true);
        email.setEnableDescription(true);

    }

    public void setupViewPager(){
        vpContractor = findViewById(R.id.emailContractorPager);
        EmailContractorDetailPagerAdapter adapter = new EmailContractorDetailPagerAdapter(getSupportFragmentManager(),contractorList,Integer.toString(buildingCase.getNumber()));
        adapter.setListener(this::setupViewPager);
        vpContractor.setAdapter(adapter);
        vpContractor.setCurrentItem(0);
        vpContractor.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        TabLayout indicator = findViewById(R.id.indicator);
        indicator.setupWithViewPager(vpContractor);
    }

    public void setupFABAction(){
        findViewById(R.id.fabAddContractor).setOnClickListener(v -> this.displayAddContractorFragment());
    }

    public void displayAddContractorFragment(){
        EmailAddContractorDialogFragment dialogFragment = EmailAddContractorDialogFragment.createInstance(contractorList);
        dialogFragment.setListener(contractor -> {
            EmailContractorDetailPagerAdapter adapter = (EmailContractorDetailPagerAdapter) vpContractor.getAdapter();
            if (null != adapter) {
                contractor.setSubject(buildingCase.getSubject());
                adapter.addSelectContractor(contractor);
                vpContractor.setCurrentItem(contractorList.getSelectedItem().size());
            }
        });
        dialogFragment.show(getSupportFragmentManager(),"TAG");
    }
}
