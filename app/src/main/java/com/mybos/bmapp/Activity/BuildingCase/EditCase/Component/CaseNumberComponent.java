package com.mybos.bmapp.Activity.BuildingCase.EditCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleSpinnerAdapter;
import com.mybos.bmapp.Data.Model.Auth.Permission;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatus;
import com.mybos.bmapp.Data.Model.Cases.BuildingCaseStatusList;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 23/04/2018.
 *
 * @author EmLaAi
 */
public class CaseNumberComponent extends ConstraintLayout {
    static TextView txtCaseNumber;
    static Spinner statusDropdown;
    static ProgressBar loadingIndicator;
    public CaseNumberComponent(Context context) {
        super(context);
        additionalInit();
    }

    public CaseNumberComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public CaseNumberComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(),R.layout.a_edit_case_f_edit_case_detail_c_case_title,this);
        txtCaseNumber = findViewById(R.id.txtCaseNumber);
        statusDropdown = findViewById(R.id.statusDropdown);
        loadingIndicator = findViewById(R.id.loadingIndicator);
    }

    public void setCaseNumber(String caseNumber) {
        String StCaseNumber = " #" + caseNumber;
        txtCaseNumber.setText(StCaseNumber);
    }

    public void setupStatusDropDown(BuildingCaseStatusList statusList, int selectedId, StatusListDropDownSelectListener listener){
        SimpleTitleSpinnerAdapter adapter = new SimpleTitleSpinnerAdapter(getContext(), statusList);
        adapter.setDisplayTextColor(getContext().getResources().getColor(R.color.text_title_dark));
        if (null == statusDropdown){
            additionalInit();
        }
        statusDropdown.setAdapter(adapter);
        statusDropdown.setSelection(statusList.getSelectedIndex(selectedId));
        if (Permission.shared(getContext()).getPermission(Permission.Types.Case) != null){
            if (Permission.shared(getContext()).getPermission(Permission.Types.Case).isEdit()) {
                statusDropdown.setEnabled(true);
            }else {
                statusDropdown.setEnabled(false);
            }
        }else{
            statusDropdown.setEnabled(false);
        }
        statusDropdown.post(()->{
            statusDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    BuildingCaseStatus model = (BuildingCaseStatus) statusDropdown.getAdapter().getItem(position);
                    listener.didSeletedItem(model);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        });

    }
    public void displayLoading(){
        this.loadingIndicator.setVisibility(VISIBLE);
    }
    public void hideLoading(){
        this.loadingIndicator.setVisibility(GONE);
    }

    public interface StatusListDropDownSelectListener{
        void didSeletedItem(BuildingCaseStatus status);
    }
}
