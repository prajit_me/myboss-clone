package com.mybos.bmapp.Activity.Inspection.InspectionList.Fragment;


import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Inspection.Adapter.InspectionDetailRecyclerAdapter;
import com.mybos.bmapp.Activity.Inspection.Adapter.InspectionDraftListAdapter;
import com.mybos.bmapp.Activity.Inspection.Adapter.InspectionListRecyclerAdapter;
import com.mybos.bmapp.Activity.Inspection.CreateInspection.MakeInspectionActivity;
import com.mybos.bmapp.Activity.Inspection.InspectionList.InspectionListController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Inspection.InspectionList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InspectionListFragment extends BaseFragment {
    private static final String INSPECTION_TYPE_KEY = "INSPECTION_TYPE_KEY";
    private int inpsectionType = 0;
    private RecyclerView rclInspectionList;
    private TextView etSearch;
    private String keyword;
    private InspectionListController controller = new InspectionListController();
    private InspectionList inspectionList = new InspectionList();

    public InspectionListFragment() {
        // Required empty public constructor
    }
    public static InspectionListFragment createInstance(int inspectionType){
        InspectionListFragment fragment = new InspectionListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(INSPECTION_TYPE_KEY,inspectionType);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            inpsectionType = getArguments().getInt(INSPECTION_TYPE_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.a_inspection_list_f_inspection_list, container, false);
        setupRecycleView(view);
        setupSearchView(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getInspectinList();
    }

    public void setupRecycleView(View view){
        rclInspectionList = view.findViewById(R.id.rclInspectionList);
        rclInspectionList.setLayoutManager(new LinearLayoutManager(getContext()));

        if (inpsectionType==99){
            InspectionDraftListAdapter adapterDraft = new InspectionDraftListAdapter(new ArrayList<>());
            adapterDraft.setListener(inspection -> {
                Intent intent = new Intent(getContext(), MakeInspectionActivity.class);
                intent.putExtra(MakeInspectionActivity.INSPECTION_KEY,inspection.getId());
                intent.putExtra(MakeInspectionActivity.INSPECTION_TYPE_KEY,inspection.getType());
                intent.putExtra(MakeInspectionActivity.INSPECTION_DRAFT,1);
                intent.putExtra(MakeInspectionActivity.INSPECTION_HIS_ID,Integer.valueOf(inspection.getHistoryIdForDraft()));
                startActivity(intent);
            });
            rclInspectionList.setAdapter(adapterDraft);
        }else {
            InspectionListRecyclerAdapter adapter = new InspectionListRecyclerAdapter(inspectionList);
            adapter.setListener(inspection -> {
                Intent intent = new Intent(getContext(), MakeInspectionActivity.class);
                intent.putExtra(MakeInspectionActivity.INSPECTION_KEY,inspection.getId());
                intent.putExtra(MakeInspectionActivity.INSPECTION_TYPE_KEY,this.inpsectionType);
                startActivity(intent);
            });
            rclInspectionList.setAdapter(adapter);
        }


    }

    public void setupSearchView(View view){
        etSearch = view.findViewById(R.id.etSearch);
        etSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH){
                v.clearFocus();
                dismissKeyboard();
                String keyword = v.getText().toString();
                if (inpsectionType==99){
                    InspectionDraftListAdapter adapter = (InspectionDraftListAdapter) rclInspectionList.getAdapter();
                    adapter.performSearch(keyword);
                }else {
                    InspectionListRecyclerAdapter adapter = (InspectionListRecyclerAdapter) rclInspectionList.getAdapter();
                    adapter.performSearch(keyword);
                }


                return true;
            }
            return false;
        });
    }

    public void getInspectinList(){
        if (Offlinemode.checkNetworkConnection(getContext())){
            SearchTableQueryBuilder builder = new SearchTableQueryBuilder();
            builder.setCurrentPage(1);
            builder.setOrderBy("added");
            builder.setOrderType("desc");
            builder.setKeyword(keyword);

            // check issue null on Inspection List
            if(SingletonObjectHolder.getInstance().getBuildingList()==null){
                Intent intent = new Intent(getContext(), DashboardActivity.class);
                getContext().startActivity(intent);
                return;
            }
            Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
            if (inpsectionType==99){
                controller.getInspectionDraft(getContext(),selectedBuilding.getId(),list->{
                    InspectionDraftListAdapter adapter = (InspectionDraftListAdapter) rclInspectionList.getAdapter();
                    adapter.setSource(list);
                },error -> {
                    Log.e("getInspectinListDraft",error.toString());
                });
            }else {
                controller.getInspectionList(getContext(),selectedBuilding.getId(),inpsectionType,builder,list->{
                    this.inspectionList = list;
                    InspectionListRecyclerAdapter adapter = (InspectionListRecyclerAdapter) rclInspectionList.getAdapter();
                    adapter.setSource(inspectionList);
                },error -> {
                    Log.e("getInspectinList",error.toString());
                });
            }
        }else{
            //offline mode
            if (InspectionList.getOfflineList()!=null){
                if (inpsectionType==0){
                    this.inspectionList = InspectionList.getOfflineList();
                }else if (inpsectionType==99){
                    return;
                }else{
                    this.inspectionList = InspectionList.getOfflineListPrivate();
                }
                InspectionListRecyclerAdapter adapter = (InspectionListRecyclerAdapter) rclInspectionList.getAdapter();
                adapter.setSource(inspectionList);
            }
        }

    }
}
