package com.mybos.bmapp.Activity.Calendar.CalendarCustom.Listener;


public interface OnSelectionAbilityListener {
    void onChange(boolean enabled);
}
