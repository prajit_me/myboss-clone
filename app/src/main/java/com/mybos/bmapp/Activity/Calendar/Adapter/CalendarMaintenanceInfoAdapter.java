package com.mybos.bmapp.Activity.Calendar.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Activity.Calendar.Fragment.MaintenanceCommentsFragment;
import com.mybos.bmapp.Activity.Calendar.Fragment.MaintenanceDocumentsFragment;
import com.mybos.bmapp.Activity.Calendar.Fragment.MaintenanceGeneralFragment;
import com.mybos.bmapp.Activity.Calendar.Fragment.MaintenanceInvoicesFragment;

import java.util.ArrayList;

public class CalendarMaintenanceInfoAdapter extends FragmentPagerAdapter {

    ArrayList<Integer> sections = new ArrayList<>();
    private Context context;
    private MaintenanceCommentsFragment maintenanceCommentsFragment;
    private MaintenanceDocumentsFragment maintenanceDocumentsFragment;
    private MaintenanceGeneralFragment maintenanceGeneralFragment;
    private MaintenanceInvoicesFragment maintenanceInvoicesFragment;

    public CalendarMaintenanceInfoAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context = context;
        sections.add(0);
        sections.add(1);
        sections.add(2);
        sections.add(3);
        maintenanceGeneralFragment = new MaintenanceGeneralFragment();
        maintenanceCommentsFragment = new MaintenanceCommentsFragment();
        maintenanceDocumentsFragment = new MaintenanceDocumentsFragment();
        maintenanceInvoicesFragment = new MaintenanceInvoicesFragment();
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return maintenanceGeneralFragment;
        }else if (position == 1){
            return  maintenanceDocumentsFragment;
        }else if (position == 2){
            return  maintenanceInvoicesFragment;
        }else if (position == 3){
            return  maintenanceCommentsFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        int section = sections.get(position);
        if (section == 0){
            return "General";
        }
        if (section == 1){
            return "Documents";
        }
        if (section == 2){
            return "Invoices";
        }
        if (section == 3){
            return "Comments";
        }
        return super.getPageTitle(position);
    }
}
