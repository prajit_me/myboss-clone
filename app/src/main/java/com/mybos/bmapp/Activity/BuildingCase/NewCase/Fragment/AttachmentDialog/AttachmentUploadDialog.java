package com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentDialog;

import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringDef;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mybos.bmapp.Component.SpinnerAdapter.SimpleTitleNullSpinnerAdapter;
import com.mybos.bmapp.Data.Functional.PhotoModel;
import com.mybos.bmapp.Data.Model.Cases.SelectedFileInvoiceQuotes;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.Error.ImageEncodeError;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Utils.BuildFileNameForUploadDocument;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

/**
 * Created by EmLaAi on 08/04/2018.
 *
 * @author EmLaAi
 */
public class AttachmentUploadDialog extends DialogFragment {
    private static final String CONTRACTOR_KEY = "AttachmentUploadDialog.CONTRACTOR_KEY";
    private static final String DISPLAY_MODE_KEY = "AttachmentUploadDialog.DISPLAY_MODE_KEY";
    private static final int PHOTO_LIBRARY_REQUEST_CODE = 1;
    private static final int PHOTO_TAKE_REQUEST_CODE = 2;
    public static final String PHOTO_MODE = "PHOTO_MODE";
    public static final String QUOTE_MODE = "QUOTE_MODE";
    public static final String INVOICE_MODE = "INVOICE_MODE";
    ImageView imgReview;
    @Mode String mode = PHOTO_MODE;
    ContractorList contractorList;
    View detailView,invoiceView;
    TextView txtInvoiceNumber,txtValue,txtNotes;
    AttachmentUploadDialogListener listener;
    PhotoModel selectedPhoto;
    Contractor selectedContractor;
    String currentPhotoPath;
    TextView message;
    Uri currentUri;
    public static final int PICKFILE_RESULT_CODE = 3;
    public AttachmentUploadDialog(){}

    public static AttachmentUploadDialog createInstance(ContractorList contractorList,@Mode String mode){
        AttachmentUploadDialog dialog = new AttachmentUploadDialog();
        Bundle arg = new Bundle();
        arg.putParcelable(CONTRACTOR_KEY,contractorList);
        arg.putString(DISPLAY_MODE_KEY,mode);
        dialog.setArguments(arg);
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.a_edit_new_case_f_attachment_d_upload_attachment,container,false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        detailView = view.findViewById(R.id.viewDetail);
        invoiceView = view.findViewById(R.id.viewInvoice);
        txtNotes = view.findViewById(R.id.txtNotes);
        txtInvoiceNumber = view.findViewById(R.id.txtInvoiceNumber);
        txtValue = view.findViewById(R.id.txtValue);
        if (null != getArguments()){
            setMode(getArguments().getString(DISPLAY_MODE_KEY));
            this.contractorList = getArguments().getParcelable(CONTRACTOR_KEY);
        }

        imgReview = view.findViewById(R.id.imgReview);
        message = view.findViewById(R.id.message);

        setupTakePhotoCameraAndLibraryAction(view);
        setupContractorDropdown(view);

        view.findViewById(R.id.btnCancel).setOnClickListener(v ->AttachmentUploadDialog.this.dismiss());
        view.findViewById(R.id.btnSave).setOnClickListener(v -> {
            txtValue.clearFocus();
            txtInvoiceNumber.clearFocus();
            txtNotes.clearFocus();
            if (null != listener){
                switch (mode){
                    case PHOTO_MODE:
                        if (null != selectedPhoto){
                            listener.uploadPhoto(selectedPhoto);
                        }
                        AttachmentUploadDialog.this.dismiss();
                        break;
                    case QUOTE_MODE:
                        uploadQuote();
                        break;
                    case INVOICE_MODE:
                        uploadInvoice();
                        break;
                }
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        int width = (int) ( Resources.getSystem().getDisplayMetrics().widthPixels * 0.9);
        if (null != getDialog().getWindow()) {
            getDialog().getWindow().setLayout(width,ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.shared().i("-------------GET BACK ----------------");
        if (requestCode == PHOTO_LIBRARY_REQUEST_CODE){
            if (data != null){
                Uri photoUri = data.getData();
                if (getActivity() != null && photoUri != null) {
                    String mime = getActivity().getContentResolver().getType(photoUri);
                    String name;
                    Cursor cursor = getActivity().getContentResolver().query(photoUri,null,null,null,null);
                    if (cursor != null) {
                        int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                        cursor.moveToFirst();
                        name = cursor.getString(nameIndex);
                        cursor.close();
                    }else {
                        name = "photo" + (System.currentTimeMillis()/1000);
                    }
                    Logger.shared().i(mime);
                    try {
                        Bitmap selectedImage = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), photoUri);
                        imgReview.setImageBitmap(selectedImage);
                        Log.e("select_image",photoUri.toString());
                        selectedPhoto = new PhotoModel(selectedImage,name,mime);
                    } catch (IOException e) {
                        Toast.makeText(getContext(), new ImageEncodeError().toString(), Toast.LENGTH_LONG).show();
                    }
                }
            }
        }
        if (requestCode == PHOTO_TAKE_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                try {
                    File file = new File(currentPhotoPath);
                    String name = file.getName();
                    String mime = "Content-Type: image/jpeg";
                    Bitmap selectedImage = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(),Uri.fromFile(file));
                    imgReview.setImageBitmap(selectedImage);
                    selectedPhoto = new PhotoModel(selectedImage,name,mime);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }

        if (requestCode == PICKFILE_RESULT_CODE){
            if (resultCode == RESULT_OK){
                    Uri fileUri = data.getData();
                    String filePath = fileUri.getLastPathSegment();
                    File file = new File(buildPath(filePath));
                    byte[] selectedByteFile = null;
                    InputStream inputStream = null;
                    try {
                        inputStream = getContext().getContentResolver().openInputStream(fileUri);
                        selectedByteFile = new byte[inputStream.available()];
                        inputStream.read(selectedByteFile);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                        Log.e("error_get_file",e.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e("error_get_file2",e.toString());
                    }
                    message.setText(file.getName());

                    ContentResolver cR = getContext().getContentResolver();
                    String mime = cR.getType(fileUri);
                    String name = file.getAbsolutePath().substring(file.getAbsolutePath().lastIndexOf("/")+1);
                    name = name.substring(name.lastIndexOf(":")+1);

                    name = BuildFileNameForUploadDocument.checkAndBuildFileName(name,mime);
                    SelectedFileInvoiceQuotes.setMime(mime);
                    SelectedFileInvoiceQuotes.setBytes(selectedByteFile);
                    SelectedFileInvoiceQuotes.setName(name);

                    selectedPhoto = null;
            }
        }
    }

    public String buildPath(String path){
        path = path.replace("/storage/emulated/0/","");
        int indexCut = path.indexOf(":");
        path = path.substring(indexCut+1,path.length());
        path = "/storage/emulated/0/"+path;
        Log.e("result",path);
        return path;
    }

    public void setMode(@Mode String mode){
        this.mode = mode;
        switch (mode){
            case PHOTO_MODE:
                detailView.setVisibility(View.GONE);
                break;
            case QUOTE_MODE:
                invoiceView.setVisibility(View.GONE);
                txtNotes.setVisibility(View.GONE);
                break;
            case INVOICE_MODE:
                break;
        }
    }

    public void setupTakePhotoCameraAndLibraryAction(View view){
        view.findViewById(R.id.btnPhotoPicker).setOnClickListener(v -> {

            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext(), R.style.DialogTheme);
            builder1.setTitle("Select photo");
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Take photo",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            if (intent.resolveActivity(getActivity().getPackageManager()) != null){
                                // Create the File where the photo should go
                                File photoFile = null;
                                try {
                                    photoFile = createImageFile();
                                } catch (IOException ex) {
                                    // Error occurred while creating the File
                                }
                                // Continue only if the File was successfully created
                                if (photoFile != null) {
                                    Uri photoURI = FileProvider.getUriForFile(getContext(),
                                            "com.mybos.bmapp.fileprovider",
                                            photoFile);
                                    currentUri = photoURI;
                                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                                    startActivityForResult(intent,PHOTO_TAKE_REQUEST_CODE);
                                }

                            }
                        }
                    });

            builder1.setNegativeButton(
                    "From library",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            intent.setType("image/jpeg,jpg,png");
                            if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                                startActivityForResult(intent, PHOTO_LIBRARY_REQUEST_CODE);
                            }
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        });

        view.findViewById(R.id.btnSelectDocument).setOnClickListener(v -> {
            Intent chooseFile = new Intent(Intent.ACTION_GET_CONTENT);
            chooseFile.setType("*/*");
            chooseFile = Intent.createChooser(chooseFile, "Choose a file");
            startActivityForResult(chooseFile, PICKFILE_RESULT_CODE);
        });
    }

    public void setupContractorDropdown(View view){
        SimpleTitleNullSpinnerAdapter adapter = new SimpleTitleNullSpinnerAdapter(view.getContext(),this.contractorList);
        Spinner spinner = view.findViewById(R.id.dropdownContractor);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SimpleTitleNullSpinnerAdapter clickAdapter = (SimpleTitleNullSpinnerAdapter) spinner.getAdapter();
                Contractor contractor = (Contractor) clickAdapter.getItem(position);
                if (null != contractor){
                    selectedContractor = contractor;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void setListener(AttachmentUploadDialogListener listener) {
        this.listener = listener;
    }

    public void uploadQuote(){
        String valueString = txtValue.getText().toString();
        if (!(valueString.length() > 0)){
            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            return;
        }
        Double value = Double.parseDouble(valueString);
//        if (!(value > 0)){
//            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
//            return;
//        }
        if (null == selectedContractor){
            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            return;
        }
        listener.uploadQuote(value,selectedContractor,selectedPhoto);
        this.dismiss();
    }

    public void uploadInvoice(){
        String invoiceNumber = txtInvoiceNumber.getText().toString();
        if (!(invoiceNumber.length() > 0)){
            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            return;
        }
        String valueString = txtValue.getText().toString();
        if (!(valueString.length() > 0)){
            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            return;
        }
        Double value = Double.parseDouble(valueString);
        if (!(value > 0)){
            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            return;
        }
        if (null == selectedContractor){
            Toast.makeText(getContext(), getString(R.string.AlertFillRequiredField), Toast.LENGTH_LONG).show();
            return;
        }
        listener.uploadInvoice(invoiceNumber,value,txtNotes.getText().toString(),selectedContractor,selectedPhoto);
        this.dismiss();
    }

    @StringDef({PHOTO_MODE,QUOTE_MODE,INVOICE_MODE})
    @Retention(RetentionPolicy.SOURCE)
    @interface Mode{}

    public interface AttachmentUploadDialogListener{
        void uploadPhoto(PhotoModel photoModel);
        void uploadQuote(Double value, Contractor contractor,PhotoModel photoModel);
        void uploadInvoice(String invoiceNumber,Double value,String notes,Contractor contractor,PhotoModel photoModel);
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpeg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

}
