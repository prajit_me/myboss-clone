package com.mybos.bmapp.Activity.Resident.Adapter;

import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Resident.ResidentDetailActivity_2nd;
import com.mybos.bmapp.Component.RecycleAdapter.AbstractScrollToLoadMoreRecyclerAdapter;
import com.mybos.bmapp.Data.Model.Resident.Resident;
import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 22/04/2018.
 *
 * @author EmLaAi
 */
public class ResidentRecyclerAdapter extends AbstractScrollToLoadMoreRecyclerAdapter<Resident> {


    @Override
    public int getItemLayout() {
        return R.layout.r_a_resident_list_n_resident_list_item;
    }

    @Override
    public RecyclerView.ViewHolder getItemViewHolder(View view) {
        return new ResidentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof  ResidentViewHolder){
            ((ResidentViewHolder) holder).lblResident.setText(sourceList.get(position).getFullName());
            ((ResidentViewHolder) holder).lblApartment.setText(sourceList.get(position).getApartment());

            ((ResidentViewHolder) holder).setListener(()->{
                Resident resident = sourceList.get(position);
                Intent intent = new Intent(((ResidentViewHolder) holder).lblApartment.getContext(), ResidentDetailActivity_2nd.class);
                intent.putExtra(ResidentDetailActivity_2nd.RESIDENT_KEY,resident);
                ((ResidentViewHolder) holder).lblApartment.getContext().startActivity(intent);
            });
        }
    }

    class ResidentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView lblResident,lblApartment;
        OnItemClickListener listener;
        public ResidentViewHolder(View itemView) {
            super(itemView);
            lblResident = itemView.findViewById(R.id.lblResidentName);
            lblApartment = itemView.findViewById(R.id.lblApartment);
            itemView.findViewById(R.id.cardView).setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (null != listener){
                listener.onClick();
            }
        }

        public void setListener(OnItemClickListener listener) {
            this.listener = listener;
        }
    }

    public static interface OnItemClickListener{
        void onClick();
    }
}
