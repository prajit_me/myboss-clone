package com.mybos.bmapp.Activity.Assets.Activity;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mybos.bmapp.Activity.Adapter.AssetsRecyclerAdapter;
import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Contractor.Activities.ContractorListActivity;
import com.mybos.bmapp.Activity.Dashboard.DashboardActivity;
import com.mybos.bmapp.Activity.Filter.FilterFragment;
import com.mybos.bmapp.Activity.Filter.FilterStoreRealm;
import com.mybos.bmapp.Activity.Filter.ListFilterStoreRealm;
import com.mybos.bmapp.Activity.Resident.Adapter.ResidentRecyclerAdapter;
import com.mybos.bmapp.Data.Mapper.Assets.AssetMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAsset;
import com.mybos.bmapp.Data.Model.BuildingAssets.BuildingAssetInfoCollector;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

public class BuildingAssetActivity extends a_base_toolbar {

    private RecyclerView recycler_viewAssets;
    private EditText edtSearchAsset;

    private String FORM_TITLE = "Assets";
    private int ConstantTextSize = 15, MinWidth = 600;
    private AssetMapper assetMapper = new AssetMapper();

    private String selectedCategory = "all";
    private String currentSearchInfo = "";
    private Button btnTagFilter,icFilter;
    private boolean insideGetList = false;
    private int currentPage = 1;

    private void setFormTitle() {
        this.updateToolbarTitle(FORM_TITLE);
    }

    private void getReferences() {
        recycler_viewAssets = findViewById(R.id.recycler_viewAssets);
        recycler_viewAssets.setLayoutManager(new LinearLayoutManager(this));
        AssetsRecyclerAdapter adapter = new AssetsRecyclerAdapter();
        adapter.setup(recycler_viewAssets);
        recycler_viewAssets.setAdapter(adapter);
        adapter.setOnNeedLoadMoreListener(this::showAssetList);

        edtSearchAsset = findViewById(R.id.edtSearchAsset_BuildingAssetActivity);
        edtSearchAsset.addTextChangedListener(new SearchListener());
    }

    public void showAssetList() {
        Log.e("check","showAssetList");
        if (insideGetList){
            return;
        }
        insideGetList = true;
        assetMapper.getBuildingAssetList(this,
                BuildingInfoCollection.getBuildingID(),selectedCategory,currentPage,
                response -> {
                    AssetsRecyclerAdapter adapter = (AssetsRecyclerAdapter) recycler_viewAssets.getAdapter();
                    if (currentPage == 1){
                        ((AssetsRecyclerAdapter) recycler_viewAssets.getAdapter()).reset();
                    }
                    ArrayList<BuildingAsset> showAssets = new ArrayList<>();

                    if (currentSearchInfo.equals("")){
                        adapter.setMaximum(response.size());
                        adapter.updateModel(response);
                        adapter.notifyDataSetChanged();
                        currentPage+=1;
                        insideGetList = false;
                    }else {
                        for (int i = 0; i < response.size(); i++) {
                            if (response.get(i).getName().toLowerCase().contains(currentSearchInfo.toLowerCase())) {
                                showAssets.add(response.get(i));

                            }
                        }
                        adapter.setMaximum(showAssets.size());
                        adapter.updateModel(showAssets);
                        adapter.notifyDataSetChanged();
                        currentPage+=1;
                        insideGetList = false;
                    }
                },
                error -> {  });
    }

    private void startup() {
        //  SET TITLE
        setFormTitle();

        //  GET REFERENCES
        getReferences();

        //  SHOW ASSET LIST
        getRealmData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_asset);
        btnTagFilter = findViewById(R.id.btnTagFilter);
        icFilter = findViewById(R.id.icFilter);

        icFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FilterFragment dialog = new FilterFragment(2);
                dialog.show(getSupportFragmentManager(),"Filter");
            }
        });

        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuildingAssetActivity.this, DashboardActivity.class);
                startActivity(intent);
            }
        });

        //  START UP
        setupBtnOption();
        startup();
    }

    public void setupBtnOption(){
        findViewById(R.id.btnOption).setVisibility(View.VISIBLE);
        findViewById(R.id.btnOption).setBackground(getDrawable(R.drawable.ic_add));
        findViewById(R.id.btnOption).setScaleX(1.5f);
        findViewById(R.id.btnOption).setScaleY(1.5f);
        findViewById(R.id.btnOption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BuildingAssetActivity.this,NewAssetActivity.class);
                startActivity(intent);
            }
        });

        //setup button filter
    }

    public void getRealmData(){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        ListFilterStoreRealm listFilterRealm = ListFilterStoreRealm.get();
        if (null != listFilterRealm && null != listFilterRealm.getFilterByBuildingId(selectedBuilding.getId())) {
            FilterStoreRealm filterRealm = listFilterRealm.getFilterByBuildingId(selectedBuilding.getId());
            if (null != filterRealm.getSelectedFilterAsset()) {
                selectedCategory = filterRealm.getSelectedFilterAsset();
                addTag(filterRealm.getSelectedFilterAssetName(), false);

            }

        }
        currentPage = 1;
        showAssetList();

    }

    public void addTag(String name, boolean save){
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

        btnTagFilter.setText(name);
        btnTagFilter.setVisibility(View.VISIBLE);
        btnTagFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnTagFilter.setVisibility(View.GONE);
                setSelectedCategory("all");
                currentPage = 1;
                showAssetList();
                ListFilterStoreRealm temp2 = ListFilterStoreRealm.get();
                if (null!=temp2){
                    if (temp2.getFilterByBuildingId(selectedBuilding.getId()) != null){
                        temp2.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterAsset(null);
                    }
                }
                ListFilterStoreRealm.save(temp2);
            }
        });
        //save Realm
        if (save){
            ListFilterStoreRealm temp = ListFilterStoreRealm.get();
            FilterStoreRealm newFilter = new FilterStoreRealm();
            newFilter.setBuildingId(selectedBuilding.getId());
            newFilter.setSelectedFilterAsset(selectedCategory);
            newFilter.setSelectedFilterAssetName(name);
            if (null!=temp){
                if (temp.getFilterByBuildingId(selectedBuilding.getId()) != null){
                    temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterAsset(selectedCategory);
                    temp.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterAssetName(name);
                }else {
                    temp.addNewFilter(newFilter);
                }

            }else{
                temp = new ListFilterStoreRealm();
                temp.addNewFilter(newFilter);

            }
            ListFilterStoreRealm.save(temp);
        }
    }

    public String getSelectedCategory() {
        return selectedCategory;
    }

    public void setSelectedCategory(String selectedCategory) {
        this.selectedCategory = selectedCategory;
    }

    public String getCurrentSearchInfo() {
        return currentSearchInfo;
    }

    public void setCurrentSearchInfo(String currentSearchInfo) {
        this.currentSearchInfo = currentSearchInfo;
    }

    class SearchListener implements TextWatcher, View.OnKeyListener {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String SearchInfo = edtSearchAsset.getText().toString().trim();
            currentSearchInfo = SearchInfo;
            currentPage = 1;
            showAssetList();
        }

        @Override
        public void afterTextChanged(Editable s) {

        }

        @Override
        public boolean onKey(View v, int keyCode, KeyEvent event) {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                    (keyCode == KeyEvent.KEYCODE_ENTER)) {
                String SearchInfo = edtSearchAsset.getText().toString().trim();
                currentSearchInfo = SearchInfo;
                currentPage = 1;
                showAssetList();
                return true;
            }
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(BuildingAssetActivity.this, DashboardActivity.class);
        startActivity(intent);
    }

    public void removeTag(){
        btnTagFilter.setVisibility(View.GONE);
        Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
        ListFilterStoreRealm temp2 = ListFilterStoreRealm.get();
        if (null!=temp2){
            if (temp2.getFilterByBuildingId(selectedBuilding.getId()) != null){
                temp2.getFilterByBuildingId(selectedBuilding.getId()).setSelectedFilterAsset(null);
            }
        }
        ListFilterStoreRealm.save(temp2);
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }
}
