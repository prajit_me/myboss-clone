package com.mybos.bmapp.Activity.Keys.Activities.History;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Activity.Keys.Activities.SignIn.KeySignInActivity;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyHistoryExpanViewAdapter;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

public class KeyHistoryActivity extends a_base_toolbar {

    ExpandableListView exHistoryKey;
    TextView tvSubTitleHistoryKey;
    ProgressBar progressBarHistory;

    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_key_history);
        updateToolbarTitle("Key history");

        exHistoryKey = findViewById(R.id.exHistoryKey);
        tvSubTitleHistoryKey = findViewById(R.id.tvSubTitleHistoryKey);
        progressBarHistory = findViewById(R.id.progressBarHistory);

        tvSubTitleHistoryKey.setText(getIntent().getStringExtra("sub_title"));
    }

    @Override
    protected void onStart() {
        super.onStart();
        controller.getKeyHistory(KeyHistoryActivity.this,selectedBuilding.getId(),
                history->{
                    exHistoryKey.setAdapter(new KeyHistoryExpanViewAdapter(KeyHistoryActivity.this));
                    progressBarHistory.setVisibility(View.INVISIBLE);
                    exHistoryKey.setVisibility(View.VISIBLE);
                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(KeyHistoryActivity.this)){
                        Log.e("getKeyHistory",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(KeyHistoryActivity.this);
                    }
                });



    }
}
