package com.mybos.bmapp.Activity.Keys.Fragment.PrivateKey.OtherKeys;


import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.Keys.Adapter.KeyApartmentOtherAdapter;
import com.mybos.bmapp.Activity.Keys.KeysController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class OtherKeyListFragment extends BaseFragment {

    public OtherKeyListFragment(){}

    KeysController controller = new KeysController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();
    ListView lvKeyOther;
    ProgressBar progressBarOtherKey;
    private static String apartmentid;

    public static String getApartmentid() {
        return apartmentid;
    }

    public static void setApartmentid(String apartmentid) {
        OtherKeyListFragment.apartmentid = apartmentid;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_other_key_list, container, false);
        progressBarOtherKey = view.findViewById(R.id.progressBarOtherKey);
        lvKeyOther = view.findViewById(R.id.lvKeyOther);
        controller.getApartmentKey(getContext(), selectedBuilding.getId(), buildParam(),
                apartment -> {
                    getApartmentOther();
                    lvKeyOther.setAdapter(new KeyApartmentOtherAdapter(getContext(),
                            R.layout.key_apartment_other_adaptor));
                    progressBarOtherKey.setVisibility(View.INVISIBLE);
                    lvKeyOther.setVisibility(View.VISIBLE);

                },
                error -> {
                    if (Offlinemode.checkNetworkConnection(getContext())){
                        Log.e("getApartmentKey",error.toString());
                    }else {
                        Offlinemode.showWhenOfflineMode(getContext());
                    }
                });

        return view;
    }

    private String buildParam() {
        return "{\"apartment\":\"" + getApartmentid() + "\"}";
    }

    private void getApartmentOther() {
        Apartment.setApartment_keys_O(new ArrayList<>());
        for (Apartment apartment : Apartment.getApartment_keys()
        ) {
            if (!apartment.getLocation().equals("1") && !apartment.getLocation().equals("0"))
                Apartment.getApartment_keys_O().add(apartment);
        }
    }

}
