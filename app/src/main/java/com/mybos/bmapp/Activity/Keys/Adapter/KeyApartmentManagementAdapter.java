package com.mybos.bmapp.Activity.Keys.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Key.Apartment;
import com.mybos.bmapp.Data.Model.Key.History;
import com.mybos.bmapp.Data.Model.Key.KeyInOut;
import com.mybos.bmapp.R;

public class KeyApartmentManagementAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private OnItemClick listener;
    private OnItemClickSign listenerSign;

    public KeyApartmentManagementAdapter(Context context, int layout){
        this.context = context;
        this.layout = layout;
    }

    @Override
    public int getCount() {
        return Apartment.getApartment_keys_M().size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(layout, null);

        ImageView imageViewStartKAManagement = view.findViewById(R.id.imageViewStartKAManagement);
        TextView tvKeyIdKAM = view.findViewById(R.id.tvKeyIdKAM);
        TextView tvKeyTypeKAM = view.findViewById(R.id.tvKeyTypeKAM);
        TextView tvLocationKAM = view.findViewById(R.id.tvLocationKAM);
        TextView tvOwnerKAM = view.findViewById(R.id.tvOwnerKAM);
        TextView tvContactKAM = view.findViewById(R.id.tvContactKAM);
        Button btnSignOutInKAM = view.findViewById(R.id.btnSignOutInKAM);
        Button btnHistoryKAM = view.findViewById(R.id.btnHistoryKAM);

        Apartment apartment = Apartment.getApartment_keys_M().get(i);

        if(apartment.getKey_status().equals("1")){
            imageViewStartKAManagement.setImageResource(R.drawable.ic_group_7);
            btnSignOutInKAM.setText(R.string.key_sign_out_key);
        }else{
            imageViewStartKAManagement.setImageResource(R.drawable.ic_group_7_1);
            btnSignOutInKAM.setText(R.string.key_sign_in_key);
        }
        tvKeyIdKAM.setText(apartment.getKey());
        tvKeyTypeKAM.setText(apartment.getTypeText());
        tvLocationKAM.setText(apartment.getLocationText());
        tvOwnerKAM.setText(apartment.getOwner().equals("null")?"-"
                :apartment.getOwner());

        btnSignOutInKAM.setOnClickListener(v -> {
            listenerSign.didSelectSign(apartment);
            KeyInOut.setKeyIdSelected(apartment.getKeyId());
            History.setKeyIdSelected(apartment.getKeyId());
        });

        btnHistoryKAM.setOnClickListener(v -> {
            // set key Id for get History button
            History.setKeyIdSelected(apartment.getKeyId());
            listener.didSelect(apartment);
        });

        return view;
    }

    public void setListenerBTNHistory(OnItemClick listener) {
        this.listener = listener;
    }

    public void setListenerBTNSign(OnItemClickSign listener) {
        this.listenerSign = listener;
    }

    public interface OnItemClick {
        void didSelect(Apartment a);
    }

    public interface OnItemClickSign {
        void didSelectSign(Apartment a);
    }
}
