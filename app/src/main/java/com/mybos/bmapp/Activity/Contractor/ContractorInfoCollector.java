package com.mybos.bmapp.Activity.Contractor;

import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;

import java.util.ArrayList;

public class ContractorInfoCollector {

    private static String Temp = "";
    private static ArrayList<Contractor> CONTRACTOR_LIST = new ArrayList<>();
    private static int SELECTED_CONTRACTOR_ID = 0;
    private static int SELECTED_CONTRACTOR_INDEX = 0;

    public static void setTemp(String temp) {
        Temp = temp;
    }

    public static String getTemp() {
        return Temp;
    }

    public static void addToContractorList(Contractor _Contractor) {
        CONTRACTOR_LIST.add(_Contractor);
    }

    public static void setContractorList(ArrayList<Contractor> _Contractor) {
        CONTRACTOR_LIST.addAll(_Contractor);
    }

    public static void setContractorList(ContractorList _Contractor) {
        CONTRACTOR_LIST.addAll(_Contractor.getAll());
    }

    public static void clearContractorList(){
        CONTRACTOR_LIST.clear();
    }

    public static ArrayList<Contractor> getContractorList() {
        return CONTRACTOR_LIST;
    }

    public static void setSelectedContractorID(int ContractorID){
        SELECTED_CONTRACTOR_ID = ContractorID;
    }

    public static int getSelectedContractorID(){
        return SELECTED_CONTRACTOR_ID;
    }
    
    public static void setSelectedContractorIndex(int ContractorIndex){
        SELECTED_CONTRACTOR_INDEX = ContractorIndex;
    }

    public static int getSelectedContractorIndex(){
        return SELECTED_CONTRACTOR_INDEX;
    }


    public static Contractor getContractorById(int id){
        for (Contractor contractor:CONTRACTOR_LIST
             ) {
            if (id == contractor.getId()){
                return contractor;
            }
        }
        return new Contractor();
    }
}
