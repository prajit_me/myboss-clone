package com.mybos.bmapp.Activity.Library;

import android.content.Context;

import com.mybos.bmapp.Data.Mapper.Library.LibraryMapper;

public class LibraryController {

    public void getLibraryList (Context context, int buildingId, String id, SuccessCallback<String> onSuccess, FailureCallback onFailure) {
        LibraryMapper mapper = new LibraryMapper();
        mapper.getLibraryList(context, buildingId,id,onSuccess::onSuccess, onFailure::onFailure);
    }

    public interface SuccessCallback<T> {
        void onSuccess(T value);
    }

    public interface FailureCallback {
        void onFailure(Exception error);
    }
}
