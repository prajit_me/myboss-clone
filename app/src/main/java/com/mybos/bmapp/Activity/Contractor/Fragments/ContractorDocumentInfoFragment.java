package com.mybos.bmapp.Activity.Contractor.Fragments;


import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Contractor.ContractorInfoCollector;
import com.mybos.bmapp.Data.Mapper.Contractor.ContractorMapper;
import com.mybos.bmapp.Data.Model.Building.BuildingInfoCollection;
import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class ContractorDocumentInfoFragment extends Fragment {

    private LinearLayout lnlyContractorDocumentContainer;
    private ContractorMapper DocumentMapper = new ContractorMapper();
    private Contractor SelectedContractor = ContractorInfoCollector.getContractorById(ContractorInfoCollector.getSelectedContractorIndex());
    private ArrayList<Contractor.ContractorDocument> DocumentList;

    private final int ConstantTextSize = 15, ConstantMeasure = 120, ConstantHeight = 50;
    private final String Expiry = "Expiry: ";
    private final String NoDataMessage = "No document was found.";

    private void loadInfo() {
        DocumentMapper.getDocuments(this.getContext(),
                BuildingInfoCollection.getBuildingID(),
                SelectedContractor.getId(),
                response -> {
                    DocumentList = SelectedContractor.getDocuments();
                    int Size = DocumentList.size();
                    if (Size > 0) {
                        for (int i = 0; i < Size; i++) {
                            Contractor.ContractorDocument CurrentDocument = DocumentList.get(i);
                            String FileName = CurrentDocument.getAttachment().getName();
                            FileName = FileName.replace("-", " ").substring(0, FileName.indexOf("."));

                            String FileType = CurrentDocument.getAttachment().getFileType();

                            String ExpireDate = CurrentDocument.getAttachment().getExpire().substring(0, 10);
                            String ExpireYear = ExpireDate.substring(0, ExpireDate.indexOf("-"));

                            ExpireDate = ExpireDate.substring(ExpireDate.indexOf("-") + 1);
                            String ExpireMonth = ExpireDate.substring(0, ExpireDate.indexOf("-"));

                            String ExpireDay = ExpireDate.substring(ExpireDate.indexOf("-") + 1);

                            ExpireDate = ExpireDay + "/" + ExpireMonth + "/" + ExpireYear;

                            lnlyContractorDocumentContainer.addView(showDocument(i, FileType, FileName, ExpireDate));
                        }
                    }
                    else {
                        LinearLayout.LayoutParams NoDataParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        NoDataParams.setMargins(10, 30, 10, 10);
                        TextView tvNoData = new TextView(this.getActivity());
                        tvNoData.setLayoutParams(NoDataParams);
                        tvNoData.setMinHeight(ConstantHeight);
                        tvNoData.setGravity(Gravity.CENTER);
                        tvNoData.setText(NoDataMessage);
                        tvNoData.setTextColor(Color.BLACK);
                        tvNoData.setTypeface(tvNoData.getTypeface(), Typeface.ITALIC);

                        lnlyContractorDocumentContainer.addView(tvNoData);
                    }
                },
                error -> {});
    }

    private RelativeLayout showDocument(int ContractorIndex, String FileType, String FileName, String ExpireDate) {
        RelativeLayout.LayoutParams DocumentLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        DocumentLayoutParams.setMargins(20, 20, 20, 30);

        //  ROW
        RelativeLayout DocumentLayout = new RelativeLayout(this.getActivity());
        DocumentLayout.setLayoutParams(DocumentLayoutParams);
        DocumentLayout.setBackground(Objects.requireNonNull(this.getActivity()).getDrawable(R.drawable.round_rectangle_bg));

        //  ITEM MARGIN
        LinearLayout.LayoutParams itemLayoutParam = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        itemLayoutParam.setMargins(10, 10, 10, 10);

        //  FILE TYPE
        LinearLayout.LayoutParams itemLayoutParamIv = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        itemLayoutParamIv.setMargins(30, 10, 10, 10);
        itemLayoutParamIv.height = 80;
        itemLayoutParamIv.width = 70;
        ImageView ivFileType = new ImageView(this.getActivity());
        ivFileType.setLayoutParams(itemLayoutParamIv);
        ivFileType.setMinimumHeight(ConstantMeasure);
        ivFileType.setMaxHeight(ConstantMeasure);
        ivFileType.setMinimumWidth(ConstantMeasure);
        ivFileType.setMaxWidth(ConstantMeasure);
        if (FileType.contains("pdf")) {
            ivFileType.setBackground(getActivity().getDrawable(R.drawable.ic_pdf));
        } else {
            ivFileType.setBackground(getActivity().getDrawable(R.drawable.ic_doc));
        }

        //  FILE NAME
        TextView tvFileName = new TextView(this.getActivity());
        tvFileName.setLayoutParams(itemLayoutParam);
        tvFileName.setMinHeight(ConstantHeight);
        tvFileName.setText(FileName);
        tvFileName.setTextSize(ConstantTextSize);
        tvFileName.setTextColor(Color.BLACK);
        tvFileName.setTypeface(tvFileName.getTypeface(), Typeface.BOLD);

        // EXPIRY TEXT
        LinearLayout.LayoutParams itemLayoutParamExpiry = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        itemLayoutParamExpiry.setMargins(0, 10, 10, 10);
        TextView tvExpiry = new TextView(this.getActivity());
        tvExpiry.setLayoutParams(itemLayoutParamExpiry);
        tvExpiry.setMinHeight(ConstantHeight);
        tvExpiry.setText(Expiry);
        tvExpiry.setTextSize(ConstantTextSize);
        tvExpiry.setTextColor(Color.LTGRAY);

        //  EXPIRE DATE
        LinearLayout.LayoutParams itemLayoutParamExpiryDate = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        itemLayoutParamExpiryDate.setMargins(0, 10, 10, 10);
        TextView tvExpireDate = new TextView(this.getActivity());
        tvExpireDate.setLayoutParams(itemLayoutParamExpiryDate);
        tvExpireDate.setMinHeight(ConstantHeight);
        tvExpireDate.setText(ExpireDate);
        tvExpireDate.setTextSize(ConstantTextSize);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            tvExpireDate.setTextColor(getActivity().getColor(R.color.orangeTextColor));
        }
        tvExpireDate.setTypeface(tvExpireDate.getTypeface(), Typeface.BOLD);

        //  GROUP: EXPIRY TEXT + EXPIRE DATE
        LinearLayout lnlyExpireDate = new LinearLayout(this.getActivity());
        lnlyExpireDate.setLayoutParams(itemLayoutParam);
        lnlyExpireDate.setOrientation(LinearLayout.HORIZONTAL);
        lnlyExpireDate.addView(tvExpiry);
        lnlyExpireDate.addView(tvExpireDate);

        //  GROUP FILE NAME + EXPIRY TEXT + EXPIRE DATE
        LinearLayout lnlyFileNameGroup = new LinearLayout(this.getActivity());
        lnlyFileNameGroup.setLayoutParams(itemLayoutParam);
        lnlyFileNameGroup.setOrientation(LinearLayout.VERTICAL);
        lnlyFileNameGroup.addView(tvFileName);
        lnlyFileNameGroup.addView(lnlyExpireDate);

        //  GROUP FILE TYPE + FILE NAME + EXPIRY TEXT + EXPIRE DATE
        RelativeLayout.LayoutParams FileGroupParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        FileGroupParams.setMargins(10, 10, 10, 10);
        FileGroupParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        LinearLayout lnlyFileTypeGroup = new LinearLayout(this.getActivity());
        lnlyFileTypeGroup.setLayoutParams(FileGroupParams);
        lnlyFileTypeGroup.setOrientation(LinearLayout.HORIZONTAL);
        lnlyFileTypeGroup.setGravity(Gravity.START|Gravity.CENTER);
        lnlyFileTypeGroup.addView(ivFileType);
        lnlyFileTypeGroup.addView(lnlyFileNameGroup);

        //  BUTTON SHOW
        RelativeLayout.LayoutParams ButtonShowParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        ButtonShowParams.setMargins(10, 10, 10, 10);
        ButtonShowParams.addRule(RelativeLayout.CENTER_VERTICAL);
        ButtonShowParams.addRule(RelativeLayout.ALIGN_PARENT_END);

        RelativeLayout ButtonShowLayout = new RelativeLayout(this.getActivity());
        ButtonShowLayout.setLayoutParams(ButtonShowParams);
        ButtonShowLayout.setGravity(Gravity.CENTER);

        ImageView ivShow = new ImageView(this.getActivity());
        ivShow.setLayoutParams(itemLayoutParam);
        ivShow.setBackground(getActivity().getDrawable(R.drawable.ic_next));
        ButtonShowLayout.addView(ivShow);

        //  ROW
        DocumentLayout.setGravity(Gravity.CENTER|Gravity.START);
        DocumentLayout.addView(lnlyFileTypeGroup);
        DocumentLayout.addView(ButtonShowLayout);
        DocumentLayout.setId(ContractorIndex);
        DocumentLayout.setOnClickListener(new DocumentSelectionListener());

        return DocumentLayout;
    }

    private void linkToDocumentFile(String FileURL) {
        Intent DocumentIntent = new Intent(Intent.ACTION_VIEW);
        DocumentIntent.setData(Uri.parse(FileURL));
        startActivity(DocumentIntent);
    }

    private void startup() {
        loadInfo();
    }
    
    public ContractorDocumentInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contractor_document_info, container, false);

        //  GET REFERENCES
        lnlyContractorDocumentContainer = rootView.findViewById(R.id.lnlyContractorDocumentContainer_ContractorDocumentInfoFragment);

        //  START UP
        startup();

        return rootView;
    }

    private class DocumentSelectionListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            linkToDocumentFile(DocumentList.get(v.getId()).getAttachment().getURL());
        }
    }

}
