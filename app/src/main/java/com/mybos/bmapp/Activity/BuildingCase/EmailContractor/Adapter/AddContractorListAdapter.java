package com.mybos.bmapp.Activity.BuildingCase.EmailContractor.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mybos.bmapp.Data.Model.Contractor.Contractor;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmLaAi on 15/04/2018.
 *
 * @author EmLaAi
 */
public class AddContractorListAdapter extends RecyclerView.Adapter<AddContractorListAdapter.ContractorItem> {
    private ContractorList contractorList;
    private List<Contractor> displayList;
    private OnContractorSelect listener;
    public AddContractorListAdapter(ContractorList contractorList){
        this.contractorList = contractorList;
        displayList = this.contractorList.getAll();
    }

    @NonNull
    @Override
    public ContractorItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.a_email_contractor_d_add_contractor_r_contractor_item,parent,false);
        return new ContractorItem(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ContractorItem holder, int position) {
        Contractor contractor = displayList.get(position);
        holder.checkBox.setChecked(contractorList.checkSelected(contractor.getId()));
        holder.tvTitle.setText(contractor.getCompany());
        holder.contractor = contractor;
        holder.setListener(mContractor -> {
            if (null != listener){
                listener.selectContractor(mContractor);
            }
        });
    }

    @Override
    public int getItemCount() {
        return displayList.size();
    }
    public void performFilter(String keyword){
        if (null != keyword && keyword.trim().length() > 0){
            displayList = new ArrayList<>();
            List<Contractor> all = contractorList.getAll();
            for (Contractor contractor:all){
                if (contractor.getCompany().toLowerCase().contains(keyword.toLowerCase())){
                    displayList.add(contractor);
                }
            }
        }else {
            displayList = new ArrayList<>();
            displayList = contractorList.getAll();
        }
        notifyDataSetChanged();
    }

    public void setListener(OnContractorSelect listener) {
        this.listener = listener;
    }

    class ContractorItem extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTitle;
        CheckBox checkBox;
        Contractor contractor;
        OnItemClickListener listener;
        public ContractorItem(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            checkBox = itemView.findViewById(R.id.check);
            itemView.setOnClickListener(this);
        }

        public void setListener(OnItemClickListener listener){
            this.listener = listener;
        }

        @Override
        public void onClick(View v) {
            listener.didSelectContractor(contractor);
        }
    }

    interface OnItemClickListener{
        void didSelectContractor(Contractor contractor);
    }
    public interface OnContractorSelect{
        void selectContractor(Contractor contractor);
    }
}
