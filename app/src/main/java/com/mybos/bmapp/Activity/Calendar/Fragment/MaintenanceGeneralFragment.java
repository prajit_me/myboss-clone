package com.mybos.bmapp.Activity.Calendar.Fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.BaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.NewBuildingCaseController;
import com.mybos.bmapp.Activity.Calendar.Activities.CalendarActivity;
import com.mybos.bmapp.Activity.Calendar.CalendarController;
import com.mybos.bmapp.Activity.Offlinemode.Offlinemode;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.Data.Model.Contractor.ContractorList;
import com.mybos.bmapp.R;
import com.mybos.bmapp.Services.SingletonObjectHolder;
import com.mybos.bmapp.Utils.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MaintenanceGeneralFragment extends BaseFragment {

    private View view;

    TextView tvEventStart,tvEventEnd,tvRecurrance;
    TextView tvEventDetailMaintenance;
    Spinner SpinnerSelectStatusMaintenance;
    Button btnSelectDateMaintenance,btnConvertMaintenance;

    CalendarController controller = new CalendarController();
    NewBuildingCaseController controllerBuildingCase = new NewBuildingCaseController();
    Building selectedBuilding = SingletonObjectHolder.getInstance().getBuildingList().getSelectedBuilding();

    private String dateToChange="";

    public MaintenanceGeneralFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maintenance_general, container, false);
        this.view = view;

        tvEventStart = view.findViewById(R.id.tvEventStart);
        tvEventEnd = view.findViewById(R.id.tvEventEnd);
        tvRecurrance = view.findViewById(R.id.tvRecurrance);
        SpinnerSelectStatusMaintenance = view.findViewById(R.id.SpinnerSelectStatusMaintenance);
        tvEventDetailMaintenance = view.findViewById(R.id.tvEventDetailMaintenance);
        btnSelectDateMaintenance = view.findViewById(R.id.btnSelectDateMaintenance);
        btnConvertMaintenance = view.findViewById(R.id.btnConvertMaintenance);

        ArrayList<String> itemsSpinner = new ArrayList<>();
        itemsSpinner.add("NA");
        itemsSpinner.add("Complete");
        itemsSpinner.add("Failed");
        ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,itemsSpinner);
        SpinnerSelectStatusMaintenance.setAdapter(arrayAdapter);
        setListener();
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        tvEventStart.setText(MaintenancePerDay.getMaintenancePerDaySelected().getDateStart());
        tvEventEnd.setText(MaintenancePerDay.getMaintenancePerDaySelected().getDateEnd());
        tvRecurrance.setText(MaintenancePerDay.getMaintenancePerDaySelected().getType_text());
        String detail = Html.fromHtml(MaintenancePerDay.getMaintenancePerDaySelected().getDetail()).toString();
        tvEventDetailMaintenance.setText(detail);
    }

    private void setListener(){
        MaintenancePerDay maintenancePerDay = MaintenancePerDay.getMaintenancePerDaySelected();

        SpinnerSelectStatusMaintenance.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String status = MaintenancePerDay.getMaintenancePerDaySelected().getStatusText();
                if (SpinnerSelectStatusMaintenance.getSelectedItemPosition()==0&&!maintenancePerDay.getStatusText().equals("")){
                    if (maintenancePerDay.getStatusText().equals("Failed")){
                        SpinnerSelectStatusMaintenance.setSelection(2);
                    }else {
                        SpinnerSelectStatusMaintenance.setSelection(1);
                    }
                }else if (SpinnerSelectStatusMaintenance.getSelectedItemPosition()==1&&!status.equals("Completed")){
                    String param = "{\"status\":1}";
                    changeStatusMaintenance(param);
                }else if (SpinnerSelectStatusMaintenance.getSelectedItemPosition()==2&&!status.equals("Failed")){
                    String param = "{\"status\":2}";
                    changeStatusMaintenance(param);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        btnSelectDateMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar c = Calendar.getInstance();
                DatePickerDialog dialog = new DatePickerDialog(getContext(), R.style.DatePickerTheme, (view1, year, month, dayOfMonth) -> {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, dayOfMonth);
                    Date updateDate = calendar.getTime();
                    btnSelectDateMaintenance.setText(DateUtils.getDateString(updateDate,"dd-MM-YYYY"));
                    dateToChange = DateUtils.getDateString(updateDate,"yyyy-MM-dd");
                }, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DATE));
                dialog.show();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if (!dateToChange.isEmpty()){
                            displayLoadingScreen();
                            String maintenanceId = MaintenancePerDay.getMaintenancePerDaySelected().getId();
                            String dateOgrin = MaintenancePerDay.getMaintenancePerDaySelected().getDate_ogrinal();
                            String param = "{\"toDay\":\""+dateToChange+"\"}";
                            controller.moveMaintenance(getContext(),selectedBuilding.getId(),maintenanceId,dateOgrin,param,
                                    move->{
                                        Intent intent = new Intent(getContext(), CalendarActivity.class);
                                        startActivity(intent);
                                        ((Activity)getContext()).finish();
                                        dismissLoadingScreen();
                                    },
                                    error -> {
                                        dismissLoadingScreen();
                                        if (Offlinemode.checkNetworkConnection(getContext())){
                                            Log.e("moveMaintenance",error.toString());
                                        }else{
                                            Offlinemode.showWhenOfflineMode(getContext());
                                        }
                                    });
                        }
                    }
                });
            }
        });

        btnConvertMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String maintenanceId = MaintenancePerDay.getMaintenancePerDaySelected().getId();
                CalendarMaintenanceWaitingConvert dialog = new CalendarMaintenanceWaitingConvert();
                dialog.show(getFragmentManager(), "Completed");
                controller.getConvert(getContext(),selectedBuilding.getId(),maintenanceId,
                        convert->{
                            controllerBuildingCase.getBuildingCaseDetail(getContext(),selectedBuilding.getId(),convert,
                                    buildingCase->{
                                        controllerBuildingCase.saveBuildingCase(getContext(),selectedBuilding.getId(),new ContractorList(),
                                                buildingCase,success->{
                                                    dialog.dismiss();
                                                },error -> {
                                                    if (Offlinemode.checkNetworkConnection(getContext())){
                                                        Log.e("btnConvertMaintenance",error.toString());
                                                    }else{
                                                        Offlinemode.showWhenOfflineMode(getContext());
                                                    }
                                                });
                                    },
                                    error -> {
                                        if (Offlinemode.checkNetworkConnection(getContext())){
                                            Log.e("btnConvertMaintenance",error.toString());
                                        }else{
                                            Offlinemode.showWhenOfflineMode(getContext());
                                        }
                                    });
                        },
                        error -> {
                            if (Offlinemode.checkNetworkConnection(getContext())){
                                Log.e("btnConvertMaintenance",error.toString());
                            }else{
                                Offlinemode.showWhenOfflineMode(getContext());
                            }
                        });
            }
        });
    }

    private void changeStatusMaintenance(String param){
        displayLoadingScreen();
        String maintenanceId = MaintenancePerDay.getMaintenancePerDaySelected().getId();
        String dateOgrin = MaintenancePerDay.getMaintenancePerDaySelected().getDate_ogrinal();
        controller.changeStatusMaintenance(getContext(),selectedBuilding.getId(),maintenanceId,dateOgrin,param,
                cs->{
                    if (param.contains("1")){
                        MaintenancePerDay.getMaintenancePerDaySelected().setStatusText("Completed");
                    }else if ((param.contains("2"))){
                        MaintenancePerDay.getMaintenancePerDaySelected().setStatusText("Failed");
                    }
                    dismissLoadingScreen();
                },
                error -> {
                    dismissLoadingScreen();
                    if (Offlinemode.checkNetworkConnection(getContext())){
                        Log.e("changeStatusMaintenance",error.toString());
                    }else{
                        Offlinemode.showWhenOfflineMode(getContext());
                    }
                });
    }
}


