package com.mybos.bmapp.Activity.BuildingCase.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Activity.BuildingCase.EditCase.Fragment.EditCaseDetailFragment;
import com.mybos.bmapp.Activity.BuildingCase.EditCase.Fragment.MessageCaseFragment;
import com.mybos.bmapp.Activity.BuildingCase.NewCase.Fragment.AttachmentFragment;
import com.mybos.bmapp.Data.Model.Cases.Message;
import com.mybos.bmapp.R;

import java.util.ArrayList;

/**
 * Created by EmLaAi on 23/04/2018.
 *
 * @author EmLaAi
 */
public class EditBuildingCasePagerAdapter extends FragmentPagerAdapter {
    private EditCaseDetailFragment caseDetailFragment;
    private AttachmentFragment attachmentFragment;
    private MessageCaseFragment messageCaseFragment;
    private Context context;
    ArrayList<Integer> sections = new ArrayList<>();

    public EditBuildingCasePagerAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context = context;
        sections.add(0);
        sections.add(1);
        sections.add(2);
        caseDetailFragment = new EditCaseDetailFragment();
        attachmentFragment = new AttachmentFragment();
        messageCaseFragment = new MessageCaseFragment();
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return caseDetailFragment;
        }else if (position == 1){
            return  attachmentFragment;
        }else if (position == 2) {
            return messageCaseFragment;
        }
        return null;

    }

    public EditCaseDetailFragment getCaseDetailFragment() {
        return caseDetailFragment;
    }

    public AttachmentFragment getAttachmentFragment() {
        return attachmentFragment;
    }

    public MessageCaseFragment getMessageCaseFragment() {
        return messageCaseFragment;
    }

    @Override
    public int getCount() {
        return Message.getSelectedReId().equals("")?2:3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        int section = sections.get(position);
        if (section == 0){
            return context.getString(R.string.edit_case_detail_fragment_title);
        }
        if (section == 1){
            return context.getString(R.string.case_attachment_fragment_title);
        }
        if (section == 2){
            return context.getString(R.string.message_fragment_title);
        }

        return super.getPageTitle(position);
    }
}
