package com.mybos.bmapp.Activity.Calendar.Activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import android.view.View;
import android.widget.TextView;

import com.mybos.bmapp.Activity.Base.a_base_toolbar;
import com.mybos.bmapp.Data.Model.Calendar.Booking;
import com.mybos.bmapp.Data.Model.Calendar.DaysEvent;
import com.mybos.bmapp.Data.Model.Calendar.MaintenancePerDay;
import com.mybos.bmapp.Data.Model.Calendar.SelectedDay;
import com.mybos.bmapp.R;

public class CalendarDayEventActivity extends a_base_toolbar {

    TextView tvDayOfWeek,tvNumberReminder,tvPendingNumB,tvCompleteNumB,tvFailedNumB,
            tvPendingNum,tvCompleteNum,tvFailedNum;
    CardView cardViewReminder,cardViewBooking,cardViewMaintenance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar_day_event);
        updateToolbarTitle(SelectedDay.getFullDate());
        findViewById(R.id.btnBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarDayEventActivity.this, CalendarActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tvDayOfWeek = findViewById(R.id.tvDayOfWeek);
        tvNumberReminder = findViewById(R.id.tvNumberReminder);
        tvPendingNumB = findViewById(R.id.tvPendingNumB);
        tvCompleteNumB = findViewById(R.id.tvCompleteNumB);
        tvFailedNumB = findViewById(R.id.tvFailedNumB);
        cardViewReminder = findViewById(R.id.cardViewReminder);
        cardViewBooking = findViewById(R.id.cardViewBooking);
        cardViewMaintenance = findViewById(R.id.cardViewMaintenance);
        tvPendingNum = findViewById(R.id.tvPendingNum);
        tvCompleteNum = findViewById(R.id.tvCompleteNum);
        tvFailedNum = findViewById(R.id.tvFailedNum);

        setListener();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(CalendarDayEventActivity.this, CalendarActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        String date = SelectedDay.getDate();
        int BP = 0;
        int BA = 0;
        int BR = 0;

        for (Booking booking: DaysEvent.getDayEFromDateOfDayEs(date).getBooking()
             ) {
            if (booking.getStatusText().equals("Pending")){
                BP++;
            }else if(booking.getStatusText().equals("Cancelled")||booking.getStatusText().equals("Reject")){
                BR++;
            }else if(booking.getStatusText().equals("Confirm")){
                BA++;
            }
        }
        int MP = 0;
        int MC = 0;
        int MF = 0;


        for (MaintenancePerDay maintenancePerDay: MaintenancePerDay.getListByDate(SelectedDay.getDate())
        ) {
            if (maintenancePerDay.getStatusText().equals("Completed")){
                MC++;
            }else if(maintenancePerDay.getStatusText().equals("Failed")){
                MF++;
            }else {
                MP++;
            }
        }

        tvPendingNumB.setText(String.valueOf(BP));
        tvCompleteNumB.setText(String.valueOf(BA));
        tvFailedNumB.setText(String.valueOf(BR));
        tvPendingNum.setText(String.valueOf(MP));
        tvCompleteNum.setText(String.valueOf(MC));
        tvFailedNum.setText(String.valueOf(MF));


        tvDayOfWeek.setText(SelectedDay.getDateOfweek());
        tvNumberReminder.setText(String.valueOf(DaysEvent.getDayEFromDateOfDayEs(date).getReminder().size()));
    }

    public void setListener(){

        cardViewReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarDayEventActivity.this, CalendarRemindersActivity.class);
                startActivity(intent);
                finish();
            }
        });

        cardViewBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarDayEventActivity.this, CalendarBookingsActivity.class);
                startActivity(intent);
                finish();
            }
        });

        cardViewMaintenance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CalendarDayEventActivity.this, CalendarMaintenancesActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
