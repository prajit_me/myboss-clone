package com.mybos.bmapp.Activity.Inspection.Adapter;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.mybos.bmapp.Activity.Inspection.InspectionList.Fragment.InspectionListFragment;
import com.mybos.bmapp.Data.Functional.SearchTableQueryBuilder;
import com.mybos.bmapp.R;

import java.util.Arrays;
import java.util.List;

/**
 * Created by EmLaAi on 17/04/2018.
 *
 * @author EmLaAi
 */
public class InspectionListPagerAdapter extends FragmentPagerAdapter {
    private @SearchTableQueryBuilder.SectionHint.InpecstionSectionHint
    List<Integer> sections = Arrays.asList(SearchTableQueryBuilder.SectionHint.InspectionCommon,
            SearchTableQueryBuilder.SectionHint.InspectionPrivate,SearchTableQueryBuilder.SectionHint.InspectionDraft);
    private Context context;
    public InspectionListPagerAdapter(Context context,FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        return InspectionListFragment.createInstance(sections.get(position));
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        int section = sections.get(position);
        if (section == SearchTableQueryBuilder.SectionHint.InspectionCommon){
            return context.getString(R.string.inspection_list_section_common);
        }
        if (section == SearchTableQueryBuilder.SectionHint.InspectionPrivate){
            return context.getString(R.string.inspection_list_section_private);
        }
        if (section == SearchTableQueryBuilder.SectionHint.InspectionDraft){
            return context.getString(R.string.inspection_list_section_draft);
        }
        return super.getPageTitle(position);
    }
}
