package com.mybos.bmapp.Activity.BuildingCase.EditCase.Component;

import android.content.Context;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 24/04/2018.
 *
 * @author EmLaAi
 */
public class CaseDescriptionComponent extends ConstraintLayout {
    TextView txtSubject,txtDescription;
    public CaseDescriptionComponent(Context context) {
        super(context);
        additionalInit();
    }

    public CaseDescriptionComponent(Context context, AttributeSet attrs) {
        super(context, attrs);
        additionalInit();
    }

    public CaseDescriptionComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        additionalInit();
    }

    public void additionalInit(){
        inflate(getContext(), R.layout.a_edit_case_f_edit_case_detail_c_case_subject,this);
        txtSubject = findViewById(R.id.txtSubject);
        txtDescription = findViewById(R.id.txtDescription);
    }

    public void setSubject(String value){
        txtSubject.setText(value);
    }
    public void setDescription(String value){
        txtDescription.setText(value);
    }
}
