package com.mybos.bmapp.Services;

import android.os.Debug;
import android.util.Log;

import com.mybos.bmapp.BuildConfig;

import org.jetbrains.annotations.Contract;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 03/01/2018.
 *
 * @author EmLaAi
 */

public class Logger {

    // singleton pattern
    private static final Logger ourInstance = new Logger();
    @Contract(pure = true)
    public static Logger shared() {
        return ourInstance;
    }
    // LOG TAG
    private static final String LOG_DEBUG_TAG = "DEBUGTAG";
    private static final String LOG_ERROR_TAG = "ERRORTAG";
    private Logger() {}


    /**
     * print an info log on debug evironment
     * @param tag log tag
     * @param message log message
     */
    public void InfoLog(String tag,String message){
        if(BuildConfig.DEBUG){
//            Log.i(tag,message);
        }
    }

    /**
     * print an error log on debug environment
     * @param tag log tag
     * @param message log message
     */
    public void  ErrorLog(String tag,String message){
        if(BuildConfig.DEBUG){
//            Log.e(tag,message);
        }
    }

    /**
     * info log with string input
     * @param message log message
     */
    public void i(String... message){
        if(BuildConfig.DEBUG){
            StringBuilder sb = new StringBuilder();
            for (String mess:message) {
                sb.append(mess);
                sb.append("\t");
            }
            InfoLog(LOG_DEBUG_TAG,sb.toString());
        }
    }

    /**
     * info log with int input
     * @param Numbers log message in int format
     */
    public void i(int... Numbers){
        if(BuildConfig.DEBUG){
            StringBuilder sb = new StringBuilder();
            for (int mess:Numbers) {
                sb.append(mess);
                sb.append("\t");
            }
            InfoLog(LOG_DEBUG_TAG,sb.toString());
        }
    }

    /**
     * info log with boolean input
     * @param Bools log message in bool format
     */
    public void i(boolean... Bools){
        if(BuildConfig.DEBUG){
            StringBuilder sb = new StringBuilder();
            for (boolean mess:Bools) {
                sb.append(mess);
                sb.append("\t");
            }
            InfoLog(LOG_DEBUG_TAG,sb.toString());
        }
    }

    /**
     * info log with object input
     * @param object log message object type
     */
    public void o(Object object){
        if(BuildConfig.DEBUG){
//            String str = "";
            if (null == object){
                InfoLog(LOG_DEBUG_TAG,"null");
                return;
            }
            if (object instanceof String){
                InfoLog(LOG_DEBUG_TAG,(String) object);
            }else {
                StringBuilder sb = new StringBuilder();
                InfoLog(LOG_DEBUG_TAG, object.getClass().toString());
                for (Field key : object.getClass().getFields()) {
                    try {
                        sb.append(key.getName());
                        sb.append(" : ");
                        sb.append(null != key.get(object) ? key.get(object) : "null");
                        sb.append("\n");
//                    str += key.getName() + " : " + (null != key.get(object)?key.get(object).toString():"null")  + "\n";
                    } catch (IllegalAccessException e) {
//                        e.printStackTrace();
                    }
                }
                InfoLog(LOG_DEBUG_TAG, object.toString() + sb.toString());
            }
        }
    }

    /**
     * info log list of object
     * @param list log message object list type
     */
    public void o(List<Object> list){
        if (BuildConfig.DEBUG){
            if (null == list){
                i("null");
                return;
            }else {
                if (list.size() == 0){
                    i("list have 0 element");
                }
            }
            for (Object item:list){
                o(item);
            }
        }
    }

    /**
     * info log with exception type
     * @param e log message exception type
     */
    public void i(Exception e){
        if (BuildConfig.DEBUG){
            String mess = "Exception Type :" + e.getClass().toString() + "\n";
            mess += "Cause :" + e.getCause() + "\n";
            mess += "Message :" + e.getMessage() + " " + e.getLocalizedMessage() +  "\n";
            StackTraceElement[] elements = e.getStackTrace();
            for (StackTraceElement element : elements) {
                mess += " " + element.toString() + "\n";
            }
            i(mess);
        }
    }


    public void i(String appendString, Exception e){
        if (BuildConfig.DEBUG) {
            String returnValue = appendString;
            returnValue += "Exception Type :" + e.getClass().toString() + "\n";
            returnValue += "Cause :" + e.getCause() + "\n";
            returnValue += "Message :" + e.getMessage() + " " + e.getLocalizedMessage() + "\n";

            StackTraceElement[] elements = e.getStackTrace();
            for (StackTraceElement element : elements) {
                returnValue += " " + element.toString() + "\n";
            }
            i(returnValue);
        }
    }
    public void i(Map<String,String> map){
        if (BuildConfig.DEBUG){
            String printMess = "";
            for (Map.Entry<String,String> set:map.entrySet()){
                printMess += set.getKey() + " " + set.getValue() + "\n";
            }
            i(printMess);
        }
    }

    /**
     * print exception on debug environmetn
     * @param e exception need to print
     */
    public void printExceptionStack(Exception e){
        if(BuildConfig.DEBUG){
            e.printStackTrace();
        }
    }
}
