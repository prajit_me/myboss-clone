package com.mybos.bmapp.Services;

import com.mybos.bmapp.Data.Mapper.Building.BuildingMapper;
import com.mybos.bmapp.Data.Model.Building.Building;
import com.mybos.bmapp.Data.Model.Building.BuildingList;

import io.realm.Realm;

/**
 * Created by EmLaAi on 05/03/2018.
 *
 * @author EmLaAi
 */

public class SingletonObjectHolder {
    private static BuildingList list;
    private static final SingletonObjectHolder ourInstance = new SingletonObjectHolder();

    public static synchronized SingletonObjectHolder getInstance() {
        return ourInstance;
    }

    private SingletonObjectHolder() {}

    public void saveBuildingList(BuildingList list){
//        if (null == SingletonObjectHolder.list) {
//            SingletonObjectHolder.list = list;
//        }else{
//            SingletonObjectHolder.list.replaceList(list.getBuildingList());
//        }
        BuildingList buildingList = BuildingList.get();
        if (null != buildingList){
            buildingList.replaceList(list.getBuildingList());
            BuildingList.save(buildingList);
        }else {
            list.setInitIndex(0);
            BuildingList.save(list);
        }
    }

    public BuildingList getBuildingList(){
        list = BuildingMapper.getUserBuildingList();
        return list;
    }

    public String printInfo(){
        return Realm.DEFAULT_REALM_NAME + (Realm.getDefaultConfiguration() != null ? Realm.getDefaultConfiguration().toString() : "null");

    }
}
