package com.mybos.bmapp.Services.VolleyRequest;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.mybos.bmapp.Error.TimeOutError;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.PreferenceManager;
import com.mybos.bmapp.Services.VolleyRequest.RequestType.FileDownloadRequest;
import com.mybos.bmapp.Services.VolleyRequest.RequestType.MultiPartRequest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by EmLaAi on 04/01/2018.
 *
 * @author EmLaAi
 */

public class VolleyRequest {
    /// singleton pattern and hold init variable
    private static VolleyRequest ourInstance;
    private static Context context;
    private RequestQueue requestQueue;
    public static synchronized VolleyRequest shared(Context context) {
        if (null == ourInstance) {
            return new VolleyRequest(context);
        }else return ourInstance;
    }
    private VolleyRequest(Context context) {
        this.context = context;
        this.requestQueue = getRequestQueue();
    }

    /**
     * get Volley request queue
     * @return Volley Request queue
     */
    private RequestQueue getRequestQueue(){
        if (null == requestQueue){
            try {
                CustomHurlStack customHurlStack = new CustomHurlStack();
                requestQueue = Volley.newRequestQueue(context.getApplicationContext(),customHurlStack);
            }
            catch (OutOfMemoryError e){
                System.gc();
                getRequestQueue();
            }
            catch (StackOverflowError StaOveErr){
                System.gc();
            }
        }
        return requestQueue;
    }

    /**
     * add request to Volley Request Queue
     * @param req the request need to add to queue
     * @param <T> generic type for Request : String or JSON
     */
    private <T> void addToRequest(Request<T> req){
        getRequestQueue().add(req);
    }

    /**
     * make a get request
     * @param urlStr a string url need to make request
     * @param header header supply to request
     * @param onSuccess callback lambda on request success
     * @param onFailure callback lambda on request failure
     */
    public void get(String urlStr, final Map<String,String> header , final onSuccessCallback onSuccess,final onErrorCallback onFailure){

        Logger.shared().i("****GET METHOD request url : " + urlStr);
        final String finalUrlStr = urlStr;
        StringRequest request = new StringRequest(Request.Method.GET, urlStr, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.shared().i("****GET METHOD request success response"+ finalUrlStr +" : " + response);
                onSuccess.callback(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.shared().i("****GET METHOD request error response : ",error);
                if (error instanceof TimeoutError){
                    onFailure.callback(new TimeOutError());
                    return;
                }
                onFailure.callback(error);



            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                header.put("Mba",PreferenceManager.getAccessToken(context));
                return header;
            }

        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addToRequest(request);
    }

    /**
     * make a post request
     * @param url url string to post
     * @param header header supply to request
     * @param data post data
     * @param onSuccess callback lambda on request success
     * @param onFailure callback lambda on request failure
     */
    public void post(String url, final Map<String,String> header , final String data  ,final onSuccessCallback onSuccess,final onErrorCallback onFailure){
        Logger.shared().i("****POST METHOD request url : " + url);
        Logger.shared().i("****POST METHOD request body : " + data);
        if (null != data) {
            Logger.shared().i("****POST METHOD request data : " + data.toString());
        }else {
            Logger.shared().i("****POST METHOD request data : null");
        }
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.shared().i("****POST METHOD request success response : " + response);
                onSuccess.callback(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.shared().i("****POST METHOD request error response : " + error.getMessage());
                if (error instanceof TimeoutError){
                    onFailure.callback(new TimeOutError());
                    return;
                }
                onFailure.callback(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                header.put("Mba",PreferenceManager.getAccessToken(context));
                return header;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", data, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                Logger.shared().i(response.statusCode);
                Logger.shared().o(response.data);
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addToRequest(request);
    }

    public void postForm(String url, final Map<String,String> header , final VolleyMultipartBuilder builder  , final onSuccessCallback onSuccess, final onErrorCallback onFailure){
        Logger.shared().i("****POST METHOD request url : " + url);
        try {
            Logger.shared().i("****POST METHOD request body : " + builder.print());
        } catch (IOException e) {

        }
        MultiPartRequest request = new MultiPartRequest(context,Request.Method.POST,url,header,builder,onSuccess::callback,error -> {
            if (error instanceof TimeoutError){
                onFailure.callback(new TimeOutError());
                return;
            }
            onFailure.callback(error);
        });
        request.setRetryPolicy(new DefaultRetryPolicy(300000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addToRequest(request);
    }

    public void getFile(String url,final Map<String,String> header , final onByteSuccessCallback onSuccess,final onErrorCallback onFailure){
        Logger.shared().i("****GET FILE METHOD request url : " + url);
        FileDownloadRequest request = new FileDownloadRequest(Request.Method.GET,url,error -> {
            if (error instanceof TimeoutError){
                onFailure.callback(new TimeOutError());
                return;
            }
            onFailure.callback(error);
        },onSuccess::callback){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                header.put("Mba",PreferenceManager.getAccessToken(context));
                return header;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(300000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addToRequest(request);
    }

    /**
     * make a put request
     * @param urlStr url to make put request
     * @param header header supply to request
     * @param data put data
     * @param onSuccess callback lambda on request success
     * @param onFailure callback lambda on request failure
     */
    public void put(String urlStr, final Map<String,String> header  , String data , final onSuccessCallback onSuccess,final  onErrorCallback onFailure){

        Logger.shared().i("****PUT METHOD request url : " + urlStr);
        Logger.shared().i("****PUT METHOD request body : " + data);
        final String finalUrlStr = urlStr;

        StringRequest request = new StringRequest(Request.Method.PUT, urlStr, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.shared().i("****PUT METHOD request success response"+ finalUrlStr +" : " + response);
                onSuccess.callback(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.shared().i("****PUT METHOD request error response : ",error);
                if (error instanceof TimeoutError){
                    onFailure.callback(new TimeOutError());
                    return;
                }
                onFailure.callback(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                header.put("Mba",PreferenceManager.getAccessToken(context));
                return header;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", data, "utf-8");
                    return null;
                }
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addToRequest(request);
    }

    public void delete(String url, final Map<String,String> header , final String data  ,final onSuccessCallback onSuccess,final onErrorCallback onFailure){
        Logger.shared().i("****DELETE METHOD request url : " + url);
        Logger.shared().i("****DELETE METHOD request body : " + data);
        if (null != data) {
            Logger.shared().i("****DELETE METHOD request data : " + data.toString());
        }else {
            Logger.shared().i("****DELETE METHOD request data : null");
        }
        StringRequest request = new StringRequest(Request.Method.DELETE, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.shared().i("****DELETE METHOD request success response : " + response);
                onSuccess.callback(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.shared().i("****DELETE METHOD request error response : " + error.getMessage());
                if (error instanceof TimeoutError){
                    onFailure.callback(new TimeOutError());
                    return;
                }
                onFailure.callback(error);
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                header.put("Mba",PreferenceManager.getAccessToken(context));
                return header;
            }
            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return data == null ? null : data.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", data, "utf-8");
                    return null;
                }
            }

            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                Logger.shared().i(response.statusCode);
                Logger.shared().o(response.data);
                return super.parseNetworkResponse(response);
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(30000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        addToRequest(request);
    }

    public interface onSuccessCallback{
        void callback(String value);
    }

    public interface onByteSuccessCallback{
        void callback(byte[] bytes);
    }

    public interface onErrorCallback{
        void callback(Exception error);
    }
}
