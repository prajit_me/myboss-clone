package com.mybos.bmapp.Services;

import android.content.Context;
import android.content.SharedPreferences;

import com.mybos.bmapp.Data.Model.Auth.Features;
import com.mybos.bmapp.Data.Model.Auth.Permission;

/**
 * Created by EmLaAi on 03/01/2018.
 *
 * @author EmLaAi
 */

public class PreferenceManager {
    /**
     * string key use to identified shared preferences file
     */
    private static final String USER_PREF_FILE = "PreferenceManager.userPrefFile";
    private static final String PERMISSION_PREF_FILE = "PreferenceManager.PERMISSION_PREF_FILE";
    private static final String FEATURES_PREF_FILE = "PreferenceManager.FEATURES_PREF_FILE";

    /**
     * identified string for key access token
     */
    private static final String ACCESS_TOKEN_KEY = "PreferenceManager.accessTokenKey";
    private static final String BASE_PERMISSION_KEY = "PERMISSION.";
    private static final String BASE_FEATURE_KEY = "FEATURE.";
    private static String[] arrString = {USER_PREF_FILE,PERMISSION_PREF_FILE};

    /**
     * check if current app session , this user already login
     * @param context to access shared preferences
     * @return true on already login , false otherwise
     */
    public static boolean isLogin(Context context){
        return  null != getAccessToken(context);
    }


    /**
     * save token into shared preference for future usage
     * @param context to access shared preferences
     * @param token need to save
     */
    public static void setAccessToken(Context context, String token){
        SharedPreferences preferences = context.getSharedPreferences(USER_PREF_FILE,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(ACCESS_TOKEN_KEY,token);
        editor.commit();
    }
    public static void savePermission(Context context, @Permission.Types.PermissionHint String key,String value){
        SharedPreferences preferences =context.getSharedPreferences(PERMISSION_PREF_FILE,0);
        SharedPreferences.Editor editor = preferences.edit();
        String permissionKey = BASE_PERMISSION_KEY + key;
        editor.putString(permissionKey,value);
        editor.commit();
    }
    public static void saveFeature(Context context, @Features.TYPES.FeatureHint Integer featureId){
        SharedPreferences preferences = context.getSharedPreferences(FEATURES_PREF_FILE,0);
        SharedPreferences.Editor editor = preferences.edit();
        String featureKey = BASE_FEATURE_KEY + featureId.toString();
        editor.putBoolean(featureKey,true);
        editor.commit();
    }

    /**
     * get saved token in shared preference
     * @param context to access shared preferences
     * @return string token
     */
    public static String getAccessToken(Context context){
        SharedPreferences preferences = context.getSharedPreferences(USER_PREF_FILE,0);
        return preferences.getString(ACCESS_TOKEN_KEY,null);
    }
    public static String getPermissionValue(Context context, @Permission.Types.PermissionHint String key){
        SharedPreferences preferences = context.getSharedPreferences(PERMISSION_PREF_FILE,0);
        String permissionKey = BASE_PERMISSION_KEY + key;
        return preferences.getString(permissionKey,null);
    }
    public static Boolean getFeatureValue(Context context, @Features.TYPES.FeatureHint Integer key){
        SharedPreferences preferences = context.getSharedPreferences(FEATURES_PREF_FILE,0);
        String featureKey = BASE_FEATURE_KEY + key.toString();
        return preferences.getBoolean(featureKey,false);
    }


    /**
     * clear all save item in shared preference
     * @param context to access shared preferences
     */
    public static void clearPreference(Context context){
        for (String s : arrString) {
            SharedPreferences preferences = context.getSharedPreferences(s,0);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear();
            editor.commit();
        }
    }

    /**
     * clear all save permission value
     * @param context to access shared preference
     */
    public static void clearPermission(Context context){
        SharedPreferences preferences = context.getSharedPreferences(PERMISSION_PREF_FILE,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public static void clearFeature(Context context){
        SharedPreferences preferences = context.getSharedPreferences(FEATURES_PREF_FILE,0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}
