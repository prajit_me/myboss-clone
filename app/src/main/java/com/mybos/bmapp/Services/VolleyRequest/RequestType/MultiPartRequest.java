package com.mybos.bmapp.Services.VolleyRequest.RequestType;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.mybos.bmapp.Services.Logger;
import com.mybos.bmapp.Services.PreferenceManager;
import com.mybos.bmapp.Services.VolleyRequest.MultipartBodyBuildError;
import com.mybos.bmapp.Services.VolleyRequest.VolleyMultipartBuilder;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Created by EmLaAi on 09/04/2018.
 *
 * @author EmLaAi
 */
public class MultiPartRequest extends StringRequest {

    private Response.Listener<String> responseListener;
    private Response.ErrorListener errorListener;
    private Map<String,String> header;
    private Context context;
    private String boundary;
    private VolleyMultipartBuilder builder;
    public MultiPartRequest(Context context, int method, String url , Map<String,String> header, VolleyMultipartBuilder builder, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(method, url,listener, errorListener);
        this.header = header;
        this.builder = builder;
        this.responseListener = listener;
        this.errorListener = errorListener;
        this.context = context;
        boundary = UUID.randomUUID().toString();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        header.put("Mba", PreferenceManager.getAccessToken(context));
        return header;
    }

    @Override
    public String getBodyContentType() {
        return "multipart/form-data;boundary=" + builder.getBoundary();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return builder.build();
        }catch (IOException e){
            errorListener.onErrorResponse(new MultipartBodyBuildError());
        }
        return null;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Logger.shared().i(response.statusCode);
        return super.parseNetworkResponse(response);
    }

    @Override
    protected void deliverResponse(String response) {
        Logger.shared().i(response);
        responseListener.onResponse(response);
        getParamsEncoding();
    }

}
