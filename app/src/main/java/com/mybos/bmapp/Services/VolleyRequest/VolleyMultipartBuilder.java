package com.mybos.bmapp.Services.VolleyRequest;

import android.net.sip.SipSession;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by EmLaAi on 09/04/2018.
 *
 * @author EmLaAi
 */
public class VolleyMultipartBuilder {
    private static final String DEFAULT_PARAMS_ENCODING = "UTF-8";
    private static final String lineEnd = "\r\n";
    private static final String hyphens = "--";
    private Map<String,String> stringParams = new HashMap<>();
    private Map<String,Object> params = new HashMap<>();
    private Map<String,List<MultipartFile>> filesParams = new HashMap<>();
    private String boundary;
    public VolleyMultipartBuilder(){
        this.boundary = "MYBOS-" + UUID.randomUUID().toString();
    }

    public VolleyMultipartBuilder addString(String key,String params){
        this.stringParams.put(key,params);
        return this;
    }
    public VolleyMultipartBuilder addObject(String key,Object object){
        this.params.put(key,object);
        return this;
    }

    public VolleyMultipartBuilder addFile(String key,MultipartFile file){
        if (this.filesParams.get(key) != null){
            this.filesParams.get(key).add(file);
        }else{
            List<MultipartFile> files = new ArrayList<>();
            files.add(file);
            this.filesParams.put(key, files);
        }
        return this;
    }

    public VolleyMultipartBuilder addFile(String key,String name,byte[] value,String mime){
        MultipartFile file = new MultipartFile(name,value,mime);
        return addFile(key,file);
    }

    public byte[] build() throws IOException{
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

        if (stringParams.size() > 0){
            for (Map.Entry<String,String> entry:stringParams.entrySet()){
                parseString(dataOutputStream,entry.getKey(),entry.getValue());
            }
        }
        if (params.size() > 0){
            for (Map.Entry<String,Object> entry:params.entrySet()){
                parseObject(dataOutputStream,entry.getKey(),entry.getValue());
            }
        }
        if (filesParams.size() > 0){
            for (Map.Entry<String,List<MultipartFile>> entry:filesParams.entrySet()){
                parseFiles(dataOutputStream,entry.getKey(),entry.getValue());
            }
        }
        dataOutputStream.writeBytes(hyphens + boundary + hyphens + lineEnd);

        return outputStream.toByteArray();
    }

    public String print() throws IOException{
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);

        if (stringParams.size() > 0){
            for (Map.Entry<String,String> entry:stringParams.entrySet()){
                parseString(dataOutputStream,entry.getKey(),entry.getValue());
            }
        }
        if (params.size() > 0 ){
            for (Map.Entry<String,Object> entry:params.entrySet()){
                parseObject(dataOutputStream,entry.getKey(),entry.getValue());
            }
        }
        if (filesParams.size() > 0){
            for (Map.Entry<String,List<MultipartFile>> entry:filesParams.entrySet()){
                parseFiles(dataOutputStream,entry.getKey(),entry.getValue());
            }
        }
        dataOutputStream.writeBytes(hyphens + boundary + hyphens + lineEnd);
        return  outputStream.toString("UTF-8");
    }

    private void parseString(DataOutputStream stream,String key,String value) throws IOException{
        if (null==value){
            value = "";
        }
        value = URLDecoder.decode(value,"ISO-8859-1");
        stream.writeBytes(hyphens + boundary + lineEnd);
//        Log.e("check","Content-Disposition: form-data; name=\""+key+"\"" +lineEnd + lineEnd+value);
        stream.writeBytes("Content-Disposition: form-data; name=\""+key+"\"" +lineEnd + lineEnd);
        stream.writeBytes(value + lineEnd);
    }

    private void parseObject(DataOutputStream stream,String key,Object value) throws IOException{
        stream.writeBytes(hyphens + boundary + lineEnd);
        stream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" +lineEnd + lineEnd);
        stream.writeBytes(value.toString() + lineEnd);
    }

    private void parseFiles(DataOutputStream stream,String key,List<MultipartFile> files) throws IOException{
        if (files.size() == 1){
            MultipartFile file = files.get(0);
            stream.writeBytes(hyphens + boundary + lineEnd);
            stream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"; filename=\"" + file.name + "\"" + lineEnd);
            if (file.mime != null && !file.mime.trim().isEmpty()) {
                stream.writeBytes("Content-Type: " + file.mime + lineEnd + lineEnd);
            }

            ByteArrayInputStream fileInputStream = new ByteArrayInputStream(file.bytes);
            int bytesLeft = fileInputStream.available();
            int maxBufferSize = 1024 * 1024;
            int bufferSize = Math.min(bytesLeft,maxBufferSize);
            byte[] buffer = new byte[bufferSize];

            int bytesRead = fileInputStream.read(buffer,0,bufferSize);
            while (bytesRead > 0){
                stream.write(buffer,0,bufferSize);
                bytesLeft = fileInputStream.available();
                bufferSize = Math.min(bytesLeft , maxBufferSize);
                bytesRead = fileInputStream.read(buffer,0,bufferSize);
            }
            stream.writeBytes(lineEnd);
        }else {
            for (int i = 0 ; i < files.size();i++){
                MultipartFile file = files.get(i);
                stream.writeBytes(hyphens + boundary + lineEnd);
                stream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"; filename=\"" + file.name + "[]\"" + lineEnd);
                if (file.mime != null && !file.mime.trim().isEmpty()) {
                    stream.writeBytes("Content-Type: " + file.mime + lineEnd + lineEnd);
                }

                ByteArrayInputStream fileInputStream = new ByteArrayInputStream(file.bytes);
                int bytesLeft = fileInputStream.available();
                int maxBufferSize = 1024 * 1024;
                int bufferSize = Math.min(bytesLeft,maxBufferSize);
                byte[] buffer = new byte[bufferSize];

                int bytesRead = fileInputStream.read(buffer,0,bufferSize);
                while (bytesRead > 0){
                    stream.write(buffer,0,bufferSize);
                    bytesLeft = fileInputStream.available();
                    bufferSize = Math.min(bytesLeft , maxBufferSize);
                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
                }
                stream.writeBytes(lineEnd);
            }
        }
    }

    public String getBoundary() {
        return boundary;
    }

    public static class MultipartFile{
        private String name;
        private byte[] bytes;
        private String mime;

        public MultipartFile(String name,byte[] value,String mime){
            this.name = name;
            this.bytes = value;
            this.mime = mime;
        }
    }
}
