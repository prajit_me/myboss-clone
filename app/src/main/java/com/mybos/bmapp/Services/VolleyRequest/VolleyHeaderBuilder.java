package com.mybos.bmapp.Services.VolleyRequest;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by EmLaAi on 05/01/2018.
 *
 * @author EmLaAi
 */

public class VolleyHeaderBuilder {
    private Map<String,String> header = new HashMap<>();

    public VolleyHeaderBuilder addHeader(String key,String value){
        header.put(key,value);
        return this;
    }

    public Map<String,String> build(){
        return header;
    }

    public Map<String,String> buildDefaultJson(){
        Map<String,String> header = new HashMap<>();
        header.put("Content-Type","application/json");
        header.put("Accept","application/json");
        return header;
    }
}
