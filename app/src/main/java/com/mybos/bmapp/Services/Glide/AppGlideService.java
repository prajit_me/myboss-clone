package com.mybos.bmapp.Services.Glide;

/**
 * Created by EmLaAi on 24/03/2018.
 *
 * @author EmLaAi
 */
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

@GlideModule
public final class AppGlideService extends AppGlideModule {
}
