package com.mybos.bmapp.Services.VolleyRequest;

import com.mybos.bmapp.Services.Logger;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by EmLaAi on 05/01/2018.
 *
 * @author EmLaAi
 */

public class VolleyBodyBuilder {
//    private JSONObject parameter;
    private Map<String,Object> parameter;

    public VolleyBodyBuilder(){
        parameter = new HashMap<>();
    }

    public VolleyBodyBuilder setParameter(String key,int value){
        parameter.put(key,value);
        return this;
    }
    public VolleyBodyBuilder setParameter(String key,String value){
        parameter.put(key,value);
        return this;
    }
    public VolleyBodyBuilder setParameter(String key,boolean value){
        parameter.put(key,value);
        return this;
    }
    public VolleyBodyBuilder setParameter(String key,Double value){
        parameter.put(key,value);
        return this;
    }
    public VolleyBodyBuilder setStringList(String key, List<String> value) {
        parameter.put(key, value);
        return this;
    }

    public VolleyBodyBuilder setIntegerList(String key,List<Integer> value){
        parameter.put(key,value);
        return this;
    }

    public VolleyBodyBuilder addSubRawJSONList(String key,List<Map<String,Object>> value){
        parameter.put(key,value);
        return this;
    }
    public VolleyBodyBuilder addSubRawJSON(String key,Map<String,Object> value){
        parameter.put(key,value);
        return this;
    }

    public Map<String,Object> export(){
        return parameter;
    }

    public String build(){
        JSONObject json = new JSONObject(parameter);
        return json.toString();
    }
}
