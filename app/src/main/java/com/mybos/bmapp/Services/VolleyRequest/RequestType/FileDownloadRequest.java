package com.mybos.bmapp.Services.VolleyRequest.RequestType;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

/**
 * Created by EmLaAi on 24/05/2018.
 *
 * @author EmLaAi
 */
public class FileDownloadRequest extends Request<byte[]> {
    private Response.Listener<byte[]> listener;

    public FileDownloadRequest(int method, String url, Response.ErrorListener listener , Response.Listener<byte[]> successListener) {
        super(method, url, listener);
        this.listener = successListener;
        setShouldCache(false);
    }


    @Override
    protected Response<byte[]> parseNetworkResponse(NetworkResponse response) {
        return Response.success(response.data, HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(byte[] response) {
        listener.onResponse(response);
    }
}
