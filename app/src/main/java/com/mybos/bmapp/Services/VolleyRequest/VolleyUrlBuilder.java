package com.mybos.bmapp.Services.VolleyRequest;

import android.net.Uri;
import androidx.annotation.StringDef;

import org.jetbrains.annotations.Contract;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * Created by EmLaAi on 05/01/2018.
 *
 * @author EmLaAi
 */

public class VolleyUrlBuilder {

    public static final String API_END_POINT = "api.mybos.com" ;
//    public static final String API_END_POINT = "api.prod.mybos-anchorbuild.com" ;

    private Uri.Builder builder;

    public VolleyUrlBuilder(){
        builder = new Uri.Builder();
//        builder.scheme("http").authority("api.mybos.net").appendPath("v1");
        builder.scheme("https").authority(API_END_POINT).appendPath("v1");
    }

    public VolleyUrlBuilder(int VersionNumber){
        String AppendPath = "v" + VersionNumber;
        builder = new Uri.Builder();
//        builder.scheme("http").authority("api.mybos.net").appendPath("v2");
        builder.scheme("https").authority(API_END_POINT).appendPath(AppendPath);
    }



    public VolleyUrlBuilder parseUrl(String uri , String... args ){
        List<String> paths = Arrays.asList(uri.split("/"));
        List<String> parameters = new ArrayList<>(Arrays.asList(args));
        for (String path:paths) {
            if (path.matches("^@[\\w]*$")){
                String parameter = parameters.get(0);
                parameters.remove(0);
                builder.appendPath(parameter);
            }else {
                builder.appendPath(path);
            }
        }
        return this;
    }
    public String build(){
        return builder.build().toString();
    }

    public static class URLPATH{
        // auth group
        public static final String LOGIN = "auth/login";
        public static final String PERMISSIONS = "auth/permission/@buildingId";
        public static final String FEATURES = "auth/feature/@buildingId";
        // error group
        public static final String ERROR = "mobile/bug-report";
        // building group
        public static final String BUILDING = "building";
        public static final String BUILDING_INFO = "building/@buildingId";
        // building case group
        public static final String BUILDING_CASE_GET_MESSAGE = "building/@buildingId/maintenance-request/@caseId/comment";
        public static final String BUILDING_CASE_E_D_MESSAGE = "building/@buildingId/maintenance-request/@caseId/comment/@commentId";
        public static final String BUILDING_CASE_LIST = "building/@buildingId/case/table";
        public static final String BUILDING_CASE_ITEM = "building/@buildingId/case/@caseId";
        public static final String BUILDING_CASE_CONTRACTOR = "building/@buildingId/contractor/table";
        public static final String BUILDING_CONTRACTOR_DETAIL = "building/@buildingId/contractor/@contractorId";
        public static final String BUILDING_CONTRACTOR_HISTORY = "building/@buildingId/case/history/@contractorId/contractor";
        public static final String BUILDING_CASE_TYPE_LIST = "building/@buildingId/case/type";
        public static final String BUILDING_CASE_STATUS_LIST = "building/@buildingId/case/status";
        public static final String BUILDING_CATEGORY_LIST = "building/@buildingId/category/tree/asset";
        public static final String BUILDING_CONTRACTOR_CATEGORY_LIST = "building/@buildingId/category/tree/contractor";
        public static final String BUILDING_LIBRARY_CATEGORY = "building/@buildingId/category";
        public static final String BUILDING_LIBRARY_CATEGORY_MANUAL = "building/@buildingId/manual";
        public static final String BUILDING_KEY = "building/@buildingId/key";
        public static final String BUILDING_KEY_HISTORY = "building/@buildingId/key/@keyID/history";

        public static final String FIREBASE_ACTIVE_DEVICE= "notification";
        public static final String LOGOUT= "auth/logout";


        public static final String APARTMENT_LIST = "building/@buildingId/apartment";
        public static final String APARTMENT_DETAIL = "building/@buildingId/apartment/@uid";
        public static final String APARTMENT_KEY = "building/@buildingId/key/apartment";
        public static final String APARTMENT_KEY_HISTORY = "building/@buildingId/key/@keyId/history";
        public static final String APARTMENT_KEY_CONTRACTOR = "building/@buildingId/contractor";
        public static final String APARTMENT_KEY_SIGNOUT = "building/@buildingId/key/@keyId/out";
        public static final String APARTMENT_KEY_SIGNIN= "building/@buildingId/key/@keyId/in";

        // building asset
        public static final String BUILDING_ASSET = "building/@buildingId/asset";
        public static final String BUILDING_ASSET_TABLE = "building/@buildingId/asset/table";
        public static final String SPECIFIC_BUILDING_ASSET = "building/@buildingId/asset/@assetId";
        public static final String SPECIFIC_BUILDING_ASSET_HISTORY = "building/@buildingId/case/history/@assetId/asset";
        public static final String BUILDING_ASSET_PER_CATEGORY = "building/@buildingId/asset/category/@categoryId";
        public static final String BUILDING_ASSET_CREATING = "building/@buildingId/asset/new";
        public static final String BUILDING_ASSET_EDITING = "building/@buildingId/asset/@assetId";


        // BUILDING CASE EMAIL
        public static final String BUILDING_CASE_EMAIL_CONTRACTOR = "building/@buildingId/case/@caseId/email";

        // attachment
        public static final String ATTACHMENT_PHOTO = "building/@buildingId/case/@caseId/attachment/photo";
        public static final String ATTACHMENT_QUOTE = "building/@buildingId/case/@caseId/attachment/quote";
        public static final String ATTACHMENT_INVOICE = "building/@buildingId/case/@caseId/attachment/invoice";
        public static final String ATTACHMENT_DOC = "building/@buildingId/case/@caseId/attachment/document";

        // inspection
        public static final String INSPECTION_LIST = "building/@buildingId/inspection/mobile/@type";
        public static final String INSPECTION_LIST_DRAFT = "building/@buildingId/inspection/search/draft";
        public static final String INSPECTION_ITEM = "building/@buildingId/inspection/@inspectionId";
        public static final String INSPECTION_CONVERT = "building/@buildingId/inspection/@inspectionId/history/@historyId/@inspectionItemId/convert";
        public static final String INSPECTION_HISTORY = "building/@buildingId/inspection/@inspectionId/history/@historyTime";

        public static final String INSPECTION_HISTORY_V2 = "building/@buildingId/inspection/@inspectionId/history";
        public static final String INSPECTION_ITEM_V2 = "building/@buildingId/inspection/@inspectionId/history/@historyId/@inspectionItemId";
        public static final String SAVE_INSPECTION_HISTORY_V2 = "building/@buildingId/inspection/@inspectionId/history/@historyId";

        // resident
        public static final String RESIDENT_LIST = "building/@buildingId/resident/table";

        // broadcast
        public static final String BROADCAST_GROUPS = "building/@buildingId/community/broadcast/group";
        public static final String BROADCAST_USERS = "building/@buildingId/community/broadcast/group/@groupId";
        public static final String BROADCAST_CREDIT = "building/@buildingId/community/broadcast/credit";
        public static final String BROADCAST_EMAIL_CONTENT = "building/@buildingId/community/broadcast/email";
        public static final String BROADCAST_SEND_MAIL = "building/@buildingId/community/broadcast/email/@idMail";
        public static final String BROADCAST_SEND_SMS = "building/@buildingId/community/broadcast/sms";
        public static final String BROADCAST_PURCHASE_SMS = "building/@buildingId/community/broadcast/credit";
        // calendar
        public static final String CALENDAR_GET_EVENT = "building/@buildingId/event";
        public static final String CALENDAR_CHANGE_DATE_REMINDER = "building/@buildingId/event/reminder/@RId";
        public static final String CALENDAR_CHANGE_COMMENT_BOOKING = "building/@buildingId/community/booking/@BId/comment";
        public static final String CALENDAR_CHANGE_STATUS_BOOKING = "building/@buildingId/community/booking/@BId/status";
        public static final String CALENDAR_GET_DATE_MAINTENANCE = "building/@buildingId/event/maintenance";
        public static final String CALENDAR_CHANGE_STATUS_MAINTENANCE = "building/@buildingId/event/maintenance/@MId/@date/status";
        public static final String CALENDAR_ADD_COMMENT_MAINTENANCE = "building/@buildingId/event/maintenance/@MId/@date/comment";
        public static final String CALENDAR_REMOVE_COMMENT_MAINTENANCE = "building/@buildingId/event/maintenance/@MId/@date/comment";
        public static final String CALENDAR_MOVE_MAINTENANCE = "building/@buildingId/event/maintenance/@MId/@date/move";
        public static final String CALENDAR_GET_CONVERT_MAINTENANCE = "building/@buildingId/event/maintenance/@MId/convert";
        // library
        public static final String LIBRARY_GET_LIST = "building/@buildingId/library/@Id";


        @Contract(pure = true)
        public static String getPath(@URLHint String hint){
            return hint;
        }

        @Contract(pure = true)
        public static String auth(@AuthUrlHint String hint){return hint;}

        @Contract(pure = true)
        public static String building(@BuildingUrlHint String hint){return hint;}

        @Contract(pure = true)
        public static String buildingCase(@BuildingCaseUrlHint String hint){return hint;}

        @Contract(pure = true)
        public static String buildingAsset(@BuildingAsset String hint){return  hint;}

        @Contract(pure = true)
        public static String buildingEmail(@EmailContractor String hint){return hint;}

        @Contract(pure = true)
        public static String error(@ErrorUrlHint String hint){return hint;}

        @Contract(pure = true)
        public static String attachment(@AttachmentHint String hint){return hint;}

        @Contract(pure = true)
        public static String inspection(@InspectionHint String hint){return hint;}

        @Contract(pure = true)
        public  static String broadcast(@BroadcastHint String hint){return hint;}

        @Contract(pure = true)
        public static String resident(@ResidentHint String hint){return  hint;}

        @Contract(pure = true)
        public static String apartment(@ApartmentHint String hint){return hint;}

        @Contract(pure = true)
        public static String calendar(@CalendarHint String hint){return hint;}

        @Contract(pure = true)
        public static String library(@LibraryHint String hint){return hint;}

        @StringDef({LOGIN})
        @Retention(RetentionPolicy.SOURCE)
        public @interface URLHint{}

        @StringDef({FEATURES,PERMISSIONS})
        @Retention(RetentionPolicy.SOURCE)
        public @interface AuthUrlHint{}

        @StringDef({ERROR})
        @Retention(RetentionPolicy.SOURCE)
        public @interface ErrorUrlHint{}

        @StringDef({BUILDING,BUILDING_INFO})
        @Retention(RetentionPolicy.SOURCE)
        public @interface BuildingUrlHint{}

        @StringDef({BUILDING_CASE_LIST,
                BUILDING_CASE_ITEM,
                BUILDING_CASE_GET_MESSAGE,
                BUILDING_CASE_E_D_MESSAGE,
                BUILDING_CASE_CONTRACTOR,
                BUILDING_CONTRACTOR_DETAIL,
                BUILDING_CONTRACTOR_HISTORY,
                BUILDING_CASE_TYPE_LIST,
                BUILDING_CASE_STATUS_LIST,
                BUILDING_CATEGORY_LIST,
                BUILDING_CONTRACTOR_CATEGORY_LIST,
                APARTMENT_LIST,
                BUILDING_LIBRARY_CATEGORY,
                BUILDING_LIBRARY_CATEGORY_MANUAL,
                BUILDING_KEY,
                BUILDING_KEY_HISTORY,
                FIREBASE_ACTIVE_DEVICE,
                LOGOUT})
        @Retention(RetentionPolicy.SOURCE)
        public @interface BuildingCaseUrlHint{}

        @StringDef({BUILDING_ASSET,
                BUILDING_ASSET_TABLE,
                SPECIFIC_BUILDING_ASSET,
                SPECIFIC_BUILDING_ASSET_HISTORY,
                BUILDING_ASSET_PER_CATEGORY,
                BUILDING_ASSET_CREATING,
                BUILDING_ASSET_EDITING})
        @Retention(RetentionPolicy.SOURCE)
        public @interface BuildingAsset{}

        @StringDef({ATTACHMENT_PHOTO,ATTACHMENT_QUOTE,ATTACHMENT_INVOICE,ATTACHMENT_DOC})
        @Retention(RetentionPolicy.SOURCE)
        public @interface AttachmentHint{}

        @StringDef({BUILDING_CASE_EMAIL_CONTRACTOR})
        @Retention(RetentionPolicy.SOURCE)
        public @interface EmailContractor{}

        @StringDef({INSPECTION_LIST,
                INSPECTION_LIST_DRAFT,
                INSPECTION_ITEM,
                INSPECTION_HISTORY,
                INSPECTION_HISTORY_V2,
                INSPECTION_ITEM_V2,
                SAVE_INSPECTION_HISTORY_V2,
                INSPECTION_CONVERT
        })
        @Retention(RetentionPolicy.SOURCE)
        public @interface InspectionHint{}

        @StringDef({BROADCAST_GROUPS,BROADCAST_USERS,
                BROADCAST_CREDIT,BROADCAST_SEND_MAIL,
                BROADCAST_SEND_SMS,BROADCAST_PURCHASE_SMS,
                BROADCAST_EMAIL_CONTENT})
        @Retention(RetentionPolicy.SOURCE)
        public @interface BroadcastHint{}

        @StringDef({RESIDENT_LIST})
        @Retention(RetentionPolicy.SOURCE)
        public @interface ResidentHint{}

        @StringDef({APARTMENT_DETAIL,APARTMENT_KEY,
                APARTMENT_KEY_HISTORY,APARTMENT_KEY_CONTRACTOR,
                APARTMENT_KEY_SIGNOUT,APARTMENT_KEY_SIGNIN})
        @Retention(RetentionPolicy.SOURCE)
        public @interface ApartmentHint{}

        @StringDef({CALENDAR_GET_EVENT,CALENDAR_CHANGE_DATE_REMINDER,
                CALENDAR_CHANGE_COMMENT_BOOKING,CALENDAR_CHANGE_STATUS_BOOKING,
                CALENDAR_GET_DATE_MAINTENANCE,CALENDAR_CHANGE_STATUS_MAINTENANCE,
                CALENDAR_ADD_COMMENT_MAINTENANCE,CALENDAR_REMOVE_COMMENT_MAINTENANCE,
                CALENDAR_MOVE_MAINTENANCE,CALENDAR_GET_CONVERT_MAINTENANCE})
        @Retention(RetentionPolicy.SOURCE)
        public @interface CalendarHint{}

        @StringDef({LIBRARY_GET_LIST})
        @Retention(RetentionPolicy.SOURCE)
        public @interface LibraryHint{}
    }
}
