package com.mybos.bmapp.Error;

import android.os.Build;

import com.mybos.bmapp.BuildConfig;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public abstract class BaseError extends Exception {
    public abstract int getCode();
}
