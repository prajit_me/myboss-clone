package com.mybos.bmapp.Error;

import android.content.Context;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 18/04/2018.
 *
 * @author EmLaAi
 */
public class InvalidInspectionResponseError extends LocalizeException {
    @Override
    public String getLocalizeMessage(Context context) {
        return context.getString(R.string.ALertInspectionInvalidResponse);
    }

    @Override
    public int getCode() {
        return 7;
    }
}
