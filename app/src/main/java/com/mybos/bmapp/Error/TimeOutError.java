package com.mybos.bmapp.Error;

import android.content.Context;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 08/04/2018.
 *
 * @author EmLaAi
 */
public class TimeOutError extends LocalizeException {
    @Override
    public String getLocalizeMessage(Context context) {
        return context.getString(R.string.AlertTimedOutError);
    }

    @Override
    public int getCode() {
        return 4;
    }
}
