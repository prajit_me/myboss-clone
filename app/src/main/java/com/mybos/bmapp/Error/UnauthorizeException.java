package com.mybos.bmapp.Error;

import android.content.Context;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 15/01/2018.
 *
 * @author EmLaAi
 */

public class UnauthorizeException extends LocalizeException {
    @Override
    public String getLocalizeMessage(Context context) {
        return context.getString(R.string.AlertMessageUnathorizeException);
    }

    @Override
    public int getCode() {
        return 0;
    }
}
