package com.mybos.bmapp.Error;

import android.content.Context;

import com.mybos.bmapp.R;

/**
 * Created by EmLaAi on 07/01/2018.
 *
 * @author EmLaAi
 */

public class InvalidResponse extends LocalizeException {


    @Override
    public String getLocalizeMessage(Context context) {
        return context.getResources().getString(R.string.AlertMessageInvalidResponse);
    }

    @Override
    public int getCode() {
        return 2;
    }
}
