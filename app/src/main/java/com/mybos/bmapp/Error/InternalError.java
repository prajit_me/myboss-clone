package com.mybos.bmapp.Error;

import android.content.Context;

/**
 * Created by EmLaAi on 27/02/2018.
 *
 * @author EmLaAi
 */

public class InternalError extends LocalizeException {
    @Override
    public String getLocalizeMessage(Context context) {
        return null;
    }

    @Override
    public int getCode() {
        return 1;
    }
}
