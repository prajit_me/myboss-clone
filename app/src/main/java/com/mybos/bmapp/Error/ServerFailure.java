package com.mybos.bmapp.Error;

import android.content.Context;

/**
 * Created by EmLaAi on 01/03/2018.
 *
 * @author EmLaAi
 */

public class ServerFailure extends InterfaceServerFailureException {
    private String message;
    public ServerFailure(String message){
        this.message = message;
    }
    @Override
    public String getLocalizeMessage(Context context) {
        return this.message;
    }

    @Override
    public int getCode() {
        return 3;
    }
}
