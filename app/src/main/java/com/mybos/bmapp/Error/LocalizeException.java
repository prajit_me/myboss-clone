package com.mybos.bmapp.Error;

import android.content.Context;

/**
 * Created by EmLaAi on 12/01/2018.
 *
 * @author EmLaAi
 */

public abstract class LocalizeException extends BaseError {
    public abstract String getLocalizeMessage(Context context);
}
