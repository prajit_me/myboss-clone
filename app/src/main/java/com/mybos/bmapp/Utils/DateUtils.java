package com.mybos.bmapp.Utils;

import android.util.Log;

import androidx.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by EmLaAi on 31/03/2018.
 *
 * @author EmLaAi
 */
public class DateUtils {
    //yyyy-MM-dd'T'HH:mm:ssZZZZZ
    public static String getDateString(Date date,String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return  dateFormat.format(date);
    }

    public static String getDateString(Date date){
        return getDateString(date,"dd/MM/yyyy");
    }

    public static String getDateString2(Date date){
        return getDateString(date,"ddmmyyyy");
    }

    @Nullable
    public static Date parseDate(String dateString, String format){
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);

        try {
            return dateFormat.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String editDateTime(String dateTime){
        String resultString = "--";
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        inputFormat.setTimeZone(utc);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy'-'hh:mm a");
        try {
            Date date = inputFormat.parse(dateTime);
            resultString = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultString;
    }

    public static String editDateTime2(String dateTime){
        String resultString = "--";
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        inputFormat.setTimeZone(utc);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = inputFormat.parse(dateTime);
            resultString = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultString;
    }

    public static String editDateTime3(String dateTime){
        String resultString = "--";
        TimeZone utc = TimeZone.getTimeZone("UTC");
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
        inputFormat.setTimeZone(utc);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date date = inputFormat.parse(dateTime);
            resultString = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultString;
    }

    public static String changeTimeformat (String time){
        String resultString = "--";
        SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mma");
        try {
            Date date = inputFormat.parse(time);
            resultString = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultString;
    }

    public static String changeTimeformat2 (String time){
        String resultString = "--";
        SimpleDateFormat inputFormat = new SimpleDateFormat("HH:mm");
        SimpleDateFormat outputFormat = new SimpleDateFormat("hh:mma");
        try {
            Date date = inputFormat.parse(time);
            resultString = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return resultString;
    }

    public static String getCurrentTime (){
        String resultString = "--";
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Date date = new Date();
        resultString = outputFormat.format(date);
        Log.e("check_current_time",resultString);
        return resultString;
    }

    public static Date convertStringToDate(String date) throws ParseException {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        Date result = inputFormat.parse(date);
        return result;
    }

}
