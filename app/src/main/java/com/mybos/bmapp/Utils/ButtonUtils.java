package com.mybos.bmapp.Utils;

import android.content.Context;
import androidx.annotation.StringRes;

import android.widget.Button;

/**
 * Created by EmLaAi on 23/03/2018.
 *
 * @author EmLaAi
 */

public class ButtonUtils {
    public static Button createButton(Context context, @StringRes int titleResId, ButtonClickActionInterface action){
        Button button = new Button(context);
        button.setText(titleResId);
        button.setOnClickListener((v)->action.onClick());
        return button;
    }

    public static Button createButton(Context context,String title,ButtonClickActionInterface action){
        Button button = new Button(context);
        button.setText(title);
        button.setOnClickListener((v)->action.onClick());
        return button;
    }

    public interface ButtonClickActionInterface{
        void onClick();
    }
}
