package com.mybos.bmapp.Utils;

import android.graphics.Bitmap;
import android.util.Base64;

import java.io.ByteArrayOutputStream;


/**
 * Created by EmLaAi on 21/04/2018.
 *
 * @author EmLaAi
 */
public class BitmapUtils {
    public static String toBase64(Bitmap bitmap){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,stream);
        return Base64.encodeToString(stream.toByteArray(),Base64.DEFAULT);
    }
}
