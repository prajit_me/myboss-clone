package com.mybos.bmapp.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Created by EmLaAi on 20/04/2018.
 *
 * @author EmLaAi
 */
public class ImageUtils {
    private static final float MAX_WIDTH=1000;
    private static final float MAX_HEIGHT = 1000;
    public static Bitmap compressImage(Bitmap image){
        int width = image.getWidth();
        int height = image.getHeight();
        float imgRatio = (float) width / (float) height;
        float maxRatio = MAX_WIDTH / MAX_HEIGHT;
        if( width > MAX_WIDTH || height > MAX_HEIGHT){
            if (imgRatio < maxRatio){
                imgRatio = MAX_HEIGHT/height;
                width = (int) (imgRatio * width);
                height = (int) MAX_HEIGHT;
            }else if (imgRatio > maxRatio){
                imgRatio = MAX_WIDTH/width;
                width = (int) MAX_WIDTH;
                height = (int) (imgRatio * height);
            }
        }
        Bitmap scaleBitmap = Bitmap.createScaledBitmap(image,width,height,false);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        boolean isPNGconvertSuccess = scaleBitmap.compress(Bitmap.CompressFormat.PNG,1,out);
        if(!isPNGconvertSuccess){
            scaleBitmap.compress(Bitmap.CompressFormat.JPEG,80,out);
        }
        byte[] bytes = out.toByteArray();
        return BitmapFactory.decodeByteArray(bytes,0,bytes.length);
    }
}
