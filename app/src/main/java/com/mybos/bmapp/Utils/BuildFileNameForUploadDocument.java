package com.mybos.bmapp.Utils;

import android.util.Log;

import java.util.Date;

public class BuildFileNameForUploadDocument {

    static final String xlsType = "ms-excel";
    static final String xlsxType = "officedocument.spreadsheetml.sheet";
    static final String docType = "msword";
    static final String docxType = "officedocument.wordprocessingml.document";
    static final String pdfType = "pdf";

    public static String checkAndBuildFileName(String fileName, String mime){
        Log.e("check_data_in",fileName+"mime:"+mime);
        String result = DateUtils.getDateString2(new Date());
        if ((mime.contains(xlsType)||(mime.contains(xlsxType)))&&!fileName.endsWith(".xls")&&!fileName.endsWith(".xlsx")){
            return mime.contains(xlsType)?result.concat(".xls"):result.concat(".xlsx");
        }
        if ((mime.contains(docType)||(mime.contains(docxType)))&&!fileName.endsWith(".doc")&&!fileName.endsWith(".docx")){
            return mime.contains(docxType)?result.concat(".docx"):result.concat(".doc");
        }
        if (mime.contains(pdfType)&&!fileName.endsWith(".pdf")){
            return result.concat(".pdf");
        }

        return fileName;
    }

}
