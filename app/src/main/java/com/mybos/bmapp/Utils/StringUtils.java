package com.mybos.bmapp.Utils;

import android.os.Build;
import android.text.Html;
import android.util.Base64;
import android.widget.*;


import java.nio.charset.StandardCharsets;


/**
 * Created by EmLaAi on 15/01/2018.
 *
 * @author EmLaAi
 */

public class StringUtils {

    public static String toBase64(String string) {
        byte[] bytes = string.getBytes(StandardCharsets.UTF_8);
        return Base64.encodeToString(bytes,Base64.NO_WRAP);
    }

    public static String fromBase64(String string){
        byte[] bytes = Base64.decode(string,Base64.NO_WRAP);
        return new String(bytes,StandardCharsets.UTF_8);
    }

    public static String firstCapitalized(String DataString){
        return DataString.substring(0, 1).toUpperCase() + DataString.substring(1).toLowerCase();
    }

    public static void firstCapitalized(Button btnSelectedButton){
        btnSelectedButton.setText(firstCapitalized(btnSelectedButton.getText().toString().trim().toLowerCase()));
    }

    public static void writeStringInBold(String DataString, TextView tvWriteData){
        DataString = "<b>" + DataString + "</b>";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvWriteData.setText(Html.fromHtml(DataString, 0));
        }
        else {
            tvWriteData.setText(Html.fromHtml(DataString));
        }

    }

    public static void writeString(String DataString, TextView tvWriteData){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            tvWriteData.setText(Html.fromHtml(DataString, 0));
        }
        else {
            tvWriteData.setText(Html.fromHtml(DataString));
        }

    }

    public static String getDate(String DateString) {
        String NewDateString = "";

        String YearString = DateString.substring(0, DateString.indexOf("-"));
        String TempString = DateString.substring(DateString.indexOf("-") + 1);
        String MonthString = TempString.substring(0, TempString.indexOf("-"));
        String DayString = TempString.substring(TempString.indexOf("-") + 1);

        NewDateString = DayString + "/" + MonthString + "/" + YearString;

        return NewDateString;
    }
}
