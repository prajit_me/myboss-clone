package com.mybos.bmapp.Utils;

import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.mybos.bmapp.BuildConfig;

import org.jetbrains.annotations.Contract;

/**
 * Created by EmLaAi on 25/03/2018.
 *
 * @author EmLaAi
 */

public class ErrorUtils {
    @Nullable
    public static String getCallerClassName(Exception e){
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        String callerClassName = null;
        for (StackTraceElement element : stackTraceElements) {
            if (!element.getClassName().equals(ErrorUtils.class.getName()) && element.getClassName().indexOf("java.lang.Thread") != 0) {
                if (null == callerClassName) {
                    callerClassName = element.getClassName();
                } else if (!callerClassName.equals(element.getClassName())) {
                    return element.getClassName();
                }
            }
        }
        return null;
    }

    @Nullable
    public static String getCallerMethod(Exception e){
        StackTraceElement[] stackTraceElements = e.getStackTrace();
        String callerClassName = null;
        for (StackTraceElement element : stackTraceElements) {
            if (!element.getClassName().equals(ErrorUtils.class.getName()) && element.getClassName().indexOf("java.lang.Thread") != 0) {
                if (null == callerClassName) {
                    callerClassName = element.getClassName();
                } else if (!callerClassName.equals(element.getClassName())) {
                    return element.getMethodName();
                }
            }
        }
        return null;
    }

    @NonNull
    @Contract(pure = true)
    public static String getOS(){
        return Build.MANUFACTURER + " " + Build.MODEL + " " + Build.VERSION.RELEASE + " " + Build.VERSION.SDK_INT;
    }

    @NonNull
    @Contract(pure = true)
    public static String getVersion(){
        return BuildConfig.VERSION_CODE + " - " + BuildConfig.VERSION_NAME;
    }
}
